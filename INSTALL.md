Installation guide for QuartzPOS
================================

This version of QuartzPOS is a Client-Server application.

Server System Requirements
--------------------------
PHP 5.6+
MySQL or MariaDB >5.0
Web server eg Nginx or Apache

Client System Requirements
--------------------------
Modern web browser able to handle HTML5, AJAX, Javascript
if in doubt use a Chromium variant, eg Google Chrome, Opera, Vivaldi

Installation steps
------------------
Extract the supplied code to the webserver's htdocs area, eg /var/www/htdocs/pos. This will vary according to the operating system and configuration of your server. It is up to you and your administrator to sort out. Restart the webserver if required.
Use the web browser to browse to the served directory, eg http://localhost/pos
What should happen under normal configuration of the webserver is that install.php will now run, giving you a GUI to enter:
*Admin name
*Admin password
*Database name
*Database user
*Database user password
------------------
The Admin name and password are going to be saved to a config file, and the database. The Database name is used to create a new database in the server. The Database user and password are required for a user that is already in the MySQL Permissions table, with privileges to create databases and use them. It may be your root user, but for better security you should create another.

After this has completed you will be presented with the settill.php, where you should enter the admin name and password, and change the till number to any arbitrary number >0. The till number is what is used by QuartzPOS to designate where a particular sale came from, ie what device, so keep incrementing the number as you add more device clients.

Now a good idea for security is to rename install.php to install.old or similar, as this stops a miscreant from rearranging your valid database credentials.
