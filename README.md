# quartzpos
An easy-to-use but comprehensive Point-of-Sale that is web browser based, built with PHP, MySQL, Javascript.


Summary:
QuartzPOS  is a stand-alone or integrated application to control your POINT-OF-SALE. It runs within major standards-compliant web-browsers such as Mozilla Firefox, Google Chrome, Chromium, and Opera; this means that it can run from any device with a large enough screen, and is therefore very friendly to your computer hardware budget.
It runs best as  a client-server type application over a local area network, so the central data is always consistent.

Client Requirements:
Mircosoft : Windows XP or better, with a non-MS browser.
Mac: OS X 10.6.7 or better, with Chrome, Firefox or Safari(untested, but should be fine). IOS 6 or better. 
Linux: Any recent distribution with Chromium, Chrome, Seamonkey, Opera, or Firefox. 
Android: Version 4 or better, with Chrome or Firefox loaded. Mopria print system has been tested.
The browser is best to be HTML5 compliant, and must be configured to allow popups from the server hosting the application, as well as full javascript access.
Printer: Currently only tested on A4 colour + mono laser printers. 

Server Requirements:
QuartzPOS was developed on Linux  Mint 17.1 x64. And there it runs inside nginx webserver, with php5-fpm, and MariaDB. It is capable of running under the typical LAMP stack also. It will most likely be fine on XAMPP – enabling it to be served from Mac, Windows, or Linux. But really, just learn and use Linux as it will be much more reliable and stable.

Features:
As many clients (known as Tills) as you like. It is up to you to number them correctly to avoid your own confusion.
Touch-screen capable interface; makes working with tablets a breeze.
Quick keys to move around the interface quickly by keyboard.
Auto-complete drop-down selector for products based upon product code and description.
Two main interfaces; the regular Point-Of-Sale, and the Administration area.
Unlimited numbers of users, with a flexible security and permission system to control access.
Flexible reporting to see what transactions have been made, and control the same place.
Import and Export to and from Wildcard Systems or QuartzBooks.
API in development for interfacing with other software.
Unlimited brands, categories, products, suppliers, and customers.
Individual or default images for products and categories.
Barcodes for products – up to four for each item.
Gift voucher register.
Laybuys.
Quotes.
Discount displayed.
Modifiable item pricing and tax with the right access level.
Database editor for the administrator.
Quick database backup.
Fast and secure SFTP-based export.
Flexible price levels; eg Retail, Trade, Wholesale, etc, with ability to include or exclude tax.
Specific customers can be assigned a price level.
Many areas to take notes for a transaction; Comments – for the POS database and staff to see only, Customer Comments – for the customer to see on their receipt, Payment comments – to note any issues at that point.
Select product by category / product code / description / image instead of using the auto-complete drop-down.
Void a sale, with the right access. Deletes sale altogether.
Edit a sale – add or delete items when the customer has changed their mind.
Re-print a receipt from administration interface.
Change a payment from administration interface.
Suspend and Resume a sale – it waits for you to come back to it. Could also be used to create a template of a common sale. This is per user-based.
Add customers and their details directly from the POS frontend – no need to go to Administration.
Switch to a higher level of access with Supervisor mode.
Date and time display.
Language compatibility subsystem in development.
Unique transaction numbers help with tracking.
Pretty themes – two available currently; Metal and Flat.
FOSS GPL code base; the code is well commented and modifiable by someone with enough knowledge and willing to learn. PHP, MySQL, HTML 4+5, CSS 3, jQuery, and vanilla javascript are the languages utilised. Any capable web developer could extend the system. The developer is also available.
Under extensive development for years.

FREE of licensing fees. Go ahead and install it on as many servers and clients as you like.
In production usage!

Known Issues:
Sole developer currently
Infrastructure for bug-tracking, feature requests still in development.
Unable to interface with scales due to the choice of programming language and platform ( the web browser required to run the application should never have access to the hardware of the machine ).
Much debugging code is still left in the system.


