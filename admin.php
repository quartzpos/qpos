<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com
  Made by Onyx


  Released under the GNU General Public License
 */
session_start();

require 'incsess.php';

if (!isset($_SESSION)) {

    session_start();
}
require_once("config.php");
if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE){
    require 'consoleLogging.php';

    ChromePhp::log($_REQUEST, "ADMIN php: starting REQUEST=");

    ChromePhp::log($_GET, "ADMIN php: GET=");

    ChromePhp::log($_POST, "ADMIN php: POST=");

    ChromePhp::log($_SESSION, "ADMIN php: starting Session=");
    }
require_once("languages/languages.php");

require_once("database.php");
require_once("functions.php");

$path = '';
get_tz($db);
if (isset($pathex))
    $path = $pathex;
$res = $db->QPResults("SELECT value FROM system WHERE name='theme'");
if (empty($res['value'])) {
    $res['value'] = "flat";
}
$themepath = $path . 'qpthemes/' . $res['value'] . '/';

//Logout
if (isset($_GET['action']) && $_GET['action'] == "logout") {

    $tillpersist = $_COOKIE["quartzpos"];
    session_destroy();
    setcookie("quartzpos", $tillpersist, time() + 5184000);
    header("Location:admin.php");
}
require_once'login.php';
require_once 'dailytotals.php';
?>

<!DOCTYPE html>
<html><head>
<title>QuartzPOS Administration</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Onyx">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="<?php echo $themepath; ?>pos.css" rel="StyleSheet" type="text/css">
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ajax.min.js"></script>
        <script language="javascript" src="js/general.js"></script>
        <script type="text/javascript" src="<?php echo $themepath; ?>jqui/jquery-ui.min.js"></script>


        <link href="<?php echo $themepath; ?>jqui/jquery-ui.min.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
        <link rel="stylesheet" type="text/css" href="js/jqplot/jquery.jqplot.css" />
        <script language="javascript" type="text/javascript" src="js/jqplot/jqplot.canvasTextRenderer.js"></script>
        <script language="javascript" type="text/javascript" src="js/jqplot/jqplot.canvasAxisTickRenderer.js"></script>
        <script language="javascript" type="text/javascript" src="js/jqplot/jqplot.dateAxisRenderer.js"></script>
        <script language="javascript" type="text/javascript" src="js/jqplot/jqplot.cursor.js"></script>
        <?php
        if (isset($_GET['action'])) {
            if ($_GET['action'] == "reports" || $_GET['action'] == "setup" || $_GET['action'] == "pricelevels" || $_GET['action'] == "login") {
                $st = FALSE;
                $fi = FALSE;
                $no = 0;
                if (isset($_POST['submit_report2'])) {
                    //the graphing function options
                    if ($_POST['date1']) {
                        $st = $_POST['date1'];
                    }
                    if ($_POST['date2']) {
                        $fi = $_POST['date2'];
                    }
                    if ($_POST['nodays']) {
                        $no = $_POST['nodays'];
                    }
                }
                ?>




                <script language="javascript" type="text/javascript">$(document).ready(function () {

                        $("#date1").datepicker({dateFormat: "yy-mm-dd"});
                        $("#date2").datepicker({dateFormat: "yy-mm-dd"});
                        $(function () {
                            $("input[type=submit], button")
                                    .button()
                                    .click(function (event) {
                                        //event.preventDefault();
                                    });
                        });
        <?php if ($_GET['action'] === "reports" || ($_GET['action'] === 'login' && $_SESSION['admin'] > 0)) {

            if (isset($_GET['report']) && $_GET['report'] === 'overview'){
            ?>

                            //begin jqplot
                            $.jqplot.config.enablePlugins = true;

                            var line1 =<?php print dailytotals_get($st, $fi, $no); ?>;

                            var plot1 = $.jqplot('chartdiv', [line1], {
                                title: 'Plotted sales totals',
                                axes: {
                                    xaxis: {
                                        renderer: $.jqplot.DateAxisRenderer,
                                        rendererOptions: {
                                            tickRenderer: $.jqplot.CanvasAxisTickRenderer
                                        },
                                        tickOptions: {
                                            fontSize: '10pt',
                                            fontFamily: 'Tahoma',
                                            angle: -90
                                        }
                                    },
                                    yaxis: {
                                        rendererOptions: {
                                            tickRenderer: $.jqplot.CanvasAxisTickRenderer},
                                        tickOptions: {
                                            fontSize: '10pt',
                                            fontFamily: 'Tahoma',
                                            angle: 0
                                        }
                                    }
                                },
                                series: [{lineWidth: 1, markerOptions: {style: 'line'}, color: "#111"}],
                                cursor: {
                                    zoom: true,
                                    looseZoom: true
                                }
                            }); //end jqplot



                <?php
                    }
                }

                ?>


                    });//end document ready
                </script>
            <?php } ?>



        <?php
    }
    ?>
                <script language="javascript" type="text/javascript">
                   function pmtwin(id) {
                        window.open("payments.php?admin=1&id=" + id + "", "", "width=630,height=650,toolbars=0,location=0,titlebar=0,left=500");
                    }

                    function loadiURL(url) {
                        console.log("load iframe url=" + url);
                        $('#dsframe').attr('src', url);

                    }
                    </script>
    </head>
    <body <?php
    if (isset($_SESSION['admin'])) {
        echo 'class="admin"';
    }
    ?> onload="setInterval('startTime()', 1000)" >
        <table width="100%" height="100%" cellspacing="0" cellpadding="0">
            <TR height="20"><TD>
<?php
$location = "admin.php";
require_once("header.php");
?>
                </TD></TR>
            <TR><TD valign="top">
                    <?php
//MAIN ADMIN AREA
//         $_SESSION['user'] = $row['id'];
//         $_SESSION['admin'] = $row['admin'];
//          $_SESSION['system'] = $row['system'];
//           $_SESSION['sales'] = $row['sales'];
//            $_SESSION['reports'] = $row['reports'];
//             $_SESSION['stock'] = $row['stock'];
                    if (isset($_SESSION['username'])) {
                        ?>
                        <div id="adminmenu">
                            <table>
                                <TR>
    <?php
    if (isset($_SESSION['stock'])) {
        if ($_SESSION['stock'] > 1) {
            ?><TD style="text-align:center;"><a href="admin.php?action=brands"><img src="<?php echo $themepath; ?>images/brands.png" title="<?php echo ADMINMENU_BRANDS; ?>" /></a></TD>
                                            <TD style="text-align:center;"><a href="admin.php?action=categories"><img src="<?php echo $themepath; ?>images/categories.png" title="<?php echo ADMINMENU_CATEGORIES; ?>" /></a></TD>
                                            <TD style="text-align:center;"><a href="admin.php?action=products"><img src="<?php echo $themepath; ?>images/products.png" title="<?php echo ADMINMENU_PRODUCTS; ?>" /></a></TD>
                                            <TD style="text-align:center;"><a href="admin.php?action=suppliers"><img src="<?php echo $themepath; ?>images/suppliers.png" title="<?php echo ADMINMENU_SUPPLIERS; ?>" /></a></TD><?php
                                        }
                                    }
                                    ?>
                                    <?php
                                    if (isset($_SESSION['system'])) {
                                        if ($_SESSION['system'] > 1) {
                                            ?><TD style="text-align:center;"><a href="admin.php?action=clients"><img src="<?php echo $themepath; ?>images/customers.png" title="<?php echo ADMINMENU_CUSTOMERS; ?>" /></a></TD>
                                            <TD style="text-align:center;" ><a href="admin.php?action=users"><img src="<?php echo $themepath; ?>images/users.png" title="<?php echo ADMINMENU_USERS; ?>" /></a></TD><?php
                                        }
                                    }
                                    ?>
                                    <TD style="text-align:center;"><a href="pos.php"><img src="<?php echo $themepath; ?>images/sale.png" title="<?php echo ADMINMENU_SALES; ?>" /></a></TD>
    <?php
    if (isset($_SESSION['sales'])) {
        if ($_SESSION['sales'] > 0) {
            ?><TD style="text-align:center;"><a href="admin.php?action=laybuys"><img src="<?php echo $themepath; ?>images/laybuys.png" title="<?php echo ADMINMENU_LAYBUYS; ?>" /></a></TD>
                                            <TD style="text-align:center;"><a href="admin.php?action=vouchers"><img src="<?php echo $themepath; ?>images/vouchers.png" title="<?php echo ADMINMENU_VOUCHERS; ?>" /></a></TD>

                                            <?php
                                        }
                                    }
                                    ?>
                                    <?php
                                    if (isset($_SESSION['reports'])) {
                                        if ($_SESSION['reports'] > 0) {
                                            ?><TD style="text-align:center;"><a href="admin.php?action=reports&report=overview"><img src="<?php echo $themepath; ?>images/reports.png" title="<?php echo ADMINMENU_REPORTS; ?>" /></a></TD><?php
                                        }
                                    }
                                    ?>
        <!-- <TD style="text-align:center;"><a href="admin.php?action=search"><img src="<?php echo $themepath; ?>images/search.png" title="Search" /></a></TD>-->
                                    <?php
                                    if (isset($_SESSION['system'])) {
                                        if ($_SESSION['system'] > 1) {
                                            ?><TD style="text-align:center;"><a href="admin.php?action=setup"><img src="<?php echo $themepath; ?>images/setup.png" title="<?php echo ADMINMENU_SETUP; ?>" /></a></TD><TD style="text-align:center;"><a href="admin.php?action=maintenance"><img src="<?php echo $themepath; ?>images/maintenance.png" title="<?php echo ADMINMENU_MAINTENANCE; ?>" /></a></TD><TD style="text-align:center;"><a href="admin.php?action=pricelevels"><img src="<?php echo $themepath; ?>images/prices.png" title="<?php echo ADMINMENU_PRICELEVELS; ?>" /></a></TD><?php
                                        }
                                    }
                                    ?>
                                                            <?php
                                                            if (isset($_SESSION['system'])) {
                                                                if ($_SESSION['system'] > 1) {
                                                                    ?><TD style="text-align:center;"><a href="export.php" target="_blank"><img src="<?php echo $themepath; ?>images/export.png" title="<?php echo ADMINMENU_EXPORT; ?>" /></a></TD><?php
                                                                }
                                                            }
                                                            ?>
                                            <?php
                                            if (isset($_SESSION['system'])) {
                                                if ($_SESSION['system'] > 1) {
                                                    ?><TD style="text-align:center;"><a href="import/index.php" target="_blank"><img src="<?php echo $themepath; ?>images/import.png" title="<?php echo ADMINMENU_IMPORT; ?>" /></a></TD><?php
                                                }
                                            }
                                            ?>
                                    <TD style="text-align:center;"><a href="admin.php?action=logout"><img src="<?php echo $themepath; ?>images/keycard.png" title="<?php echo ADMINMENU_LOGOUT; ?>" /></a></TD>
                                </TR>
                                <TR>
    <?php
    if (isset($_SESSION['stock'])) {
        if ($_SESSION['stock'] > 1) {
            ?><TD><a href="admin.php?action=brands"><?php echo ADMINMENU_BRANDS; ?></a></TD>
                                            <TD><a href="admin.php?action=categories"><?php echo ADMINMENU_CATEGORIES; ?></a></TD>
                                            <TD><a href="admin.php?action=products"><?php echo ADMINMENU_PRODUCTS; ?></a></TD>
                                            <TD><a href="admin.php?action=suppliers"><?php echo ADMINMENU_SUPPLIERS; ?></a></TD><?php
                                        }
                                    }
                                    ?>
                                    <?php
                                    if (isset($_SESSION['system'])) {
                                        if ($_SESSION['system'] > 1) {
                                            ?><TD><a href="admin.php?action=clients"><?php echo ADMINMENU_CUSTOMERS; ?></a></TD>
                                            <TD><a href="admin.php?action=users"><?php echo ADMINMENU_USERS; ?></a></TD><?php
                                        }
                                    }
                                    ?>
                                    <TD><a href="pos.php"><?php echo ADMINMENU_SALES; ?></a></TD>
                                    <?php
                                    if (isset($_SESSION['sales'])) {
                                        if ($_SESSION['sales'] > 0) {
                                            ?><TD><a href="admin.php?action=laybuys"><?php echo ADMINMENU_LAYBUYS; ?></a></TD>
                                            <TD><a href="admin.php?action=vouchers"><?php echo ADMINMENU_VOUCHERS; ?></a></TD>
                                            <?php
                                        }
                                    }
                                    ?>
                                        <?php
                                        if (isset($_SESSION['reports'])) {
                                            if ($_SESSION['reports'] > 0) {
                                                ?><TD><a href="admin.php?action=reports&report=overview"><?php echo ADMINMENU_REPORTS; ?></a></TD><?php
                                        }
                                    }
                                    ?>
              <!--  <TD><a href="admin.php?action=search"><?php //echo ADMINMENU_SEARCH;  ?></a></TD>-->
                                    <?php
                                    if (isset($_SESSION['system'])) {
                                        if ($_SESSION['system'] > 1) {
                                            ?><TD><a href="admin.php?action=setup"><?php echo ADMINMENU_SETUP; ?></a></TD><TD><a href="admin.php?action=maintenance"><?php echo ADMINMENU_MAINTENANCE; ?></a></TD><TD><a href="admin.php?action=pricelevels"><?php echo ADMINMENU_PRICELEVELS; ?></a></TD><?php
                                                    }
                                                }
                                                if (isset($_SESSION['system'])) {
                                                    if ($_SESSION['system'] > 1) {
                                                        ?><TD><a href="export.php" target="_blank"><?php echo ADMINMENU_EXPORT; ?></a></TD><?php
                                                    }
                                                }
                                                ?>
                                        <?php
                                        if (isset($_SESSION['system'])) {
                                            if ($_SESSION['system'] > 1) {
                                                ?><TD><a href="import/index.php" target="_blank"><?php echo ADMINMENU_IMPORT; ?></a></TD><?php
                                            }
                                        }
                                        ?>
                                    <TD><a href="admin.php?action=logout"><?php echo ADMINMENU_LOGOUT; ?></a></TD>
                                </TR>
                            </table>
                        </div>

                        <?php
                    }//end username check
                    if (!isset($_SESSION['username'])) {
                        if (isset($_GET['msg'])) {
                            loginnow($_GET['msg']);
                        } else {
                            loginnow();
                        }
                    }
                    if (!isset($_GET['action']))
                        $_GET['action'] = "login";
                    if ($_GET['action'] == "login") {
                        //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php login");
                        if (isset($_SESSION['admin'])) {
                            if ($_SESSION['admin'] > 0) {
                                echo' <div class="admin_content">' . ADMIN_WELCOME_TEXT;
                                ?>
                                <a href="admin.php?action=reports&report=overview"><div id="chartdiv" style="height:400px;width:1600px; "></div></a> </div>
                                <?php
                            }
                        }
                    }


                    if (isset($_SESSION['stock'])) {
                        if ($_SESSION['stock'] > 1) {
                            if ($_GET['action'] == "brands") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call brands.php");
                                require_once("brands.php");
                            }

                            if ($_GET['action'] == "categories") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call categories.php");
                                require_once("categories.php");
                            }


                            if ($_GET['action'] == "products") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call products.php");
                                require_once("products.php");
                            }

                            if ($_GET['action'] == "suppliers") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call suppliers.php");
                                require_once("suppliers.php");
                            }
                        }
                    }
                    if (isset($_SESSION['system'])) {
                        if ($_SESSION['system'] > 1) {
                            if ($_GET['action'] == "clients") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call clients.php");
                                require_once("clients.php");
                            }
                            if ($_GET['action'] == "users") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call users.php");
                                require_once("users.php");
                            }
                            if ($_GET['action'] == "maintenance") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call maintenance.php");
                                require_once("maintenance.php");
                            }
                            if ($_GET['action'] == "pricelevels") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call pricelevels.php");
                                require_once("pricelevels.php");
                            }
                        }
                    }
                    if (isset($_SESSION['reports'])) {
                        if ($_SESSION['reports'] > 1) {
                            if ($_GET['action'] == "reports") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call reports.php");
                                require_once("reports.php");
                            }
                        }
                    }

//     if ($_GET['action'] == "search") {
//     //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION,"ADMIN php: checking get[action]=search, Session=");
//         //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call search.php");
//                 require_once("search.php");
//             }
                    if (isset($_SESSION['system'])) {
                        if ($_SESSION['system'] > 1) {
                            if ($_GET['action'] == "setup") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call setup.php");
                                require_once("setup.php");
                            }
                        }
                    }
                    if (isset($_SESSION['sales'])) {
                        if ($_SESSION['sales'] > 1) {
                            if ($_GET['action'] == "laybuys") {
                                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("ADMIN php call laybuys.php");
                                $list = 'all';
                                require_once("laybuys.php");
                            }
                        }
                    }
                    if (isset($_SESSION['sales'])) {
                        if ($_SESSION['sales'] > 1) {
                            if ($_GET['action'] == "vouchers") {

                                $list = 'all';
                                require_once("vouchers.php");
                            }
                        }
                    }
                    ?>
                </TD></TR>
            <TR height="16"><TD><div id="footer">
<?php require_once("footer.php"); ?></div>
                </TD></TR>
        </table>

    </body>
</html>
