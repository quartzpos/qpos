<?php

//Ajax autocomplete handler for input fields
require_once("config.php");
require_once("database.php");


if (!isset($_GET['term']) || !isset($_GET['functioncall'])) {
    echo 'Failure to send q query and functioncall function.';
}
$_GET['term'] = trim($_GET['term']);
require 'consoleLogging.php';
//echo $_GET['q'];
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_GET);
$searchterm = $db->clean($_GET['term']); // off the get string
if ($_GET['functioncall'] != 'barcode') {
    $searchterm = '%' . str_replace(' ', '%', $searchterm) . '%'; // get all LIKE the searchterm
}
$functioncall = 'ac_' . $_GET['functioncall']; //Very important: code only needs to send get value of standard name for function

if (is_callable($functioncall)){
    $data = call_user_func_array($functioncall, array($searchterm)); // go get the function
   //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($data,"Autocomplete data=");
}
if (empty($data))
    return;
$SQL = $data['0']; //break out the sql
$subdata = $data['1']; //break out the array of the columns required from the sql
$fetch = $db->query($SQL); //run the sql
$acreturn = '[';
while ($row = $db->fetchArray($fetch, MYSQLI_ASSOC)) { //work on every record from the sql results
    $return = ""; //temporary return var clear/define


    foreach ($subdata as $key => $value) { //for each of the columns from the sql
        $name = $row[$value]; // get the column value
        $name = str_replace('"', 'in.', $name);
        if (!empty($return))
            $return .=' :: '; //add a delimiter for later processing to delineate multiple columns

        $return .= "$name "; //add  value to temp var
    }
    $return = '{"value":"' . $return . '"},';


    //$return .="\n"; // add a new line to temp var
    $acreturn .= $return; //add temp var to final return
}
$acreturn = substr($acreturn, 0, -1);
$acreturn.=']';
echo $acreturn;

function ac_prodcode($searchterm) {
//SELECT item_number, item_name FROM items LEFT JOIN barcodes ON items.item_number= barcodes.prodcode left join brands on items.brand_id=brands.id left join categories on items.category_id=categories.id WHERE items.item_name LIKE '%" . $searchterm . "%' OR items.item_number LIKE '%" . $searchterm . "%' OR items.description LIKE '%" . $searchterm . "%' OR barcodes.barcode LIKE '%" . $searchterm . "%' or brands.brand like '%" . $searchterm . "%' or categories.category LIKE '%" . $searchterm . "%'
    $SQL = "SELECT item_number, item_name FROM items LEFT JOIN barcodes ON items.item_number= barcodes.prodcode left join brands on items.brand_id=brands.id left join categories on items.category_id=categories.id WHERE items.item_name LIKE '%" . $searchterm . "%' OR items.item_number LIKE '%" . $searchterm . "%' OR items.description LIKE '%" . $searchterm . "%' OR barcodes.barcode LIKE '%" . $searchterm . "%' or brands.brand like '%" . $searchterm . "%' or categories.category LIKE '%" . $searchterm . "%'";
    $subarray = array('item_number', 'item_name');
    $dataarray = array($SQL, $subarray); //join the sql and the fields required in an array

    return $dataarray;
}

function ac_barcode($searchterm) {

    $SQL = "SELECT prodcode
					FROM barcodes
					WHERE barcode='" . $searchterm . "'";
    $subarray = array('prodcode');
    $dataarray = array($SQL, $subarray); //join the sql and the fields required in an array

    return $dataarray;
}

?>
