<?php
//barcodes handler
if (!isset($_SESSION))
    session_start();

require 'consoleLogging.php';
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_REQUEST, "barcodes php: starting REQUEST=");

require_once("config.php");
require_once("languages/languages.php");
require_once 'helpers.php';

if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_REQUEST, " barcode REQUEST vars=");
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST, " barcode post vars=");
require_once("database.php");
$msg ='';
if (isset($_REQUEST['prodcode'])) {
    $prodcode = $_REQUEST['prodcode'];
    if (isset($_POST['submit'])) {
       //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST, " barcode post vars=");

        //the form has submitted into itself
        $array = remove_element($_POST, "Submit");
        $array = remove_element($array, $prodcode);
       //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($array, "barcodes array=");
        //$array only conatins valid barcodes
        $sql = "DELETE from barcodes WHERE prodcode = '$prodcode'";
        $results = $db->query($sql);    //cleared all, now add back
        foreach ($array as $key => $val) {

            if (!empty($val)) {
                $val = $db->clean($val);
                // check to see if already assigned
                $sql = "SELECT * FROM barcodes WHERE barcode = '$val' AND prodcode ='$prodcode'";
                $results = $db->QPResults($sql);
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($results, "find SELECT results");
                if (!isset($results['barcode'])) {
                    $sql = "INSERT INTO barcodes (barcode, prodcode) VALUES ('$val', '$prodcode')";
                    $results = $db->query($sql);
                    $msg .= "Added barcode '$val' to product '$prodcode'.</br>";
                } else {
                    $msg .="Failure to add barcode '$val'. Already assigned to product code '".$results['prodcode']."'</br>";
                }
            }
        }
    }
    $sql = "SELECT * from barcodes WHERE prodcode = '$prodcode'";
    $results = $db->QPComplete($sql);
   //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($results, "barcodes.php sql results=");
    require_once 'template.php';
    ?><script type='text/javascript'>
        function formclear() {
            $('#barcodeentry :input[type=text]').val('');
        }
        function delete_code(idno) {
            op = confirm('Do you really want to delete this barcode?');
            if (op) {
               console.log('clear idno=' + idno);
                document.getElementById(idno).value = '';
            }
        }

        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == 'text')) {
                return false;
            }
        }

        document.onkeypress = stopRKey;
        function addbc() {
            var table = document.getElementById('bc');

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            var cell1 = row.insertCell(0);
            var element1 = document.createElement('input');
            element1.type = 'text';
            element1.name = 'bc' + rowCount;
            element1.id = 'bc' + rowCount;

            cell1.appendChild(element1);
            cell1.innerHTML += "<img src='images/delete.png' title='Delete' onclick='delete_code(\"bc" + rowCount + "\")'>";
            document.getElementById(element1.id).focus();
        }
    </script>
    <?php
    echo "<h3>".BARCODES_FOR." ".$prodcode." :</h3>";
    echo "<form name='barcodeentry' id='barcodeentry' method='post' action='barcodes.php'>";
    echo "<input type='hidden' value='$prodcode' name='prodcode' />";
    echo "<table name='bc' id='bc'>";
    $count = '0';
    foreach ($results as $key => $val) {
        $bcount = "bc" . $count;

        echo "<tr><td><input type='text' id='" . $bcount . "' name='" . $bcount . "' value='" . $val['barcode'] . "' />";
        echo "<img src='images/delete.png' title='Delete' onclick='delete_code(\"" . $bcount . "\")'></td></tr>";
        $count++;
    }
    echo "</table>";
    echo "<input type='button' value='Add' onclick='addbc()' /><input type='button' value='".CLEAR_ALL."' onclick='formclear()' /><input type='submit' name='submit' value='Submit' /></form>";
    if(!empty($msg)) echo '<br>'. $msg;
} else {
    echo "<div id='warn'>".PRODCODE_FAILURE."</div>";
}
?>
