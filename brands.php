<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
*/

if(!isset($_SESSION))session_start();
if(!isset($_SESSION['admin'])){
header("Location:admin.php");
}
//Add brand
if(isset($_POST['submitbrand'])){
$_POST['brand']=$db->clean($_POST['brand']); //cleaned input for later usage
    $sql = "select brand from brands where brand='".$_POST['brand']."'" ;
	$brand = $db->QPResults($sql);

        if(empty($brand['brand'])&& !empty($_POST['brand'])){
$sql = "insert into brands(brand) values('" .$_POST['brand'] ."')";
$db->query($sql);
        } else {
            $output = "<br>".BRAND_LOADED;
        }
}

//Edit brand
if(isset($_POST['editbrand'])){
	$_POST['brand']=$db->clean($_POST['brand']); //cleaned input for later usage
	$sql = "update brands set brand='" .$_POST['brand'] ."' where id=" .$_POST['brand_id'];
	$db->query($sql);
}

//Delete brand
if(isset($_GET['delete'])){
$sql = "delete from brands where id=" .$db->clean($_GET['delete']);
$db->query($sql);
}
?>
<div class="admin_content">
<?php
if(isset($_GET['edit'])){
	$sql = "select * from brands where id=" .$db->clean($_GET['edit']);
	$result = $db->query($sql);
	$brand = $db->fetchRow($result);
?>
<form action="admin.php?action=brands" method="POST">
<input type="hidden" name="brand_id" value="<?php echo $_GET['edit']; ?>">
<?php echo HDR_BRAND; ?>: <input type="text" size="30" name="brand" value="<?php echo htmlspecialchars($brand[1]); ?>">
<input type="text" size="5" name="code" value="<?php echo $brand[2]; ?>">
<input type="submit" name="editbrand" value="<?php echo TXT_SAVE; ?>">
</form>
<?php
}
else{
?>
<form action="admin.php?action=brands" method="POST">
<?php echo ADD_NEW_BRAND; ?>:<br>
<input type="text" size="30" name="brand" value="" placeholder="Brand name">
<input type="text" size="5" name="code" value="" placeholder="Code">
<input type="submit" name="submitbrand" value="<?php echo BRAND_SUBMIT; ?>">
</form>
<br>
<?php
$sql = "select * from brands order by brand asc";
$result = $db->query($sql);
$res=$db->query($sql);
$rowchk=$db->fetchRow($res);
if(!empty($rowchk[1])){
    ?>

<table cellspacing="0">
<TR><TH width="300"><?php echo HDR_BRAND; ?></TH><TH><?php echo TXT_CODE;?></TH><TH><?php echo TXT_EDIT; ?></TH><TH><?php echo TXT_DELETE; ?></TH></TR>
<?php

while($row = $db->fetchRow($result)){
?>
<TR>
    <TD class="btvalue"><a href="admin.php?action=products&brand=<?php echo $row[0]; ?>"><?php echo htmlspecialchars($row[1]); ?></a></TD>
    <TD class="tvalue" align="center"><a href="admin.php?action=products&brand=<?php echo $row[0]; ?>"><?php echo $row[2];?></a></TD>
    <TD class="tvalue" align="center"><a href="admin.php?action=brands&edit=<?php echo $row[0]; ?>"><img src="<?php echo $themepath; ?>images/edit.png" title="Edit <?php echo htmlspecialchars($row[1]); ?>" /></a></TD>
    <TD class="tvalue" align="center"><a href="admin.php?action=brands&delete=<?php echo $row[0]; ?>"><img src="<?php echo $themepath; ?>images/delete.png" title="Delete <?php echo htmlspecialchars($row[1]); ?>" /></a></TD>
</TR>
    <?php
}
?>

</table>
<?php
} else {
    echo "No brands loaded.";
}
if(isset($output)){
    echo $output;
}
}
?>
</div>
