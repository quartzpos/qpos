<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */
//TODO: add 'code' field for entry - already in db
if (!isset($_SESSION))
    session_start();
if (!isset($_SESSION['admin'])) {
    header("Location:admin.php");
}
require_once 'helpers.php';

//Add category
if (isset($_POST['submitcategory'])) {
    foreach ($_POST as $key => $val) {
        $_POST[$key] = $db->clean($_POST[$key]);
    }

//Upload image file

    $category_image = $themepath."images/categories/";
    if (isset($_FILES['category_image']['name']) && image_upload($_FILES['category_image'], $category_image)) {

        $category_image.=$_FILES['category_image']['name'];

    } else {
        $category_image.="category.png";
    }

    $sql = "select category from categories where category='" . $_POST['category'] . "'";
    $cat = $db->QPResults($sql);

    if(!empty($_POST['ccode'])){
        $code=substr($_POST['ccode'],0,5);
    } else {
        $code="";
    }
    if (empty($cat['category']) && !empty($_POST['category'])) {
        $sql = "insert into categories(category,code,image) values('" . $_POST['category']  . "','" .$code. "','" . $category_image . "')";

        if($db->query($sql)){
			echo $_POST['category']." inserted.<br/>";
		} else {
			echo $_POST['category']." failure to insert with $sql.<br/>";
		}
    } else {
        $output = "<br>".CATEGORY_LOADED;
    }
}

//Edit category
if (isset($_POST['submiteditcategory'])) {

    //Upload image file
    foreach ($_POST as $key => $val) {
        $_POST[$key] = $db->clean($_POST[$key]);
    }
    $category_image = $themepath."images/categories/";
    $imgsql = "";
    if (isset($_POST['imagedefault']) && $_POST['imagedefault'] == "1") {
        $category_image.="category.png";
        $imgsql = ", image='" . $category_image . "'";
    }
    if (isset($_FILES['category_image']['name']) && image_upload($_FILES['category_image'], $category_image)) {

        $category_image.=$_FILES['category_image']['name'];
        $imgsql = ", image='" . $category_image . "'";
    } else {
        $category_image.="category.png";
    }
    $sql = "update categories set category='" . $_POST['category'] . "', code='" . $_POST['ccode'] . "' $imgsql where id=" . $_POST['category_id'];
    $db->query($sql);
}

//Delete category
if (isset($_GET['delete'])) {
    $sql = "delete from categories where id=" . $_GET['delete'];
    $db->query($sql);
}
?>
<div class="admin_content">
<?php
if (isset($_GET['edit'])) {
    $result = $db->query("select * from categories where id=" . $_GET['edit']);
    $row = $db->fetchRow($result);
    ?>
        <div id="cat_form"><form action="admin.php?action=categories" enctype="multipart/form-data" method="post">
            <input type="hidden" name="category_id" value="<?php echo $row[0]; ?>">
            <TABLE>

                <TR><TD><?php echo TXT_CATEGORY; ?></TD><TD><input type="text" name="category" size="30" value="<?php echo htmlspecialchars($row[1]); ?>"> </TD></TR>
                <TD><?php echo TXT_CODE; ?></TD><TD><input type="text" name="ccode" value="<?php echo htmlspecialchars($row[3]); ?>" title="<?php echo CODE_MESSAGE; ?>"> </TD></TR>
                <TR><TD><?php echo TXT_IMAGE; ?></TD><TD><input type="file" id="category_image" name="category_image" title="<?php echo IMAGE_SIZE_MESSAGE; ?>"> </TD></TR>
                </TABLE>
            <input type="submit" name="submiteditcategory" id="submiteditcategory" value="<?php echo TXT_SAVE; ?>">
        </div>
    <?php
    if (isset($row['2'])) {
        echo " <div id='cat_image'><label id='imagelabel'>".TXT_CURRENTLY.": " . $row['2'] . "</label>";

        echo'&nbsp;&nbsp;<img src="' . $row['2'] . '" id="image" class="category-image" style="max-width:400px;" />';

        //insert a button to clear the image to default
        ?>
                            <input type="button" value="Clear" onclick="defaultimage();" />
                            <input type="hidden" name="imagedefault" id="imagedefault" value="" />
                            <script type="text/javascript">
                                function defaultimage() {
                                    document.getElementById('imagedefault').value = "1";
                                    document.getElementById('imagelabel').innerText = "<?php echo CATEGORY_PNG; ?>";
                                    document.getElementById('image').src = "<?php echo $themepath;?>images/categories/categories.png";

                                }
                            </script>
                            </form></div>
                            <?php
                        }
                        ?>




        <?php
    } else {
        ?>
        <form action="admin.php?action=categories" enctype="multipart/form-data" method="POST">
            <table>
                <TR>
                    <TD><?php echo TXT_CATEGORY; ?></TD>
                    <TD><input type="text" name="category" size="30"> </TD>
                </TR>
                 <TR>
                    <TD><?php echo TXT_CODE; ?></TD>
                    <TD><input type="text" name="ccode" id="code" title="<?php echo CODE_MESSAGE; ?>"></TD>
                </TR>
                <TR>
                    <TD><?php echo TXT_IMAGE; ?></TD>
                    <TD><input type="file" name="category_image" id="category_image" title="<?php echo IMAGE_SIZE_MESSAGE; ?>"></TD>
                </TR>
                <TR>
                    <TD><input type="submit" name="submitcategory" value="<?php echo TXT_ADD_CATEGORY; ?>"></TD>
                    </TR>
            </table>
        </form>
        <br>
    <?php
    $sql = "select * from categories order by category ";
    $result = $db->query($sql);
    $res = $db->query($sql);
    $rowchk = $db->fetchRow($res);
    if (!empty($rowchk[1])) {
        ?>
            <table cellspacing="0">
                <TR><TH colspan="2" width="400"><?php echo TXT_CATEGORY; ?></TH><TH><?php echo TXT_EDIT; ?></TH><TH><?php echo TXT_DELETE; ?></TH></TR>
            <?php
            while ($row = $db->fetchRow($result)) {
                if (empty($row[2])) {
                    $row[2] = $themepath . "images/categories/category.png";
                }
                ?>
                    <TR><TD width="<?php echo CATEGORY_IMG_SIZE; ?>" class="tvalue"><img width="<?php echo CATEGORY_IMG_SIZE; ?>" height="<?php echo CATEGORY_IMG_SIZE; ?>" src="<?php echo $row[2]; ?>"></TD><TD class="btvalue"><a href="admin.php?action=products&category=<?php echo $row[0]; ?>"><?php echo htmlspecialchars($row[1]); ?></a></TD><TD class="tvalue" align="center"><a href="admin.php?action=categories&edit=<?php echo $row[0]; ?>"><img src="<?php echo $themepath; ?>images/edit.png" title="<?php echo TXT_EDIT." ".htmlspecialchars($row[1]); ?>" /></a></TD><TD class="tvalue" align="center"><a href="admin.php?action=categories&delete=<?php echo $row[0]; ?>"><img src="<?php echo $themepath; ?>images/delete.png" title="<?php echo TXT_DELETE." ".htmlspecialchars($row[1]); ?>" /></a></TD></TR>
                    <?php
                }
                ?>
            </table>
                        <?php
                    } else {
                        echo CATEGORY_UNLOADED;
                    }
                    if (isset($output)) {
                        echo $output;
                    }
                }
                ?>
</div>
