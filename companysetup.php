<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */

if (!isset($_SESSION))
    session_start();
if (!isset($_SESSION['admin'])) {
    header("Location:admin.php");
}
require_once 'config.php';

require_once("languages/languages.php");

require_once("database.php");
require 'consoleLogging.php';

if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_REQUEST, "companysetup.php request=");
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_FILES, "companysetup.php request=");
$msg = '';
//TODO: make a setup sql file that has sane defaults, use following mysql syntax:
// INSERT IGNORE INTO `system`
//    SET `name` = 'sales_footer1',
//    `value` = ''
// it ensures it only cretes what doesn't already exist
if (isset($_POST)) {
// the form has received data - act upon it
    if (isset($_POST['name']) && !empty($_POST['name'])) {
        $name = $db->clean($_POST['name']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='company_details_name'");
        if (empty($res['value'])) {
            $msg .=' A company name is required.';
        } else {
            $name = $res['value'];
        }
    }
    if (isset($_POST['address1']) && !empty($_POST['address1'])) {
        $address1 = $db->clean($_POST['address1']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='company_details_address1'");
        if (empty($res['value'])) {
            $msg .=' A company address1 is required.';
        } else {
            $address1 = $res['value'];
        }
    }
    if (isset($_POST['address2']) && !empty($_POST['address2'])) {
        $address2 = $db->clean($_POST['address2']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='company_details_address2'");
        if (empty($res['value'])) {
            $msg .=' A company address2 is required.';
        } else {
            $address2 = $res['value'];
        }
    }
    if (isset($_POST['address3']) ){
        $address3 = $db->clean($_POST['address3']);
    } else {
            $address3='';
        
    }
    if (isset($_POST['phone1']) && !empty($_POST['phone1'])) {
        $phone1 = $db->clean($_POST['phone1']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='company_details_phone1'");
        if (!empty($res['value'])) {

            $phone1 = $res['value'];
        }else {
            $phone1='';
        }
    }
    if (isset($_POST['phone2']) ) {
        $phone2 = $db->clean($_POST['phone2']);
    } else {
        
            $phone2='';
        
    }
    if (isset($_POST['fax']) ) {
        $fax = $db->clean($_POST['fax']);
    } else {
       
            $fax='';
        }
    
    if (isset($_POST['email']) && !empty($_POST['email'])) {
        $email = $db->clean($_POST['email']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='company_details_email'");
        if (!empty($res['value'])) {

            $email = $res['value'];
        }else {
            $email='';
        }
    }
    if (isset($_POST['website']) && !empty($_POST['website'])) {
        $website = $db->clean($_POST['website']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='company_details_website'");
        if (!empty($res['value'])) {

            $website = $res['value'];
        }else {
            $website='';
        }
    }
    if (isset($_POST['tax_no']) && !empty($_POST['tax_no'])) {
        $tax_no = $db->clean($_POST['tax_no']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='company_tax_no'");
        if (!empty($res['value'])) {

            $tax_no = $res['value'];
        }else {
            $tax_no='';
        }
    }
    if (isset($_POST['sales_header1']) && !empty($_POST['sales_header1'])) {
        $sales_header1 = $db->clean($_POST['sales_header1']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='sales_header1'");
        if (!empty($res['value'])) {

            $sales_header1 = $res['value'];
        } else {
            $sales_header1 = '';
        }
    }
    if (isset($_POST['sales_header2']) && !empty($_POST['sales_header2'])) {
        $sales_header2 = $db->clean($_POST['sales_header2']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='sales_header2'");
        if (!empty($res['value'])) {

            $sales_header2 = $res['value'];
        } else {
            $sales_header2 = '';
        }
    }
        if (isset($_POST['sales_footer1']) && !empty($_POST['sales_footer1'])) {
        $sales_footer1 = $db->clean($_POST['sales_footer1']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='sales_footer1'");
        if (!empty($res['value'])) {

            $sales_footer1 = $res['value'];
        } else {
            $sales_footer1 = '';
        }
    }
    if (isset($_POST['sales_footer2']) && !empty($_POST['sales_footer2'])) {
        $sales_footer2 = $db->clean($_POST['sales_footer2']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='sales_footer2'");
        if (!empty($res['value'])) {

            $sales_footer2 = $res['value'];
        } else {
            $sales_footer2 = '';
        }
    }
       if (isset($_POST['quote_footer']) && !empty($_POST['quote_footer'])) {
        $quote_footer = $db->clean($_POST['quote_footer']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='quote_footer'");
        if (!empty($res['value'])) {

            $quote_footer = $res['value'];
        } else {
            $quote_footer = '';
        }
    }
    if (isset($_POST['receipt_logo']) && !empty($_POST['receipt_logo'])) {
        $receipt_logo = $db->clean($_POST['receipt_logo']);
    } else {
        $res = $db->QPResults("SELECT value FROM system WHERE name='receipt_logo'");
        if (!empty($res['value'])) {

            $receipt_logo = $res['value'];
        } else {
            $receipt_logo = '';
        }
    }






    if (isset($_FILES['receipt_logo']) && $_FILES['receipt_logo']['size'] > 0) {
        // Where the file is going to be placed 
        $target_path = "files/";
        $fileName = $_FILES['receipt_logo']['name'];
        $tmpName = $_FILES['receipt_logo']['tmp_name'];
        $fileSize = $_FILES['receipt_logo']['size'];
        $fileType = $_FILES['receipt_logo']['type'];
       //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($fileName, "companysetup.php filename=");


        if (!get_magic_quotes_gpc()) {
            $fileName = addslashes($fileName);
        }

// Add the original filename to our target path.  

        $target_path = $target_path . basename($_FILES['receipt_logo']['name']);
        $filemsg = '';
        $receipt_logo = $target_path;
        if (move_uploaded_file($_FILES['receipt_logo']['tmp_name'], $target_path)) {
            $filemsg.= "The file " . basename($_FILES['receipt_logo']['name']) .
                    " has been uploaded<br/>";
        } else {
            $filemsg.= "There was an error uploading the file, please try again!<br/>";
        }
    }
    if (empty($msg)) {
        $sql = "UPDATE system SET  value='$name' WHERE  name = 'company_details_name'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$address1' WHERE  name = 'company_details_address1'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$address2' WHERE  name = 'company_details_address2'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$address3' WHERE  name = 'company_details_address3'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$phone1' WHERE  name = 'company_details_phone1'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$phone2' WHERE  name = 'company_details_phone2'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$fax' WHERE  name = 'company_details_fax'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$email' WHERE  name = 'company_details_email'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$website' WHERE  name = 'company_details_website'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$tax_no' WHERE  name = 'company_tax_no'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$sales_header1' WHERE  name = 'sales_header1'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$sales_header2' WHERE  name = 'sales_header2'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
                $sql = "UPDATE system SET  value='$sales_footer1' WHERE  name = 'sales_footer1'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$sales_footer2' WHERE  name = 'sales_footer2'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$quote_footer' WHERE  name = 'quote_footer'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
        $sql = "UPDATE system SET  value='$receipt_logo' WHERE  name = 'receipt_logo'";
       if(!$db->query($sql)){echo "SQL failure: $sql"; }
       //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($receipt_logo, "companysetup.php receipt_logo=");
        if (isset($_POST['submit']))
            $msg .= " Saved company configuration.";
    }
}
$title = 'Import configuration';
require_once 'template.php';
?>

<body>
    <form action="companysetup.php" method="post"  enctype="multipart/form-data"><input type="hidden" name="MAX_FILE_SIZE" value="30000000" />
        <table border="0" cellpadding="2" cellspacing="2" width="100%">
            <tbody>
            <th><h3><?php echo COMPANY_SETUP; ?></h3></th>
            <tr>
                <td align="right" valign="top"><?php echo COMPANY_NAME; ?><br>
                </td>
                <td valign="top"><input type="text" value="<?php echo (isset($name)) ? $name : ''; ?>" name="name" title="Edit the company name."  class="path"/>
                </td>
                <td><div id="logo" style="position:absolute; display:block; right:50px; top:50px;">
                        <?php if (isset($receipt_logo)) {
                            ?>
                            <img src="<?php echo $receipt_logo; ?>" title="Receipt Logo" style="max-width:150px;max-height:150px;"/>
                        <?php }
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top"><?php echo COMPANY_ADDRESS1; ?><br>
                </td>
                <td valign="top"><input type="address1" value="<?php echo (isset($address1)) ? $address1 : ''; ?>" name="address1" title="Edit the company address1." class="path"  />
                </td>
            </tr>
            <tr>
                <td align="right" valign="top"><?php echo COMPANY_ADDRESS2; ?><br>
                </td>
                <td valign="top"><input value="<?php echo (isset($address2)) ? $address2 : ''; ?>" name="address2" title="Edit the company address2." type="text" class="path"> </td>
            </tr>
            <tr>
                <td align="right" valign="top"><?php echo COMPANY_ADDRESS3; ?><br>
                </td>
                <td valign="top"><input value="<?php echo (isset($address3)) ? $address3 : ''; ?>" name="address3" title="Edit the company address3." type="text" class="path"> </td>
            </tr>
            <tr>
                <td align="right" valign="top"><?php echo COMPANY_PHONE1; ?><br>
                </td>
                <td valign="top"><input value="<?php echo (isset($phone1)) ? $phone1 : ''; ?>" name="phone1" title="Edit the company phone1." type="text" class="path"> </td>
            </tr>
            <tr>

                <td align="right" valign="top"><?php echo COMPANY_PHONE2; ?><br>
                </td>
                <td valign="top"><input value="<?php echo (isset($phone2)) ? $phone2 : ''; ?>" name="phone2" title="Edit the company phone2." type="text" class="path"> </td>
            </tr>
            <tr>
                <td align="right" valign="top"><?php echo COMPANY_FAX; ?><br>
                </td>
                <td valign="top"><input value="<?php echo (isset($fax)) ? $fax : ''; ?>" name="fax" title="Edit the company fax." type="text" class="path"> </td>
            </tr>

            <tr>
                <td align="right" valign="top"><?php echo COMPANY_EMAIL; ?><br>
                </td>
                <td valign="top"><input value="<?php echo (isset($email)) ? $email : ''; ?>" name="email" title="Edit the company email." type="text" class="path"> </td>
            </tr>

            <tr>
                <td align="right" valign="top"><?php echo COMPANY_WEBSITE; ?><br>
                </td>
                <td valign="top"><input value="<?php echo (isset($website)) ? $website : ''; ?>" name="website" title="Edit the company website." type="text" class="path"> </td>
            </tr>

            <tr>
                <td align="right" valign="top"><?php echo COMPANY_TAX_NO; ?><br>
                </td>
                <td valign="top"><input value="<?php echo (isset($tax_no)) ? $tax_no : ''; ?>" name="tax_no" title="Edit the company tax number." type="text" class="path"> </td>
            </tr>
            <tr>
                <td align="right" valign="top"><?php echo SALES_HEADER1; ?><br>
                </td>
                <td valign="top"><input value="<?php echo (isset($sales_header1)) ? $sales_header1 : ''; ?>" name="sales_header1" title="Edit the first invoice header." type="text" class="path"> </td>
            </tr> 
            <tr>
                <td align="right" valign="top"><?php echo SALES_HEADER2; ?><br>
                </td>
                <td valign="top"><input value="<?php echo (isset($sales_header2)) ? $sales_header2 : ''; ?>" name="sales_header2" title="Edit the second invoice header." type="text" class="path"> </td>
            </tr>
              <tr>
                <td align="right" valign="top"><?php echo SALES_FOOTER1; ?><br>
                </td>
                <td valign="top"><textarea name="sales_footer1" placeholder="Edit the first invoice footer." class="path"><?php echo (isset($sales_footer1)) ? $sales_footer1 : ''; ?></textarea></td>
            </tr> 
            <tr>
                <td align="right" valign="top"><?php echo SALES_FOOTER2; ?><br>
                </td>
                 <td valign="top"><textarea name="sales_footer2" placeholder="Edit the second invoice footer." class="path"><?php echo (isset($sales_footer2)) ? $sales_footer2 : ''; ?></textarea></td>
            </tr>
              <tr>
                <td align="right" valign="top"><?php echo QUOTE_FOOTER; ?><br>
                </td>
                 <td valign="top"><textarea name="quote_footer" placeholder="Edit the quote footer." class="path"><?php echo (isset($quote_footer)) ? $quote_footer : ''; ?></textarea></td>
            </tr>
            <tr>
                <td align="right" valign="top"><?php echo RECEIPT_LOGO; ?><br>
                </td>
                <td valign="top"><input  name="receipt_logo" title="File path to the receipt logo." type="file"  ><span class="pathspan">File: <?php echo (isset($receipt_logo)) ? $receipt_logo : ''; ?></span> </td>
            </tr>   
            <tr>                
                <td align="right" valign="top"><br>
                </td>
                <td valign="top"><input value="Submit" name="submit" title="Save settings" type="submit" /></td>
            </tr>
            <tr><td></td><td> <?php echo(isset($filemsg)) ? $filemsg : '';
                        echo(isset($msg)) ? $msg : ''; ?> </td></tr>
            </tbody>
        </table>
    </form>

    <br>
</body>
</html>
