<?php
// do the report for daily summary
// the printable invoice
if (!isset($_SESSION)) {
    session_start();
}


include("config.php");
include("consoleLogging.php");
require_once("languages/languages.php");
require_once("database.php");
require_once 'functions.php';
require_once 'dailytotals.php';
if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
    ChromePhp::log($_REQUEST, "dailysummary.php request=");
if (isset($_GET['date'])) {
    $date = $_GET['date'];
    //get the data for this date                
    $sql = "SELECT sale_total_cost, paid_with, sale_sub_total, till FROM sales WHERE date='$date' AND paid_with != 'NULL'";
    if (isset($_GET['till'])) {
        $sql .= " AND till='" . intval($_GET['till']) . "'";
    }
    $rows = $db->QPComplete($sql);
    //Array ( [0] => Array ( [sale_total_cost] => 60.00 [paid_with] => a:5:{s:10:"mastercard";s:5:"60.00";s:9:"remainder";s:4:"0.00";s:5:"total";s:4:"60.00";s:2:"id";s:2:"19";s:4:"cost";s:5:"60.00";} [sale_sub_total] => 51.00 [till] => 1 ) [1] => Array ( [sale_total_cost] => 90.00 [paid_with] => a:6:{s:3:"eft";s:5:"90.00";s:9:"remainder";s:4:"0.00";s:5:"total";s:2:"90";s:9:"laybuyfrm";s:11:"15:40,15:50";s:2:"id";s:2:"45";s:4:"cost";s:5:"90.00";} [sale_sub_total] => 76.50 [till] => 1 ) )
    $sql = "SELECT * FROM voids WHERE date='" . $date . "'";
    $void = $db->QPComplete($sql);
    $tills = array();
    foreach ($rows as $key => $val) {
        //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($tills, "tills=");
        if (!isset($tills[$val['till']])) {
            //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("initialize to zero");
            $tills[$val['till']] = array('id' => $val['till']);
            $tills[$val['till']]['total'] = '0'; //initialize the total
            $tills[$val['till']]['subtotal'] = '0';
            $tills[$val['till']]['eft'] = '0';
            $tills[$val['till']]['ceft'] = '0'; // credit eft
            $tills[$val['till']]['eftn'] = '0'; // eft number of
            $tills[$val['till']]['ceftn'] = '0'; // credit eft number of
            $tills[$val['till']]['visa'] = '0';
            $tills[$val['till']]['cvisa'] = '0';
            $tills[$val['till']]['visan'] = '0';
            $tills[$val['till']]['cvisan'] = '0';
            $tills[$val['till']]['amex'] = '0';
            $tills[$val['till']]['amexn'] = '0';
            $tills[$val['till']]['camex'] = '0';
            $tills[$val['till']]['camexn'] = '0';
            $tills[$val['till']]['mastercard'] = '0';
            $tills[$val['till']]['mastercardn'] = '0';
            $tills[$val['till']]['cmastercard'] = '0';
            $tills[$val['till']]['cmastercardn'] = '0';
            $tills[$val['till']]['voucher'] = '0';
            $tills[$val['till']]['vouchern'] = '0';
            $tills[$val['till']]['cvoucher'] = '0';
            $tills[$val['till']]['cvouchern'] = '0';
            $tills[$val['till']]['account'] = '0';
            $tills[$val['till']]['accountn'] = '0';
            $tills[$val['till']]['caccount'] = '0';
            $tills[$val['till']]['caccountn'] = '0';
            $tills[$val['till']]['cash'] = '0';
            $tills[$val['till']]['cashn'] = '0';
            $tills[$val['till']]['ccash'] = '0';
            $tills[$val['till']]['ccashn'] = '0';
            $tills[$val['till']]['cheque'] = '0';
            $tills[$val['till']]['chequen'] = '0';
            $tills[$val['till']]['ccheque'] = '0';
            $tills[$val['till']]['cchequen'] = '0';
            $tills[$val['till']]['laybuy'] = '0';
            $tills[$val['till']]['laybuyn'] = '0';
            $tills[$val['till']]['claybuy'] = '0';
            $tills[$val['till']]['claybuyn'] = '0';
        }

        $tills[$val['till']]['total'] = $tills[$val['till']]['total'] + $val['sale_total_cost'];
        $tills[$val['till']]['subtotal'] = $tills[$val['till']]['subtotal'] + $val['sale_sub_total'];
        $paid = unserialize(base64_decode($val['paid_with']));
        if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
            ChromePhp::log($paid, "paid=");
        if (isset($paid['eft'])) {
            if ($paid['eft'] !== 0) {
                $tills[$val['till']]['eft'] += $paid['eft'] + $paid['remainder'];
                $paid['remainder'] = 0;
                $tills[$val['till']]['eftn'] += '1';
                if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
                    ChromePhp::log($tills[$val['till']]['eft'], "eft=");
            } else {
                $tills[$val['till']]['ceft'] += $paid['eft'];
                $tills[$val['till']]['ceftn'] += '1';
            }
        }
        if (isset($paid['visa'])) {
            if ($paid['visa'] !== 0) {
                $tills[$val['till']]['visa'] += $paid['visa'] + $paid['remainder'];
                $paid['remainder'] = 0;
                $tills[$val['till']]['visan'] += '1';
                if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
                    ChromePhp::log($tills[$val['till']]['visa'], "visa=");
            } else {
                $tills[$val['till']]['cvisa'] += $paid['visa'];
                $tills[$val['till']]['cvisan'] += '1';
            }
        }
        if (isset($paid['amex'])) {
            if ($paid['amex'] !== 0) {
                $tills[$val['till']]['amex'] += $paid['amex'] + $paid['remainder'];
                $paid['remainder'] = 0;
                $tills[$val['till']]['amexn'] += '1';
                if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
                    ChromePhp::log($tills[$val['till']]['amex'], "amex=");
            } else {
                $tills[$val['till']]['camex'] += $paid['amex'];
                $tills[$val['till']]['camexn'] += '1';
            }
        }
        if (isset($paid['mastercard'])) {
            if ($paid['mastercard'] !== 0) {
                $tills[$val['till']]['mastercard'] += $paid['mastercard'] + $paid['remainder'];
                $paid['remainder'] = 0;
                $tills[$val['till']]['mastercardn'] += '1';
                if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
                    ChromePhp::log($tills[$val['till']]['mastercard'], "mastercard=");
            } else {
                $tills[$val['till']]['cmastercard'] += $paid['mastercard'];
                $tills[$val['till']]['cmastercardn'] += '1';
            }
        }
        if (isset($paid['voucher'])) {
            if ($paid['voucher'] !== 0) {
                $tills[$val['till']]['voucher'] += $paid['voucher'] + $paid['remainder'];
                $paid['remainder'] = 0;
                $tills[$val['till']]['vouchern'] += '1';
                if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
                    ChromePhp::log($tills[$val['till']]['voucher'], "voucher=");
            } else {
                $tills[$val['till']]['cvoucher'] += $paid['voucher'];
                $tills[$val['till']]['cvouchern'] += '1';
            }
        }
        if (isset($paid['account'])) {
            if ($paid['account'] !== 0) {
                $tills[$val['till']]['account'] += $paid['account'] + $paid['remainder'];
                $paid['remainder'] = 0;
                $tills[$val['till']]['accountn'] += '1';
                if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
                    ChromePhp::log($tills[$val['till']]['account'], "account=");
            } else {
                $tills[$val['till']]['caccount'] += $paid['account'];
                $tills[$val['till']]['caccountn'] += '1';
            }
        }
        if (isset($paid['cash'])) {
            if ($paid['cash'] !== 0) {
                $tills[$val['till']]['cash'] += $paid['cash'] + $paid['remainder'];
                $paid['remainder'] = 0;
                $tills[$val['till']]['cashn'] += '1';
                if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
                    ChromePhp::log($tills[$val['till']]['cash'], "cash=");
            } else {
                $tills[$val['till']]['ccash'] += $paid['cash'];
                $tills[$val['till']]['ccashn'] += '1';
            }
        }
        if (isset($paid['cheque'])) {
            if ($paid['cheque'] !== 0) {
                $tills[$val['till']]['cheque'] += $paid['cheque'] + $paid['remainder'];
                $paid['remainder'] = 0;
                $tills[$val['till']]['chequen'] += '1';
                if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
                    ChromePhp::log($tills[$val['till']]['cheque'], "cheque=");
            } else {
                $tills[$val['till']]['ccheque'] += $paid['cheque'];
                $tills[$val['till']]['cchequen'] += '1';
            }
        }
        if (isset($paid['laybuy'])) {
            if ($paid['laybuy'] !== 0) {
                $tills[$val['till']]['laybuy'] += $paid['laybuy'] + $paid['remainder'];
                $paid['remainder'] = 0;
                $tills[$val['till']]['laybuyn'] += '1';
                if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)
                    ChromePhp::log($tills[$val['till']]['laybuy'], "laybuy=");
            } else {
                $tills[$val['till']]['claybuy'] += $paid['laybuy'];
                $tills[$val['till']]['claybuyn'] += '1';
            }
        }
    }
} else {
    die('Failure to pass date to dailysummary.php');
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8">
        <title>QuartzPOS Daily Summary</title>
        <style>

            *{font-family:arial, helvetica, sans-serif;font-size:12px;}
            @media print{
                .noprint, .noprint *,#top{display:none !important;}

            }
            small{font-size:10px;color:#788;}
            body{margin:0px;background:#fff;padding:2em;}
            .dailysummarytable { color:#333; font-weight:bold; }
            #top {background:#9cf;border-bottom:solid 1px #248;}
            #menu td{border-left:solid 1px #788;width:60px;cursor:hand;
                     text-align:center;color:#248;font-weight:bold;}
            #dailysummary_info{margin-top:10px;}
            #dailysummary_info td{}
            #total b{color:#922;font-size:14px;}
            #logo img{max-width:300px; }
            #logo {padding:1.5em;display:inline-block; vertical-align:top;}
            .right{text-align:right;float:right;}
            #details{ display: inline-block;}
            #dailysummary_header{width:100%; min-height:250px; border-bottom: 1px solid #333;}
            .light{background: #ccc; color:#000; }
            #client{border: 1px solid #666; padding:10px;max-width: 300px;margin-top: 10px;}
        </style>
        <script language="javascript">
            var closewin =<?php
if (isset($_GET['closewin'])) {
    echo $_GET['closewin'];
} else {
    echo '0';
}
?>;
            function closed() {
                console.log("close function");
                if (closewin == '0') {
                    window.close();
                }
            }
            function focus() {
                document.getElementById("print").focus();
            }
            document.addEventListener("keydown", function (event) {
                if (event.altKey && (String.fromCharCode(event.keyCode) === "P" || String.fromCharCode(event.keyCode) === "p")) {
                    console.log("you pressed alt-p");
                    event.preventDefault();
                    event.stopPropagation();

                    console.log("alt+ p clicked");
                    window.print();
                }
            }, true);
            document.addEventListener("keydown", function (event) {
                if (event.altKey && (String.fromCharCode(event.keyCode) === "c" || String.fromCharCode(event.keyCode) === "C")) {
                    console.log("you pressed alt-c");
                    event.preventDefault();
                    event.stopPropagation();

                    console.log("alt+ c clicked");
                    closed();
                }
            }, true);
        </script>
    </head>
    <body onload="focus()">
        <h4>Daily Summary for <?php echo $date; ?></h4>
        <table id="top" width="100%" cellpadding="0">
            <tr>
                <td>&nbsp;</td>
                <td align="right">
                    <input type="button" onClick="window.print()" id="print" value="Print"  title="Print. Hotkey Alt-p"/>
                </td>
            </tr>
        </table>
        <div id="dailysummary_info">
            <?php
            $grandtotalsale = 0;
            $grandtotaln = 0;
            $grandctotal = 0;
            $grandctotaln = 0;
            $grandgtotaln = 0;
            $grandtotal = 0;

            foreach ($tills as $k => $v) {
                print "<h4>Till $k</h4>";
                ?>
                <table width="100%" cellspacing="0"><tbody><tr style='background:#ccc;'><th class="dailysummarytable" align="left">Payment type</th><th class="dailysummarytable" align="left">Sale Amount</th><th class="dailysummarytable" align="left">Number</th><th class="dailysummarytable" align="left">Credit Amount</th><th class="dailysummarytable" align="left">Number</th><th class="dailysummarytable" align="right">Total Number</th><th class="dailysummarytable" align="right">Total $</th></tr>
                        <?php
                        $totaln = 0;
                        $ctotaln = 0;
                        $ctotal = 0;
                        $totalsale = 0;
                        $linetotal = 0;
                        $totaleftn = intval($v['eftn']) + intval($v['ceftn']);
                        $totaleft = floatval($v['eft']) + floatval($v['ceft']);
                        $totalsale += $v['eft'];
                        $totaln += $v['eftn'];
                        $ctotaln += $v['ceftn'];
                        $ctotal += $v['ceft'];
                        echo "<tr><td align='left'>EFT</td><td>" . $v['eft'] . "</td><td>" . $v['eftn'] . "</td><td>" . $v['ceft'] . "</td><td>" . $v['ceftn'] . "</td><td align='right'>$totaleftn</td><td align='right'>$totaleft</td></tr>";
                        $linetotal += $totaleft;
                        $totalsale += $v['visa'];
                        $totaln += $v['visan'];
                        $ctotaln += $v['cvisan'];
                        $ctotal += $v['cvisa'];
                        $totalvisan = intval($v['visan']) + intval($v['cvisan']);
                        $totalvisa = floatval($v['visa']) + floatval($v['cvisa']);
                        echo "<tr><td align='left'>Visa</td><td>" . $v['visa'] . "</td><td>" . $v['visan'] . "</td><td>" . $v['cvisa'] . "</td><td>" . $v['cvisan'] . "</td><td align='right'>$totalvisan</td><td align='right'>$totalvisa</td></tr>";
                        $linetotal += $totalvisa;
                        $totalsale += $v['amex'];
                        $totaln += $v['amexn'];
                        $ctotaln += $v['camexn'];
                        $ctotal += $v['camex'];
                        $totalamexn = intval($v['amexn']) + intval($v['camexn']);
                        $totalamex = floatval($v['amex']) + floatval($v['camex']);
                        echo "<tr><td align='left'>AMEX</td><td>" . $v['amex'] . "</td><td>" . $v['amexn'] . "</td><td>" . $v['camex'] . "</td><td>" . $v['camexn'] . "</td><td align='right'>$totalamexn</td><td align='right'>$totalamex</td></tr>";
                        $linetotal += $totalamex;
                        $totalsale += $v['mastercard'];
                        $totaln += $v['mastercardn'];
                        $ctotaln += $v['cmastercardn'];
                        $ctotal += $v['cmastercard'];
                        $totalmastercardn = intval($v['mastercardn']) + intval($v['cmastercardn']);
                        $totalmastercard = floatval($v['mastercard']) + floatval($v['cmastercard']);
                        echo "<tr><td align='left'>Mastercard</td><td>" . $v['mastercard'] . "</td><td>" . $v['mastercardn'] . "</td><td>" . $v['cmastercard'] . "</td><td>" . $v['cmastercardn'] . "</td><td align='right'>$totalmastercardn</td><td align='right'>$totalmastercard</td></tr>";
                        $linetotal += $totalmastercard;
                        $totalsale += $v['voucher'];
                        $totaln += $v['vouchern'];
                        $ctotaln += $v['cvouchern'];
                        $ctotal += $v['cvoucher'];
                        $totalvouchern = intval($v['vouchern']) + intval($v['cvouchern']);
                        $totalvoucher = floatval($v['voucher']) + floatval($v['cvoucher']);
                        echo "<tr><td align='left'>Voucher</td><td>" . $v['voucher'] . "</td><td>" . $v['vouchern'] . "</td><td>" . $v['cvoucher'] . "</td><td>" . $v['cvouchern'] . "</td><td align='right'>$totalvouchern</td><td align='right'>$totalvoucher</td></tr>";
                        $linetotal += $totalvoucher;
                        $totalsale += $v['account'];
                        $totaln += $v['accountn'];
                        $ctotaln += $v['caccountn'];
                        $ctotal += $v['caccount'];
                        $totalaccountn = intval($v['accountn']) + intval($v['caccountn']);
                        $totalaccount = floatval($v['account']) + floatval($v['caccount']);
                        echo "<tr><td align='left'>Account</td><td>" . $v['account'] . "</td><td>" . $v['accountn'] . "</td><td>" . $v['caccount'] . "</td><td>" . $v['caccountn'] . "</td><td align='right'>$totalaccountn</td><td align='right'>$totalaccount</td></tr>";
                        $linetotal += $totalaccount;
                        $totalsale += $v['cash'];
                        $totaln += $v['cashn'];
                        $ctotaln += $v['ccashn'];
                        $ctotal += $v['ccash'];
                        $totalcashn = intval($v['cashn']) + intval($v['ccashn']);
                        $totalcash = floatval($v['cash']) + floatval($v['ccash']);
                        echo "<tr><td align='left'>Cash</td><td>" . $v['cash'] . "</td><td>" . $v['cashn'] . "</td><td>" . $v['ccash'] . "</td><td>" . $v['ccashn'] . "</td><td align='right'>$totalcashn</td><td align='right'>$totalcash</td></tr>";
                        $linetotal += $totalcash;
                        $totalsale += $v['cheque'];
                        $totaln += $v['chequen'];
                        $ctotaln += $v['cchequen'];
                        $ctotal += $v['ccheque'];
                        $totalchequen = intval($v['chequen']) + intval($v['cchequen']);
                        $totalcheque = floatval($v['cheque']) + floatval($v['ccheque']);
                        echo "<tr><td align='left'>Cheque</td><td>" . $v['cheque'] . "</td><td>" . $v['chequen'] . "</td><td>" . $v['ccheque'] . "</td><td>" . $v['cchequen'] . "</td><td align='right'>$totalchequen</td><td align='right'>$totalcheque</td></tr>";
                        $linetotal += $totalcheque;
                        $totalsale += $v['laybuy'];
                        $totaln += $v['laybuyn'];
                        $ctotaln += $v['claybuyn'];
                        $ctotal += $v['claybuy'];
                        $totallaybuyn = intval($v['laybuyn']) + intval($v['claybuyn']);
                        $totallaybuy = floatval($v['laybuy']) + floatval($v['claybuy']);
                        echo "<tr><td align='left'>Laybuy</td><td>" . $v['laybuy'] . "</td><td>" . $v['laybuyn'] . "</td><td>" . $v['claybuy'] . "</td><td>" . $v['claybuyn'] . "</td><td align='right'>$totallaybuyn</td><td align='right'>$totallaybuy</td></tr>";
                        $linetotal += $totallaybuy;
                        $gtotaln = intval($ctotaln) + intval($totaln);
                        echo "<tr style='font-weight:bold; background: #eee;'><td align='left'>Totals</td><td>" . money_formatter($totalsale) . "</td><td>" . $totaln . "</td><td>" . $ctotal . "</td><td>" . $ctotaln . "</td><td align='right'>" . $gtotaln . "</td><td align='right'>" . money_formatter($linetotal) . "</td></tr>";
                        $grandtotalsale = $grandtotalsale + $totalsale;
                        $grandtotaln = $grandtotaln + $totaln;
                        $grandctotal = $grandctotal + $ctotal;
                        $grandctotaln = $grandctotaln + $ctotaln;
                        $grandgtotaln = $grandgtotaln + $gtotaln;
                        $grandtotal = $grandtotal + $linetotal;
                        ?>

                    </tbody></table>
                                                    <?php
                                                }
// totals bar
                                                ?>
            <br><table width='100%' cellspacing='0'>
                <tr style='background:#ccc;'><th class="dailysummarytable" align="left">&nbsp;</th><th class="dailysummarytable" align="left">Sale Amount</th><th class="dailysummarytable" align="left">Number</th><th class="dailysummarytable" align="left">Credit Amount</th><th class="dailysummarytable" align="left">Number</th><th class="dailysummarytable" align="right">Total Number</th><th class="dailysummarytable" align="right">Total $</th></tr>
                        <?php
                        echo "<tr style='font-weight:bold; background: #eee;'><td align='left' >Grand Totals</td><td>" . money_formatter($grandtotalsale) . "</td><td>" . $grandtotaln . "</td><td>" . $grandctotal . "</td><td>" . $grandctotaln . "</td><td align='right'>" . $grandgtotaln . "</td><td align='right'>" . money_formatter( $grandtotal) . "</td></tr>";
                        ?>    </table><br><h3>Voids</h3><table width='100%' cellspacing='0'>
                <tr style='background:#ccc;'><th class="dailysummarytable" align="left">User</th><th class="dailysummarytable" align="left">Amount</th><th class="dailysummarytable" align="left">Till</th><th class="dailysummarytable" align="left">Time</th></tr>
<?php
foreach ($void as $k => $v) {
    $user = get_userdata($v['user_id'], $db);
    echo "<tr style='font-weight:bold; background: #eee;'><td align='left' >" . $user['username'] . "</td><td>" . money_formatter( $v['value']) . "</td><td>" . $v['till'] . "</td><td align='left' >" . $v['time'] . "</td></tr>";
}
?>
            </table>

    </body>
</html>
<?php
// now add to the dailytotals
dailytotals_set($date, $grandtotal);
?>
