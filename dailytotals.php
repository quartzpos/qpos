<?php

/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 * 
 * 
 * this file is to tally the sales totals for the usage within the sales plotting feature
 */

function dailytotals_build($verbose = FALSE, $date = FALSE, $count = 0, $days = 0, $finish = FALSE, $truncate = TRUE) {
    global $db;
    $output='';
    if (!isValidDate($date)) {
        $date = FALSE;
    }
        if (!$date) {
        $date = $db->QPComplete("SELECT MIN(date) FROM sales")[0]['MIN(date)'];
    } //gives us the  first date entry of the sales
    $timestamp = new DateTime($date); //must use the datetime object function in order to get DST etc right
   
    if (!isValidDate($finish)) {
        $finish = FALSE;
    }
    if(is_integer($days)){
   if($days > 0 ){
       //need to modify finish
       $mod = 'P'.$days.'D';
       $finish = $timestamp->add(new DateInterval($mod));
       $finish = $finish->format('Y-m-d');
       if ($verbose) {
            $output .= DAYS_FINISH." = $finish<br>";
        }
   }
    }
    if ($verbose) {
        $output .= DATE_IS." ".$date." ". COUNT_IS." ". $count." ".FINISH_IS." ". $finish." ". DAYS_ARE." ".$days." ".TRUNCATE_IS." ". $truncate."<br>";
    }
    if ($truncate) {
        $db->query("TRUNCATE sales_totals");
        if ($verbose) {
            $output .= SALES_TRUNCATED."<br>";
        }
    }
    $where = "";
    if ($finish) {
        $fin = new DateTime($finish);
        $where = " WHERE date <= DATE('" . $fin->format('Y-m-d') . "')";
    }
    if (empty($count)) {
        $sql = "SELECT * FROM sales" . $where;
        if ($verbose) {
            $output .=  "Selection SQL = $sql.<br>";
        }
        $arr = $db->QPComplete($sql);
        $count = count($arr);
    }
    if ($verbose) {
        $output .=  "Sales count = $count.<br>";
    }


    $data = Array();
    for ($i = 1; $i <= $count; $i++) {
        $total = 0;
        if ($verbose) {
            $output .=  SALES_CHECK." $date<br>";
        }
        $timestamp = new DateTime($date); //must use the datetime object function in order to get DST etc right
        if(isset($fin)){
            if ($timestamp > $fin) {
            // the last date has been crossed
            break;
        }
        }
        $arr = $db->QPComplete("SELECT * FROM sales WHERE date = '$date'");
        if ($verbose && $arr) {
            $output .=  SALES_FOUND." $date<br>";
        }



        if ($verbose) {
            $output .=  TIMESTAMP_SET . $timestamp->format('Y-m-d') . "<br>";
        }
        if ($arr) {
            foreach ($arr as $k => $v) {

                $total = $total + (float) $v['sale_total_cost'];
            }

            if ($total > 0) {
                $sql = "INSERT INTO sales_totals (date,sale_total) VALUES ('$date',$total)";
                $db->query($sql);
                if ($verbose) {
                    $output .=  SQL_ADDED."$sql<br>";
                }
            }
        }
        $timestamp->add(new DateInterval('P1D'));
        if ($verbose) {
            $output .=  TIMESTAMP_CHANGED . $timestamp->format('Y-m-d') . "<br>";
        }
        $date = $timestamp->format('Y-m-d'); // set the next date
        if ($verbose) {
            $output .=  DATE_CHANGED."$date<br>";
        }
    }
    if ($verbose) {
        $output .=  FINISHED_INSERT."<br>";
    }
    if(isset($output)) {
        return $output;
    }
}

function dailytotals_set($date, $total) {
    global $db;
    
    if (is_numeric($total) && !empty($date)) {
        $arr = $db->QPComplete("SELECT sale_total FROM sales_totals WHERE date = '$date'");
        
        if ($total > 0 && empty($arr[0]['sale_total'])) {
            $sql = "INSERT INTO sales_totals (date,sale_total) VALUES ('$date',$total)";
            $db->query($sql);
        } else if ($total > 0 && !empty($arr[0]['sale_total'])) {
            //we have existing record to update
            
            $sql = "UPDATE sales_totals SET sale_total='$total' WHERE date='$date'";
            $db->query($sql);
        }
    }
}

function dailytotals_get($start = FALSE, $finish = FALSE, $days = 0) {
    global $db;
    if(!isValidDate($start)){
        $start = FALSE;
       // die("Start date wrong.");
    }
    if (!$start) {
        $start = $db->QPComplete("SELECT MIN(date) FROM sales")[0]['MIN(date)'];
         //die("Start date false.");
    } //gives us the  first date entry of the sales
    $timestamp = new DateTime($start);
    if(!isValidDate($finish)){
        $finish = FALSE;
    }
      if(is_numeric($days)){
        if($days > 0 ){
       //need to modify finish
       $mod = 'P'.(int) $days.'D';
       $finish = $timestamp->add(new DateInterval($mod));
       $finish = $finish->format('Y-m-d');
            }
        }
     $where = "";
    
        $sta = new DateTime($start);
        $where = " date >= DATE('" . $sta->format('Y-m-d') . "')";
    
    if ($finish) {
        $fin = new DateTime($finish);
        if(!empty($where)){ $where.=" AND "; }
        $where .= " date <= DATE('" . $fin->format('Y-m-d') . "')";
    }  
   
    if(!empty($where)){ $where =" WHERE ".$where; }
    $sql="SELECT * FROM sales_totals ".$where;
    
    $arr = $db->QPComplete($sql);
    $restr = '[';
    foreach ($arr as $k => $v) {
        $restr .= "['" . $v['date'] . "'," . $v['sale_total'] . "],";
    }

    $restr = rtrim($restr, ",");
    $restr .= "]";
    return $restr;
}

function isValidDate($date) {
    //checks validity of a date sent to the function, return date or false.
    return preg_match("/^(\d{4})-(\d{1,2})-(\d{1,2})$/", $date, $part) ? checkdate(intval($part[2]), intval($part[3]), intval($part[1])) : false;
}

//begin process of page load
// examples of loading page manually:
// http://quartzpos.com/dailytotals.php?dt_rebuild=1   
// http://quartzpos.com/dailytotals.php?dt_rebuild=2015-08-01&finish=2016-08-02
// http://quartzpos.com/dailytotals.php?dt_rebuild=2015-08-01&count=1500
// http://quartzpos.com/dailytotals.php?dt_rebuild=2015-08-01&finish=2016-08-02&truncate=1&verbose=1
// http://quartzpos.com/dailytotals.php?dt_get=1&start=2015-08-01&finish=2016-08-01
// http://quartzpos.com/dailytotals.php?dt_get=1&start=2015-08-01&days=50
// dt_rebuild = do a recalculation of the dailytotals figures. can be sent a date to start with, or anything else to make it true
// finish = a date to finish the report. All dates are in YYYY-MM-DD format.
// truncate = rebuild the sales_totals database table rather than selectively updating it. May take longer, but is thorough. Send it anything to make it TRUE.
// verbose = send html to screen of the work in progress. Send it anything to make it TRUE.
// count = the number of records in the sales database to iterate over. 0= all (default)
// days = the number of days to iterate over. 0= all (default). Setting days will override finish.
// dt_get = return with the data, give it any value to make it true
// start = start date for dt_get

if (!isset($db)) {
    include'database.php';
}
$count = 0;
$days = 0;
$finish = FALSE;
$truncate = TRUE;
$verbose = FALSE;
$start= FALSE;
if (isset($_GET['count'])) {
    if (is_numeric($_GET['count'])) {
        $count = (int) $_GET['count'];
    }
}
if (isset($_GET['start'])) {
    $start = $_GET['start'];
}
if (isset($_GET['finish'])) {
    $finish = $_GET['finish'];
}
if (isset($_GET['days']) && is_numeric($_GET['days'])) {
    $days = (int) $_GET['days'];
}
if (isset($_GET['verbose'])) {
    $verbose = TRUE;
}
if (isset($_GET['truncate'])) {
    $truncate = TRUE;
}

if (isset($_GET['dt_rebuild'])) {

//direct access build then return
    $out = dailytotals_build($verbose, $_GET['dt_rebuild'], $count, $days, $finish, $truncate);
    
}

if (isset($_GET['dt_get'])) {
    print dailytotals_get($start,$finish,$days);
}
