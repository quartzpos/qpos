<?php

/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */

class database {

    var $conn = null;

    function __construct($server, $username, $password, $database) {
        ini_set('display_errors', '0');
        if (!$this->conn = mysqli_connect($server, $username, $password, $database)) {
            $title = 'QuartzPos Database Error';
            require_once 'template.php';
            print "<body class='admin'><div style='min-height: 100%;height: auto !important;height: 100%;margin: 0 auto -61px;'>";
            require_once 'header.php';
            print "<div class='admin_content' ><h3>Database connection error.</h3><p>Please have your administrator check the config.php for the correct database values.</p></div>";
            print "</div><div id='footer' style='height:32px'>";
            require_once('footer.php');
            print "</div></body></html>";
            die();
        }
        ini_set('display_errors', '1');
    }

    function close() {
        mysqli_close($this->conn);
    }

    function query($query) {
        $result = mysqli_query($this->conn, $query);
        return $result;
    }

    function getConnection() {
        return $this->conn;
    }

    function clean($data) {
        $result = mysqli_real_escape_string($this->conn, trim($data)); // remove spaces, then prepare for sql insertion
        $result= htmlentities($result, ENT_QUOTES);    // clean up the quoting     
        return $result;
    }
    function display($data){
         return html_entity_decode($data); // reverse quoting clean for display purposes
    }
    
    function set_charset($string){
         $result=mysqli_set_charset($this->conn,$string);
         return $result;
    }
    function charset(){
        $result= mysqli_character_set_name ( $this->conn );
         return $result;
    }
    function numRows($result) {
        $num = mysqli_num_rows($result);
        return $num;
    }

    function fetchRow($result) {
        $row = mysqli_fetch_row($result);
        return $row;
    }

    function fetchAssoc($result) {
        $assoc = mysqli_fetch_assoc($result);
        return $assoc;
    }

    function fetchArray($result, $type = MYSQLI_ASSOC) {
        $assoc = mysqli_fetch_array($result, $type);
        return $assoc;
    }

    function freeResult($result) {
        return mysqli_free_result($result);
    }

    function insertId($result) { // return the ID of the last record inserted
        $id = mysqli_insert_id($result);
        return $id;
    }

    function QPResults($query) {
        $result = $this->query($query);
        
        $arr = $this->fetchAssoc($result);
        
        return $arr;
    }

    function QPComplete($query) {
        if($result = $this->query($query)){

        $retarr = Array();
        while ($arr = mysqli_fetch_assoc($result)) {
            $retarr[] = $arr;
        }
        mysqli_free_result($result);
        return $retarr;
    }
    }

    function QPsearch($keyword, $option, $field, $table) {
        //provides advanced text search, returns associative array
        if ($option == "allwords") {
            $result = mysqli_query("SELECT * FROM $table WHERE MATCH ( $field ) AGAINST ('{$keyword}' IN BOOLEAN MODE)");
        } elseif ($option == "exactphrase") {
            $result = mysqli_query("SELECT * FROM $table WHERE MATCH ( $field ) AGAINST ('\"{$keyword}\"' IN BOOLEAN MODE)");
        } elseif ($option == "anywords") {
            $result = mysqli_query("SELECT * FROM $table WHERE MATCH ( $field ) AGAINST ('{$keyword}' IN BOOLEAN MODE)");
        } elseif ($option == "withouttheword") {
            $result = mysqli_query("SELECT * FROM $table WHERE MATCH ( $field ) AGAINST ('-{$keyword}' IN BOOLEAN MODE)");
        }
        $retarr = Array();
        while ($arr = mysqli_fetch_assoc($result)) {
            $retarr[] = $arr;
        }
        return $retarr;
    }

    function QPError($result) {
        return mysqli_error($result);
    }

    function QPInsertUpdate($tableName, $valuesArray, $whereArray) {
		$sql = BuildSQL($tableName, $whereArray);

			if ( $this->query($sql)) {

		//$this->SelectRows($tableName, $whereArray);

			if ($this->numRows() > 0) {
				return $this->UpdateRows($tableName, $valuesArray, $whereArray);
			} else {
				return $this->InsertRow($tableName, $valuesArray);
			}

            }
	}
    function UpdateRows($tableName, $valuesArray, $whereArray = null) {

			$sql = BuildSQLUpdate($tableName, $valuesArray, $whereArray);
			// Execute the UPDATE
			if (! $this->query($sql)) {
				return false;
			} else {
				return true;
			}

	}
    function InsertRow($tableName, $valuesArray) {

			// Execute the query
			$sql = BuildSQLInsert($tableName, $valuesArray);
			if (! $this->query($sql)) {
				print "Failure in SQL: ". $sql;
				return false;
			} else {
				return $this->insertId();
			}

	}
    function BuildSQLInsert($tableName, $valuesArray) {
		$columns = BuildSQLColumns(array_keys($valuesArray));
		$values  = BuildSQLColumns($valuesArray, false, false);
		$sql = "INSERT INTO `" . $tableName .
			   "` (" . $columns . ") VALUES (" . $values . ")";
		return $sql;
	}
    function BuildSQLUpdate($tableName, $valuesArray, $whereArray = null) {
		$sql = "";
		foreach ($valuesArray as $key => $value) {
			if (strlen($sql) == 0) {
				$sql = "`" . $key . "` = " . $value;
			} else {
				$sql .= ", `" . $key . "` = " . $value;
			}
		}
		$sql = "UPDATE `" . $tableName . "` SET " . $sql;
		if (is_array($whereArray)) {
			$sql .= BuildSQLWhereClause($whereArray);
		}
		return $sql;
	}
    function BuildSQL($tableName,$where=NULL){
        $sql = "SELECT " . $sql . " FROM `" . $tableName . "`";
		if (is_array($where)) {
			$sql .= BuildSQLWhereClause($where);
		}
        return $sql;
    }
    function BuildSQLColumns($columns, $addQuotes = true, $showAlias = true) {
		if ($addQuotes) {
			$quote = "`";
		} else {
			$quote = "";
		}
		switch (gettype($columns)) {
			case "array":
				$sql = "";
				foreach ($columns as $key => $value) {
					// Build the columns
					if (strlen($sql) == 0) {
						$sql = $quote . $value . $quote;
					} else {
						$sql .= ", " . $quote . $value . $quote;
					}
					if ($showAlias && is_string($key) && (! empty($key))) {
						$sql .= ' AS "' . $key . '"';
					}
				}
				return $sql;
				break;
			case "string":
				return $quote . $columns . $quote;
				break;
			default:
				return false;
				break;
		}
	}
    function BuildSQLWhereClause($whereArray) {
		$where = "";
		foreach ($whereArray as $key => $value) {
			if (strlen($where) == 0) {
				if (is_string($key)) {
					$where = " WHERE `" . $key . "` = " . $value;
				} else {
					$where = " WHERE " . $value;
				}
			} else {
				if (is_string($key)) {
					$where .= " AND `" . $key . "` = " . $value;
				} else {
					$where .= " AND " . $value;
				}
			}
		}
		return $where;
	}
}

require_once('config.php');
$db = new database($dbhost, $dbuser, $dbpassword, $dbname);
if (!$db->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $db->error);
}