<?php

/*
 * debugger.php
 *
 * Copyright 2013 onyx <onyx@onyxlaptop>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
require_once('config.php');
require_once('database.php');
require('consoleLogging.php');
if (isset($_POST)){
    if(isset($_POST['sale_id'])){
        $sale_id = $db->clean($_POST['sale_id']);
        
        $sql="SELECT * from logs WHERE transaction='$sale_id'";
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql,"Result=");
        $res=$db->QPResults($sql);
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($res,"Res=");
        $result = unserialize($res['transmitted']);
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($result,"Result=");
    }
}
?>


<html>
    <head></head>
    <body>
        <div id="debug_div">
            <form name="debugger" action="debugger.php" method="POST">
                <table border="1">
                    
                    <tbody>
                        <tr>
                            <td>Sale ID#</td>
                            <td><input name="sale_id" type="text" id="sale_id" value="<?php if(isset($_POST['sale_id'])){ echo $_POST['sale_id']; } ?>" /></td>
                        </tr>
                        <tr>
                            <td>Result</td>
                            <td><?php 
                            if(isset($result)){
                                var_dump($result);
                            }
                            ?></td>
                        </tr>
                        
                    </tbody>
                </table>
                <input type="submit" name="submit" value="Submit" />
            </form>
        </div>
            
    </body>
</html>