<?php

// to load the database for the demo data
// for up to 50 times, get up to 10 random items, with quantity between 1 and 10 randomly, then build sale as if sold by random user, and save to db
// then rebuild the other tables, ie items, barcodes, etc must stay the same



include("config.php");
include("consoleLogging.php");

require_once("database.php");
include 'helpers.php';
require_once 'api/http_request.php';
$count=0;
$output = '';
$subtotal = 0;
$saletotal = 0;
//***********************************
//remove following 2 lines after dev

$db->query("TRUNCATE TABLE sales_items");
$db->query("TRUNCATE TABLE sales");
for ($d = 1; $d < 6; $d++) {

    $date = mktime(0, 0, 0, date("m"), date("d") - $d, date("Y"));
    $date = date("Y-m-d",$date);
    $output .= $date."<br>";
// step through 6 days, backwards, to load the data for as many days
    $times = mt_rand(3, 10); 
    for ($t = 0; $t < $times; $t++) {
        $sql = "SELECT * from items";
        $rows = $db->QPComplete($sql);

        $rowcount = count($rows);
        $num = mt_rand(1, 10);

        $saletotal = 0;
        $totalqty = 0;
        $subtotal = 0;
        for ($i = 0; $i < $num; $i++) {

            $randitem = mt_rand(1, $rowcount);

            $items[$i] = $rows[$randitem - 1];

            $price = $items[$i]['unit_price'];
            $tax = "1." . $items[$i]['tax_percent'];

            $qty[$count][$i] = mt_rand(1, 10);
            $totalqty+=$qty[$count][$i];
            $subtotalt = $qty[$count][$i] * $price;
            $subtotal+=$subtotalt;
            $saletotal+=money_formatter( $subtotalt * money_formatter($tax));
        }
        $user[$t] = mt_rand(1, 4);
        $till[$t] = mt_rand(1, 5);
        //$date = date("Y-m-d");
        $time = date("H-i-s");
        $session = $date . "-" .$time."-". $user[$t] . "-" . $till[$t];
        $arr = Array('eft' => $saletotal, 'remainder' => 0.00, 'total' => $saletotal, 'id' => 1, 'cost' => $saletotal, 'smallcoin' => .10);

        $paidwith = base64_encode(serialize($arr));
//        $sqlinsert = "INSERT INTO sales (date,time,customer_id,sale_sub_total,sale_total_cost,paid_with,items_purchased,sold_by,till,session,state) VALUES ('" . $date . "','" . $time . "',1," . $subtotal . "," . $saletotal . ",'" . $paidwith . "'," . $totalqty . "," . $user[$t] . "," . $till[$t] . ",'" . $session . "','completed')";
        $salearray=Array('date'=>$date,'time'=>$time,'customer_id'=>1,'sale_sub_total'=>$subtotal,'sale_total_cost'=>$saletotal,'paid_with'=>$paidwith,'items_purchased'=>$totalqty,'sold_by'=>$user[$t],'till'=>$till[$t],'session'=>$session,'state'=>'completed');
        
      
         //   $id = $db->insertID($db->getConnection());
           
            //we have id, go on to insert the sales items
            for ($i = 0; $i < $num; $i++) {
                $tax = "1." . $items[$i]['tax_percent'];
                $itemstotaltax = $items[$i]['unit_price'] - (money_formatter( $items[$i]['unit_price'] / $tax));
                $itemstotalprice = $items[$i]['unit_price'] * $qty[$count][$i];

//                $sql = "insert into sales_items(sale_id, item_id, quantity_purchased, item_unit_price, item_trade_price, item_tax_percent, item_total_tax,  item_total_cost, item_name, item_description) values(" . $id . "," . $items[$i]['id'] . "," . $qty[$count][$i] . ",'" . $items[$i]['unit_price'] . "','" . $items[$i]['trade_price'] . "','" . $items[$i]['tax_percent'] . "','" . $itemstotaltax . "','" . $itemstotalprice . "','" . $items[$i]['item_number'] . "','" . $items[$i]['item_name'] . "')";
                $itemsarray[]=Array('sale_id'=>1, 'item_id'=>$items[$i]['id'], 'quantity_purchased'=>$qty[$count][$i], 'item_unit_price'=>$items[$i]['unit_price'], 'item_trade_price'=>$items[$i]['trade_price'], 'item_tax_percent'=>$items[$i]['tax_percent'], 'item_total_tax'=>$itemstotaltax,  'item_total_cost'=>$itemstotalprice, 'item_name'=>$items[$i]['item_number'], 'item_description'=>$items[$i]['item_name']);
                
                //$result = $db->query($sql);
            }
      //now use the API to load the data from the $salearray and $itemsarray
            $data['sale']=$salearray;
            $data['items']=$itemsarray;
     //$output.= dumparray($data);
     //var_dump($data);
            //http__Request(Array('function'=>'make_sale','data' => $data));
            
            $count++;
    }// end for t
}//end for d
print $output;
