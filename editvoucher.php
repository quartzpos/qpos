<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */

if (!isset($_SESSION)) {
    session_start();
}

require_once 'database.php';
include("consoleLogging.php");
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_REQUEST, "editvoucher.php request=");
$extrascripts = "function done(){window.location='sale.php?vouchers=1&closewin=1";
$url="";
if (isset($_GET['sale'])) {
    $sale = 1;
    $extrascripts.="&sale=" . $sale;
    $url .="&sale=" . $sale;
}
if (isset($_GET['id'])) {
    $saleid = $_GET['id'];
    $extrascripts.="&id=" . $saleid;
    $url.="&id=" . $saleid;
}
$extrascripts.="'};";

if (isset($_POST)) {
    unset($_POST['submit']);
    foreach ($_POST as $k => $v) {
        $idarr = explode('-', $k);
        $ids = $idarr[1];
        $field = $idarr[0];
        $val = $db->clean($v);
        $sql = "UPDATE vouchers SET $field='$val' WHERE id=$ids";
       //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql, 'Addvoucher post sql=');
        $db->query($sql);
    }
}


if (isset($_GET['action']) && $_GET['action'] == 'edit') {
    $id = $db->clean($_GET['edit']);
    $sql = "select * from vouchers where sale_id=$id";
   //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql, "editvoucher.php sql=");
    $result = $db->query($sql);


//    $extracss = '<link rel="stylesheet" type="text/css" href="easyui/themes/gray/easyui.css">
//	<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
//	<link rel="stylesheet" type="text/css" href="easyui/demo.css">';
    $extrascripts .= ' jQuery(function() {console.log("datepicker read");
    jQuery( ".datepick" ).datepicker(
    { dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true });
   console.log("datepicked");
        });
        ';
    require_once('template.php');
    //$voucher = $db->fetchRow($result);
    ?>
    <body> <h3><?php echo VOUCHER_HEADER; ?></h3>

        <form action="editvoucher.php?action=edit&edit=<?php echo $id.$url; ?>" method="POST"name="vouch" id="vouch">

            <?php
            while ($row = $db->fetchAssoc($result)) {
                ?>
                <table class="advch" style="width:100%;border-bottom:2px solid #111;margin-bottom:20px;padding:15px;">
                    <tr class="vouch-tr-1 even" style="vertical-align:top;">
                    <tr><td><?php echo VOUCHER_ID; ?></td><td class="vouch-td-0 td-id">
                            <input type="text" id="id-<?php echo $row['id']; ?>" value="<?php echo $row['id']; ?>" name="id-<?php echo $row['id']; ?>" disabled="disabled" class="QPInput vouch-input-0 id ">
                        </td></tr>
                    <tr><td><?php echo VOUCHER_CODE;?></td><td class="vouch-td-1 td-code">
                            <input type="text" id="code-<?php echo $row['id']; ?>" value="<?php echo (isset($row['code']) ? $row['code'] : ''); ?>" name="code-<?php echo $row['id']; ?>" class="QPInput vouch-input-1 code ">
                        </td></tr>
                    <tr><td><?php echo VOUCHER_DATE;?></td><td class="vouch-td-2 td-date">
                            <input type="text" id="date-<?php echo $row['id']; ?>" value="<?php echo (isset($row['date']) ? $row['date'] : ''); ?>" name="date-<?php echo $row['id']; ?>" class="QPInput vouch-input-2 date datepick ">
                        </td></tr>
                    <tr><td><?php echo VOUCHER_CUSTID;?></td><td class="vouch-td-3 td-customer_id">
                            <input type="text" id="customer_id-<?php echo $row['id']; ?>" value="<?php echo (isset($row['customer_id']) ? $row['customer_id'] : ''); ?>" name="customer_id-<?php echo $row['id']; ?>" class="QPInput vouch-input-3 customer_id ">
                        </td></tr>
                    <tr><td><?php echo VOUCHER_RECIPIENT;?></td><td class="vouch-td-4 td-recipient_name"> 
                            <input type="text" id="recipient_name-<?php echo $row['id']; ?>" value="<?php echo (isset($row['recipient_name']) ? $row['recipient_name'] : ''); ?>" name="recipient_name-<?php echo $row['id']; ?>" class="QPInput vouch-input-4 recipient_name ">
                        </td></tr>
                    <tr><td><?php echo VOUCHER_PURCH;?></td><td class="vouch-td-5 td-purchase_value">
                            <input type="text" id="purchase_value-<?php echo $row['id']; ?>" value="<?php echo (isset($row['purchase_value']) ? $row['purchase_value'] : ''); ?>" name="purchase_value-<?php echo $row['id']; ?>" class="QPInput vouch-input-5 purchase_value ">
                        </td></tr>
                    <tr><td><?php echo VOUCHER_CURRENT;?></td><td class="vouch-td-6 td-current_value">
                            <input type="text" id="current_value-<?php echo $row['id']; ?>" value="<?php echo (isset($row['current_value']) ? $row['current_value'] : ''); ?>" name="current_value-<?php echo $row['id']; ?>" class="QPInput vouch-input-6 current_value ">
                        </td></tr>
                    <tr><td><?php echo VOUCHER_EXPIRY;?></td><td class="vouch-td-7 td-expiry">
                            <input type="text" id="expiry-<?php echo $row['id']; ?>" value="<?php echo (isset($row['expiry']) ? $row['expiry'] : ''); ?>" name="expiry-<?php echo $row['id']; ?>" class="QPInput vouch-input-7 expiry datepick" <?php echo (isset($_SESSION['admin']) ? '' : 'disabled'); ?>>
                        </td></tr>
                    <tr><td><?php echo VOUCHER_CUSTCONTACT;?></td><td class="vouch-td-8 td-customer_contact">
                            <input type="text" id="customer_contact-<?php echo $row['id']; ?>" value="<?php echo (isset($row['customer_contact']) ? $row['customer_contact'] : ''); ?>" name="customer_contact-<?php echo $row['id']; ?>" class="QPInput vouch-input-8 customer_contact ">
                        </td></tr>
                    <tr><td><?php echo VOUCHER_RECPCONTACT;?></td><td class="vouch-td-9 td-recipient_contact">
                            <input type="text" id="recipient_contact-<?php echo $row['id']; ?>" value="<?php echo (isset($row['recipient_contact']) ? $row['recipient_contact'] : ''); ?>" name="recipient_contact-<?php echo $row['id']; ?>" class="QPInput vouch-input-9 recipient_contact ">
                        </td></tr>
                </table>
    <?php } ?>
            <div id="buttons" style="width:230px;text-align:right; float:right; padding:20px;"><input type="submit" name="submit" value="<?php echo TXT_SAVE; ?>"><?php if(isset($_POST)){?><button type="button" name="Done" onclick="done()"><?php echo TXT_DONE;?></button><?php } ?></div>
        </form>
        <?php
    }
    ?>

</body>
</html>
