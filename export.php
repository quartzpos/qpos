<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */

if (!isset($_SESSION))
    session_start();
require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");
require_once("functions.php");

require_once('Net/SFTP.php');
$timezone = $db->QPResults("SELECT value FROM system WHERE name='timezone'")['value'];
set_time_limit(30);
$filename = $db->QPResults("SELECT value FROM system WHERE name='export_filename'")['value'];

$getconf = $db->query("SELECT * FROM system");

while ($row = $db->fetchAssoc($getconf)) {
    if ($row['name'] == 'export')
        $export = $row['value'];
    if ($row['name'] == 'export_type')
        $exporttype = $row['value'];
}

$usermod = "";
$tillmod = "";
$fixmod="";
$message = "";
if (isset($_GET['date1'])) {
    $_POST['date1'] = $_GET['date1'];
}
if (isset($_GET['user'])) {
    $usermod = " AND sold_by=" . $db->clean($_GET['user']);
    $userd = get_userdata(intval($_GET['user']), $db);
    $user = $userd['username'];
    $message = "User:" . $user . ".<br>";
}
if (isset($_GET['till'])) {
    $tillmod = " AND till=" . $db->clean($_GET['till']);
    $message .="Till:" . $_GET['till'] . ".<br>";
}
if (isset($_GET['zero_fix']) && isset($_GET['start_date'])){
	$fixmod = " WHERE date >'" . $db->clean($_GET['start_date'])."'";
    $message .="Zerofix applying from " . $db->clean($_GET['start_date']) . " <br>";
}
$nz_time = new DateTime(null, new DateTimezone($timezone));
if (isset($_POST['date1']) && $_POST['date1'] != '') {
    $datemod = $_POST['date1'];
} else {
    $datemod = $nz_time->format('Y-m-d');
}

$title = 'Export sales';
require_once 'template.php';
?>
<body><?php
    if (isset($exporttype)) {
        switch ($exporttype) {
            case 'wildcard':



                $mod = null;
                $total = 0;
				$sisqlmod="";
				$fix_count=100000;
                $arr = Array();
// build mod based upon above mod values
                if (isset($datemod)) {
                    $mod = " WHERE date='$datemod'";
                    $mod .= $usermod;
                    $mod .= $tillmod;
                }
                if (isset($fixmod)) {
                    $mod = $fixmod;
                    $sisqlmod=" AND item_unit_price = 0";
                }
                $sql = "SELECT * FROM sales " . (isset($mod) ? $mod : '');
                $result = $db->query($sql);
                if ($result) {
                    $fh = fopen(EXPORT_FILE_PATH . $filename, 'w') or die("Can't open export file:" . EXPORT_FILE_PATH . $filename);

                    while ($irow = $db->fetchAssoc($result)) {
                        $output = '';
                        $sql = "SELECT * FROM sales_items WHERE sale_id='" . $irow['id'] . "' ".$sisqlmod;
                        $iresult = $db->query($sql);

                        $item = Array();
                        $qty = 0;
                        while ($itrow = $db->fetchAssoc($iresult)) {
							if($itrow['item_name'] !="E"){//unsure how, but when in sales a blank item is produced it results in the item name of E
                            $item[] = $itrow;
                            $qty += floatval(abs($itrow['quantity_purchased'])); //in wildcard,the quantity is absolute for the pos data. Duh...I know...
							}
                        }
                        //get payment array and break it out
                        //Array ( [eft] => 123 [visa] => 32 [amex] => 43 [mastercard] => 54 [voucher] => 56 [voucherref] => 123456 [account] => 13 [accountref] => abcdef [cash] => 160.85 [comment] => totally split payment [remainder] => 0.00 [total] => 481.85 [id] => 49 [cost] => 481.85 )
                        if ($irow['customer_id'] == '1') {
                            $customer = "cash";
                            $price = "1";
                        } else {
                            $customer = "2";
                            $price = "2";
                        }
                        $charge = "N";
                        $date = new DateTime($irow['date']);
                        $trdate = $date->format('d/m/y');
                        if(isset($fixmod)){
							$trdate="04/08/19";
						}
                        if (isset($irow['account']) && floatval($irow['account']) > 0)
                            $charge = "Y";
                        if ($irow['sold_by'] != '0') {
                            $userd = get_userdata($irow['sold_by'], $db);
                            $user = $userd['username'];
                        } else {
                            $user = "admin";
                        }

                        $pmt = unserialize(base64_decode($irow['paid_with']));
                        $pmtarr = Array();
                        if (!empty($pmt)) {
                            $totalpmt = $pmt['total'];
                            $tmp = $pmt; //the payment values only
                            unset($tmp['cost']);
                            unset($tmp['total']);
                            unset($tmp['id']);
                            unset($tmp['remainder']);
                            unset($tmp['comment']);
                            unset($tmp['accountref']);
                            unset($tmp['voucherref']);

                            if (isset($pmt['accountref']) && isset($pmt['account'])) {
                                $pmtarr[2]['id'] = $pmt['accountref'];
                                $pmtarr[2]['type'] = 'account';
                                $pmtarr[2]['amount'] = $pmt['account'];
                            }
                            if (isset($pmt['voucherref']) && isset($pmt['voucher'])) {
                                $pmtarr[1]['id'] = $pmt['voucherref'];
                                $pmtarr[1]['type'] = 'voucher';
                                $pmtarr[1]['amount'] = $pmt['voucher'];
                            }
                            $pmtarr[0]['id'] = 'eft/card/cash';
                            $pmtarr[0]['type'] = "EFT_";
                            $pmtarr[0]['amount'] = isset($pmt['eft']) ? floatval($pmt['eft']) : '0';
                            $pmtarr[0]['amount']+=isset($pmt['visa']) ? floatval($pmt['visa']) : '0';
                            $pmtarr[0]['amount']+=isset($pmt['mastercard']) ? floatval($pmt['mastercard']) : '0';
                            $pmtarr[0]['amount']+=isset($pmt['cash']) ? floatval($pmt['cash']) : '0';
                        }

                        
                        if(isset($fixmod)){
							$irow['sale_total_cost']=0;
							$irow['id']=$fix_count;
							$fix_count++;
						}
						$output.= $irow['id'] . ',';
                        $output.= $irow['sale_total_cost'] . ',';
                        $output.= count($item) . ',';
                        $output.= $qty . ',';
                        if (!empty($tmp)) {
                            $output.= count($tmp) . ',';
                        } else {
                            $output.= '0,';
                        }
                        $output.= '0,'; //number of refunds -- irrelevant
                        $output.= $customer . ',';
                        $output.= $price . ',';
                        $output.= $charge . ',';
                        $output.= 'C,'; // transaction status C= complete???
                        $output.= $trdate . ',';
                        $output.= $irow['time'] . ',';
                        $output.= $user . ',';
                        $output.= $irow['till'] . ',';
                        for ($i = 0; $i <= 2; $i++) {
                            if (isset($pmtarr[$i]['type']) && !empty($pmtarr[$i]['type'])) {
                                $output.= $pmtarr[$i]['type'] . ',';
                            } else {
                                $output.= ' ,';
                            }
                            if (isset($pmtarr[$i]['amount']) && !empty($pmtarr[$i]['amount'])) {
                                $output.= $pmtarr[$i]['amount'] . ',';
                            } else {
                                $output.= ' ,';
                            }
                            if (isset($pmtarr[$i]['id']) && !empty($pmtarr[$i]['id'])) {
                                $output.= $pmtarr[$i]['id'] . ',';
                            } else {
                                $output.= ' ,';
                            }
                        }
                        $output.='AK,'; //sales area code - unsure if necessary for wildcard???
                        $output.='|';
						if(empty($item)){
							$pmt=0; //set the payment to nothing so that the line doesn't get written when doing the fixmod
						}
                        foreach ($item as $k => $v) {
                            //print $k.'--'.$v.'<br>';
                            $sql = "SELECT * FROM items WHERE item_number='" . $v['item_name'] . "'";
                            $res = $db->query($sql);
                            $itemdata = $db->fetchAssoc($res);
                            $output.=$itemdata['item_number'] . ',';
                            $desc= str_replace(',', '-', $itemdata['description']);
                            $output.=$desc . ',';
                            $output.='EACH,'; //TODO: db doesn't handle this yet should be set also
                            $output.=$v['item_unit_price'] . ',';
                            $output.=$v['quantity_purchased'] . ',';
                            $output.=$v['item_total_cost'] . ',';
                            $output.='|';
                        }
                        if (!empty($pmt)) { //only add the rows when there is a payment for this transaction
                            $out = export_to_wc($output);

                            fwrite($fh, $out);
                        }
                    }

            //transaction number, total, no lines, total qty, no payments, no refunds, customer code(cash or 2-trade), customer price level(1or2), if account is chargeY N, Transaction status, last updated date, last updated time,user who created transaction, till no, 1st pmt type, 1st pmt amount, 1st payment id, 2nd pmt type, 2nd pmt amount,2nd pmt id,  3rd pmt type, 3rd pm
            //
            // IMPORTANT!! The fields are fixed width...see function export_to_wc
            //08105301,48.18     ,1  ,1.00      ,1,0,CASH    ,1,N,C,04/05/15,12:45:32,LT        ,001,EFT_,48.18     ,                     ,    ,0.00      ,                     ,    ,0.00      ,                     ,PETONE    ,
            //VIC5640315      ,BONING KNIFE 15CM STRAIGHT BLACK   ,EACH  ,48.18     ,1         ,48.18     ,
            //08105401,-9.60     ,1  ,-1.00     ,0,1,CASH    ,1,N,C,04/05/15,13:32:07,LT        ,001,CASH,-9.60     ,                     ,    ,0.00      ,                     ,    ,0.00      ,                     ,PETONE    ,
            //TOM0144007      ,CUTTER ROUND 90MM S/S   (NOT A SET),EACH  ,9.60      ,-1        ,-9.60     ,
            //08105501,67.20     ,2  ,8.00      ,1,0,CASH    ,1,N,C,04/05/15,13:37:51,LT        ,001,EFT_,67.20     ,                     ,    ,0.00      ,                     ,    ,0.00      ,                     ,PETONE    ,
            //4TS1000         ,KNIFE SHARPEN STANDARD NO SERRATED ,EAC   ,6.00      ,6         ,36.00     ,
            //TIL52012        ,FOOD STACKER 18/10 83 D X 60MM HIGH,EACH  ,15.60     ,2         ,31.20     ,t amount,3rd pmt id, sales area code
            //
            //Item code, Item description, Unit, Price used, Qty, Line subtotal
            //Array ( [0] => Array ( [id] => 42 [sale_id] => 44 [item_id] => 2 [quantity_purchased] => 5 [item_unit_price] => 56.00 [item_trade_price] => 45.00 [item_tax_percent] => 15 [item_total_tax] => 42.00 [item_total_cost] => 322.00 ) )
            //Array ( [0] => Array ( [id] => 47 [sale_id] => 49 [item_id] => 2 [quantity_purchased] => 4 [item_unit_price] => 56.00 [item_trade_price] => 45.00 [item_tax_percent] => 15 [item_total_tax] => 33.60 [item_total_cost] => 257.60 ) [1] => Array ( [id] => 48 [sale_id] => 49 [item_id] => 1 [quantity_purchased] => 3 [item_unit_price] => 65.00 [item_trade_price] => 45.00 [item_tax_percent] => 15 [item_total_tax] => 29.25 [item_total_cost] => 224.25 ) )
                    fclose($fh);
                    echo "<div id='exported' >";
                    if(!empty($message)){echo $message; }
                    echo connect();
                    echo "</div>";
                }
                break;

            default:
                break;
        }
    } else {
        print "Use Administration -> Setup to configure export, as Export Type is not set.";
    }
    ?>

</body>
</html>
<?php

function export_to_wc($indata) {
    //accept input file of raw pos data, output the file for wildcard,file must be clean of spaces
//print ($indata."<hr>");
// setup output parameters


    $tadata = explode('|', $indata);
    $output = "";
    $t = count($tadata);
    for ($i = 0; $i < $t; $i++) {
        $rdata = explode(',', $tadata[0]); //the first line of the current transactional array
        $output.= reformat($rdata);
        array_shift($tadata); //nibble away at the array until it's finished   
    }


    return $output;
}

function reformat($data) {
    //print_r($data);
    $pos = Array(); // start the output mapping for transaction. first is left position, second is length
    $pos[0] = Array(0, 8); //tranid
    $pos[1] = Array(10, 10); //value
    $pos[2] = Array(21, 3); //nlines
    $pos[3] = Array(25, 10); //nitems
    $pos[4] = Array(36, 1); //npayments
    $pos[5] = Array(38, 1); //nrefunds
    $pos[6] = Array(40, 8); //account
    $pos[7] = Array(49, 1); //pricebook
    $pos[8] = Array(51, 1); //charge
    $pos[9] = Array(53, 1); //completed? must be C!!
    $pos[10] = Array(55, 8); //date
    $pos[11] = Array(64, 8); //time
    $pos[12] = Array(73, 10); //operator
    $pos[13] = Array(84, 3); //station
    $pos[14] = Array(88, 4); //paytype1
    $pos[15] = Array(93, 10); //payamt1
    $pos[16] = Array(104, 21); //payid1
    $pos[17] = Array(126, 4); //paytype2
    $pos[18] = Array(131, 10); //payamt2
    $pos[19] = Array(142, 21); //payid2
    $pos[20] = Array(164, 4); //paytype3
    $pos[21] = Array(169, 10); //paamty3
    $pos[22] = Array(180, 21); //payid3
    $pos[23] = Array(202, 10); //branch
    $line[0] = Array(0, 16); //start the output data for line items ==prodcode
    $line[1] = Array(17, 35); //descript
    $line[2] = Array(53, 6); //units
    $line[3] = Array(60, 10); //price
    $line[4] = Array(71, 10); //qty
    $line[5] = Array(82, 10); //value

    $row = 1;
    $output = "";
    $total = 0;
    $sr = 0;
    $li = 0;


    $num = count($data) - 1;
    //echo "<p> $num fields in line $row: <br /></p>\n";
    $row++;
    if ($num > 21) {
        $diff = $pos;
        $ct = $num;
    } else {
        $diff = $line;
        $ct = $num;
    }
    for ($c = 0; $c < $ct; $c++) {

        if (!isset($data[$c])) {
            $data[$c] = " ";
        } //catch the end of the line
        $mod = "";
        $pre = "";

        if ($c == 23) {
            $data[$c] = "AK";
        }
        if ($c == 18 && floatval($data[$c]) == 0) {
            $data[$c] = "0.00";
        }
        if ($c == 21 && floatval($data[$c]) == 0) {
            $data[$c] = "0.00";
        }
        if ($c == 13) {
            $pre = "00";
        }
        $data[$c] = $pre . $data[$c];
        $data[$c] = $data[$c] . $mod;
        $len = strlen($data[$c]);
        if (isset($diff[$c][1])) {
            $cell = $diff[$c][1];
        }
        if ($len > $cell) {
            $data[$c] = substr($data[$c], 0, $cell);
        }
        $pad = $cell - $len;
        $padstr = "";

        if ($pad > 0) {
            for ($i = 0; $i < $pad; $i++) {
                $padstr.=" ";
            }
        }

        $output.= $data[$c] . $padstr;

        $output.= ",";
        if ($c == $ct - 1) {
            $output.= "\r\n";
        }
    }


    return $output;
}

function connect(){
    global $db;
                    $message ="<h2>File Exporter</h2><br>";
                    $message.="Trying the export system across the network...";
                   
                    $exportuser = $db->QPResults("SELECT value FROM system WHERE name ='export_username'")['value'];
                    $exportpass = $db->QPResults("SELECT value FROM system WHERE name ='export_password'")['value'];
                    $exporthost = $db->QPResults("SELECT value FROM system WHERE name ='export_hostpath'")['value'];
                    $exportdir = $db->QPResults("SELECT value FROM system WHERE name ='export_hostdir'")['value'];
                    $exportfile = $db->QPResults("SELECT value FROM system WHERE name ='export_filename'")['value'];
                    
                    //rather than SAMBA, which proves to be a PITA with so many implentation issues under PHP+OSes in general, I have opted for SFTP - fast+secure http://phpseclib.sourceforge.net/

                    $sftp = new Net_SFTP($exporthost);
                    if (!$sftp->login($exportuser, $exportpass)) {
                        $message.="SFTP login failed for the user credentials supplied.";
                        $message.="Host =".$exporthost."<br>";
                        $message.="User = " . $exportuser . "<br>";
                        $message.="Password hidden. $exportpass<br>";
                        
                    } else {
                        //now do the work
                        
                        if ($sftp->put($exportdir."/".$exportfile, "export/".$exportfile, NET_SFTP_LOCAL_FILE)) {
                            $message.= "export file saved successfully, you may close this tab: CTRL+W";
                        } else{
                            $message.="failed to save file.<br>";
                            $message.="Host = " . $exporthost . "<br>";
                            $message.="Dir = " . $exportdir . "<br>";
                            $message.="File = " . $exportfile . "<br>";
                        }
                    }
                    
                    return $message;
                   
}
?>
