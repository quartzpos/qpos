<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com

  Released under the GNU General Public License
 */

?>

<div id="version">
    <div id="username" style="float:left;margin-top: 2px;font-size:10px;">
        <?php if (isset($_SESSION['username'])) {
            ?>
            <a href='?action=logout' title='Log out'><img src='<?php echo $themepath; ?>images/keycard.png' title='<?php echo TXT_LOGOUT ?>:<?php echo $_SESSION['username']; ?>' /></a>
            <?php echo TXT_USER; ?>: <?php echo $_SESSION['username']; ?>
            . <?php echo TXT_TILL; ?>:<?php echo (isset($_SESSION['till'])) ? $_SESSION['till'] : '';
    } ?>
        <input type="hidden" name="permsystem" id="permsystem" value="<?php echo (isset($_SESSION['system'])) ? $_SESSION['system'] : ''; ?>"/>
        <input type="hidden" name="permsales" id="permsales" value="<?php echo (isset($_SESSION['sales'])) ? $_SESSION['sales'] : ''; ?>"/>
        <input type="hidden" name="permreports" id="permreports" value="<?php echo (isset($_SESSION['reports'])) ? $_SESSION['reports'] : ''; ?>"/>
        <input type="hidden" name="permstock" id="permstock" value="<?php echo (isset($_SESSION['stock'])) ? $_SESSION['stock'] : ''; ?>"/>
        <input type="hidden" value="<?php echo (isset($_SESSION['till'])) ? $_SESSION['till'] : ''; ?>" id="tillno"/>
    </div>
    <a href="http://www.Quartzpos.com/" target="_blank">
        <div id="versiondetails">QuartzPOS <?php echo QUARTZPOS_VERSION.' '.VERSION; ?>.</a>
</div>  
