<?php

require_once('database.php');
if(isset($_POST)){
     $_POST=clean_post($_POST); //sanitise input
}
if(isset($_GET)){
     $_GET=clean_post($_GET); //sanitise input
}
//set_error_handler('QP_error_handler');

function get_system_variable($varname, $db) {// return a value from the system table based upon the name of the varaiable
    $sql = "SELECT value FROM system  WHERE name='$varname'";
    $result = $db->QPResults($sql);

    if ($result) {
        return $result['value'];
    } else {
        return false;
    }
}
function clean_post($vars){
    global $db;
    $retarr=array();
    if(is_array($vars)){
	foreach ($vars as $k=>$v){
	    $retarr[$k]=$db->clean($v);
	}
    }
    return $retarr;
}
function warn($html) {
    $div = "<div id='warn' class='warn'>";
    $div .= $html;
    $div .="</div>";
    return $div;
}

function listpls($db) { //spit out the price lists to pos.php so that javascript can use them
    $result = $db->QPComplete("select * from pricelevel ");
    $count = count($result);
    $html = "<input type='hidden' name='plcount' id='plcount' value='$count' />";
    foreach ($result as $k => $v) {
        $html.="<input type='hidden' name='pluid" . $v['uid'] . "' id='pluid" . $v['uid'] . "' value='" . $v['uid'] . "' />";
        $html.="<input type='hidden' name='plname" . $v['uid'] . "' id='plname" . $v['uid'] . "' value='" . $v['name'] . "' />";
        $html.="<input type='hidden' name='pltaxinc" . $v['uid'] . "' id='pltaxinc" . $v['uid'] . "' value='" . $v['taxinc'] . "' />";
        $html.="<input type='hidden' name='plmarkup" . $v['uid'] . "' id='plmarkup" . $v['uid'] . "' value='" . $v['markup'] . "' />";
        $html.="<input type='hidden' name='plbase" . $v['uid'] . "' id='plbase" . $v['uid'] . "' value='" . $v['base'] . "' />";
    }
    return $html;
}

function get_userdata($id, $db) {//return the user data given the user's id number
    $sql = "SELECT * FROM users WHERE id='" . $id . "'";
    $res = $db->query($sql);
    $userdata = $db->fetchAssoc($res);

    return $userdata;
}

function PDFMailer($to, $from, $subject, $pdffile) {

    if (!file_exists($pdffile)) {
        return PDF_MISSING;
    }


    $att = file_get_contents($pdffile);
    $att = base64_encode($att);
    $att = chunk_split($att);

    $BOUNDARY = "anystring";

    $headers = <<<END
From: Your Name <$from>
Content-Type: multipart/mixed; boundary=$BOUNDARY
END;

    $body = <<<END
--$BOUNDARY
Content-Type: text/plain

See attached file!

--$BOUNDARY
Content-Type: application/pdf
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename="your-file.pdf"

$att
--$BOUNDARY--
END;

    mail($to, $subject, $body, $headers);
}

function QP_error_handler($number, $message, $file, $line, $vars) {
    $email = "
		<p>An error ($number) occurred on line
		<strong>$line</strong> and in the <strong>file: $file.</strong>
		<p> $message </p>";

    $email .= "<pre>" . print_r($vars, 1) . "</pre>";

    $headers = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $e1 = "errorreport";
    $e2 = "quartzpos.com";
    $emailaddress = $e1 . "@" . $e2;
    // Email the error to someone...
    error_log($email, 1, $emailaddress, $headers);

    // Make sure that you decide how to respond to errors (on the user's side)
    // Either echo an error message, or kill the entire project. Up to you...
    // The code below ensures that we only "die" if the error was more than
    // just a NOTICE.
    if (($number !== E_NOTICE) && ($number < 2048)) {
        die("There was an error. Please try again later.");
    }
}

function QP_stats() {
    $errno = 0;
    $errstr = '';
    $project = md5($_SERVER["SCRIPT_FILENAME"]);
    $fp = fsockopen("www.quartzpos.com", 80, $errno, $errstr, 5);
    if ($fp) {
        $out = "GET /qp_usage_tracker.php?p=" . urlencode($project) . " HTTP/1.1\r\n";
        $out .= "Host: www.quartzpos.com\r\n";
        $out .= "Connection: Close\r\n\r\n";
        fwrite($fp, $out);
        fclose($fp);
    }
}

function get_tz($db){
// get and set the default timezone
$timezone = $db->QPResults("SELECT value FROM system WHERE name='timezone'");
$timezone = $timezone['value'];
date_default_timezone_set($timezone);
return $timezone;
}



	if(!function_exists('money_formatter')){
		function money_formatter($number)
		{
	    $format = round( (float) $number,2);
	    return $format;
		}
	}
function getSystemvalue($sysval){
    global $db;
      $sql ="select * from system  where name = '".$sysval."'";
            $result = $db->QPResults($sql);
            if($result){
            return $result['value'];
            }
}
function getLocations($item_id){
    global $db;
    if(is_numeric($item_id)){
    $sql=" SELECT * FROM items_locations WHERE items_id=".$item_id;
    $res = $db->QPComplete($sql);
    $output='';
    foreach($res as $k=>$v){
        $output .= '<tr><td>'.$v['location'].'</td><td>'.$v['qty'].'</td><td><textarea disabled>'.$v['notes'].'</textarea></td></tr>';
    }
    return $output;
    }
}
function getBarcodes($item_number){
    global $db;
    $sql="SELECT * FROM barcodes WHERE prodcode='".$db->clean($item_number)."'";

    if($res = $db->QPComplete($sql)){

    return $res;
    } else {
        return false;
    }
}
?>
