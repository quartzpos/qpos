<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com

  

  Released under the GNU General Public License
 */

header('Content-Type:text/xml; charset="utf8"');
if (!isset($_SESSION))
    session_start();

include("config.php");

require_once("database.php");

$result = $db->query("select id, first_name, last_name, pricelevel from customers");
echo '<?xml version="1.0" encoding="utf8" ?>';
?>
<customers>
    <?php
    while ($row = $db->fetchRow($result)) {
        ?>
        <customer>
            <id><?php echo $row[0]; ?></id>
            <name><?php echo htmlspecialchars($row[1] . " " . $row[2]); ?></name>
            <plevel><?php echo $row[3]; ?></plevel>
        </customer>
        <?php
    }
    $db->freeResult($result);
    $db->close();
    ?>
</customers>

