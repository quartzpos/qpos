<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */
if (isset($_GET['category'])) {
    header('Content-Type:text/xml; charset="iso-8859-1"');
}
session_start();





if (!isset($_SESSION['user'])) {
//header("Location:admin.php");
}
include("config.php");
require_once 'consoleLogging.php';
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_GET);
require_once("database.php");
$path='';
    if(isset($pathex))$path=$pathex;
    $res = $db->QPResults("SELECT value FROM system WHERE name='theme'");
    $themepath = $path.'qpthemes/'.$res['value'].'/';
if (isset($_GET['category'])) {
    $result = $db->QPComplete("select item_number, item_name, image, trade_price, unit_price, tax_percent, total_cost, description from items where category_id=" . $_GET['category']);

    echo '<?xml version="1.0" encoding="iso-8859-1" ?>';



    ?>
    <items>
        <?php
        foreach ($result as $key=>$row) {
            if(empty($row['image'])){
                $row['image']=$themepath."images/products.png";
            }
            ?>
            <item>
                <id><?php echo $row['item_number']; ?></id>
                <name><?php echo htmlspecialchars($row['item_name']); ?></name>
                <image><?php echo $row['image']; ?></image>
                <tradeprice><?php echo $row['trade_price']; ?></tradeprice>
                <unitprice><?php echo $row['unit_price']; ?></unitprice>
                <tax><?php echo htmlspecialchars($row['tax_percent']); ?></tax>
                <price><?php echo htmlspecialchars($row['total_cost']); ?></price>
                <description><?php echo htmlspecialchars($row['description']); ?></description>
            </item>
            <?php
        }
        //$db->freeResult($result);

        ?>
    </items>
    <?php
}
if (isset($_GET['item'])) {
    $_POST['item'] = $_GET['item'];
    if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST['item'], "getItems.php item=");
}
$sql = "SELECT value FROM system WHERE name='laybuy_item'";
$result = $db->QPResults($sql);
$laybuyitem = $result['value'];
if (isset($_POST['item']) && $_POST['item'] == $laybuyitem) {
    $sql = "SELECT value FROM system WHERE name='GST_rate'";
    $result = $db->QPResults($sql);
    $gst = $result['value'];
    $array = Array($laybuyitem, 'Laybuy payment', 'images/laybuy.png', '0.00', '0.00', "$gst", '0.00', 'Laybuy payment');

    $ret = json_encode($array);
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($ret, "getItems.php laybuyitem array=");
    echo $ret;
}
if (isset($_POST['item']) && $_POST['item'] != $laybuyitem) {
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log('item=' . $_POST['item']);
    $sql = "select * from items where item_number='" . $_GET['item'] . "'";
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log('sql=' . $sql);
    $result = $db->query($sql);

    while ($row = $db->fetchAssoc($result)) {
            // now check the matrix for pricing changes
            if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($row,"ROW=");
        $price = $row['total_cost'];
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($price,"Price=");
        $supplier=$row['supplier_id'];
        $category=$row['category_id'];
        $brand=$row['brand_id'];
            $sql="SELECT code FROM suppliers WHERE id=$supplier";
            $suppname=$db->QPResults($sql);
            if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($suppname,"Supplier=");
            $sql="SELECT code FROM brands WHERE id=$brand";
            $brandname=$db->QPResults($sql);
            if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($brandname,"Brand=");
            $sql="SELECT code FROM categories WHERE id=$category";
            $categoryname=$db->QPResults($sql);
            if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($categoryname,"Category=");
             $sql="SELECT * FROM hkmatrix WHERE prodcode='".$row['item_number']."' AND date_on <= CURDATE() AND date_off >= CURDATE()";
            if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql);
            $matrix=$db->QPResults($sql);
            if(!isset($matrix['matrix'])){
                //if we have no prodcode result, then check for supplier/category
                if(isset($categoryname)){
            $sql="SELECT * FROM hkmatrix WHERE prodgroup='".$categoryname['code']."' AND date_on <= CURDATE() AND date_off >= CURDATE()";
            if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql);
            $pmatrix=$db->QPResults($sql);
            if (!empty($pmatrix['supplier'])){
                //supplier is also true, only set if they are the same for the item
                if($pmatrix['supplier'] ===$suppname['code']){
                    $matrix=$pmatrix;
                }
            } elseif(isset ($pmatrix['matrix'] )){
                // the product category is true
                $matrix=$pmatrix;
            }
			}
            //
            if(isset($suppname)){
            $sql="SELECT * FROM hkmatrix WHERE supplier='".$suppname['code']."' AND date_on <= CURDATE() AND date_off >= CURDATE()";
            if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql);
            $smatrix=$db->QPResults($sql);
            if (!empty($smatrix['prodgroup'])){
                //supplier is also true, only set if they are the same for the item
                if($smatrix['prodgroup'] ===$categoryname['code']){
                    $matrix=$smatrix;
                }
            } elseif(isset ($smatrix['matrix'] )){
                // the product category is true
                $matrix=$smatrix;
            }
			}
            //
            }

            if(isset($matrix['matrix'])){
                 if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($matrix['matrix']," matrix discount to apply");
                //there is a matrix entry to check
                if($matrix['matrix']=="D"){
                    //supplier or category based special



                    $price=(100-$matrix['retail'])/100*$price;
                    if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($price,"Supplier or category discount=");

            }
                if($matrix['matrix']=="C"){
                    //prodcode special
                    $price=$matrix['retail'];
                    if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($price,"prodcode discount=");
                }

                 //now make sure the normally lower price levels are also brought down
                    if($row['trade_price'] > $price){
                        $row['trade_price'] = $price;
                    }
                    if($row['unit_price'] > $price){
                        $row['unit_price'] = $price;
                    }

            }
        $array = Array($row['item_number'], $row['item_name'], $row['image'], $row['trade_price'], $row['unit_price'], $row['tax_percent'], $price, $row['description']);
    }
    $db->freeResult($result);
    $db->close();

   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($array,"getitems.php item array=");
    if (!empty($array)) {
        echo json_encode($array);
    } else {
        echo json_encode("Error retrieving data.");
    }
}
if(isset($_GET['clearitem']) && !empty($_GET['clearitem'])){ // toggle clearance status
	$_GET['clearitem'] = $db->clean($_GET['clearitem']);
	$sql = 'UPDATE items SET clearance = !clearance WHERE item_number ="'.$_GET['clearitem'].'"';

	if ($db->query($sql)){
	echo json_encode("Success toggling.");
	}
}
?>
