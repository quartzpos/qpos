<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */

header('Content-Type:text/xml; charset="utf8"');
if (!isset($_SESSION))
    session_start();
// if (!isset($_SESSION['user'])) {
//     header("Location:admin.php");
// }
include("config.php");

require_once("database.php");
if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE){
    require 'consoleLogging.php';
}
$uid = '';
if (isset($_GET['pricelevel'])) {
    $uid = "where uid = " . $db->clean($_GET['pricelevel']);
    //only use 1 return pricelevel if the get var not avialable
}
if (isset($_GET['item'])) {
    $item = $db->clean($_GET['item']);
}
$result = $db->QPComplete("select * from pricelevel " . $uid);

$sql = "SELECT * FROM items WHERE item_number = '" . $item . "'";
$iresult = $db->QPResults($sql);
if ($iresult !== NULL) {

    echo'<pricelevels>';
    foreach ($result as $k => $v) {
        $price = $iresult[$v['base']];
        if (is_numeric($v['markup']) && !empty($v['markup'])) {
            $price = (1 + ($v['markup'] / 100)) * $iresult[$v['base']]; //
        }
         if($v['taxinc']==='0'){ // tax is not included so we add it now
              $price = $price*(1+($iresult['tax_percent']/100));
         }
        $price = round($price, 2); // round to 2dp
        ?>
        <pricelevel>
            <uid><?php echo $v['uid']; ?></uid>
            <name><?php echo htmlspecialchars($v['name']); ?></name>
            <taxinc><?php echo $v['taxinc']; ?></taxinc>
            <markup><?php echo $v['markup']; ?></markup>
            <base><?php echo $v['base']; ?></base>
            <price><?php echo $price; ?></price>
            <security_level><?php echo $v['security_level']; ?></security_level>
        </pricelevel>
        <?php
    }
    echo'</pricelevels>';
}
