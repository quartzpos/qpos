<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com

  

  Released under the GNU General Public License
 */

session_start();
require 'consoleLogging.php';

if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_GET);


if (!isset($_SESSION['user'])) {
//header("Location:admin.php");
}
include("config.php");

require_once("database.php");


    $result = $db->query("select * from users ");
    echo '<?xml version="1.0" encoding="utf8" ?>';
    ?>
    <users>
        <?php
        while ($row = $db->fetchRow($result)) {
            ?>
            <user>
                <id><?php echo $row[0]; ?></id>
                <name><?php echo htmlspecialchars($row[1]).' '.htmlspecialchars($row[2]);?></name>
                <username><?php echo $row[3]; ?></username>
                <password><?php echo $row[4]; ?></password>
               
                <type><?php echo $row[5]; ?></type>
             
            </user>
            <?php
        }
        $db->freeResult($result);
        $db->close();
        ?>
    </users>
    
