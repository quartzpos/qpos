<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */
 $datecur = date("F d, Y H:i:s", time());
 $adclass='';
 $loc = $_SERVER['REQUEST_URI'];
 $admin=stripos($loc,'admin');
 if(isset($_SESSION['id'])){$adclass= 'admin_header';}
$extracss = '<style>@media print{
                .noprint, .noprint *,#top{display:none !important;}

            }</style>';
?>
<script type="text/javascript">

<?php if (defined('DEBUG_CONSOLE') && !DEBUG_CONSOLE){
?>console.log("If you need debug messages from QuartzPOS then set DEBUG_CONSOLE to TRUE in the config.php file. It is currently FALSE.");
<?php } ?>
var currenttime = '<?php echo $datecur;?>';
var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
var serverdate=new Date(currenttime);
<?php if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE){
?>console.log("time="+currenttime);
<?php } ?>
</script>
<div id="headercontainer" class=".noprint"><div id="menucontainer"><?php if(isset($location) && $location !="admin.php") require_once('menu.inc'); ?></div>
<table id="header"  cellspacing="0" width="100%">
    <TR><TD><img src="images/quartzposlogo.png" title="Quartz Point of Sale System" height="60px" /></TD><TD align="right" valign="bottom" class="sessid">

    <div id="headerdetail" >
    <?php if(isset( $_SESSION['id']) && !$admin && !strripos($loc, 'settill.php')){ echo '<div id="transactionno" style="float:left">Transaction #<input type="text" name="sessionid" id="sessionid"  value="'.$_SESSION['id'].'" disabled />&nbsp; &nbsp;&nbsp;</div>';}?>
    <div id="clock"
    <?php if($admin) echo 'class="'.$adclass.'"'; ?>
    ></div>
    </div></TD></TR>
</table>
</div>
