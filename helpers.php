<?php

//helper functions

function remove_element($arr, $val) {
    foreach ($arr as $key => $value) {
        if ($arr[$key] == $val) {
            unset($arr[$key]);
        }
    }
    return $arr = array_values($arr);
}

function cleanInput($input) {

    $search = array(
        '@<script[^>]*?>.*?</script>@si', // Strip out javascript
        '@<[\/\!]*?[^<>]*?>@si', // Strip out HTML tags
        '@<style[^>]*?>.*?</style>@siU', // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
    );

    $output = preg_replace($search, '', $input);
    return $output;
}

function sanitize($input) {
    if (is_array($input)) {
        foreach ($input as $var => $val) {
            $output[$var] = sanitize($val);
        }
    } else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input = cleanInput($input);
        $output = mysqli_real_escape_string($input);
    }
    return $output;
}

function db_clean($vars, $db) {
    // returns a db ready array. Use it by sending $_POST for instance, with the $db connection
    $newvars = Array();
    //var_dump($vars);
    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($vars, "vars ");
    foreach ($vars as $key=>$val) {
        $newvars[$key] = $db->clean($vars[$key]);
        //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($vars[$key], "varskey ");

    }
    return $newvars;
}

function image_upload($formvar, $target_dir) {

    $target_file = $target_dir . basename($formvar['name']);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
    if (isset($_POST["submit"])) {
        $check = getimagesize($formvar["tmp_name"]);
        if ($check !== false) {
            //echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
// Check if file already exists
    if (file_exists($target_file)) {
        // echo " File already exists.";
        $uploadOk = 0;
    }
// Check file size
    if ($formvar["size"] > 4096000) {
        echo " File is too large.";
        $uploadOk = 0;
    }
// Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
//        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
//        echo " File was not uploaded.";
        return false;
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($formvar["tmp_name"], $target_file)) {
//            echo " The file " . basename($formvar["name"]) . " has been uploaded.";
            return true;
        } else {
            echo " Error uploading file.";
            return false;
        }
    }
}

function dumparray($var, $title = false, $level = 0) {
    if ($level == 0) {
        echo '<table border="0" cellspacing="1" cellpadding="2" class="dump">';

        if ($title){
            echo '<tr style="background:#111;color:white;padding:5px;">
                     <th colspan="2">' . $title . '</th>
                   </tr>';
        }
        echo '
          <tr style="background:#111;color:white;padding:5px;">
            <th >Variable</th>
            <th >Value</th>
          </tr>';
    }
    else {
        echo '<tr style="background:#111;color:white;padding:5px;">
                <td colspan="2">
                    <table border="0" cellspacing="2" cellpadding="2" class="dump_b">
                </td>
              </tr>';
    }
    $color = "eee";
    foreach ($var as $i => $value) {
        if (is_array($value) or is_object($value)) {
            dumparray($value, $i, ($level + 1));
        } else {
            if ($color === "eee") { //zebra stripe
                $color = "ccc";
            } else {
                $color = "eee";
            }
            echo '<tr style="background:#' . $color . ';color:black;padding:5px;">
                        <td >' . $i . '</th>
                        <td >' . $value . '</th>
                       </tr>';
        }
    }
    echo '</table>';
}

function get_QP_timezones() { //provide list of timezones
    $zones = timezone_identifiers_list();
    $locations = Array();
    foreach ($zones as $zone) {
        $zone = explode('/', $zone); // 0 => Continent, 1 => City
        // Only use "friendly" continent names
        if ($zone[0] == 'Africa' || $zone[0] == 'America' || $zone[0] == 'Antarctica' || $zone[0] == 'Arctic' || $zone[0] == 'Asia' || $zone[0] == 'Atlantic' || $zone[0] == 'Australia' || $zone[0] == 'Europe' || $zone[0] == 'Indian' || $zone[0] == 'Pacific') {
            if (isset($zone[1]) != '') {
                $locations[$zone[0]][$zone[0] . '/' . $zone[1]] = str_replace('_', ' ', $zone[1]); // Creates array(DateTimeZone => 'Friendly name')
            }
        }
    }
    return $locations;
}
function multiexplode ($delimiters,$string) {

    $ready = str_replace($delimiters, $delimiters[0], $string);
    $launch = explode($delimiters[0], $ready);
    return  $launch;
}

?>
