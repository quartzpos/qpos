 #!/bin/bash
# Data grabber for QuartzPOS
# This script sends the hkstock file to the webserver. DO NOT DELETE!
# usage: ~/htdocs/pos/import/datastore$ ./getdata.sh username password //192.168.0.1/data /home/onyx/htdocs/pos/import/datastore
# the following needs to be active in the /etc/sudoers file so that we can run sudo commands
#
# User privilege specification
#root    ALL=(ALL:ALL) ALL
#username    ALL=(ALL:ALL) ALL
#
# Allow members of group sudo to execute any command
#%sudo   ALL=(ALL:ALL) ALL
#%users    ALL=(ALL) /bin/mount,/bin/umount,/sbin/mount.cifs,/sbin/umount.cifs
#%samba   ALL=(ALL) NOPASSWD: /bin/mount,/bin/umount,/sbin/mount.cifs,/sbin/umount.cifs
#
# sudo groupadd samba
# and add user to group: sudo usermod -a -G samba username
set -x


u=$1
p=$2
pt=$3
dp=$4
# u=username
#p=password
#pt=path of samba files eg //192.168.0.1/data
#dp=absolute datapath for saving datastore eg /home/user/htdocs/datastore


PATH=$PATH:$HOME/bin
export PATH
#flush the mount
sudo umount  $pt
sudo mount -t cifs $pt  -o username=$u,noexec,password=$p $dp/mount

# get the data files, drop them in cwd
cp $dp/mount/hokes/HKMATRIX.DBF ./hkmatrix.dbf

cp $dp/mount/hokes/hkmatrix.NTX .
cp $dp/mount/hokes/hkstock.* .
cp $dp/mount/hokes/HKSTOCK.* .
cp $dp/mount/hokes/REFCODES.DBF .
cp $dp/mount/hokes/REFCODES.DBT .
if [ $? -gt 0 ]; then
    echo "ERROR copying files!"
    else
    echo "Success copying files!"
fi
sudo umount $pt
sudo -K
# sudo -K removes the elevated privileges
