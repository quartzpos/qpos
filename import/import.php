<?php

if (!isset($_SESSION))
    session_start();
include_once("../config.php");
include_once("../languages/" . POS_DEFAULT_LANGUAGE . ".php");

require_once("../database.php");
$db = new database($dbhost, $dbuser, $dbpassword, $dbname);
$title="HKSTOCK importer";
 $pathex="../";

include_once $pathex.'template.php';

$result = $db->query("SELECT * FROM import_hkstock");
$counter=0;
$date=date('Y-m-d');
//$date="2013-05-24";
echo "<body class='admin'><div class='admin_content'>";
echo "<h2>Import for HKSTOCK and Matrix for :$date...</h2><br>";
while ($row = $db->fetchAssoc($result)) {
	$counter++;
//print_r($row);
$sql="SELECT * FROM hkmatrix WHERE prodcode='".$row['prodcode']."' AND date_on <= '$date' AND date_off >='$date'";
//echo $sql.'<br>';
$matrix=$db->query($sql);
if(!empty($matrix)){
$rm=$db->fetchAssoc($matrix);
if(!empty($rm['retail'])&& $rm['retail'] !='0.00'){
echo "<br>Discount will be applied for ".$row['prodcode']." - retail <del>".$row['price']."</del> ".$rm['retail'];
//$row['price']=$rm['retail'];

}
if(!empty($rm['trade'])&& $rm['trade'] !='0.00'){
echo "<br>Discount will be applied for ".$row['prodcode']." - trade<del>".$row['trade']."</del> ".$rm['trade'];
//$row['trade']=$rm['trade'];

}
}

    $arr['item_name'] = $row['descr'];
    $arr['item_number'] = $row['prodcode'];
    $arr['description'] = $row['descr'];
    $arr['trade_price'] = $row['trade'];
    $arr['wholesale'] = $row['wholesale'];
    $arr['unit_price'] = $row['price']/1.15;
    $arr['total_cost'] = $row['price'];
    $arr['quantity'] = $row['stocklevel'];
    $arr['tax_percent'] = '15';
    $code = $row['category'];
    $cresult = $db->query("SELECT hkcategory FROM import_categories WHERE code='" . $code . "'");
    $crow = $db->fetchAssoc($cresult);
    $category = $crow['hkcategory'];
    $brand = substr($row['prodcode'], 0, 3);
    $data = Array();
    $supplier = $row['supplier'];
    if (!empty($category)) {
        $cat = Array('category' => $category, 'code' => $code);
        upins('categories', $cat, '', $db);
        $table = 'categories';
        $data['col'] = 'category';
        $data['val'] = $category;
        $id = getid($table, $data, $db);
        $arr['category_id'] = $id;
    }
//supplier and brand populate the import_hkstock with wildcard code not full text
    if (!empty($brand)) {

        $brandn = getbrcat($brand, $db);

        $brn = Array('brand' => $brandn, 'code' => $brand);
        upins('brands', $brn, '', $db);
        $table = 'brands';
        $data['col'] = 'brand';
        $data['val'] = $brandn;
        $id = getid($table, $data, $db);
        $arr['brand_id'] = $id;
    }
    if (!empty($supplier)) {
        $suppliern = getbrcat($supplier, $db);
        $sup = Array('supplier' => $suppliern, 'code' => $supplier);
        upins('suppliers', $sup, '', $db);
        $table = 'suppliers';
        $data['col'] = 'supplier';
        $data['val'] = $suppliern;
        $id = getid($table, $data, $db);
        $arr['supplier_id'] = $id;
    }
    upins('items', $arr, '', $db);
}

echo "<br><h3>Import completed of $counter items</h3><br><br>";
echo "<input type='button' value='Close' onclick='window.close();'>";
echo "</div></body>";
//CREATE TABLE IF NOT EXISTS `items` (
//  `id` int(8) NOT NULL AUTO_INCREMENT,
//  `item_name` varchar(30) NOT NULL,
//  `item_number` varchar(20) DEFAULT NULL,
//  `description` tinytext,
//  `brand_id` int(8) NOT NULL,
//  `category_id` int(8) NOT NULL,
//  `supplier_id` int(8) NOT NULL,
//  `trade_price` varchar(30) NOT NULL,
//  `unit_price` varchar(30) NOT NULL,
//  `supplier_item_number` varchar(50) DEFAULT NULL,
//  `tax_percent` varchar(10) NOT NULL,
//  `total_cost` varchar(30) NOT NULL,
//  `quantity` int(8) DEFAULT NULL,
//  `reorder_level` int(8) DEFAULT NULL,
//  `image` varchar(80) DEFAULT 'images/items/item.gif',
//import_hkstock
//uid
//prodcode
//descr
//model
//category
//url
//price
//trade
//stocklevel
//updated
//onorder
//webready
//supplier
//brand
function upins($table, $data, $cond = '', $db) {
//INSERT INTO table (a,b,c) VALUES (1,2,3)
//  ON DUPLICATE KEY UPDATE c=c+1;
    $keys = array_keys($data);
    $vals = array_values($data);
    $sql = 'INSERT INTO ' . $table . ' (';
    foreach ($keys as $key => $val) {
        $sql.='`' . $val . '`,';
    }
    $sql = substr($sql, 0, -1); // remove last comma
    $sql .=') VALUES (';
    foreach ($vals as $key => $val) {
        $sql.='\'' . $db->clean($val) . '\',';
    }
    $sql = substr($sql, 0, -1); // remove last comma
    $sql .=') ON DUPLICATE KEY UPDATE ';
    foreach ($keys as $key => $val) {
        $sql.= $val . '= VALUES(' . $val . '),';
    }
    $sql = substr($sql, 0, -1); // remove last comma
    $sql.=$cond;

    if (!$db->query($sql))
        print"Error running $sql <br>";
}

function getbrcat($data, $db) {
    $sql = "SELECT * FROM import_categories WHERE code='$data' ";
    $ret = '';
    $result = $db->query($sql);
    while ($row = $db->fetchAssoc($result)) {
        if (!empty($row['hkcategory']))
            $ret = $row['hkcategory'];
        if (!empty($row['hksupplier']))
            $ret = $row['hksupplier'];
    }
    if ($ret)
        return $ret;
}

function getid($table, $data, $db) {
    $sql = "SELECT id FROM $table WHERE " . $data['col'] . "='" . $db->clean($data['val']) . "'";
    $result = $db->query($sql);
    while ($row = $db->fetchAssoc($result)) {
        $ret = $row['id'];
    }
    if (isset($ret))
        return $ret;
}

?>
