<?php
//used to import special spreadsheet format, in csv:
 //"id";"item_name";"item_number";"prodcode";"description";"brand_id";"category_id";"supplier_id";"trade_price";"wholesale";"unit_price";"supplier_item_number";"tax_percent";"total_cost";"quantity";"reorder_level";"image";"Date";"Sum total"

if (!isset($_SESSION))
    session_start();
include_once("../config.php");
//include_once("../languages/" . POS_DEFAULT_LANGUAGE . ".php");

require_once("../database.php");
$db = new database($dbhost, $dbuser, $dbpassword, $dbname);
$title="CSV importer";
 $pathex="../";

include_once $pathex.'template.php';

$handle = fopen("./datastore/data.csv", "r");
if($handle){
 $db->query("TRUNCATE TABLE suppliers");
  $db->query("TRUNCATE TABLE items");
   $db->query("TRUNCATE TABLE brands");
    $db->query("TRUNCATE TABLE categories");
    $db->query("TRUNCATE TABLE items_aging");
    $db->query("TRUNCATE TABLE barcodes");
    $db->query("INSERT INTO `barcodes` (`barcode`, `prodcode`, `uid`) VALUES
('5034934431215', 'Thankyou733', 1),
('9337694011908', 'przzy1', 2),
('9421009560257', 'tgc1', 3),
('9337694026841', 'msxb25', 4),
('9337694026858', 'msxb40', 11),
('9337694026865', 'msxb70', 10),
('9421009567027', 'GGGift', 12),
('9421009569885', 'm10gc', 13),
('9337694053205', 'msxbg12mth', 14),
('9337694053199', 'msxbg3mth', 15),
('9421009567010', 'twgc', 16),
('9421009561209', 'repcogc', 17),
('9421009560905', 'restgc', 18),
('9421009566068', 'huntgc', 19),
('9337694004498', 'playgc50', 20),
('5450537004326', 'appleappgc', 21),
('5450537004333', 'applegc30', 22),
('5450537004357', 'applegc100', 23),
('5450537004340', 'applegc50', 24),
('9421001153037', 'TalkPG', 25),
('9421001153020', 'TalkTalk', 26)
");

$row=1; $ignore_header=TRUE;
$date=date('Y-m-d');
//$date="2013-05-24";
echo "<body class='admin'><div class='admin_content'>";
echo "<h2>Import for CSV data for :$date...</h2><br>";
echo "<h3>First line always ignored; for headers only!</h3>";
while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
        $num = count($data);
        if(!$ignore_header){ //ignore first row
        echo "<p> $num fields in line $row: <br /></p>\n";
//"id";"item_name";"item_number";"prodcode";"description";"brand_id";"category_id";"supplier_id";"trade_price";"wholesale";"unit_price";"supplier_item_number";"tax_percent";"total_cost";"quantity";"reorder_level";"image";"Date";"Sum total"
        for ($c=0; $c < $num; $c++) {
            echo $data[$c] . "&nbsp;";
        }
        echo "\n";

        // do db logic to handle brand, cat, supp; as we need to translate from name to id
        if(!empty($data[5])){ $brandid=$data[5]; } else {$brandid="NoBrand";}
        if(!empty($data[6])){ $catid=$data[6]; } else {$catid="NoCategory";}
        if(!empty($data[7])){ $suppid=$data[7]; } else {$suppid="NoSupplier";}
        if(!is_numeric($brandid)){
        $sql="SELECT * FROM brands WHERE brand = '".$brandid."'";
        $result=$db->QPComplete($sql);

        if(isset($result[0]['id'])){
			$bid=$result[0]['id'];
			} else{
				//insert this brand, get new id
				$sql="INSERT INTO `brands` (`id`, `brand`, `code`) VALUES (NULL, '".$db->clean($brandid)."', NULL);";
				if($db->query($sql)){
				$bid=$db->insertId($db->getConnection());

					} else {
						$bid=0;
					}
				}
		}
		if(!is_numeric($catid)){
        $sql="SELECT * FROM categories WHERE category = '".$catid."'";
        $result=$db->QPComplete($sql);
        if(isset($result[0]['id'])){
			$cid=$result[0]['id'];
			} else{
				//insert this cat, get new id
				$sql="INSERT INTO `categories` (`id`, `category`, `code`) VALUES (NULL, '".$db->clean($catid)."', NULL);";
				if($db->query($sql)){
				$cid=$db->insertId($db->getConnection());

					} else {
						$cid=0;
					}
				}
		}
		if(!is_numeric($suppid)){
        $sql="SELECT * FROM suppliers WHERE supplier = '".$suppid."'";
        $result=$db->QPComplete($sql);
        if(isset($result[0]['id'])){
			$sid=$result[0]['id'];
			} else{
				//insert this supp, get new id
				$sql="INSERT INTO `suppliers` (`id`, `supplier`, `code`) VALUES (NULL, '".$db->clean($suppid)."', NULL);";
				if($db->query($sql)){
				$sid=$db->insertId($db->getConnection());

					} else {
						$sid=0;
					}
				}
		}
		//insert the item
		//0		1						2						3					4					5					6						7					8					9					10					11								12						13   	14				15					16		17 18
		//"id";"item_name";"item_number";"prodcode";"description";"brand_id";"category_id";"supplier_id";"trade_price";"wholesale";"unit_price";"supplier_item_number";"tax_percent";"total_cost";"quantity";"reorder_level";"image";"Date";"Sum total"; barcode
  //"0id";"1item_name";"2item_number";"3prodcode";"4description";"5brand_id";"6category_id";"7supplier_id";"8trade_price";"9wholesale";"10unit_price";"11supplier_item_number";"12tax_percent";"13total_cost";"14quantity";"15reorder_level";"16image";"17Date";"18Sum total";"19barcode"
		$sql="SELECT * FROM items where item_number = '".$db->clean($data[3])."'";
		$result=$db->QPComplete($sql);
		 if(isset($result[0]['id'])){
			$iid=$result[0]['id'];
			} else{
				//insert this item, get new id

    if(empty($data[8])){$data[8]=round((float)$data[13]*.75,2);}
     if(empty($data[9])){$data[9]= round((float)$data[13]*.5,2);}
     if(empty($data[10])){$data[10]= round((float)$data[13]*.4,2);}
     if(empty($data[15])){$data[15]='0';}
				$sql="INSERT INTO `items` (`id`, `item_name`, `item_number`, `description`, `clearance`, `brand_id`, `category_id`, `supplier_id`, `trade_price`, `wholesale`, `unit_price`, `supplier_item_number`, `tax_percent`, `total_cost`, `quantity`, `reorder_level`) VALUES (NULL, '".$db->clean($data[1])."', '".$db->clean($data[3])."', '".$db->clean($data[4])."', '0', '$bid', '$cid', '$sid', ".$db->clean($data[8]).", ".$db->clean($data[9]).", ".$db->clean($data[10]).", '".$db->clean($data[11])."',  ".$db->clean($data[12]).",  ".$db->clean($data[13]).", ".$db->clean($data[14]).",  ".$db->clean($data[15]).")";
				if($db->query($sql)){
				$iid=$db->insertId($db->getConnection());

					} else {
						$iid=0;
      echo "Failure to insert with $sql <br/>";
					}
				}
		if(!empty($data[17])){
			if($iid>0){// add to the aging
    if(is_numeric($data[17])){
     $data[17]=$data[17]."-01-01";
				$sql="INSERT INTO items_aging (`id`,`items_id`,`date`,`quantity`) VALUES (NULL,$iid,'".$db->clean($data[17])."',".$data[14].")";
				if(!$db->query($sql)){ echo "Failure to add to aging for item $iid <br/>"; }
    }
			}

		}
  if(!empty($data[19])){
			if($iid>0){// add to the barcodes
    if(is_numeric($data[19])){

				$sql="INSERT INTO barcodes (`prodcode`,`barcode`) VALUES ('".$db->clean($data[3])."','".$data[19]."')";
				if(!$db->query($sql)){ echo "Failure to add to barcode for item $iid <br/>"; }
    }
			}

		}
	}
           $row++;
           $ignore_header=FALSE;
    }
    fclose($handle);
} else{ //cannot get csv to open
	echo"Error retrieving the data file. Note that it should be in the import/datastore folder, and named 'data.csv'.";
}


echo "<br><h3>Import completed of $row items</h3><br><br>";

echo "</div></body>";
?>
