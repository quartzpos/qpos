<?php

//
//
//		** HKSTOCK to Web PHP script by Chalcedony.co.nz **
// 		Version 1.1Beta Copyright (c) 2010 Chalcedony
//	For use with HKSTOCK DBF file - to convert it to web ready MySQL
// Requirements: Ultimate MYSQL class, update.php, dbf_class.php, HKSTOCK.dBF (should be updated often using sftp or wget or similar)
// handles CLI running and submit form from CLI without interaction
// PHP commandline:
// /usr/local/bin/php /home/worldog9/www/hkstock/read.php cli /home/worldog9/www/hkstock/
//
//
// used to connect to the drupal remote bootstrapper
//$shell = '/usr/local/bin/php'; //for webserver
$shell = '/usr/bin/php'; //for local


$securitykey = "c39286326e93c0f83d7e83b389f44d4b";
$cli = "0";
$counter = 0;
$updatemsg = '';
if (isset($argv) && $argv[1] == "cli")
    $cli = "1";
//include_once('../remote.php');

if ($cli == "0") {
   $title="HKSTOCK importer";
 $pathex="../";
 $extracss = '<link rel="stylesheet" type="text/css" href="read.css">';
include_once $pathex.'template.php';
}

include_once('mysql.class.php');
include_once('dbf_class.php');


$db = new MySQL(true) or die('Cannot connect to MySQL server. Please check settings in mysql.class.php');
// Begin a new transaction, but this time, let's check for errors.
if (!$db->TransactionBegin()) {
    $db->Kill();
    die('Cannot start DB transactions...quitting now.');
}
// We'll create a flag to check for any errors
$success = true;
global $hkdata;
//sql building: BE CAREFUL NOT TO USE semicolon inside the sql!!!!

$sql = "CREATE TABLE IF NOT EXISTS `import_tempstock` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `prodcode` varchar(16) NOT NULL,
    `descr` varchar(254) NOT NULL,`model` varchar(254) NOT NULL,
    `category` varchar(254) NULL,
  `url` varchar(254) NULL,
  `price` decimal(10,2) NOT NULL,
  `trade` decimal(10,2) NOT NULL,
  `wholesale` decimal(10,2) NOT NULL,
  `stocklevel` decimal(10,0) NOT NULL,
  `updated` date DEFAULT NULL,
  `onorder` int(5) NOT NULL DEFAULT '0',
  `webready` tinyint(1) NOT NULL DEFAULT '-1',
  `supplier` varchar(254) NULL,
  `brand` varchar(254) NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `prodcode` (`prodcode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
$results = $db->Query($sql); //remove then build table
if (!$results)
    die('Error contacting database!' . $sql);
$db->TruncateTable("import_tempstock"); //destroy table data

$sql = "CREATE TABLE IF NOT EXISTS `import_categories` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) NOT NULL,
  `hkcategory` varchar(50) NULL,
  `wwwcategory` varchar(254) NULL,
  `hksupplier` varchar(50) NULL,
  `wwwsupplier` varchar(254) NULL,
  PRIMARY KEY (`uid`),
  KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ";
$results = $db->Query($sql);
if (!$results)
    die('Error contacting database!' . $sql);
// this table is not to be emptied, as it contains a chart of SUBSTITUTE names of categories or suppliers
//
//
$now = time();
$results = $db->Query("SHOW TABLE STATUS WHERE name =  'import_categories'"); // read the timestamp to the table to tell us when it was last updated
$timeres = $db->RowArray();
//print_r($timeres);
$dbtime = $timeres['Comment'];

if ($now > ($dbtime + 3600)) { //last updated an hour ago
// process the categories first!
    $dbr = new dbf_class('datastore/REFCODES.DBF');
    $num_rec = $dbr->dbf_num_rec;

    $field_num = $dbr->dbf_num_field;

    for ($i = 0; $i < $num_rec; $i++) {

        unset($code); //empty our temp array for each iteration
        $row = $dbr->getRow($i);

        switch ($row[0]) {
            case 'Q': //category
                $code = $row[1];
                $hkcategory = $row[2];
                $hksupplier = '';
                break;
            case 's': //supplier
                $code = $row[1];
                $hkcategory = '';
                $hksupplier = $row[2];
                break;
            default:
                break;
        }//end switch
        //run conditional sql
        if (isset($code)) {
            //escape the sql inserts for safety
            $hkcategory = MySQL::SQLValue($hkcategory, MySQL::SQLVALUE_TEXT);
            $code = MySQL::SQLValue($code, MySQL::SQLVALUE_TEXT);
            $hksupplier = MySQL::SQLValue($hksupplier, MySQL::SQLVALUE_TEXT);
            //check existence
            $results = $db->Query("SELECT * FROM import_categories WHERE code=$code ");
            $results2 = $db->RowArray(); //gets actual results rather than resource id
            //print_r($results2);
            if (!$results2) {//no entry yet in the categories table... make it!
                $sql = "INSERT INTO import_categories (code, hkcategory, hksupplier) VALUES ($code,$hkcategory,$hksupplier)";
                $newresults = $db->Query($sql);
                if ($newresults) {
                    $updatemsg .= " Added categories successfully.";
                } else {
                    $updatemsg .= " Failed to update categories.$sql !";
                }
            } else { //we have a corresponding entry, lets update it
                $sql = "UPDATE import_categories  SET  hkcategory=$hkcategory, hksupplier=$hksupplier WHERE code=$code";
                $newresults = $db->Query($sql);
                if (!$newresults) {
                    $updatemsg .= " Failed to update categories.$sql !";
                }
            }
        }
    }
    if (!empty($updatemsg)) {
        $now = time();
        $db->Query("ALTER TABLE import_categories COMMENT = '$now'"); // add the timestamp to the table to tell us when it was last updated
    }
}

//
if (isset($_POST['dir'])) {
    $dir = $_POST['dir'];
} elseif (isset($_GET['dir'])) {
    $dir = $_GET['dir'];
} elseif (isset($argv[2])) {
    $dir = $argv[2];
} else {
    $dir = "./";
}
if ($cli == "0") {
    $hdrrow = '<thead>
<tr class="headrow" >
    <th class="col1">#</th>
    <th class="col2">Product code</th>
    <th class="col3">Description</th>
       <th class="col4">Supplier :: Model/Range</th>
    <th class="col5">In stock</th>
    <th class="col6">On order</th>
    <th class="col7">RRP</th>

</tr></thead>'; //<th class="col8">Web&nbsp;</th>
    echo '<div id="toolbar">';
    echo '<span class="left">HKSTOCK Updater from Wizard</span>';
    echo '<table class="tablehead" cellspacing=0 >' . $hdrrow . '</table>';
    echo '</div><div id="content">';
    echo '<div style="display:none;">';
    echo '<h3>HKSTOCK web enabler></h3>';

    echo '<form method="post">';
    echo '<table id="datatable" border="2">';
    echo '<tr><td>Directory</td><td><input type="text" size="50" name="dir" value="' . $dir . '"></td></tr>';
}
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            if(filetype($dir . $file) == 'file' && preg_match("/\.DBF/",strtoupper($file))){
                if ($cli == "0") {
                    echo '<tr><td>' . filesize($dir . $file) . '</td><td>'
                    . '<a href="' . $_SERVER['PHP_SELF'] . '?f=' . $file . '&dir=' . $dir . '">' . $file . '</a></td></tr>';
                }
            }
        }
        closedir($dh);
    }
}
if ($cli == "0") {
    echo '<tr><td>File</td><td><input type="text" size="50" name="f" value=""></td></tr>';
    echo '<tr><td>&nbsp;</td><td><input type="submit" name="doit" value="Show file"></td></tr>';
    echo '</table>';
    echo '</form></div>';
}
if (isset($_GET['f'])) {
    $thefile = $_GET['f'];
} elseif (isset($_POST['f'])) {
    $thefile = $_POST['f'];
} else {
    $thefile = "./datastore/HKSTOCK.DBF";
}
if ($thefile) {
    if (function_exists('dbase_open')) { // only if php was compiled with dbase support
        $updatemsg.="Packing database.<br/>";
        $dbo = dbase_open($thefile, 2);
        // expunge the database
        dbase_pack($dbo);
    }
    //process hkstock.dBF
    $timer = new timerClass();
    $timer->start();
    $dbf = new dbf_class($dir . $thefile);
    $num_rec = $dbf->dbf_num_rec;
    $updatemsg.="DBF records: $num_rec. <br/>";
    $field_num = $dbf->dbf_num_field;
    $endexct = $timer->end();
    if ($cli == "0") {

        echo('<form method="post" action="./update.php">');

        echo('<table id="hkstock" border=0 width="100%" cellspacing=0  class="tablesorter">');

        //echo $hdrrow;
        echo ('<tbody>');
    }
    for ($i = 0; $i < $num_rec; $i++) {
        $stockcount = 0;
        $insert = array();
        $webready = "0";
        $checksupp = FALSE;
        $newsupplier = NULL;
        $newmodel = NULL;
        if ($row = $dbf->getRow($i)) {
            if (!empty($row[0])) { // in case there is nothing in the dbf prodcode field continue looping thru records
                $resultsweb = $db->Query("SELECT * FROM import_webstock WHERE prodcode='" . $row[0] . "' ");
                if ($resultsweb) {
                    $row2 = $db->RowArray(); //print_r($row2);
                } else {
                    $row2 = Array();
                }

                if (isset($row2['webready']) == "1") {

                    $webready = "1";
                }
                if ($row[124] == "Y" || $row[124] == "y") {

                    $webready = "1";
                }


                for ($j = 0; $j < $field_num; $j++) {

                    switch ($j) { //each number here corresponds to the COLUMN in the stock db
                        // following rows are the data we are interested in:
// 124 = WWW = webready
// 123 = URL = url  -- not displayed in table
// 0 = PRODCODE
// 1= Description
// 3=category
// 2=model
//5=wholesale
// 7=onorder
//20 = Updated date
//30 = retail price
//32=trade
//4 = shop1 stock
//13 = shop2
// 112=brand (2-letter)
// 122=supplier
//81= shipped
                        case 0://prodcode
                            if (empty($row[$j])) {
                                print" At ROW: $i there was an ";
                                print'ERROR!!! No PRODCODE found. ';
                                $row[$j] = "ZZZ$i";
                            }

                            $insert["prodcode"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_TEXT);
                            break;
                        case 1://description
                            if (empty($row[$j]))
                                $row[$j] = ""; //no desc
                            $row[$j] = str_replace("'", "", $row[$j]); //clean up for bad data - remove '
                            $insert["descr"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_TEXT);
                            break;
                        case 2://model

                            if (empty($row[$j])) {

                                $checksupp = TRUE; // we are to grab the supplier on our way through
                                //$row[$j] = "HOK";
                            }
                            $row[$j] = str_replace("'", "", $row[$j]); //clean up for bad data - remove '

                            $newsuppliers = explode("-", $row[$j]);

                            if (isset($newsuppliers[0])) {
                                $newsupplier = trim($newsuppliers[0]);
                                $newsupplierex = str_replace('-', '', $newsupplier);
                                $newsupplier = $newsupplierex;
                            }

                            if (isset($newsuppliers[1])) {
                                $newmodel = trim($newsuppliers[1]);
                            }


                            break;
                        case 3://category
                            if (empty($row[$j])) {
                                $row[$j] = "All"; //no category
                            } //else {
//				$code=MySQL::SQLValue($row[$j], MySQL::SQLVALUE_TEXT);
//				$sql="SELECT hkcategory FROM import_categories WHERE code=$code ";
//				$resultsdbc = $db->Query($sql);
//				$resultsdbc = $db->RowArray();
//				$results = $resultsdbc['hkcategory'];
//				$results2dbc = $db->Query("SELECT wwwcategory FROM import_categories WHERE code=$code ");
//				$results2dbc = $db->RowArray();
//				$results2 = $results2dbc['wwwcategory'];
//				if (empty($results2)){ $row[$j]=$results; }else{ $row[$j]=$results2; }//choose the override
                            // }

                            $insert["category"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_TEXT);

                            break;
                        case 123://url
                            if (empty($row[$j]) || ($row[$j]) == NULL)
                                $row[$j] = "contact"; //no url


// TODO: would be good to be able to parse manufacturer and output their url if there is no url already supplied
                            $insert["url"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_TEXT);
                            break;
                        case 7://onorder
                        
                            if (empty($row[$j]) || is_null($row[$j]) )
                                $row[$j] = "0"; //none on order
                            $insert["onorder"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_NUMBER);
                            break;
                        case 81://onorder shipped
                        
                            if (empty($row[$j]) || is_null($row[$j]) )
                                $row[$j] = "0"; //none on order
                            if (!empty($insert["onorder"])) {
                                $insert["onorder"] = intval($insert["onorder"]) + intval(MySQL::SQLValue($row[$j], MySQL::SQLVALUE_NUMBER));
                            } else {
                                $insert["onorder"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_NUMBER);
                            }
                            break;
                        case 20://updated
                            if (empty($row[$j]) && $j == "20")
                                $row[$j] = "19870721"; //no date? give it birthdate
                            $insert["updated"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_DATE);
                            break;
                        case 30://retail
                            if (empty($row[$j]))
                                $row[$j] = "0.00"; //no price?
                            $insert["price"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_NUMBER);
                            break;
                        case 5://wholesale
                            if (empty($row[$j]))
                                $row[$j] = "0.00"; //no price?
                            $insert["wholesale"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_NUMBER);
                            break;
                        case 32://trade
                            if (empty($row[$j]))
                                $row[$j] = "0.00"; //no price?
                            $insert["trade"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_NUMBER);

                            break;



                        case 4://shop1 E
                            //case 13://shop2 N
                            if (empty($row[$j]))
                                $row[$j] = "0"; //no stock?
                            $adjust = htmlentities($row[$j]);
                            $stockcount+=$adjust;
                            //if ($j==13){ //stops printout for shop1 from dbf, but still adds it
                            $insert["stocklevel"] = MySQL::SQLValue($stockcount, MySQL::SQLVALUE_NUMBER);
                            //}
                            break;
                        // case 76: //notes
                        // if (!empty($row[$j]))  { $insert['notes'] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_TEXT);
                        // print 'Notes for '.$insert['prodcode'].':'.$insert['notes'].'<br>';
                        //}
                        //break;
                        case 122://supplier
                            if (empty($row[$j])) {
                                $row[$j] = "HOK"; //no supplier?
                            }

                            //$code=MySQL::SQLValue($row[$j], MySQL::SQLVALUE_TEXT);
                            //$sql="SELECT hksupplier FROM import_categories WHERE code=$code ";
                            //$resultsdbc = $db->Query($sql);
                            //$resultsdbc = $db->RowArray();
                            //$results = $resultsdbc['hksupplier'];
                            //$results2dbc = $db->Query("SELECT wwwsupplier FROM import_categories WHERE code=$code ");
                            //$results2dbc = $db->RowArray();
                            //$results2 = $results2dbc['wwwsupplier'];
                            //if (empty($results2)){ $row[$j]=$results; }else{ $row[$j]=$results2; }//choose the override
                            //if (isset($newsupplier) && !empty($newsupplier) && $newsupplier != $row[$j]) $row[$j] = $newsupplier;
                            //if ($checksupp) $newsupplier = $row[$j];
                            $insert["supplier"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_TEXT);

                            break;
                        case 112://brand
                            if (empty($row[$j]))
                                $row[$j] = "--";

                            $insert["brand"] = MySQL::SQLValue($row[$j], MySQL::SQLVALUE_TEXT);

                            break;
                        //case 124://webready
                        //if (empty($row[$j]) || $row[$j]=="N") $row[$j]="-1"; //not webready?
                        //if($webready=="1") 	$row[$j]="1";
                        //$insert['webready']=MySQL::SQLValue($row[$j], MySQL::SQLVALUE_NUMBER);
                        //if ($row[$j]!="1"){
                        //} else {
                        //$row[$j]="1";
                        //} //close else if webready
                        //break;




                        default:
                            break;
                    } //close switch
                } //close for $j - dbf row loop

                $modelstring = '';
                if (isset($newsupplier)) {
                    $modelstring .= $newsupplier;
                }
                if (isset($newmodel) && !empty($newmodel)) {

                    $modelstring .= '/' . $newmodel;
                    //print"<br> Newmodel isset $newmodel ; Setting modelstring: ".$modelstring;
                } else {
                    $modelstring .='/';
                    //print"<br> Setting modelstring: ".$modelstring;
                }
                $insert['model'] = MySQL::SQLValue(trim($modelstring), MySQL::SQLVALUE_TEXT);
                //$row[2] = $modelstring;
                //print'Modeldata='.$insert['model'].'<br>';
                //var_dump($newsuppliers);
                //print 'newmodel='.$newmodel.'<br>';
                if ($cli == "1") {
                    echo "Updating:" . $row[0] . "-" . $insert['stocklevel'] . "-" . $insert['updated'] . "-" . $insert['price'] . "-" . $insert['trade'] . "\r\n"; //command line output only
                    // commandline is true, and webready is true, build array of data to update
                    $row[2] = $modelstring;
                    $hkdata[] = "INSERT INTO import_webstock (prodcode,descr,model,category,url,updated,onorder,stocklevel,price,trade,wholesale,supplier,brand) VALUES ('$row[0]','$row[1]','$row[2]','$row[3]','$row[123]','$row[20]','$row[7]','$stockcount','$row[30]','$row[32]','$row[5]','$row[122]','$row[112]')";
                    $counter++;
                }// close cli is true

                if ($cli == "0") {
                   // print_r($insert); print '<br/>';
                    if(is_null($insert['onorder'])){$insert['onorder']="0";}
                    $new_id = $db->InsertRow("import_tempstock", $insert);
                    if (!$new_id)
                        die('Unable to write record to DB:' . $db->Error . print_r($insert));
                    unset($insert);
                }
            }
        } //close if able to get dbf info loop
    } // close for $i loop
    $db->Query('DROP TABLE import_hkstock');
    $db->Query('CREATE TABLE import_hkstock LIKE import_tempstock');
    if (!$db->Query('INSERT INTO import_hkstock (prodcode,descr,model,category,url,price,trade,wholesale,onorder,stocklevel,updated,webready,supplier,brand) SELECT prodcode,descr,model,category,url,price,trade,wholesale,onorder,stocklevel,updated,webready,supplier,brand FROM import_tempstock ORDER BY import_tempstock.prodcode'))
        die('problem duplicating db.');


    // ------------------------------------------------------------------------
    $flip = 0;
    $rwctr = 1;
    $webenabled = 0;
    for ($i = 1; $i < $num_rec; $i++) {
        $webready = "0";
        $classstd = '';
        $where = "SELECT * FROM import_hkstock WHERE uid='" . $i . "'";
        if ($hksdb = $db->Query($where)) {
            $record = $db->RowArray();
            //print_r($record);print"<br>";

            $resultsweb = $db->Query("SELECT * FROM import_webstock WHERE prodcode='" . $record['prodcode'] . "' ");
            if ($resultsweb) {
                $record2 = $db->RowArray(); //print_r($record2);
            } else {
                $record2 = Array();
            }
            if ($flip == 1) {
                $classstd.=' odd';
                $flip = 0;
            } else {
                $classstd.=' even';
                $flip++;
            }
            if (($record['onorder'] == '0') && ($record['stocklevel'] <= 0)) {
                $classstd.=' nonstock';
                //there is no stock and nothing on order...subdue the colour
            }
            //if (isset($record2['webready'])=="1"){
            //$classstd.=' webready';
            //$webenabled ++;
            //$webready="1";
            //}


            if ($cli == "0") {

                //if ($rwctr == 0) //print $hdrrow; //show the header row for usability
                $rwctr++;
                if ($rwctr == 31)
                    $rwctr = 0;


                echo('<tr class="' . $classstd . '">');

                $align = "right";

                print '<td class="col1" align=' . $align . '>';
                print htmlentities($record['uid']) . '</td>';

                print '<td class="col2" align=' . $align . '>';
                print htmlentities($record['prodcode']) . '</td>';

                print '<td class="col3" align=' . $align . '>';
                print htmlentities($record['descr']) . '</td>';

                print '<td class="col4"  align=' . $align . '>';
                print htmlentities($record['supplier']) . '::' . htmlentities($record['model']) . '</td>';

                print '<td class="col5" align=' . $align . '>';
                print htmlentities($record['stocklevel']) . '</td>';

                print '<td class="col6" align=' . $align . '>';
                print htmlentities($record['onorder']) . '</td>';

                print '<td class="col7" align=' . $align . '>';
                print htmlentities($record['price']) . '</td>';

                //print '<td class="col8"  align='.$align.'>';
                //if($webready !="1") {
                //print $record['prodcode'].'<input type="checkbox" name="webr-'.$i.'"  value="'.$i.'"/>';
                //} else {
                //print $record['prodcode'].'<input type="checkbox" name="webr-'.$i.'"  value="'.$i.'" checked />';
                //}
                //print '</td>';



                echo '<tr>';
            }//end cli check
        } //close if able to get dbf info loop
    } // close for $i loop
    if ($cli == "0") {

        echo('</tbody></table>');


        echo '<table width="100%"><tr class="endform"><td align="right" colspan="7"><span class="message">Click "Close" and continue with import.</span><input type="submit" name="submit" value="Close" onclick="window.close();"/></td></tr></table>';
        echo '</form></div>';
        echo("<blockquote>File Name : $thefile<br>Number of Items : $num_rec<br>$thefile was last modified: " . date('F d Y H:i:s.', filemtime('datastore/HKSTOCK.DBF')) . "<br>Executed Time : $endexct</blockquote>");
        echo('<br> Orange= Sent to web by Wizard<br> GREEN= Already online<br>Blue text on Silver = No Stock &amp; None on Order');
        echo '</body></html>';
        if (!empty($updatemsg))
            print "<h2>Report:</h2><hr>$updatemsg<br>";
    }
    shell_exec('nice -n 19 ' . $shell . $script); //run the connector
} //end if $thefile

function backup_db($dbhost = 'localhost', $dbuser, $dbpass, $dbname) {
    $backupFile = 'backup/' . $dbname . date("Y-m-d-H-i-s") . '.gz';
    $command = "mysqldump --opt -h $dbhost -u $dbuser --password=$dbpass $dbname | gzip > $backupFile";
    system($command);
}

function runupdate($array, $num) { //command line updating of webstock db
    removewebstock();
    $db = new MySQL(true) or die('Cannot connect to MySQL server. Please check settings in mysql.class.php');
    foreach ($array as $value) {
        $new_id = $db->Query($value);
        if (!$new_id) {
            print $value;
            die('Unable to write record to DB.');
        }
    }
    return "Success!\r\n";
}

function removewebstock() {

    echo "Create/clear update DB...\r\n";
    $db = new MySQL(true) or die('Cannot connect to MySQL server. Please check settings in mysql.class.php');
    $sql = "CREATE TABLE IF NOT EXISTS `import_webstock` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `prodcode` varchar(16) NOT NULL,
  `descr` varchar(254) NOT NULL,
  `category` varchar(254) NULL,
  `url` varchar(254) NULL,
  `price` decimal(10,2) NOT NULL,
  `trade` decimal(10,2) NOT NULL,
  `wholesale` decimal(10,2) NOT NULL,
  `onorder` int(5) NOT NULL DEFAULT '0',
  `stocklevel` decimal(10,0) NOT NULL,
  `updated` date DEFAULT NULL,
  `webready` tinyint(1) NOT NULL DEFAULT '1',
    `supplier` varchar(254) NULL,
  `brand` varchar(254) NOT NULL,
  `model` varchar(254) NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `prodcode` (`prodcode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
    $results = $db->Query($sql); //remove then build table
    if (!$results)
        die('Error contacting database!' . $sql);
    $db->TruncateTable("import_webstock"); //destroy table data
}

class timerClass {

    var $startTime;
    var $started;

    function timerClass($start = true) {
        $this->started = false;
        if ($start)
            $this->start();
    }

    function start() {
        $startMtime = explode(' ', microtime());
        $this->startTime = (double) ($startMtime[0]) + (double) ($startMtime[1]);
        $this->started = true;
    }

    function end($iterations = 1) {
        $endMtime = explode(' ', microtime());
        if ($this->started) {
            $endTime = (double) ($endMtime[0]) + (double) ($endMtime[1]);
            $dur = $endTime - $this->startTime;
            $avg = 1000 * $dur / $iterations;
            $avg = round(1000 * $avg) / 1000;
            return "$avg milliseconds";
        } else {
            return "timer not started";
        }
    }

}

function similar_file_exists($filename) {
    $timefn = 0;
    $retfn = NULL;
    foreach (new DirectoryIterator('./') as $fileinfo) {
        if ($fileinfo->isDot())
            continue;
        $filenm = strtolower($fileinfo->getFilename());
        if ($filenm == strtolower($filename)) {
            $timefn = $fileinfo->getMTime();
            if (isset($timecheck) && $timefn > $timecheck) {
                $timecheck = $timefn;
                $retfn = $fileinfo->getFilename();
            } elseif (!isset($timecheck)) {
                $timecheck = $timefn;
                $retfn = $fileinfo->getFilename();
            }
        }
    }
    if (empty($retfn))
        die("$filename is not existing");
    if ($retfn != $filename) {
        rename($retfn, $filename) or die("Unable to rename $retfn to $filename.");
    }
}

?>
