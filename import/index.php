<?php
/*
  index.php

  Copyright 2012 Onyx <onyx@chalcedony.co.nz>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.
 */
$shell = '/usr/bin/php'; //for webserver
//$shell = '/usr/bin/php';//for local
$securitykey = "c39286326e93c0f83d7e83b389f44d4b";
//include_once('../remote.php');
$action = (isset($_GET['action'])) ? $_GET['action'] : null;
$pathex = "../";
if (!isset($_SESSION))
    session_start();
include_once($pathex . "config.php");
include_once($pathex . "languages/" . POS_DEFAULT_LANGUAGE . ".php");

require_once($pathex . "database.php");

$title = 'Import Menu';
include_once '../template.php';

$import = '';
$sql = "SELECT * FROM system WHERE name='import_type'";
$result = $db->query($sql);
while ($row = $db->fetchAssoc($result)) {
    $import = $row['value'];
}
if (isset($_POST)) {

    function escape_string($str) {
        if ($str !== null) {
            $str = str_replace(array('\\', '\''), array('\\\\', '\\\''), $str);
        } else {
            $str = "null";
        }
        return $str;
    }

    foreach ($_POST as $key => $val) {
        ${$key} = escape_string($val); //NOTE this dynamically created variable!!!
    }

    if (isset($server) && isset($user) && isset($pass) && isset($dbase) && isset($prefix)) {
        //we have all the needed vars, time to create a connection
        //first save to pos database ; these queries use a non-blocking check update or insert
        $db->query("INSERT INTO system ( name, value ) VALUES ('import_qb_server','$server') ON DUPLICATE KEY UPDATE value = '$server'");
        $db->query("INSERT INTO system ( name, value ) VALUES ('import_qb_user','$user') ON DUPLICATE KEY UPDATE value = '$user'");
        $db->query("INSERT INTO system ( name, value ) VALUES ('import_qb_pass','$pass') ON DUPLICATE KEY UPDATE value = '$pass'");
        $db->query("INSERT INTO system ( name, value ) VALUES ('import_qb_database','$dbase') ON DUPLICATE KEY UPDATE value = '$dbase'");
        $db->query("INSERT INTO system ( name, value ) VALUES ('import_qb_prefix','$prefix') ON DUPLICATE KEY UPDATE value = '$prefix'");
    }
}
?>




<body class="admin">
    <div  class="admin_content" style="width:95%;">
        <h3>Import Menu</h3>
        <?php
        if ($import === "wildcard") {
            ?>
            <p>Importing from Wildcard.</p><br>
            <p>Run in order.</p>
            <br>
            <table width="90%" id="impmenu">
                <tr><td style="text-align:right;"><a href="../samba.php?test=import&live=1" target="_blank"><img src="../images/import.png" title="Click to import" /></a></td><td><a href="../samba.php?test=import&live=1" target="_blank">1. Get external data file.</a></td></tr>
                <tr><td style="text-align:right;"><a href="importread.php" target="_blank"><img src="../images/import.png" title="Click to import" /></a></td><td><a href="importread.php" target="_blank">2. Read Stock from external data file.</a></td></tr>
                <tr><td style="text-align:right;"><a href="matrixread.php" target="_blank"><img src="../images/import.png" title="Click to import Matrix data" /></a></td><td><a href="matrixread.php" target="_blank">3. Read the Matrix data file.</a></td></tr>
                <tr><td style="text-align:right;"><a href="import.php" target="_blank"><img src="../images/import.png" title="Click to finish import" /></a></td><td><a href="import.php" target="_blank">4. Add the data into Quartzpos.</a></td></tr>
                <tr><td></td>
                    <td><input type="button" value="Close" onclick="window.close()" /></td>
            </table>
            <?php
        } else {
            ?>
            <p>Importing from QuartzBooks.</p><br>

            <br>
            <table width="90%" id="impmenu">
                <tr><td></td><td style="text-align:left;"><form method="POST" name="qbform">
                            <input name="server" type="text" value="<?php echo (isset($server)) ? $server : ''; ?>">Server<br>
                            <input name="user" type="text" value="<?php echo (isset($user)) ? $user : ''; ?>">User<br>
                            <input name="pass" type="text" value="<?php echo (isset($pass)) ? $pass : ''; ?>">Pass<br>
                            <input name="dbase" type="text" value="<?php echo (isset($dbase)) ? $dbase : ''; ?>">Database<br>
                            <input name="prefix" type="text" value="<?php echo (isset($prefix)) ? $prefix : ''; ?>">Prefix<br>
                            <input name="save" type="submit" value="Save">
                        </form></td></tr>
                <tr><td style="text-align:right;"><a href="importquartz.php" target="_blank"><img src="../images/import.png" title="Click to import" /></a></td><td><a href="../samba.php?test=import&live=1" target="_blank">1. Get QuartzBooks data.</a></td></tr>

                <tr><td></td>
                    <td><input type="button" value="Close" onclick="window.close()" /></td>
            </table>
            <?php
        }
        ?>
    </div>
</body>

</html>
