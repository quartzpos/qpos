<?php 
// read the matrix specials table and send it to mysql
$cli="0"; 
if (isset($argv) && $argv[1]=="cli") $cli="1";
include_once('mysql.class.php');
include_once('dbf_class.php');
$db= new MySQL(true) or die('Cannot connect to MySQL server. Please check settings in mysql.class.php');
// Begin a new transaction, but this time, let's check for errors.
if (! $db->TransactionBegin()) {
	$db->Kill();
	die ('Cannot start DB transactions...quitting now.');
}
// We'll create a flag to check for any errors
$success = true; global $hkdata;
//sql building: BE CAREFUL NOT TO USE semicolon inside the sql!!!!
$db->TruncateTable("hkmatrix");//destroy table data
$sql="CREATE TABLE IF NOT EXISTS `hkmatrix` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `custgroup` varchar(4) NULL,
  `refnum` varchar(10) NULL,
  `prodgroup` varchar(254) NULL,
  `supplier` varchar(254) NULL,
  `prodcode` varchar(16) NULL,
  `matrix` varchar(1) NULL,
  `discount` decimal(10,2) NULL,
  `contract` decimal(10,2) NULL,
  `trade` decimal(10,2) NULL,
  `retail` decimal(10,2) NULL,
  `date_on` date DEFAULT NULL,
  `date_off` date DEFAULT NULL,
	`break1` decimal(5,2) NULL,
	
	`break2` decimal(5,2) NULL,
	
	`break3` decimal(5,2) NULL,
	
	`break4` decimal(5,2) NULL,
	
	`break5` decimal(5,2) NULL,
	
	`break6` decimal(5,2) NULL,
	
	`break7` decimal(5,2) NULL,
	`price1` decimal(10,2) NULL,`price2` decimal(10,2) NULL,`price3` decimal(10,2) NULL,`price4` decimal(10,2) NULL,`price5` decimal(10,2) NULL,`price6` decimal(10,2) NULL,`price7` decimal(10,2) NULL,
	   `text` varchar(254) NULL,
	   `type` varchar(4) NULL,
  PRIMARY KEY (`uid`)
  
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
$results = $db->Query($sql);
if (!$results) die('Error contacting database!'.$sql);
// open in read-write mode
if (function_exists('dbase_open')) { // only if php was compiled with dbase support
        $updatemsg.="Packing database.<br/>";
    $dbo = dbase_open('datastore/hkmatrix.dbf', 2);
  // expunge the database
  dbase_pack($dbo);
    }


    $dbr = new dbf_class('datastore/hkmatrix.dbf');
    $num_rec=$dbr->dbf_num_rec;    
    $field_num=$dbr->dbf_num_field;
    //print" Num_rec: $num_rec Num_field $field_num <br>";
    	 for($i=0; $i<$num_rec; $i++){
			 //print"<br> Record : $i <br>================<br>";
		 $row = $dbr->getRow($i);
		 
		 if (is_array($row)){
		 $rowkeys=array_keys($row);
		 $sql="INSERT INTO hkmatrix (custgroup,refnum,prodgroup,supplier,prodcode,matrix,discount,contract,trade,retail,date_on,date_off,break1,break2,break3,break4,break5,break6,break7,price1,price2,price3,price4,price5,price6,price7,text,type) VALUES (";
		 $k=0;
		 //build SQL from db columns
		 foreach($rowkeys as $key){
			 if ($row[$key] == NULL)$row[$key]='';
		
		 $sql.="'$row[$key]'";
			
		 if($k!=count($rowkeys)-1)$sql.=",";
		 $k++;
	 }
	} else {
		print "Bad line in DBF: $row";
		exit;
	}
	 $sql.=")";
	 //print"<br> $sql";
		  
		 //	$sql="UPDATE hkmatrix  SET  hkcategory=$hkcategory, hksupplier=$hksupplier WHERE code=$code";
			$newresults = $db->Query($sql);
			if (!$newresults) die('Error building database!'.$sql);
	}

	

	  //get supplier from categories
	 // print"<br>";
	  for($i=1; $i<$num_rec; $i++){
			$sql="SELECT supplier FROM hkmatrix WHERE uid = ".$i;
			$results = $db->QuerySingleRowArray($sql);
			$suppcode =  $results[0];
			if ($suppcode){
			$sql2="SELECT hksupplier, wwwsupplier FROM categories WHERE code = '".$suppcode."'";
			//print $sql2."<br>";
			$subresults = $db->Query($sql2);
			if(!empty($subresults)){
			$ns = $db->fetchRow($subresults);
			//print_r($ns);
			if ($ns[1]){ 
				$newsupp = $db->clean($ns[1]);
				} else {
				$newsupp = $db->clean($ns[0]);
				}
				
			$sql3="UPDATE hkmatrix SET supplier ='".$newsupp."' WHERE uid = ".$i;
			//print "<br> $sql3";
			$results = $db->Query($sql3);
			}
		 } //finish suppcode check
		 	$sql="SELECT prodgroup FROM hkmatrix WHERE uid = ".$i;
			$results = $db->QuerySingleRowArray($sql);
			$pgcode =  $results[0];
			
			if ($pgcode){
			$sql2="SELECT hkcategory, wwwcategory FROM categories WHERE code = '".$pgcode."'";
			//print $sql2."<br>";
			$subresults = $db->Query($sql2);
			if(!empty($subresults)){
			$ns = $db->fetchRow($subresults);
			//print_r($ns);
			if ($ns[1]){ 
				$newsupp = $db->clean($ns[1]);
				} else {
				$newsupp = $db->clean($ns[0]);
				}
			$sql3="UPDATE hkmatrix SET prodgroup ='".$newsupp."' WHERE uid = ".$i;
			//print "<br> $sql3";
			$results = $db->Query($sql3); 
			}
		 }// end pgcode check
		 }
		 
if($cli==0){
$sql = "SELECT * FROM hkmatrix";
$newresults = $db->Query($sql);
print $db->GetHTML(false, 'width:100%;', 'font-size:10px;color:#000;background:#cc0;', 'font-size:9px;color:#000;border-width:1px;border-style:solid;');
echo '<br>All imported. It is safe to close this tab and continue with import.<input type="button" value="Close" onclick="window.close()" /><br>';

}
?>
