<?php

//check for existence of the config file - a fairly sure bet that QuartzPOS is installed, route accordingly
if(file_exists('dbconfig.php') && file_exists('config.php')){
    header("Location: pos.php");
} else {
    if(file_exists('install.php')){
    header("Location: install.php");
    } else {
        print "dbconfig.php doesn't exist or cannot be read. Same problem with install.php.<br>You need to fix dbconfig.php in order to connect to the database.<br>Exiting.";
    }
}
?>
