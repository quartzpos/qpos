<?php

if (isset($_POST['submitinstall'])) {
$installed=false;
    if (!isset($_POST['dbname']) || empty($_POST['dbname']))
        $_POST['dbname'] = "quartzpos";
    if (isset($_POST['dbuser'])){
        $dbuser=$_POST['dbuser'];
    }
        if (isset($_POST['dbpass'])){
        $dbpass=$_POST['dbpass'];
    }
        if (isset($_POST['dbname'])){
        $dbname=$_POST['dbname'];
    }
        if (isset($_POST['dbhost'])){
        $dbhost=$_POST['dbhost'];
    }
    if (isset($_POST['adminpassword'])){
        $adminpass=md5($_POST['adminpassword']);
    } else 
    {
        $adminpass=md5("admin");
    }
    $configfile = '<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com

   

  Released under the GNU General Public License
*/

/************************
Configuration file
************************/

//Administrator login
$adminname = "admin";
$adminpassword = "'.$adminpass.'";


//GENERAL VALUES
define("QUARTZPOS_VERSION","Diamond");
// Jan - Garnet
// Feb - Amethyst
// Mar - Aquamarine
// Apr - Diamond
// May - Emerald
// Jun - Pearl
// Jul - Ruby
// Aug - Sardonyx
//Sep - Sapphire
// Oct - Opal
// Nov - Topaz
// Dec - Turquoise
define("POS_DEFAULT_LANGUAGE","english");
define("CATEGORY_IMG_SIZE","32");
define("ITEM_IMG_SIZE","32");
define("CURRENCY","NZD");
define("ITEMS_PER_PAGE","10");
define("IMPORT_FILE_PATH", "");
define("EXPORT_FILE_PATH", "export/"); // a directory with trailing slash
define("BACKUP_FILE_PATH", "backup/"); // a directory with trailing slash
require_once "dbconfig.php";
define("DEBUG_CONSOLE",TRUE);
?>
';
    $dbconfigfile='<?php

/* 
 Keep the database connection parameters here for QuartzPOS
 */

$dbhost = "'.$dbhost.'";
$dbuser = "'.$dbuser.'";
$dbpassword = "'.$dbpass.'";
$dbname = "'.$dbname.'";
?>';
//Create configuration files
    $file = fopen("config.php", "w+") or die("Failed to create config file!");
    fwrite($file, $configfile);
    fclose($file);
$file = fopen("dbconfig.php", "w+") or die("Failed to create dbconfig file!");
    fwrite($file, $dbconfigfile);
    fclose($file);
    $vars=Array();
    $vars['dbhost']=$dbhost;
    $vars['dbname']=$dbname;
    $vars['dbuser']=$dbuser;
    $vars['dbpass']=$dbpass;
    $vars['adminpass']=$adminpass;
    $output = install($vars);
}
?>

<html>
    <head>
        <title>Quartzpos Installer</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8">
        <link rel="stylesheet" href="installer.css" type="text/css">
    </head>
    <body><h2><img src="images/quartzposlogo.png" title="Quartzpos Point of Sale System" /> Installer</h2><div id="content">
            <?php
            if (isset($installed) && $installed ===true) {

                echo '<table align="center"><tr><td>Installation finished.<br>
To access the administration area click <a href="admin.php">here</a>.</td></tr></table>';
                if (isset($output)) {
                    echo '<div id="output">' . $output . '</div>';
                }
            } else {
                ?>
                <form action="install.php" method="POST">
                    <table align="center">
                        <TR><TD colspan="2"><b>Administrator login:</b></TD></TR>
                        <TR><TD>Admin name*</TD><TD><input type="text" name="adminname" value='admin' disabled></TD></TR>
                        <TR><TD>Password*</TD><TD><input type="text" name="adminpassword"></TD></TR>
                        <TR><TD colspan="2"><b>Database values:</b></TD></TR>
                        <TR><TD>Host*</TD><TD><input type="text" name="dbhost" value="localhost"></TD></TR>
                        <TR><TD>Database</TD><TD><input type="text" name="dbname"></TD></TR>
                        <TR><TD>User*</TD><TD><input type="text" name="dbuser"></TD></TR>
                        <TR><TD>Password*</TD><TD><input type="text" name="dbpass"></TD></TR>
                        <TR><TD colspan="2"><input type="submit" name="submitinstall" value="Install"></TD></TR>
                        <tr><td colspan="2"><p>This installer will attempt to create the database if you have the right username and password.<br> It will default to a database of name "quartzpos" if the Database field is left empty. <br>* indicates mandatory field.</p></td></tr>
                    </table>
                </form>

                <?php
                if (!empty($output)) {
                    echo '<div id="output">' . $output . '</div>';
                }
            }
            ?></div>
    </body>
</html>
<?php


function install($vals) {
    
    global $installed;
    $output = '';$output.= "Running the installation...<br>";
    // Connect to MySQL
    $link = mysqli_connect($vals['dbhost'], $vals['dbuser'], $vals['dbpass']);
    if (!$link) {
        die('Could not connect: ' . mysqli_error());
    } else {
        $output.= "connected...<br>";
    }

// Make my_db the current database
    $db_selected = mysqli_select_db($link, $vals['dbname']);

    if (!$db_selected) {
        // If we couldn't, then it either doesn't exist, or we can't see it.
        $sql = 'CREATE DATABASE `' . $vals['dbname'] . '` CHARACTER SET utf8';

        if (mysqli_query($link,$sql)) {
            $output = "Database " . $vals['dbname'] . " created successfully\n";
        } else {
            $output = 'Error creating database: ' . mysqli_error() . "\n";
        }
    } else {
        $output.= "Database exists...<br>";
    }

    mysqli_close($link);

    $filename = "sql/quartzpos.sql";
    $handle = fopen($filename, "r");
    $query = fread($handle, filesize($filename));
    fclose($handle);
    
    $link = mysqli_connect($vals['dbhost'], $vals['dbuser'], $vals['dbpass'], $vals['dbname'])or die("Error " . mysqli_error($link));
    if(!$link){$output.= "Unable to create link...<br>";}
    /* check connection */
    if (mysqli_connect_errno()) {
        $output = "Connect failed:" . mysqli_connect_error();
        return false;
    }
    /* execute multi query */
    if ( mysqli_multi_query($link,$query)) {
        $output.= "Running the table loading...<br>";
        do {
            /* store first result set */
            if ($result = mysqli_store_result($link)) {
                $output.= "Mysqli store result...<br>";
                while ($row = mysqli_fetch_row($result)) {
                    $output.=$row[0];
                }
                mysqli_free_result($result);
            }
            /* print divider */
            if (mysqli_more_results($link)) {
                $output.=".";
            }
        } while (mysqli_next_result($link));
        $sql="INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `password`, `type`, `sales`, `system`, `admin`, `reports`, `stock`) VALUES
(1, 'admin', 'istrator', 'admin', '".$vals['adminpass']."', 4, 4, 4, 4, 4, 4);";
        mysqli_query($link, $sql);
        header("Location: admin.php?msg=installed&action=logout");
    } else {
        $output.= "Error running the table query...<br>";
    }

    /* close connection */
    mysqli_close($link);
    return $output;
}
