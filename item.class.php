<?php

/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */

class Item {
// Item class for stock product handling
//CREATE TABLE `items` (
//  `id` int(8) NOT NULL,
//  `item_name` varchar(80) NOT NULL,
//  `item_number` varchar(80) DEFAULT NULL,
//  `description` tinytext DEFAULT NULL,
//  `clearance` tinyint(1) NOT NULL DEFAULT 0,
//  `brand_id` int(8) NOT NULL,
//  `category_id` int(8) NOT NULL,
//  `supplier_id` int(8) NOT NULL,
//  `trade_price` decimal(10,2) NOT NULL,
//  `wholesale` decimal(10,2) DEFAULT NULL,
//  `unit_price` decimal(10,2) NOT NULL,
//  `supplier_item_number` varchar(50) DEFAULT NULL,
//  `tax_percent` varchar(10) NOT NULL,
//  `total_cost` decimal(10,2) NOT NULL,
//  `markup` decimal(4,2) DEFAULT NULL,
//  `quantity` int(8) DEFAULT NULL,
//  `reorder_level` int(8) DEFAULT NULL,
//  `image` varchar(80) DEFAULT NULL
//) ENGINE=InnoDB DEFAULT CHARSET=utf8;
//CREATE TABLE `items_dimensions` (
//  `id` int(8) NOT NULL,
//  `items_id` int(8) NOT NULL,
//
//  `net_weight_kg` varchar(32) DEFAULT NULL,
//  `gross_weight_kg` varchar(32) DEFAULT NULL COMMENT 'With packaging',
//  `net_height_m` varchar(32) DEFAULT NULL,
//  `gross_height_m` varchar(32) DEFAULT NULL COMMENT 'With packaging',
//  `net_width_m` varchar(32) DEFAULT NULL,
//  `gross_width_m` varchar(32) DEFAULT NULL COMMENT 'With packaging',
//  `gross_depth_m` varchar(32) DEFAULT NULL COMMENT 'With packaging',
//  `net_depth_m` varchar(32) DEFAULT NULL,
//  `supplier_url` text DEFAULT NULL,
//  `packaging_type` text DEFAULT NULL,
//  `style` text DEFAULT NULL,
//  `colour` varchar(255) DEFAULT NULL,
//  `materials` text DEFAULT NULL,
//  `manufacture_components` text DEFAULT NULL COMMENT 'For compound products made of multiple other products',
//  `certifications` text DEFAULT NULL,
//  `hazards` text DEFAULT NULL,
//  `awards` text DEFAULT NULL,
//  `website_url` text DEFAULT NULL COMMENT 'For ecommerce site url',
//  `deleted_date` date DEFAULT NULL,
//  `author` tinytext DEFAULT NULL,
//  `creation_user_id` int(8) DEFAULT NULL,
//  `deletion_user_id` int(8) DEFAULT NULL
//) ENGINE=InnoDB DEFAULT CHARSET=utf8;

public $db; // database connection
public $id;
public $item_name;
public $item_number;
public $description;
public $clearance;
public $brand_id;
public $category_id;
public $supplier_id;
public $trade_price;
public $wholesale;
public $unit_price;
public $supplier_item_number;
public $tax_percent;
public $total_cost;
public $markup;
public $quantity;
public $reorder_level;
public $image;
public $net_weight;
public $gross_weight;
public $net_height;
public $gross_height;
public $net_width;
public $gross_width;
public $net_depth;
public $gross_depth;
public $supplier_url;
public $packaging_type;
public $style;
public $colour;
public $manufacture_components;
public $materials;
public $certifications;
public $hazards;
public $awards;
public $website_url;
public $deleted_date;
public $author;
public $creation_user_id;
public $deletion_user_id;




function __construct(){


}
//magic getter setters because there are so many aspects to the items
  public function __get($property) {
    if (property_exists($this, $property)) {
      return $this->$property;
    }
  }

  public function __set($property, $value) {
    if (property_exists($this, $property)) {
      $this->$property = $value;
    }

    return $this;
  }

function load($data, $key){

}
function update($data){

}
function delete(){

}
function search($data){

}
function getPrice($type){
 switch ($type){
  case 'wholesale':
 }
}
function getStock(){

}
function getDetails(){

}
}
?>