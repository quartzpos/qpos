// Listen for keyboard input, capture barcode reader here
    window.addEventListener("keydown", handlekd, true);
    
    var keyCache="";
    
		function handlekd(e) {
		  
		  if(e.keyCode == 13 && focal===""){
			 
			  // take the existing keycache and compare it to the barcodes 
			  var data = { functioncall: "barcode", term: keyCache };
			  // use the jquery ajax method to check the barcode
			  $.ajax({
				  type: "GET",
				  url: "autocomplete.php",
				  data: data,
				  dataType: "json",
				  success: function(result) {
				      responseHandler(result);
				  },
				  error: function() {
				      if(keyCache.length > 5) { alert("Barcode unknown."); } //check the length of the input, for validity - is it a barcode?
                      //if not length then do nothing - may be user input of Enter key
				  },
			      });
		    
			  keyCache="";
			  } else {
			  keyCache += String.fromCharCode(e.keyCode);	 
			  }
			  calcS();
		      }
		
		function responseHandler(responseData){
		      // get the response from the ajax barcode call, get the prodcode from it, then add it to the list of items
		      var itemname = responseData[0].value;
		      document.getElementById("prodcodeDiv").value = itemname;
		      getVal();
		      clearfn("prodcodeDiv");
		      }


