/*
 Quartzpos, Open Source Point-Of-Sale System
 http://Quartzpos.com



 Released under the GNU General Public License
 */

// General JavaScript functions for POS
var req = false;
var disabledConfirm_exit = false;
var rn = 1; // set row number as initial
var setel = "";
var focal = "";
var xmlret = "";
var keycounter = 0;
var rowcount = 0;
var TimeoutID = '';


function loadXMLDoc(url, type, id) {
    // branch for native XMLHttpRequest object
    if (window.XMLHttpRequest) {
        try {
            req = new XMLHttpRequest();
        } catch (e) {
            req = false;
        }
    }
    // branch for IE/Windows ActiveX version
    else if (window.ActiveXObject) {
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                req = false;
            }
        }
    }
    if (req) {
        switch (type) {
            //  the req.onreadystatechange initiates a call back function to handle the incoming data in XML format from PHP. Check for the function further below.
            case "getItems":
                req.onreadystatechange = getItems;
                req.open("GET", url, true);
                req.send("");
                break;
            case "getCustomers":
                req.onreadystatechange = getCustomers;
                req.open("GET", "getCustomers.php?findcustomertext=" + document.getElementById('findcustomertext').value, true);
                req.send("");
                break;
            case "getCustomersList":
                req.onreadystatechange = getCustomersList;
                req.open("GET", "getCustomersList.php", true);
                req.send("");
                break;
            case "getUsers":
                req.onreadystatechange = getUsers;
                req.open("GET", "getUsers.php?user=" + document.getElementById('superusername'), true);
                req.send("");
                break;
            case "getPrice":
                req.onreadystatechange = getPrice;
                req.open("GET",url, true);
                req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf8");
                req.send("");
                break;
            case "getPricelevels":
                console.log("getPricelevels Ajax");
                req.onreadystatechange = getPricelevels;
                var gpl=id.split(",");
                console.log("id="+id);
                req.el = gpl[0];
                if ( typeof gpl[1] !== "undefined" ) {
                console.log("case GPL id="+gpl[0]+" item="+gpl[1]);
                req.open("GET", "getPricelevel.php?item="+gpl[1], true);
                req.send(id);
                }
                break;
            case "getSystemvalue":
                req.onreadystatechange = getSystemvalue;
                req.open("GET", "getSystemvalue.php?systemval=" + id, true);
                req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf8");
                req.send("");
                break;
            case "getSystemvalues":
                req.onreadystatechange = getSystemvalue;
                //req.el = id;
                req.open("GET", "getSytemvalue.php", true);

                req.send("");
                break;
            case "rounding":
                req.onreadystatechange = getRounding;
                req.open("GET", "rounding.php?val=" + id, true);
                req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf8");
                req.send("");
                break;
            case "addCustomer":
                var str = getCustomerValues();
                req.onreadystatechange = getAddedCustomer;
                req.open("POST", "setCustomer.php", true);
                req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf8");
                req.send(str);
                break;
            case "saveSession":
                var strs = getSessionValues();
                //console.log("saveSession=" + str);
                req.onreadystatechange = sessionSaved;
                req.open("POST", "session_save.php", true);
                req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf8");
                req.send(strs);
                break;
            case "resumeSession":
                if (id) {
                    //console.log("resumeSession");
                    req.onreadystatechange = sessionResume;
                    req.open("GET", "session_resume.php?id=" + id, true);
                    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf8");
                    req.send(id);
                }
                break;
            case "editSale":
                if (id) {
                    //console.log("editSale");
                    req.onreadystatechange = sessionResume;
                    req.open("GET", "sale_edit.php?id=" + id, true);
                    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf8");
                    req.send(id);
                }
                break;
        }

    }
}

function synchronous_ajax_old(url, passData) { //for blocking calls to php
    console.log("sync ajax passdata=" + passData);
    console.log("sync ajax url="+url);
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (req) {
        req.open("GET", url, false);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send(passData);
        return req.responseText;
    } else {
        return false;
    }
}
function synchronous_ajax(url) {



var result= ajax().get(url).then(function (response, xhr) {
  // Do something
  console.log("SA response="+response);
});
console.log("SA result="+result);
return result;
}
function resume(id) {
    loadXMLDoc('', 'resumeSession', id);
}
function saleedit(id) {
    document.getElementById('editsaleid').value = id;
    loadXMLDoc('', 'editSale', id);

}
function getSystemValues(name) {
    if (typeof name !== "undefined") {
        loadXMLDoc('', 'getSystemvalue', name);
    } else {
        loadXMLDoc('', 'getSystemvalues');
    }
}
//Add customer - Frontend (Uses Ajax)
function getSessionValues() {
    var details = '';
    var comments = '';
    var custcomments = '';
    if (document.getElementById('comment').value != false)
        comments = document.getElementById('comment').value;
    if (document.getElementById('custcomment').value != false)
        custcomments = document.getElementById('custcomment').value;
    if (getSalestable() != false)
        details = getSalestable();
    //console.log("details from salestable=" + details);
    //console.log("comments=" + comments);
    //console.log("custcomments=" + custcomments);
    //console.log("customerid="+document.getElementById('customer_id').value);
    //console.log("userid="+document.getElementById('userid').value);
    //console.log("name="+document.getElementById('sessionid').value);
    //console.log("till="+document.getElementById('tillno').value);
    var str = "custid=" + document.getElementById('customer_id').value + "&userid=" + document.getElementById('userid').value + "&name=" + document.getElementById('sessionid').value + "&till=" + document.getElementById('tillno').value + "&comment=" + comments + "&custcomments=" + custcomments + "&details=" + details;
    return str;
}
function sessionSaved() {
    //check if the return is valid before locking out, starting new session
    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
            var response = req.responseXML;
            var root = response.getElementsByTagName('session')[0];
            //  var sid = root.getElementsByTagName('id')[0].firstChild.nodeValue;
            var sname = root.getElementsByTagName('name')[0].firstChild.nodeValue;
            console.log("sessionsaved=" + sname);
        }
    }
}
function sessionResume() {
    // used to resume session and to edit a sale
    //check if the return is valid before locking out, starting new session
    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
            var response = req.responseXML;
            var root = response.getElementsByTagName('session')[0];

            var sid = root.getElementsByTagName('id')[0].firstChild.nodeValue;
            //console.log("sessionresumed=" + sid);
            var sname = root.getElementsByTagName('name')[0].firstChild.nodeValue;
            //console.log("sname=" + sname);
            var scust = root.getElementsByTagName('custid')[0].firstChild.nodeValue;
            //console.log("scust=" + scust);
            var suser = root.getElementsByTagName('userid')[0].firstChild.nodeValue;
            //console.log("suser=" + suser);
            var schanged = root.getElementsByTagName('changed')[0].firstChild.nodeValue;
            var sccom ='';
            var scomments='';
            //console.log("schanged=" + schanged);
            //console.log("type=" + root.getElementsByTagName('comment')[0].firstChild);
            if (root.getElementsByTagName('comment')[0].firstChild) {
                scomments = root.getElementsByTagName('comment')[0].firstChild.nodeValue;
                //console.log("scomments=" + scomments);
            }
            if (root.getElementsByTagName('customer_comment')[0].firstChild) {
                sccom = root.getElementsByTagName('customer_comment')[0].firstChild.nodeValue;
                //console.log("sccom=" + sccom);
            }
            var sdetails = root.getElementsByTagName('details');
            //console.log("sdetails=" + sdetails);
            items = parse(sdetails);
            //  var sitem =sdetails.getElementsByTagName('item')[0].firstChild.nodeValue; //console.log("sitem="+sitem);
            //<?xml version="1.0" encoding="utf8" ?> <session><id>12</id><name>20130520105033-0-1</name><userid>0</userid><changed>2013-05-20 11:16:31</changed><till>1</till><custid>1</custid><details><item><prodcode>TRD3066</prodcode><qty>1</qty><tax>15</tax><uprice>23.1</uprice><price>1</price></item><item><prodcode>DIC72000280</prodcode><qty>1</qty><tax>15</tax><uprice>278.26</uprice><price>1</price></item><item><prodcode>KRE22208</prodcode><qty>1</qty><tax>15</tax><uprice>4.65</uprice><price>1</price></item></details><comment></comment><customer_comment></customer_comment></session>

            // populate using the return to parent function IPC
            var data = [];
            data[0] = sid;
            data[1] = sname;
            data[2] = scust;
            data[3] = suser;
            data[4] = schanged;
            if (scomments) {
                data[5] = scomments;
            } else {
                data[5] = "";
            }
            if (sccom) {
                data[6] = sccom;
            } else {
                data[6] = "";
            }
            if (sdetails) {
                data[7] = items;
            } else {
                data[7] = "";
            }
            parent.IPC('sr', data);
        }
    }
}
function parse(details) { // break up the details sent for items of session resume
    var retd = [];
    var tot = [];
    var items = jQuery(details).find("item");
    console.log("parse items="+items);
    for (var i = items.length; i > -1 ; i--) {

        retd[0] = jQuery(items[i]).find('prodcode').text();
        console.log("prodcode from parser=" + retd[0]);
        retd[1] = jQuery(items[i]).find('qty').text();
        console.log("qty from parser=" + retd[1]);
        retd[2] = jQuery(items[i]).find('tax').text();
        retd[3] = jQuery(items[i]).find('uprice').text();
        retd[4] = jQuery(items[i]).find('price').text();
        console.log("price from parser=" + retd[4]);
        tot.push(retd); retd=[];
    }
    console.log("parse(details) tot =" + tot);
    return tot;
}

function getSalestable() {
    //console.log("getsalestable function.");
    var retArr = [];
    numitems = document.getElementById('tbl_sitems').rows.length - 1;
    items = document.getElementsByName('items[]');
    itemsqty = document.getElementsByName('itemsqty[]');
    itemstax = document.getElementsByName('itemstax[]');
    itemsuprice = document.getElementsByName('itemsuprice[]');
    itemsprice = document.getElementsByName('price[]');

    for (i = 0; i < numitems; i++) {
        item = items[i].value;
        qty = itemsqty[i].value;
        tax = itemstax[i].value;
        uprice = itemsuprice[i].value;
        price = itemsprice[i].value;
        retArr.push([item, qty, tax, uprice, price]);

    }
    //console.log("retArr=" + retArr);
    return serialize(retArr);
}

//Get customer info after added
function getAddedCustomer() {
    //console.log("getaddedcustomer function.");
    // only if req shows "loaded"
    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
            var response = req.responseXML;
            var root = response.getElementsByTagName('customer')[0];
            var cid = root.getElementsByTagName('id')[0].firstChild.nodeValue;
            var cname = root.getElementsByTagName('name')[0].firstChild.nodeValue;
            var plevel = root.getElementsByTagName('plevel')[0].firstChild.nodeValue;
            showCustomerDiv(0);
            document.getElementById('customer_id').value = cid;
            document.getElementById('pricelevel').value = plevel;
            document.getElementById('customerDiv0').innerHTML = cname;
        }
    }
}
//Get rounded cash value
function getRounding() {
    //console.log("getRounding");

    // only if req shows "loaded"
    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
            var response = req.responseXML;

            var value = response.getElementsByTagName('value')[0].firstChild.nodeValue;

            document.getElementById('cashlabel').innerHTML = value;
            //console.log("getRounding value="+value);
        }
    }
}
function cashRound(val) {
    loadXMLDoc('', 'rounding', val);
}

//Get items from a category
function getItems() {
    // only if req shows "loaded"
    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
            document.getElementById('fe_items').innerHTML = "&nbsp;";
            var response = req.responseXML;
            //Get the xml root
            var root = response.getElementsByTagName("items")[0];
            var items = root.getElementsByTagName("item");
            //var insertMeta = "";
            for (i = 0; i < items.length; i++) {
                var iid = items[i].getElementsByTagName("id")[0].firstChild.nodeValue;
                //console.log("getItems() iid=" + iid);
                var iname = addslashes(items[i].getElementsByTagName("name")[0].firstChild.nodeValue);
                //console.log("getItems() name=" + iname);
                var iimg = items[i].getElementsByTagName("image")[0].firstChild.nodeValue;
                //console.log("getItems() image=" + iimg);
                var itax = items[i].getElementsByTagName("tax")[0].firstChild.nodeValue;
                //console.log("getItems() itax=" + itax);
                var iprice = items[i].getElementsByTagName("price")[0].firstChild.nodeValue;
                //console.log("getItems() iprice=" + iprice);
                var ibprice = 0;
                var x=items[i].getElementsByTagName("tradeprice")[0];
                // fix for when tradeprice is empty
                if(x.hasChildNodes()){  ibprice = x.firstChild.nodeValue; }
                if(ibprice==0){ ibprice=iprice; }
                //console.log("getItems() ibprice=" + ibprice);
                var iuprice = items[i].getElementsByTagName("unitprice")[0].firstChild.nodeValue;
                //console.log("getItems() iuprice=" + iuprice);

                if (document.getElementById('pricelevel').value == '2') { //TODO: strip out to make full stack pricing
                    iprice = ibprice;
                }
                var description = addslashes(items[i].getElementsByTagName("description")[0].firstChild.nodeValue);
                //console.log("getItems() description=" + description);
                var item = document.createElement("table");
                item.className = "tabitem";
                item.title = stripslashes(description);
                item.onclick = new Function("addItem(\"" + iid + "\",\"" + iname + "\",\"" + itax + "\",\"" + ibprice + "\",\"" + iuprice + "\",\"" + iprice + "\",\"" + description + "\",\"\");");//last empty string is insertmeta
                var tbitem = document.createElement("tbody");
                item.appendChild(tbitem);
                var row1 = document.createElement("tr");
                var row2 = document.createElement("tr");
                tbitem.appendChild(row1);
                tbitem.appendChild(row2);
                var cell1 = document.createElement("td");
                var cell2 = document.createElement("td");
                row1.appendChild(cell1);
                row2.appendChild(cell2);
                im1 = document.createElement("img");
                im1.setAttribute("src", iimg);
                cell1.appendChild(im1);
                cell2.innerHTML = "<span class='tabitemid'>" + iid + "</span><br>" + item.title;
                document.getElementById('fe_items').appendChild(item);
            }

        } else {
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }
    document.getElementById('fe_categories').scrollIntoView();

}


//Find customers
function getCustomers() {

    // only if req shows "loaded"
    //console.log("getCustomers req readystate =" + req.readyState);
    if (req.readyState === 4) {
        // only if "OK"
        if (req.status === 200) {
            var response = req.responseXML;
            //Get the xml root
            var root = response.getElementsByTagName('customers')[0];
            var customers = root.getElementsByTagName('customer');

            document.getElementById('findCustomersDiv').innerHTML = "";
            tcustomers = document.createElement("table");
            tcustomers.setAttribute("width", "100%");
            tcustomers.setAttribute("cellspacing", "0");
            tbcustomers = document.createElement("tbody");
            tcustomers.appendChild(tbcustomers);
            for (i = 0; i < customers.length; i++) {
                var cid = customers[i].getElementsByTagName("id")[0].firstChild.nodeValue;
                var cname = customers[i].getElementsByTagName("name")[0].firstChild.nodeValue;
                var plevel = customers[i].getElementsByTagName("plevel")[0].firstChild.nodeValue;
                row = document.createElement("tr");
                tbcustomers.appendChild(row);
                cell = document.createElement("td");
                cell.className = "tvalue_lnk";
                cell.onclick = new Function("setCustomer('" + cid + "','" + cname + "','" + plevel + "')");
                cell.innerHTML = '<input type="hidden" value="' + cid + '">' + cname;
                row.appendChild(cell);
            }
            document.getElementById('findCustomersDiv').appendChild(tcustomers);

        } else {
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }
}
function getCustomersList() {
    //console.log("getCustomersList function")
    if (req.readyState === 4) {
        // only if "OK"
        if (req.status === 200) {
            var response = req.responseXML;
            //Get the xml root
            var root = response.getElementsByTagName('customers')[0];
            var customers = root.getElementsByTagName('customer');
            var custid = parseInt(document.getElementById("customer_id").value) - 1;
            //console.log("custid="+custid);
            // ... occurs when restoring a session

            var cid = customers[custid].getElementsByTagName("id")[0].firstChild.nodeValue;
            var cname = customers[custid].getElementsByTagName("name")[0].firstChild.nodeValue;
            var plevel = customers[custid].getElementsByTagName("plevel")[0].firstChild.nodeValue;
            //console.log("custid="+custid+" cid="+cid+" cname="+cname+" plevel="+plevel);
            setCustomer(cid, cname, plevel);

        } else {
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }
}
//Find price levels from ajax call
function getPricelevels() {
    //el should be the id of the pricelevel div

    var el = req.el;
    var split = el.split('pl');
    var rn = split[1];
    console.log("getPricelevels rn="+rn);
    var currentpl = parseInt(document.getElementById('rowpricelevel' + rn).value);
    console.log("getPricelevels passed el =" + el);

    //console.log("getPricelevels req readystate =" + req.readyState);
    // only if req shows "loaded"
    if (req.readyState === 4) {
        // only if "OK"
        if (req.status === 200) {
            var response = req.responseXML;
            console.log("getPricelevel response =" + response);
            //Get the xml root
            var root = response.getElementsByTagName('pricelevels')[0];
            var prices = root.getElementsByTagName('pricelevel');
            document.getElementById(el).innerHTML = '<div class="closepl" id="closepl' + rn + '" onclick="showhide(\'pl' + rn + '\', \'FALSE\');showhide(\'closepl' + rn + '\', \'FALSE\');"><img src="images/close.png" title="Close the pricelist : alt+o"/></div>';
            tprices = document.createElement("table");
            tprices.setAttribute("width", "100%");
            tprices.setAttribute("cellspacing", "0");
            tprices.className = "pltable";
            tbprices = document.createElement("tbody");
            tprices.appendChild(tbprices);
            var userlevel = document.getElementById('permsales').value;
            for (i = 0; i < prices.length; i++) {
                var uid = prices[i].getElementsByTagName("uid")[0].firstChild.nodeValue;

                var name = prices[i].getElementsByTagName("name")[0].firstChild.nodeValue;
                var taxinc = prices[i].getElementsByTagName("taxinc")[0].firstChild.nodeValue;
                var markup = prices[i].getElementsByTagName("markup")[0].firstChild.nodeValue;
                var base = prices[i].getElementsByTagName("base")[0].firstChild.nodeValue;
                var price = prices[i].getElementsByTagName("price")[0].firstChild.nodeValue;
                var perms = prices[i].getElementsByTagName("security_level")[0].firstChild.nodeValue;
                console.log("getPL price="+price+" for pl="+name);
                var taxes='';
                if (userlevel >= perms) {
                    if (taxinc === 0) {
                        taxes = "No";
                    } else {
                        taxes = "Yes";
                    }
                    var mark = "";
                    if (markup !== 0) {
                        mark = "Markup = " + markup + "%";
                    }
                    row = document.createElement("tr");
                    tbprices.appendChild(row);
                    cell = document.createElement("td");
                    cell.className = "tvalue_lnk";
                    if (currentpl == uid) {
                        cell.className += " selected";
                    }
                    cell.id = "pl-" + uid + "-" + rn;

                    cell.innerHTML = '<a onclick="setPricelevel(\''+price+ '\',\'' + rn + '\',\'' + uid + '\');" title="Set row ' + rn + ' to price level ' + name + ' at value of '+price+'. From base :'+base+'. Tax included :' + taxes + '. ' + mark + '" ><input type="button" id="pricebutton-' + uid + '-' + rn + '" data-price="'+ price+ '" value="' + name + '" /></a>';
                    row.appendChild(cell);
                }
            }
            document.getElementById(el).appendChild(tprices);
        } else {
            alert("There was a problem retrieving the XML data at getPricelevels:\n" + req.statusText);
        }
    }
}
//Find price level from ajax call for item calculations
function getPricelevel() {

    console.log("getPricelevel req readystate =" + req.readyState);
    // only if req shows "loaded"
    if (req.readyState === 4) {
        // only if "OK"
        if (req.status === 200) {
            var response = req.responseXML;
            console.log("getPricelevel response =" + response);
            //Get the xml root
            var root = response.getElementsByTagName('pricelevels')[0];
            var prices = root.getElementsByTagName('pricelevel');
            var uid = prices[0].getElementsByTagName("uid")[0].firstChild.nodeValue;
            console.log("getPricelevel uid =" + uid);
            var name = prices[0].getElementsByTagName("name")[0].firstChild.nodeValue;
            var taxinc = prices[0].getElementsByTagName("taxinc")[0].firstChild.nodeValue;
            var markup = prices[0].getElementsByTagName("markup")[0].firstChild.nodeValue;
            var base = prices[0].getElementsByTagName("base")[0].firstChild.nodeValue;
            var ret = uid + '-' + name + '-' + taxinc + '-' + markup + '-' + base;
            xmlret = ret;
            console.log("xmlret set =" + xmlret);
        } else {
            alert("There was a problem retrieving the XML data at getPricelevel:\n" + req.statusText);
        }
    }
}
//After a customer is selected, insert values
function setCustomer(cid, cname, plevel) {
    //console.log("Setcustomer function, cid="+cid+" cname="+cname+" plevel="+plevel);
    document.getElementById('sales_items_customer').innerHTML = '<input type="hidden" name="customer_id" id="customer_id" value="' + cid + '">';
    document.getElementById('customerDiv0').innerHTML = cname;
    document.getElementById('pricelevel').value = plevel;
    showCustomerDiv(0);
}

//Set the pricelevel for a row
//
function setPricelevel(newprice, row, pl) {
    console.log("Setpricelevel pricelevel function.");
    console.log("newprice="+newprice+" row="+row+" pl="+pl);
    //var plcount=document.getElementById('plcount').value;
    //these are hidden inputs in the pos.php #pls div
    var pluid = document.getElementById('pluid' + pl).value;
    var plname = document.getElementById('plname' + pl).value;
    console.log("plname="+plname);
    var pltaxinc = document.getElementById('pltaxinc' + pl).value;
    console.log("pltaxinc="+pltaxinc);
    var plmarkup = document.getElementById('plmarkup' + pl).value;
    console.log("plmarkup="+plmarkup);
    var plbase = document.getElementById('plbase' + pl).value;
    var item = document.getElementById('items' + row).value; // get the row's item name
    var price = new Array(2);
    price[1] = newprice;

    if (plmarkup !== 0) {
        //we have a markup - above the regular base...get base from items table, apply markup

        //price[1] = price[1] * (1 + (plmarkup / 100));
        document.getElementById('itemsuprice' + row).value = price[1];
        //console.log("Setpricelevel plmarkup!=0. price="+price[1]);
    }

    //mark the row chosen in pls
    var plcount = parseInt(document.getElementById('plcount').value) + 1;
    //console.log("setpricelevel plcount="+plcount);
    for (i = 1; i < plcount; i++) {
        var el = "#pl-" + i + "-" + row;
        jQuery(el).removeClass("selected");
        //console.log("setpricelevel remove class on el="+el);
    }
    var rel = "#pl-" + pl + "-" + row;
    jQuery(rel).addClass("selected");
    document.getElementById('rowpricelevel' + row).value = pl;
    //now mark the taxinc for that row
    document.getElementById('itemstaxinc' + row).value = pltaxinc;
    calcS();
    showhide("pl" + row, "TRUE");
}
function writePls(uid, name, tax, mark, base) {
    //console.log("writepls");
    var plcount = document.getElementById('plcount').value;
    document.getElementById('pls').innerHTML += "<input type='hidden' id='pluid" + uid + "' value='" + uid + "' />";
    document.getElementById('pls').innerHTML += "<input type='hidden' id='plname" + uid + "' value='" + name + "' />";
    document.getElementById('pls').innerHTML += "<input type='hidden' id='pltaxinc" + uid + "' value='" + tax + "' />";
    document.getElementById('pls').innerHTML += "<input type='hidden' id='plmarkup" + uid + "' value='" + mark + "' />";
    document.getElementById('pls').innerHTML += "<input type='hidden' id='plbase" + uid + "' value='" + base + "' />";
    document.getElementById('plcount').value = plcount + 1;
}
//Add item to the sales table
function addItem(id, iitem, tax, bprice, uprice, price, description, meta, qty, pl, ti) {
    console.log("addItem price="+price+" bprice="+bprice+" uprice="+uprice);
    if(iitem==="E" || description ==="r" || bprice==="o"){
		console.log("addItem bad data arrived , so ignore it.");

		return false; }
    var inpmod ='';
    if (meta !== "") {
        description = description + ": " + meta;
    }
    // ti = taxinc -  boolean 1 or 0
    // pl = pricelevel - from db table - likely 1-3 retail trade wholesale
    if (typeof qty === 'undefined') {
        qty = 1;
    }
    if (typeof pl === 'undefined') {
        pl = 1;
    }
    if (typeof ti === 'undefined') {
        ti = 1;
    }

    if (document.getElementById("admincheck").value === "admin" || document.getElementById("admincheck").value === "Admin") {
        inpmod = "";
    } else {
        inpmod = "disabled";
    }

    var exclass = "";
    var extitle = "";
    //console.log("pricelevel == "+document.getElementById('pricelevel').value);//TODO : expand for all levels of pricing not just 2
    if (document.getElementById('pricelevel').value == '2') {
        uprice = bprice;
        //console.log("Customer price level 2,uprice set to "+uprice);
    }

    sit = document.getElementById('tbl_sitems');
    sittb = sit.getElementsByTagName('tbody')[0];
    row = document.createElement("tr");
    row.className = "itemListing";
    //var rn = document.getElementById('tbl_sitems').rows.length;
    row.id = "row" + rn; //assign a row number
    row.onclick = new Function("selectRow(document.getElementById('tbl_sitems'),this.rowIndex);");
    var tFC = sit.firstChild;
    sit.insertBefore(row, tFC);
    cell1 = document.createElement("td");
    row.appendChild(cell1);
    cell2 = document.createElement("td");
    row.appendChild(cell2);
    cell3 = document.createElement("td");
    row.appendChild(cell3);
    cell4 = document.createElement("td");
    row.appendChild(cell4);
    cell5 = document.createElement("td");
    row.appendChild(cell5);
    cell6 = document.createElement("td");
    row.appendChild(cell6);
    cell1.className = "tvalue firsttd";
    cell2.className = "tvalue midtd";
    cell2.setAttribute("align", "right");
    cell3.className = "tvalue midtd";
    cell3.setAttribute("align", "right");
    cell4.className = "tvalue midtd" + exclass;
    cell4.setAttribute("align", "right");
    cell5.className = "tvalue midtd";
    cell5.setAttribute("align", "right");
    cell6.className = "tvalue lasttd";
    cell6.setAttribute("align", "right");
    cell1.innerHTML = '<input type="hidden" name="items[]" id="items' + rn + '" value="' + id + '"><input type="hidden" name="itemsmeta[]" id="itemsmeta' + rn + '" value="' + meta + '"><input type="hidden"  id="items_name' + rn + '" name="items_name[]" value="' + iitem + '">' + id + ':&nbsp;' + description;
    cell2.innerHTML = '<input type="number" min="0" max="1000" step="1"  class="inp2" name="itemsqty[]"  id="itemsqty' + rn + '" size="4" onkeypress="return isNumberKey(event);" title="Quantity of items. Hotkey alt-1" onclick="select(this);setelement(this)" onchange="setprice(this);calcS()" value="' + qty + '">';
    cell3.innerHTML = '<input type="number" min="0" max="100" step=".25" size="2" class="inp2 dacheck"  onchange="setprice(this);calcS();"  onfocus="gotfocus(this)" onblur="lostfocus(this)" id="itemstax' + rn + '" name="itemstax[]" value="' + tax + '"' + inpmod + ' data-taxinc="' + ti + '"><input type="hidden" id="itemstotaltax' + rn + '" name="itemstotaltax[]" value="0"><input type="hidden" id="itemstaxinc' + rn + '" name="itemstaxinc[]" value="' + ti + '">';
    cell4.innerHTML = '<input type="number" min="0" max="1000000" step=".01"  class="inp2 dacheck" size="6" onchange="setprice(this);calcS();"  onfocus="gotfocus(this)" onblur="lostfocus(this)" onkeypress="return ispriceKey(event)"  onclick="select(this);setelement(this)" id="itemsuprice' + rn + '"  name="itemsuprice[]" value="' + toCurrency(uprice) + '"' + inpmod + ' data-pricelevel="' + pl + '">';
    cell4.innerHTML += '<input type="hidden" name="itemsprice[]"  id="itemsprice' + rn + '" value="' + uprice + '"><input type="hidden"  id="itemstotalprice' + rn + '" name="itemstotalprice[]" value="0"><input name="rowpricelevel[]" type="hidden" id="rowpricelevel' + rn + '" value="' + pl + '">';
    cell4.innerHTML += '<input type="hidden" name="itemsretailprice[]"  id="itemsretailprice' + rn + '" value="' + price + '">';
    // TODO : if price is used for itemsretailprice then the calculation of discount doesn't work for trade sale resumed

    cell5.innerHTML = '<input type="text" class="inp2 dacheck" name="price[]"  id="price' + rn + '" size="6" title="' + extitle + '"   value="' + price + '"' + inpmod + '><input type="hidden" name="itemsbprice[]"  id="itemsbprice' + rn + '" value="' + bprice + '">';
    cell6.innerHTML = '<div class="switches"><img src="images/credit.png"  id="cre' + rn + '" title="Credit ' + iitem + '" onclick="credit(this);" />&nbsp;<img src="images/delete.png"  id="del' + rn + '" title="Delete ' + iitem + '" onclick="keyboardDEL(document.getElementById(\'tbl_sitems\'),' + rn + ')" /></div>';
    cell6.innerHTML += '<div id="taxed' + rn + '" title="Tax excluded for this item.">*</div>';
    cell6.innerHTML += '<div class="pricelevel"><img style="margin:0;padding:1px;" src="images/p.png" id="\pimg' + rn + '\" title="Price levels. ALT+p to open, ALT+o to close" onclick="loadXMLDoc(\'\',\'getPricelevels\',\'pl' + rn + ','+id+'\');showhide(\'pl' + rn + '\',\'TRUE\',\'pimg' + rn + '\');showhide(\'closepl' + rn + '\',\'TRUE\');"></div><div id="pl' + rn + '" class="pldisplay"><div class="closepl" id="closepl' + rn + '" onclick="showhide(\'pl' + rn + '\', \'FALSE\');showhide(\'closepl' + rn + '\', \'FALSE\');"><img src="images/close.png" title="Close the pricelist : alt+o"/></div></div>';
    selectRow(document.getElementById('tbl_sitems'), row.rowIndex);

    //setfoc('itemsqty' + rn);
    rn = rn + 1;
    rowcount = rowcount + 1;
    calcS();
    clearfn('prodcodeDiv');
    setfoc('prodcodeDiv');
}

function setelement(el) {
    setel = el;
    //console.log("Set element " + el.id);
}
function setprice(el) {
    //console.log("setprice");
    var rid = el.id;
    var x = rid.match(/\d+$/)[0];//get the row number from the element id
    //console.log("rowid=" + x);
    qty = document.getElementById('itemsqty' + x).value;
    if (qty < 0) {
        // this qty is negative
        mod = -1;
    } else {
        mod = 1;
    }
    //console.log("mod=" + mod);
    tax = document.getElementById('itemstax' + x).value;

    uprice = Math.abs(document.getElementById('itemsuprice' + x).value);
    //console.log("uprice=" + uprice);
    pre = Math.abs(qty) * uprice * (1 + (tax / 100)); //how does this handle negative qty?
    //tax=tax*mod;
    //console.log("pretotal=" + pre);
    price = toCurrency(pre);
    tt = toCurrency(price - (uprice * Math.abs(qty)));
    //console.log("================");
    //console.log("mod=" + mod);
    document.getElementById('price' + x).value = mod * price;
    //console.log(x + "price=" + mod * price);
    document.getElementById('itemsuprice' + x).value = mod * uprice;
    //console.log(x + "uprice=" + mod * uprice);
    //console.log("tt=" + tt);
    document.getElementById('itemstotaltax' + x).value = mod * tt;
    //console.log(x + "ttax=" + mod * tt);
    document.getElementById('itemstotalprice' + x).value = mod * price;
    //console.log("================");
    //calcS();
}
function credit(el) {
    //console.log("credit");
    var rid = el.id;
    var x = rid.match(/\d+$/)[0];//get the row number from the element id
    //console.log("rowid=" + x);
    qty = document.getElementById('itemsqty' + x).value;

    document.getElementById('itemsqty' + x).value = qty * -1;

    setprice(el);
    calcS();

}
//Calculate sales items
function calcS() {
    //console.log("Calcs");
    var notsu = true;
    if (document.getElementById("admincheck").value === "admin") {
        notsu = false;
    }
    //console.log("calcS ; rowcount="+rowcount);

    numitems = rowcount;


    var tax = 0;
    var total = 0;
    var taxinc = 1;
    var vdiscount = 0;
    for (i = 1; i < numitems + 1; i++) {
        //console.log("calcS i loop i="+i);
        o = document.getElementById("itemstax" + i);

        if (o && o.value !== "undefined") {
            //console.log("row exists = row"+i);
            //console.log(" itemstax="+ document.getElementById("itemstax"+i).value);
            itemstax = document.getElementById("itemstax" + i);
            itemsprice = document.getElementById("itemsprice" + i);
            price = document.getElementById("price" + i);
            itemstaxinc = document.getElementById("itemstaxinc" + i);
            itemsqty = document.getElementById("itemsqty" + i);
            itemsuprice = document.getElementById("itemsuprice" + i);
            itemsretailprice = document.getElementById("itemsretailprice" + i);
            itemstotalprice = document.getElementById("itemstotalprice" + i);
            itemstotaltax = document.getElementById("itemstotaltax" + i);
            //switch some inputs on or off based upon the admin rights
            itemstax.disabled = notsu;
            itemsprice.disabled = notsu;
            price.disabled = notsu;
            taxinc = itemstaxinc.value;
            iqty = parseInt(itemsqty.value);	 	//Items quantity
            //console.log("iqty=" + iqty);
            if (iqty < 0) {
                // this qty is negative
                modc = -1;
            } else {
                modc = 1;
            }
            //console.log("*********************");
            tip = iqty * toCurrency(parseFloat(itemsuprice.value));  //Calculate total item price
            //console.log("tip=" + tip);
            var inctax = 1;
            var el = "taxed" + i;
            //console.log(" calc el="+el);
            var newclass = "hidden";
            if (taxinc === "0") {
                inctax = (parseFloat(itemstax.value) / 100) + 1;
                newclass += " visibletaxed";
            }
            document.getElementById(el).className = newclass;
            //console.log("itemsuprice=" + itemsuprice.value);
            //console.log("itemstax=" + itemstax.value / 100 + " inctax ="+inctax+" modc="+modc);
            price.value = toCurrency(iqty * itemsuprice.value * inctax * modc);
            //console.log("add price=" + price.value);
            total += parseFloat(price.value);
            taxpre = (parseFloat(itemstax.value) / 100) + 1;
            taxhalf = 0;
            if (taxinc == 1) {
                taxhalf = toCurrency(parseFloat(itemsuprice.value)) / taxpre;// with taxinc only
                //console.log("taxhalf="+taxhalf);
                taxpost = toCurrency(parseFloat(itemsuprice.value)) - taxhalf;
                //console.log("taxpost = "+taxpost+" taxpre="+taxpre);
                titax = taxpost * iqty * modc; 	//Calculate total item tax
            } else {
                taxhalf = toCurrency(parseFloat(itemsuprice.value)) * taxpre;
                //console.log("taxhalf="+taxhalf);
                taxpost = taxhalf - toCurrency(parseFloat(itemsuprice.value));
                //console.log("taxpost = "+taxpost+" taxpre="+taxpre);
                titax = taxpost * iqty * modc; 	//Calculate total item tax
            }
           //console.log("itemsretailprice=" + itemsretailprice.value);
           //console.log("iqty=" + iqty);
            var predisc = itemsretailprice.value * iqty;
           //console.log("predisc=" + predisc);
            tax += titax;
           //console.log("titax=" + titax);
            itemstotaltax.value = toCurrency(titax) * modc;
           //console.log("itemstotaltax=" + itemstotaltax.value);
            itemstotalprice.value = toCurrency(tip) * modc;
            if (predisc - itemstotalprice.value > 0) {
                vdiscount += predisc - itemstotalprice.value;
               //console.log("vdiscount=" + vdiscount);
            }
           //console.log("itemstotalprice=" + itemstotalprice.value);
           //console.log("*********************");
        }
    }
    total = toCurrency(total);
    tax = toCurrency(tax);
    var subtotal = toCurrency(total - tax);
    document.getElementById('vtax').value = tax;
    document.getElementById('vsubtotal').value = subtotal;
    document.getElementById('vtotal').value = total;
    document.getElementById('vdiscount').value = toCurrency(vdiscount);
}

//Get the selected Row in a table
function getSelectedIndex(tbl) {
    // this steps through the rows of the table, regardless of top -down orientation , to produce the row that is selected
    console.log("getSelectedIndex of " + tbl);

    if (typeof tbl === "string") {
        tbl = document.getElementById(tbl);

    }

    for (i = 0; i < tbl.rows.length; i++) {
        if (tbl.rows[i].className == "selected"){
            selrow = tbl.rows[i].id;

            var selsplit=selrow.split("w");
            console.log("selrow="+selrow+" row="+selsplit[1]);
            return selsplit[1];
        }
    }
    return -1;
}

//Select a row
function selectRow(tbl, ind) {
    for (i = 1; i < tbl.rows.length; i++) {
        if (i == ind)
            tbl.rows[i].className = "selected";
        else
            tbl.rows[i].className = "itemListing";
    }
}

//Delete an item in the sales table
function keyboardDEL(tbl, rn) {
    //console.log("keyboarddel...rn=" + rn + " tbl id=" + tbl.id);

    if (!rn) {
        selectedRow = getSelectedIndex(tbl);
    } else {

        if (document.getElementById('row' + rn)) {
            var getrow = document.getElementById('row' + rn).rowIndex;
            //console.log("getrow=" + getrow);
            selectedRow = getrow;
        }

    }
    if (selectedRow > 0) {
        tbl.deleteRow(selectedRow);
        selectRow(tbl, selectedRow);

    }
    calcS();
}


//Move up
function keyboardUP(tbl) {
    selectedRow = getSelectedIndex(tbl);
    lastRow = tbl.rows.length - 1;
    if (lastRow > 0) {
        if (selectedRow > 0) {
            tbl.rows[selectedRow].className = "";
            if (selectedRow == 1) {
                tbl.rows[lastRow].className = "selected";
            } else {
                tbl.rows[selectedRow - 1].className = "selected";
            }
        } else {
            tbl.rows[lastRow].className = "selected";
        }
    }
}


//Move down
function keyboardDOWN(tbl) {
    selectedRow = getSelectedIndex(tbl);
    lastRow = tbl.rows.length - 1;

    if (lastRow > 0) {
        if (selectedRow > 0) {
            tbl.rows[selectedRow].className = "";
            if (selectedRow == lastRow) {
                tbl.rows[1].className = "selected";
            } else {
                tbl.rows[selectedRow + 1].className = "selected";
            }
        } else {
            tbl.rosws[1].className = "selected";
        }
    }
}
//Change an item quantity with the keyboard
function setValue(el, tbl, qty) {
    //console.log("Setvalue of " + el.id + " = " + el.value + " add " + qty);
    if (qty == '.') {
        if (parseInt(el.value) - el.value == 0) {
            el.value += qty;
        }
    } else {
        el.value += qty;
    }
}

//Change an item quantity with the keyboard
function setQuantity(tbl, qty) {
    selectedRow = getSelectedIndex(tbl);
    if (selectedRow > 0) {
        document.getElementsByName('itemsqty[]')[selectedRow - 1].value += qty;
    }
}

//Clear an item quantity
function keyboardCLEAR(el, tbl) {

    selectedRow = getSelectedIndex(tbl);
    if (el != true)
        el = document.getElementById('itemsqty' + selectedRow);
    //console.log("Keyboard clear = " + el.id + " row=" + selectedRow);
    if (selectedRow > 0) {
        el.value = "";
        calcS();
    }
}

//Convert an value to a currency value
function toCurrency(val) {
    //console.log("Tocurrency=" + val);
    if (val < 0) {
        // this val is negative
        modtc = -1;
    } else {
        modtc = 1;
    }
    val = val * modtc; // set to positive number
    val = (Math.round(parseFloat("0" + val) * 100)) / 100;
    val = val * modtc; // set to positive number
    //console.log("val=" + val);
    sval = val.toString();
    vals = sval.split(".");
    if (vals[1]) {
        cents = (vals[1] + "00").substring(0, 2);
    } else {
        cents = "00";
    }
    curr = vals[0] + "." + cents;
    //curr=curr*mod;
    //console.log("Curr=" + curr);
    return curr;
}


//Change customer tab
function showCustomerDiv(t) {
    if (t == 0) {
        document.getElementById('customerDiv1').style.display = 'none';
        document.getElementById('customerDiv2').style.display = 'none';
    }
    if (t == 1) {
        document.getElementById('customerDiv1').style.display = 'block';
        document.getElementById('customerDiv2').style.display = 'none';
    }
    if (t == 2) {
        document.getElementById('customerDiv2').style.display = 'block';
        document.getElementById('customerDiv1').style.display = 'none';
    }
}

//Perform sale checkout
function checkout() {
    //do check to see if there is a valid sale first, carry on if so
    //var total = parseInt(document.getElementById('vtotal').value);
    //rowcount will be zero if there are no items
    if (rowcount !== 0) {
        toggleFormElements(false); //enable the form fields to submit
        document.getElementsByName('frm_sale')[0].submit();
    } else {
        alert("No sale processed - zero items.");
        disabledConfirm_exit = false;
        return false;
    }
}

//Add customer - Frontend (Uses Ajax)
function getCustomerValues() {
    var str = "firstname=" + document.getElementsByName('firstname')[0].value + "&lastname=" + document.getElementsByName('lastname')[0].value + "&account_number=" + document.getElementsByName('account_number')[0].value + "&address=" + document.getElementsByName('address')[0].value + "&city=" + document.getElementsByName('city')[0].value + "&pcode=" + document.getElementsByName('pcode')[0].value + "&state=" + document.getElementsByName('state')[0].value + "&country=" + document.getElementsByName('country')[0].value + "&phone_number=" + document.getElementsByName('phone_number')[0].value + "&email=" + document.getElementsByName('email')[0].value + "&comments=" + document.getElementsByName('comments')[0].value + "&trade=" + document.getElementsByName('trade')[0].value;
    return str;
}
/* code from qodo.co.uk */
function toggleFormElements(bDisabled) {
    var inputs = document.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].disabled = bDisabled;
    }
    var selects = document.getElementsByTagName("select");
    for (i = 0; i < selects.length; i++) {
        selects[i].disabled = bDisabled;
    }
    var textareas = document.getElementsByTagName("textarea");
    for (i = 0; i < textareas.length; i++) {
        textareas[i].disabled = bDisabled;
    }
    var buttons = document.getElementsByTagName("button");
    for (i = 0; i < buttons.length; i++) {
        buttons[i].disabled = bDisabled;
    }
}
//The pageSet element
function setPages(npages, page,qty,passstr) {
    //Pages visible per set
    pvis = 5;
    // Present set
    pset = Math.ceil(page / pvis);
    // Number of page sets
    psets = Math.ceil(npages / pvis);
    var getstring = '';
    if (typeof passstr !== 'undefined') {
		//we have some get string to append
    getstring= passstr;
    }
    switch (pset) {
        //If we are on page 1 can't have a 'previous' link
        case 1:
            var tbl = document.createElement("table");
            var tblb = document.createElement("tbody");
            var row = document.createElement("tr");
            var tdata = 0;
            tbl.appendChild(tblb);
            tblb.appendChild(row);
            for (i = 1; i < pvis + 1 && ((pset - 1) * pvis + i) < npages + 1; i++) {
                tdata = document.createElement("td");
                tdata.innerHTML = '<a href="admin.php?action=products'+getstring+'&row_quantity='+qty+'&page=' + ((pset - 1) * pvis + i) + '">' + ((pset - 1) * pvis + i) + '</a>';
                tdata.className = "pset";
                row.appendChild(tdata);
            }
            if (psets > pset) {
                tdata = document.createElement("td");
                tdata.innerHTML = '<a href="javascript:setPages(' + npages + ',' + ((pset * pvis) + 1) + ','+qty+')">Next</a>';
                tdata.className = "pset";
                row.appendChild(tdata);
            }
            document.getElementById('pageset').innerHTML = "";
            document.getElementById('pageset').appendChild(tbl);
            break;
        default:
             tbl = document.createElement("table");
             tblb = document.createElement("tbody");
             row = document.createElement("tr");
            tbl.appendChild(tblb);
            tblb.appendChild(row);
            tdata = document.createElement("td");
            tdata.innerHTML = '<a href="javascript:setPages(' + npages + ',' + ((pset - 1) * pvis) + ','+qty+ ')">Previous</a>';
            tdata.className = "pset";
            row.appendChild(tdata);
            for (i = 1; i < pvis + 1 && ((pset - 1) * pvis + i) < npages + 1; i++) {
                tdata = document.createElement("td");
                tdata.innerHTML = '<a href="admin.php?action=products'+getstring+'&row_quantity='+qty+'&page=' + ((pset - 1) * pvis + i) + '">' + ((pset - 1) * pvis + i) + '</a>';
                tdata.className = "pset";
                row.appendChild(tdata);
            }
            if (psets > pset) {
                tdata = document.createElement("td");
                tdata.innerHTML = '<a href="javascript:setPages(' + npages + ',' + ((pset * pvis) + 1) + ','+qty+ ')">Next</a>';
                tdata.className = "pset";
                row.appendChild(tdata);
            }
            document.getElementById('pageset').innerHTML = "";
            document.getElementById('pageset').appendChild(tbl);
            break;
    }
}
function showhide(hideDiv, show, obj) {
    var objtop=0;
    if (typeof show === "undefined") {
        show = 'FALSE';
    }
    if (typeof obj !== "undefined") {
       //console.log("Object to get="+obj);
        objid=document.getElementById(obj);
        var p = jQuery( "#"+obj );
        var position = p.position();

        objtop = position.top;
    }

    //console.log("showhide = " + hideDiv + " , show=" + show+ " this top="+objtop);
     hideDiv = "#" + hideDiv;

    var disp = jQuery(hideDiv).css("display");
    //console.log("showhide disp=" + disp);
    if (disp == "none" && show !== 'FALSE') {
        if(objtop !=0 ){
            jQuery(hideDiv).css('top', objtop+'px');
        }
        jQuery(hideDiv).show("slow");
    } else if (show !== 'TRUE') {
        //console.log("hide " + hideDiv + " slowly");
        jQuery(hideDiv).hide("slow");
    }
}

function shiftPLfocus(el, pl, row) {
    //used for keyboard control of the pricelevel div for current selected row
    if (rowcount>0) {

    if (typeof pl === "undefined") {
        pl = 1;
    }
    if (typeof row === "undefined") {
        row = getSelectedIndex('tbl_sitems');
        console.log("row="+row);
    }
    if (typeof el === "undefined") {
        el = "pl" + row;
    }
    item = document.getElementById('items'+row).value;
    console.log("shiftplfocus el="+el+" row="+row );
    loadXMLDoc("", "getPricelevels", "pl" + row+","+item);
    showhide(el, 'TRUE','pimg'+row); //make sure the pl div open
    showhide('closepl'+row,'TRUE');
    waitUntilExists(el, function () {
        setfoc("pricebutton-" + pl + "-" + row);
    });
    } else {
      alert("Cannot do a price level without items.");
        disabledConfirm_exit = false;
    }
}
function closePL() { //close the current pricelist div
    var row = getSelectedIndex('tbl_sitems');
    var el = "pl" + row;
    showhide(el, 'FALSE');
}
function reload(el) {
    document.getElementById(el).contentWindow.location.reload();
}
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //console.log("Keypress=" + charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45)
        return false;

    return true;
}
function isCurrencyKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //if (charCode ==45 && wmsg !==false) alert ("For credits use a negative quantity.");
    //console.log("Keypress=" + charCode);
    if (charCode > 31 && (charCode < 46 || charCode > 57))
        return false;

    return true;
}
function ispriceKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 45)
        alert("For credits use a negative quantity.");
    //console.log("Keypress=" + charCode);
    if (charCode > 31 && (charCode < 46 || charCode > 57))
        return false;

    return true;
}
function isAlphaKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //console.log("Keypress=" + charCode);
    if ((charCode < 48 || charCode > 122) && charCode != 8 && charCode != 32)
        return false;

    return true;
}
function isProdKey(evt)
{ // works with the prodcode div
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //console.log("Keypress=" + charCode);
    if ((charCode < 48 || charCode > 122) && charCode != 8 && charCode != 32 && charCode != 13) {
        keycounter = 0;
        return false;
    }
    if (TimeoutID === '') {
        TimeoutID = window.setTimeout(clearKeyCounter, 2000);
    }
    keycounter++;
    //console.log("Keycounter now="+keycounter);
    if (charCode == 13) {
        if (keycounter > 4) {//will only happen for barcode entry due to entry speed
            //console.log("isProdKey registers a barcode entry");

        }

    }
    return true;
}
function firePCD() {
    //console.log("FirePCD function");
    // trick to fire the autocomplete's system after barcode entry
    keycounter = 0; // go to zero because otherwise we have an unending loop called on focus.
    var downKeyEvent = $.Event("keydown");
    downKeyEvent.keyCode = $.ui.keyCode.DOWN;  // event for pressing "down" key

    var enterKeyEvent = $.Event("keydown");
    enterKeyEvent.keyCode = $.ui.keyCode.ENTER;  // event for pressing "enter" key


    $("#prodcodeDiv").trigger(downKeyEvent);  // First downkey invokes search
    $("#prodcodeDiv").trigger(downKeyEvent);  // Second downkey highlights first item
    $("#prodcodeDiv").trigger(enterKeyEvent); // Enter key selects highlighted item

}
function clearKeyCounter() {
    keycounter = 0;
    TimeoutID = '';
}
function clearfn(cleardiv) {

    document.getElementById(cleardiv).value = '';

}
function allt(elid) {

    val = document.getElementById('remainder').value;
    laybuy = document.getElementById('laybuy').value;

    if (val !== 0 && laybuy <= 0) {
        if (elid === 'cash') {
            val = document.getElementById('cashlabel').innerHTML; //apply the rounded cash value
        }
        document.getElementById(elid).focus();
        document.getElementById(elid).value = val;
        document.getElementById(elid).blur();
        document.getElementById('submit').focus();
    }

}
function setfoc(elid) {
    //console.log("setfocus at " + elid);
    var element = document.getElementById(elid);
    if (typeof (element) != 'undefined' && element != null) {
        document.getElementById(elid).focus();
    }
}
function clickit(elid) {

    document.getElementById(elid).click();
}
function swapval(el1, el2) {
    var myval = document.getElementById(el1).value;

    document.getElementById(el2).innerHTML = myval;

}
function openInNewTab(url)
{
    var win = window.open(url, '_blank');
    win.focus();
}







function padlength(what) {
    var output = (what.toString().length == 1) ? "0" + what : what;
    return output;
}

function startTime() {
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var datestring = montharray[serverdate.getMonth()] + " " + padlength(serverdate.getDate()) + ", " + serverdate.getFullYear();
    var timestring = padlength(serverdate.getHours()) + ":" + padlength(serverdate.getMinutes()) + ":" + padlength(serverdate.getSeconds());
    document.getElementById("clock").innerHTML = datestring + " " + timestring;
}


function startTime2()
{
    t = setTimeout(function () {
        startTime();
    }, 1000);
}

function checkTime(i)
{
    if (i < 10)
    {
        i = "0" + i;
    }
    return i;
}
function serialize(mixed_value) {
    // http://kevin.vanzonneveld.net
    // +   original by: Arpad Ray (mailto:arpad@php.net)
    // +   improved by: Dino
    // +   bugfixed by: Andrej Pavlovic
    // +   bugfixed by: Garagoth
    // +      input by: DtTvB (http://dt.in.th/2008-09-16.string-length-in-bytes.html)
    // +   bugfixed by: Russell Walker (http://www.nbill.co.uk/)
    // +   bugfixed by: Jamie Beck (http://www.terabit.ca/)
    // +      input by: Martin (http://www.erlenwiese.de/)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net/)
    // +   improved by: Le Torbi (http://www.letorbi.de/)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net/)
    // +   bugfixed by: Ben (http://benblume.co.uk/)
    // %          note: We feel the main purpose of this function should be to ease the transport of data between php & js
    // %          note: Aiming for PHP-compatibility, we have to translate objects to arrays
    // *     example 1: serialize(['Kevin', 'van', 'Zonneveld']);
    // *     returns 1: 'a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}'
    // *     example 2: serialize({firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'});
    // *     returns 2: 'a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}'
    var val, key, okey,
            ktype = '', vals = '', count = 0,
            _utf8Size = function (str) {
                var size = 0,
                        i = 0,
                        l = str.length,
                        code = '';
                for (i = 0; i < l; i++) {
                    code = str.charCodeAt(i);
                    if (code < 0x0080) {
                        size += 1;
                    } else if (code < 0x0800) {
                        size += 2;
                    } else {
                        size += 3;
                    }
                }
                return size;
            },
            _getType = function (inp) {
                var match, key, cons, types, type = typeof inp;

                if (type === 'object' && !inp) {
                    return 'null';
                }
                if (type === 'object') {
                    if (!inp.constructor) {
                        return 'object';
                    }
                    cons = inp.constructor.toString();
                    match = cons.match(/(\w+)\(/);
                    if (match) {
                        cons = match[1].toLowerCase();
                    }
                    types = ['boolean', 'number', 'string', 'array'];
                    for (key in types) {
                        if (cons == types[key]) {
                            type = types[key];
                            break;
                        }
                    }
                }
                return type;
            },
            type = _getType(mixed_value)
            ;

    switch (type) {
        case 'function':
            val = '';
            break;
        case 'boolean':
            val = 'b:' + (mixed_value ? '1' : '0');
            break;
        case 'number':
            val = (Math.round(mixed_value) == mixed_value ? 'i' : 'd') + ':' + mixed_value;
            break;
        case 'string':
            val = 's:' + _utf8Size(mixed_value) + ':"' + mixed_value + '"';
            break;
        case 'array':
        case 'object':
            val = 'a';
            /*
             if (type === 'object') {
             var objname = mixed_value.constructor.toString().match(/(\w+)\(\)/);
             if (objname == undefined) {
             return;
             }
             objname[1] = this.serialize(objname[1]);
             val = 'O' + objname[1].substring(1, objname[1].length - 1);
             }
             */

            for (key in mixed_value) {
                if (mixed_value.hasOwnProperty(key)) {
                    ktype = _getType(mixed_value[key]);
                    if (ktype === 'function') {
                        continue;
                    }

                    okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
                    vals += this.serialize(okey) + this.serialize(mixed_value[key]);
                    count++;
                }
            }
            val += ':' + count + ':{' + vals + '}';
            break;
        case 'undefined':
        // Fall-through
        default:
            // if the JS object has a property which contains a null value, the string cannot be unserialized by PHP
            val = 'N';
            break;
    }
    if (type !== 'object' && type !== 'array') {
        val += ';';
    }
    return val;
}
function gotfocus(el) {
    focal = el.id;
    //console.log("Focus at " + focal);
}
function lostfocus() {
    focal = "";
    //console.log("Focus gone");
}
function addslashes(str) {
    str = str.replace(/\\/g, '\\\\');
    str = str.replace(/\'/g, '\\\'');
    str = str.replace(/\"/g, '\\"');
    str = str.replace(/\0/g, '\\0');
    return str;
}
function stripslashes(str) {
    str = str.replace(/\\'/g, '\'');
    str = str.replace(/\\"/g, '"');
    str = str.replace(/\\0/g, '\0');
    str = str.replace(/\\\\/g, '\\');
    return str;
}
function doQuote() {
    document.getElementById('quote').value = "quote";
}
