
function getVal(item, qty, tax, uprice, price) {
    var insertMeta = ""; var itemname="";
    if (typeof qty === "undefined") {
        qty = 1;
    }
    if (typeof item === "undefined") { // use the same function to get the data from the form, or when restoring session
        var itemtext = document.getElementById("prodcodeDiv").value;
        var arrayitem = itemtext.split(" :: ");
         itemname = arrayitem[0];
        var itemdesc = arrayitem[1];
       console.log("getVal itemname=" + itemname + ",itemdesc=" + itemdesc);
        if (typeof itemdesc !== "undefined") {
            var arritemdesc = itemdesc.split("="); // seperate the id for things like laybuy payments

            if (typeof arritemdesc[0] !== "undefined" && arritemdesc[0] === "Laybuy_id") { // this is a laybuyitem; get id 
                insertMeta = "laybuy=" + arritemdesc[1];
            }
        }
       console.log("getVal item undefined, insertMeta=" + insertMeta);
    } else { //item is defined
        itemname = item;
        console.log("getVal item defined=" + itemname);
    }

    if (itemname != "") {

        $.ajax({
            type: "GET",
            url: "getItems.php",
            data: "item=" + itemname,
            dataType: "json",
            cache: false,
            success: function (result) {

                //addItem(id, iitem, tax, bprice, uprice, price, description, meta, qty, pl, ti)
                // build the function according to above rules
                if (typeof item === "undefined") {
                   console.log("getVal.js : item undefined > additem");
                   console.log("Getval first addItem..." + result);
                    addItem("" + result[0] + "", "" + result[1] + "", "" + result[5] + "", "" + result[3] + "", "" + result[6] + "", "" + result[6] + "", "" + result[7] + "", "" + insertMeta + "", "" + qty + "");
                } else {
                    if (typeof tax === "undefined") {
                        tax = result[5];
                       console.log("posscripts.php - tax=" + tax);
                        qty = 1;
                    }
                   console.log("second additem " + result);
                   console.log("ADDITEM function = result0=" + result[0] + " result1=" + result[1] + " tax=" + tax + " result3=" + result[3] + " uprice=" + uprice + " price=" + price + " result7=" + result[7] + " insertmeta=" + insertMeta + " qty=" + qty + "");
                    addItem("" + result[0] + "", "" + result[1] + "", "" + tax + "", "" + result[3] + "", "" + uprice + "", "" + price + "", "" + result[7] + "", "" + insertMeta + "", "" + qty + "");

                }
            },
            error: function () {
                // no data coming back from this method getting items. how about barcodes?

            },
        });

    }


}

tstFunction = function (message) {
    if (message == "close") {
        $(".pmtoutimg").css("display", "none");
        $("#pmtouts").css("display", "none");
    }
}
        
         
        

function IPC(n, data) { //inter-process-communication for iframes to talk to parent and action
    switch (n)
    {
        case "sr":
// for session resume
            document.getElementById("sessionid").value = data[1];
            var user = data[3];
            document.getElementById("userid").value = data[3];
            //$_SESSION["username"] needs to be reset --or should it rely upon session at all?
            var cust = data[2];
            document.getElementById("customer_id").value = data[2];
            loadXMLDoc("", "getCustomersList", "");

            if (data[5] && data[5] !== "")
                document.getElementById("comment").value = data[5];
            if (data[6] && data[6] !== "")
                document.getElementById("custcomment").value = data[6];
            if (data[7] && data[7] !== "") {
                details = data[7];
               console.log(" resume returned details="+details);
                //clear the table
                jQuery(".itemListing").remove();
                jQuery(".selected").remove();
                //add each item in details, using getVal
                for (t = 0; t < details.length; t++) {
                    var item = details[t][0];
                    console.log("IPC sr item item="+item);
                    var qty = details[t][1];
                    console.log("IPC sr item qty="+qty);
                    var tax = details[t][2];
                    var uprice = details[t][3];
                    var price = details[t][3];    // TODO: problem here with correct discount visible after restore
                   console.log("resume price = "+price);
                    getVal(item, qty, tax, uprice, price);
                }
            }
            break;
        case "su":
// Jump up to supervisor level for this transaction
//console.log("sudata="+data);

            document.getElementById("admincheck").value = "admin";
            // enable the items prices and tax inputs
            jQuery(".dacheck").prop("disabled", false);
            calcS();
            break;

        case "lp":
// do the laybuy payment item
            //console.log("IPC call data ="+data);
            // get the laybuyitem
        laybuyitem = document.getElementById('laybuyitem').value;
        console.log("laybuyitem="+laybuyitem);
if (laybuyitem==""){laybuyitem="Laybuyitem";}
            document.getElementById("prodcodeDiv").value =  laybuyitem +" :: Laybuy_id=" + data;
            getVal();
            showhide("laybuydiv", "FALSE");
            break;
        default:

    }
}
function suspend() {
    window.location = "pos.php?action=logout";
}

jQuery("#nav > li > a").click(function (e) { // binding onclick
    if (jQuery(this).parent().hasClass("selected")) {
        jQuery("#nav .selected div div").slideUp(100); // hiding popups
        jQuery("#nav .selected").removeClass("selected");
    } else {
        jQuery("#nav .selected div div").slideUp(100); // hiding popups
        jQuery("#nav .selected").removeClass("selected");

        if (jQuery(this).next(".subs").length) {
            jQuery(this).parent().addClass("selected"); // display popup
            jQuery(this).next(".subs").children().slideDown(200);
        }
    }
    e.stopPropagation();
});

jQuery("body").click(function () { // binding onclick to body
    jQuery("#nav .selected div div").slideUp(100); // hiding popups
    jQuery("#nav .selected").removeClass("selected");
});

// Add the focus value to class attribute 
jQuery("input,  td, img").focusin(function () {
    $(this).addClass("focus");
}
);
// Remove the focus value to class attribute 
jQuery("input, td, img").focusout(function () {
    $(this).removeClass("focus");
}
);

