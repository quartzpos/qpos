<?php
/*
 *
 *
 * This file is an example of language loading for QuartzPOS, and it is called at the beginning of every PHP file that outputs HTML to the browser,
 * so that the user can get the interface in their own language.
 *
 * If you wish to translate QuartzPOS, just translate this file, and please provide your translation to the developers.
 *
 * For every define it is given an ALL-CAPS name. QuartzPOS uses these as the place-holders in it's code, so please don't change them unless you are modifying code.
 * The part you translate is the second parameter.
 *
 * NOTE: QuartzPOS was built in an English-speaking country for English speakers...if for some reason it doesn't work for you the way that it is, please let us know!
 * ALSO NOTE: English is to be read Left-to-Right. We have not planned for RtL languages at all.If you have some ideas how they could be accomodated
 we would be happy to hear them.
 *
 * Where you are entering the translation, remember to enclose the PHP in "", eg: value="<?php echo MNT_SALES_REBUILD;?>" if it is required
 *
 * For new entries, the rules are loose, but try to do TXT_NAME for one-word entries where it is just a basic term, and MODULENAME_DESCRIPTION for all else. The module name is merely the name of the PHP file used.
 */

// Language definition
define("LANGUAGE_NAME", "English"); //Remember: change the second parameter "English" to whatever language you are translating to; eg. "Klingon"

// Menu entries
define("ADMINMENU_BRANDS", "Brands");
define("ADMINMENU_CATEGORIES", "Categories");
define("ADMINMENU_CUSTOMERS", "Customers");
define("ADMINMENU_PRODUCTS", "Products");
define("ADMINMENU_SUPPLIERS", "Suppliers");
define("ADMINMENU_USERS", "Users");
define("ADMINMENU_SALES", "Sales");
define("ADMINMENU_REPORTS", "Reports");
define("ADMINMENU_PAYMENTS","Payments");
define("ADMINMENU_SEARCH", "Search");
define("ADMINMENU_LAYBUYS", "Laybuys");
define("ADMINMENU_VOUCHERS", "Vouchers");
define("ADMINMENU_EXPORT", "Export");
define("ADMINMENU_IMPORT", "Import");
define("ADMINMENU_SETUP", "Setup");
define("ADMINMENU_MAINTENANCE", "Maintenance");
define("ADMINMENU_PRICELEVELS", "Prices");
define("ADMINMENU_LOGOUT", "Logout");

// messages
define("ADMIN_WELCOME_TEXT", "<p>Welcome to QuartzPOS Point of Sale system.<br>You are currently logged in as an administrator.<br>With administrative rights you have full power over this system.</p>");
define("INSTALL_WARNING","<bold>WARNING !!!</bold><p>The INSTALL.PHP file is still in place. please rename or delete it so that it cannot be used to overwrite your settings.</p>");
define("EDIT_PASSWORD_INFO", "Leave the password field empty if you don't want to change it!");
define("CONFIRM_DELETE_USER", "Do you really want to delete this user?");
define("ERROR_SUPPLIER_SUBMIT", "Field Supplier can't be empty!");
define("CONFIRM_DELETE_SUPPLIER", "Do you really want to delete this supplier?");
define("TXT_ADJUST_ITEM","This item is a duplicate, please adjust it.");
define("SET_TILL","Set the till number below.<br>");
define("NUMERIC_VALUES","Numeric values above zero only.");
define("CONFIRM_DELETE_CUSTOMER", "Do you really want to delete this customer?");
define("AUTH_FAILURE","Username or password incorrect.");
define("TRY_AGAIN","Try again.");
define("SUPER_LOGIN","Supervisor login required for setting till.");
define("QP_TILL_SET","Quartzpos Till Setting");
define("SET_TILL_NO","Set the till number for each workstation that individually connects to the POS server.");
define("BROWSER_WARN","Note that this can be different if the workstation has multiple browsers.");
define("TILL_SET","Till set to");
define("SESSIONS_DESC","Sessions are the individual sales- in progress- on the tabs of the browser windows. If a session is SUSPENDED you can find it here.Likewise, in the event of a browser crash, you may have it here to RESUME.");
define("COMMENTS_MESSAGE","These comments are kept in the company database; not to be displayed to the customer.");
define("INVOICE_MESSAGE","These customer comments are printed on the client invoice.");
define("DEL_MESSAGE","Delete the line item.");
define("CLEAR_MESSAGE","Clear the quantity.");
define("CALCULATE_MESSAGE","Calculate the sale.");
define("SUSPEND_MESSAGE","Suspend the current sale to be resumed later.");
define("SUPERVISOR_MESSAGE","Go into supervisor mode.");
define("RESUME_MESSAGE","Choose a sale to resume.");
define("PAYMENTS_MESSAGE","Payments outstanding:");
define("DISCOUNT_MESSAGE","The amount discounted from retail");
define("PRODCODE_FAILURE","Failure to pass product code to this page.");
define("BRAND_LOADED","That brand name already loaded.");
define("CATEGORY_LOADED","That category is already loaded.");
define("CATEGORY_UNLOADED","No categories loaded.");
define("CATEGORY_PNG","Default : category.png");
define("CUSTOMER_LOADED","That customer already loaded.");
define("CUSTOMER_UNLOADED","No customers loaded.");
define("CLIENT_PRICING","price level. Tax included:");
define("CLIENTS_NOTE1","In order to add a pricelevel to the customer you need to add");
define("CODE_MESSAGE","Code for this category, limited to 5 characters.");
define("IMAGE_SIZE_MESSAGE","Upload a JPG or PNG. Size limit is 4MB.");
define("MSG_NO_MOVEMENTS","No movements for this period.");
//actions
define("CHOOSE_CUSTOMER","Click to choose a customer");
define("CLOSE_SESSIONS_WINDOW","Close the sessions window");
define("CLOSE_SUPERVISOR_WINDOW","Close the supervisor window");
define("CLOSE_COMMENTS_WINDOW","Close the comments window");
define("CLOSE_CUSTCOMMENTS_WINDOW","Close the customer comments window");
define("CLOSE_CATEGORIES_WINDOW","Close the categories window");
define("CLOSE_PAYMENTS_WINDOW","Close the payments window");
define("CLOSE_VOUCHERS_WINDOW","Close the vouchers window");
define("CLOSE_LAYBUYS_WINDOW","Close the laybuys window");
define("CLEAR_PRODCODE","Clear product code");
define("CLICK_CATEGORIES","Click to show the categories");
define("CLICK_COMMENTS","Click to show the comments");
define("CLICK_CUSTCOMMENTS","Click to show the customer comments");
define("CLICK_PAYMENTS","Click to show the payments owing");
define("CHECKOUT_KEY","Checkout. Hotkey alt-z");
define("UP","Up");
define("DOWN","Down");
define("CLEAR","Clear");
define("DEL","Del");
define("ENTER","Enter");
define("SUSPEND","Suspend");
define("CLEAR_ALL","Clear all");
define("CLICK_TO_SEE","Click to see items in ");

//login
define("LOGIN_WELCOME", "Welcome to QuartzPOS!<br>Use a valid username and password to login.");
define("LOGIN_INSTALL_SUCCESS","QuartzPOS has been installed successfully.");
define("LOGIN_USERNAME","A valid username. See the administrator or supervisor if you are not sure.");
define("LOGIN_USERPASS","The user's password");
define("LOGIN_TILLSET","Till is set by the administrator in the administration setup area.");
define("LOGIN_POSACCESS","Access to the POS screen.");
define("LOGIN_ADMINACCESS","Direct access to the administration screens if given valid administrative login.");
define("LOGIN_ADMINLOGIN","Direct access to the administration screens if given valid administrative login.");
// headings
define("LOGIN_ADMIN", "Administrator");
define("LOGIN_PASSWORD", "Password");
define("LOGIN_SUBMIT", "Sales Login");
define("LOGIN_USER", "Username");
define("ADD_NEW_BRAND", "Add New Brand");
define("BRAND_SUBMIT", "Add Brand");
define("HDR_BRAND", "Brand");
define("TXT_DELETE", "Delete");
define("TXT_EDIT", "Edit");
define("TXT_NAME", "Name");
define("ADD_NEW_USER", "Add New User");
define("TXT_FIRSTNAME", " First Name");
define("TXT_LASTNAME", "Last Name");
define("TXT_USERNAME", "Username");
define("LOGIN_CONFIRM_PASSWORD", "Confirm Password");
define("USER_SUBMIT", "Add User");
define("TXT_LOGOUT", "Logout");
define("TXT_SAVE", "Save");
define("TXT_CLOSE", "Close");
define("TXT_DONE","Done");
define("TXT_ADMINISTRATION", "Administration");
define("TXT_NOTE","Note");
define("TXT_SUPPLIER", "Supplier");
define("TXT_ADDRESS", "Address");
define("TXT_CITY", "City");
define("TXT_PCODE", "Postal Code");
define("TXT_STATE", "State");
define("TXT_COUNTRY", "Country");
define("TXT_PHONE", "Phone");
define("TXT_CONTACT", "Contact");
define("TXT_EMAIL", "E-Mail");
define("TXT_COMMENTS", "Comments");
define("TXT_PRICELEVELS","Pricelevels");
define("TXT_PRICELEVEL","Pricelevel");
define("TXT_MARKUP", "Markup");
define("TXT_MOVEMENT", "Movement");
define("CUSTOMER_COMMENTS","Customer comments");
define("SUPPLIER_SUBMIT", "Add Supplier");
define("ADD_NEW_SUPPLIER", "Add New Supplier");
define("VERSION","Version");
define("TXT_ACCOUNT_NUMBER", "Account n.");
define("CUSTOMER_SUBMIT", "Add Customer");
define("TXT_CLEARANCE","Clearance");
define("TXT_CUSTOMER", "Customer");
define("TXT_SESSION","Session");
define("TXT_SUPERVISOR","Supervisor");
define("TXT_USER","User");
define("TXT_CHANGED","Changed");
define("TXT_RESUME","Resume");
define("TXT_PRODUCTCODE", "Add Item");
define("ADD_NEW_CUSTOMER", "Add New Customer");
define("CHECKOUT","CHECKOUT");
define("TXT_OF"," of "); //for example: 3 of 6
define("TXT_FIND", "Find");
define("TXT_ITEM", "Item");
define("TXT_NOTES","Notes");
define("TXT_QTY", "Qty");
define("TXT_TAX", "Tax");
define("TXT_PRICE", "Price");
define("TXT_LINE", "Line");
define("TXT_TILL","Till");
define("TXT_CODE","Code");
define("TXT_SUBTOTAL", "Sub-Total");
define("TXT_TOTAL", "Total");
define("TXT_DISCOUNT","Discount");
define("TXT_QUOTE","Quote");
define("TXT_IMAGE", "Image");
define("TXT_CURRENTLY","Currently");
define("TXT_CATEGORY", "Category");
define("TXT_PRICING","Pricing");
define("TXT_ADD_CATEGORY", "Add Category");
define("TXT_FIND_ITEMS", "Find Items");
define("ADD_NEW_ITEM", "Add New Item");
define("TXT_ITEM_NAME", "Item Name");
define("TXT_ITEM_NUMBER", "Item Number (SKU)");
define("TXT_BRAND", "Brand");
define("TXT_DIMENSIONS","Dimensions");
define("TXT_STOCK_CONTROL","Stock Control");
define("TXT_SUPPLIER_ITEM_NUMBER", "Supplier Item Number");
define("TXT_BUYING_PRICE", "Buying Price");
define("TXT_TRADE_PRICE", "Trade Price");
define("TXT_UNIT_PRICE", "Unit Price");
define("TXT_WHOLESALE_PRICE", "Wholesale Price");
define("TXT_TAX_PERCENT", "Tax Percent");
define("TXT_SELLING_PRICE", "Retail Price");
define("TXT_STOCK", "Stock");
define("TXT_REORDER_LEVEL", "Reorder level");
define("TXT_LOCATION", "Location");
define("TXT_LOCATIONS", "Locations");
define("TITLE_LOCATION","Where the item is located in the store.");
define("TXT_SUPPLIER_URL","Supplier URL");
define("TITLE_SUPPLIER_URL","What webpage the supplier uses for this item.");
define("TXT_WEBSITE_URL","Website URL");
define("TITLE_WEBSITE_URL","Where on our company website this item sits.");
define("TXT_NET_WEIGHT", "Net Weight");
define("TITLE_NET_WEIGHT","This item's weight unpackaged.");
define("TXT_GROSS_WEIGHT", "Gross Weight");
define("TITLE_GROSS_WEIGHT","This item's weight fully packaged.");
define("TXT_NET_HEIGHT", "Net Height");
define("TITLE_NET_HEIGHT","This item's height unpackaged.");
define("TXT_GROSS_HEIGHT", "Gross Height");
define("TITLE_GROSS_HEIGHT","This item's height fully packaged.");
define("TXT_GROSS_DEPTH", "Gross Depth");
define("TITLE_GROSS_DEPTH","This item's depth fully packaged.");
define("TXT_GROSS_WIDTH", "Gross Width");
define("TITLE_GROSS_WIDTH","This item's width fully packaged.");
define("TXT_NET_WIDTH", "Net Width");
define("TITLE_NET_WIDTH","This item's width unpackaged.");
define("TXT_NET_DEPTH", "Net Depth");
define("TITLE_NET_DEPTH","This item's depth unpackaged.");
define("TXT_STYLE", "Style");
define("TITLE_STYLE","This item's style.");
define("TXT_COLOUR", "Colour");
define("TITLE_COLOUR","This item's colour.");
define("TXT_MATERIALS", "Materials");
define("TITLE_MATERIALS","This item's materials.");
define("TXT_MANUFACTURE_COMPONENTS", "Manufacturing Components");
define("TITLE_MANUFACTURE_COMPONENTS","This item's manufacturing components when utilising other items.");
define("TXT_PACKAGING_TYPE", "Packaging type");
define("TITLE_PACKAGING_TYPE","This item's type of packaging.");
define("TXT_CERTIFICATIONS", "Certifications");
define("TITLE_CERTIFICATIONS","This item's certifications by governmental bodies.");
define("TXT_AWARDS", "Awards");
define("TITLE_AWARDS","This item's awards won in the industry.");
define("TXT_HAZARDS", "Hazards");
define("TITLE_HAZARDS","This item's known hazards.");
define("TXT_ITEM_IMAGE", "Item Image");
define("TXT_DESCRIPTION", "Description");
define("TXT_PRICE_LEVEL","Price level");
define("TXT_TYPE","Type");
define("TXT_SALES","Sales");
define("TXT_ADMIN","Admin");
define("TXT_SYSTEM","System");
define("TXT_REPORTS","Reports");
define("TXT_TRANSACT","Transaction");
define("TXT_FROM","from");
define("TXT_FOR","for");
define("TXT_QUANTITY","Quantity");
define("TXT_TO","To");
define("TXT_BY","by");
define("TXT_TIME","Time");
define("TXT_DATE","Date");
define("TXT_PAYMENT","Payment");
define("ADD_ITEM", "Add Item");
define("ABBR_NUMBER","No.");
define("NEGATIVE","No");
define("POSITIVE","Yes");
define("TXT_SUBMIT", "Submit");
define("TILL_NUMBER", "Till number");
define("BARCODES_FOR","Barcodes for");
define("TXT_BARCODES","Barcodes");
define("TXT_LANGUAGE","Language");
define("TXT_SUSPENDED","Suspended");
define("TXT_STAFF","Staff");
define("TXT_MANAGER","Manager");
define("TXT_ADMINISTRATOR","Administrator");
define("TXT_AGEING_ENTRIES","Ageing entries");
define("NO_LOCATIONS_MSG","Add locations after creating item.");
define("NO_BARCODES_MSG","Add barcodes");
define("NO_BARCODES_TITLE","There are no barcodes for this item. Please add them where appropriate.");
define("BARCODES_TITLE","There are barcodes for this item. Click to view and edit.");
define("TITLE_PRICING","Click to show/hide pricing area.");
define("TITLE_STOCK_CONTROL","Click to show/hide stock control area.");
define("TITLE_DIMENSIONS","Click to show/hide dimensions area.");
define("TITLE_STYLECOL","Click to show/hide style area.");
define("TITLE_SYSTEM","Click to show/hide system area.");
//functions
define("BT_PARAMETERS","The buildtable was not passed the right parameters.");
define("BT_TABLENAME","Tablename not passed.");
define("BT_TABLEID","TableID not passed.");
define("BT_STYLEPROPS","Style properties needs to be an associative array; not correctly passed.");
define("BT_TABLEPROPS","Table properties needs to be an associative array; not correctly passed.");
define("BT_DATA","Data for the table needs to be an associative array; not correctly passed.");
define("BT_HEADER","The table header needs to be an array; not correctly passed.");
define("BT_FUNCTIONS","Please examine functions.php.");
define("PDF_MISSING","Bad or non-existing PDF file");

//maintenance
define("ST_MSG","Daily sales totals rebuilt. Plotting should be fine now.");
define("MNT_DB_CLR","Successfully cleared bad transactions.");
define("MNT_DB_CLR_FAIL","Database error clearing bad transactions.");
define("MNT_EDITOR","Click to do manual data manipulation.");
define("MNT_EDITOR_NAME","DB Editor");
define("MNT_BACKUP_NAME","DB Backup");
define("MNT_OPTIMIZE_NAME","DB Optimize");
define("MNT_SALES_REBUILD","Sales Totals Rebuild");
define("MNT_CLEAR_BAD","Clear Bad Transactions");
define("MNT_HELP_MSG","We can run the clearing maintenance by CURL on a cronjob; example: curl -F 'clear=1' http://quartzpos.com/maintenance.php");
define("MAINT_BACKUP_SUCCESS","File successfully backed up ");
define("MAINT_BACKUP_FAIL","Backup failed! Failed to write to file system using");
define("MAINT_OPTIMI","Optimized");
define("MAINT_OPTIFAIL","Failed to optimize");
//PAYMENTS
define("PAY_VOID","Void Sale");
define("PAY_EDIT","Edit Sale");
define("PAY_REF","Account payment requires reference to account number");
define("PAY_VREF","Voucher payment requires reference to voucher number or detail");
define("PAY_CHANGE","Change");
define("PAY_MADE","Payment made of");
define("PAY_INSUFF","This amount insufficient");
define("PAY_TITLE","Quartzpos Payments");
define("PAY_OWED","Enter the payment below for amount owing");
define("PAY_CREDIT","Enter the method of credit for");
define("PAY_EFTTITLE","Pay all on EFTPOS. Hotkey alt-e");
define("PAY_VTITLE","Pay all on Visa. Hotkey alt-v");
define("PAY_ATITLE","Pay all on Amex. Hotkey alt-a");
define("PAY_MTITLE","Pay all on Mastercard. Hotkey alt-m");
define("PAY_VOTITLE","Pay all on Voucher. Hotkey alt-h");
define("PAY_ACTITLE","Pay all on Account. Hotkey alt-x");
define("PAY_CTITLE","Pay all on Cash. Hotkey alt-c");
define("PAY_QTITLE","Pay all on Cheque. Hotkey alt-q");
define("PAY_LTITLE","Pay all on Laybuy. Hotkey alt-l");
define("PAY_LAYTITLE","Type in any laybuy comments.");
define("PAY_COMTITLE","Type in any payment comments.");
define("PAY_EFT","EFT");
define("PAY_VISA","Visa");
define("PAY_AMEX","Amex");
define("PAY_MC","Mastercard");
define("PAY_VOUCHER","Voucher");
define("PAY_ACCOUNT","Account");
define("PAY_CASH","Cash");
define("PAY_ROUNDED","Rounded value");
define("PAY_CHEQUE","Cheque");
define("PAY_LAYBUY","Laybuy");
define("PAY_ALL","All");
define("PAY_REMAIN","Remainder");
define("PAY_TOTAL","Total");
define("PAY_COMMENTS","Comments");
define("PAY_SUBMIT","A4Receipt");
define("PAY_SUBMITR","SmallReceipt");
define("PAY_SUBTITLE","A4 receipt. Hotkey Alt-z");
define("PAY_SUBTITLER","Small receipt. Hotkey Alt-r");
define("PAY_ERR_PREV","There has been a payment for this transaction previously.");
define("PAY_ERR_PAY","Amount of payment wrong.");
define("PAY_ERR_SID","Sale id must be numeric.");
define("PAY_ERR_CID","Customer id must be numeric.");
define("PAY_ERR_LAY","Failure to insert into laybuys table.");
define("PAY_ERR_ULAY","Failure to update laybuys table.");
define("PAY_MSG1_LAY","Laybuy Balance");
define("PAY_MSG2_LAY","now in credit for laybuy id");
//PMTOUT
define("PMT_NONE","None outstanding");
define("PMT_CLOSE","Close");
//POS
define("POS_HK_ITEM","Item search : alt+i");
//POSSCRIPTS
define("PS_VOID","Do you really want to void this transaction?");
define("PS_INPCH","Input changed");
//PRICELEVELS
define("PL_NMRQ","Name of new pricelevel is required.");
define("PL_PLM1","The price level called");
define("PL_PLM2","already exists. Edit it here");
define("PL_UPID","You can only update the ID# or name at one time.");
define("PL_DPL","This could have serious repercussions! Do you really want to delete price level");
define("PL_ID","ID#");
define("PL_NAME","Name");
define("PL_TAX","Tax included");
define("PL_MARKUP","Markup");
define("PL_MARKUP_TITLE","Use an integer above 0; it is the percentage markup.");
define("PL_BASE","Base");
define("PL_RETAIL","Retail");
define("PL_TRADE","Trade");
define("PL_WHOLE","Wholesale");
define("PL_STOCK","Stock");
define("PL_SUSP","Suspended");
define("PL_STAFF","Staff");
define("PL_SUPER","Supervisor");
define("PL_MGR","Manager");
define("PL_ADMIN","Administrator");
define("PL_IDMSG","Only edit ID# if you understand the potential repercussions!");
define("PL_ADD","Add a new price level");
define("PL_PERM","Permission Level");
define("PL_SUBMIT","Submit");
define("PL_HELP1","Markup only required if you are using a standard for the selected price level. For discount use negative, with base of retail.");
define("PL_HELP2","The 'Base' refers to the columns of the items table in database. Use the default unless otherwise advised.");
define("PL_HELP3","The 'security level' is what level of");
define("PL_HELP4","Sales permission");
define("PL_HELP5","is required to be able to choose this pricelevel.");
define("PL_USERTITLE","Click to see the current permissions set");
//processsale
define("PS_HELP1","General failure getting the form variables from the POS screen. Please contact your QuartzPOS support.");
define("PS_HELP2","Click to return to ");
define("PS_HELP3","POS");
define("PS_HELP4","Error log created " );
define("PS_HELP5","Failure to post all required variables to processsale.php.");
define("PS_HELP6","Wrote error file: ");
define("PS_HELP7","Failure to write error file. File system permission problem?");

//USERS
define("TXT_HELPKEY","Help Key");
define("TXT_INDEX","Index");
define("TXT_PERMISSION_LEVEL","Permission Level");
define("USER_ADDED","Added user");
define("USER_UNABLE","User already exists. Unable to create user with username of :" );
define("VERIFY_FIELDS","Error; verify fields! All are required.");

//EDITVOUCHER
define("VOUCHER_HEADER","Fill Voucher Details");
define("VOUCHER_ID","ID");
define("VOUCHER_CODE","Code");
define("VOUCHER_DATE","Date");
define("VOUCHER_CUSTID","Customer ID");
define("VOUCHER_RECIPIENT","Recipient");
define("VOUCHER_PURCH","Purchase value");
define("VOUCHER_CURRENT","Current Value");
define("VOUCHER_EXPIRY","Expiry");
define("VOUCHER_CUSTCONTACT","Customer contact");
define("VOUCHER_RECPCONTACT","Recipient contact");

//DAILYTOTALS
define("DAYS_FINISH","Days set, date to finish");
define("DATE_IS","Date is");
define("COUNT_IS","Count is");
define("FINISH_IS","Finish is");
define("DAYS_ARE","Days are");
define("TRUNCATE_IS","Truncate is");
define("SALES_TRUNCATED","Sales_totals truncated.");
define("SALES_CHECK","Checking for sales data from");
define("SALES_FOUND","Sales data found for");
define("TIMESTAMP_SET","timestamp set to ");
define("SQL_ADDED","Added ");
define("TIMESTAMP_CHANGED","Timestamp changed to ");
define("DATE_CHANGED","Date changed to ");
define("FINISHED_INSERT","Finished inserting.");

//SAMBA
define("SMB_MSG"," Trying the import system across the network...");
define("SMB_FAIL_MSG"," Import Failure! Consult network support. Failure in:");
define("SMB_SUCC_MSG"," Import Success!");
define("SMB_CLOSE"," Trying the import system across the network...");
define("SMB_BACK","Back to Config");

//EXPORTCONFIG
define("USERNAME_REQD"," A username is required.");
define("PASSWORD_REQD"," A password is required.");
define("PATH_REQD"," A host path is required.");
define("HOST_REQD"," A host dir is required.");
define("FILENAME_REQD"," A file name is required.");
define("EXPORT_CONFIG","Export configuration");
define("EXPORT_HELP","Example: bob. This user must have SSH write access on the host, to the remote filename specified below.");
define("ADD_USERNAME","Add the User's username.");
define("ADD_USERPASSWORD","Add the User's password. It will display differently after saving.");
define("HOST_PATH","Host path");
define("ADD_PATH","Add the path to the host where the files reside. Ask your network administrator.");
define("PATH_EXAMPLE","Example: 192.168.0.1(:port if not 22)");
define("HOST_DIR","Host directory");
define("HOST_DIR_TITLE","Add the host directory where the files reside. Leave off final slash.");
define("HOST_DIR_EXAMPLE","Example: company/shop1");
define("FILE_NAME","File name");
define("FILE_NAME_TITLE","Add the filename for your export file.");
define("FILE_NAME_EXAMPLE","Example: filename.ext");

//LAYBUYS
define("LB_TITLE","Laybuys");
define("LB_ID","ID");
define("LB_FIRSTNAME","FIRST NAME");
define("LB_LASTNAME","LAST NAME");
define("LB_SALEID","SALE ID");
define("LB_BALANCE","BALANCE");
define("LB_PAYMENTS","PAYMENTS");
define("LB_DUEDATE","DUEDATE");
define("LB_NOTES","NOTES");
define("LB_MESSAGE","This table lists the laybuys. You can change the Duedate and Notes. A balance of zero or above means that the sale is fully paid for!");
define("LB_SAVE","Save Changes");
define("LB_NOFOUND","No laybuys found.");
define("LB_CHOOSE","Choose");
define("LB_SELECT","Select Laybuy ID to pay down.");
define("LB_DETAILS","LAYBUY DETAILS :");
define("LB_CREDIT","** IN CREDIT **");

//SETUP
define("SETUP_CTTITLE","Choose which theme to run");
define("SETUP_CO","Company Setup");
define("SETUP_TITLE","Configure company options");
define("SETUP_TIMEZONE","Timezone");
define("SETUP_TZHELP","The timezone the QuartzPOS server  is in.");
define("SETUP_EXPORT","Export");
define("SETUP_EXTITLE","What application do you wish to export to?");
define("SETUP_IMPORT","Import");
define("SETUP_IMTITLE","What application do you wish to import from?");
define("SETUP_CASH","Cash Rounding");
define("SETUP_CSHTITLE1","Round what values down to next value; comma-separated values");
define("SETUP_CSHTITLE2","Round what values up to next value; comma-separated values ");
define("SETUP_CSHTITLE3","Smallest coin denomination (decimal value eg .10 for 10 cents)");
define("SETUP_CSHHELP","Example: Swedish rounding= 1,2,3,4,5 DOWN || 6,7,8,9 UP || .10 Dollars");
define("SETUP_CURRENCY","Currency");
define("SETUP_CURTITLE","Only change currency if really necessary!");
define("SETUP_DOLLARS","Dollars");
define("SETUP_AUTOCOMP","Auto-complete");
define("SETUP_ACHELP","On the sales screen there is an auto-complete dropdown for the Find Products area.");
define("SETUP_VCHR","Voucher setup");
define("SETUP_VCHRHELP","Use comma delimited list of ITEM  that are vouchers.");
define("SETUP_LAYBUYS","Laybuys setup");
define("SETUP_LYBYHELP1","Use product code of ITEM  used  for laybuy payment.");
define("SETUP_LYBYHELP2","How long a laybuy runs for in seconds.");
define("SETUP_THEME","Theme setup");
define("SETUP_THMEHELP","The theme to run.");
define("SETUP_TILL","Till");
define("SETUP_TILLTITLE","Click to set till number for this workstation.");
define("SETUP_SAVE","Save settings");
define("SETUP_EWTITLE","Close the export window");
define("SETUP_IWTITLE","Close the import window");
define("SETUP_CWTITLE","Close the company setup window");
//SUPER
define("SUPER_MSG1","In order to go to a higher level of privilege, you need a supervisor login.");
define("SUPER_MSG2","Set to supervisor");
define("SUPER_MSG3","This is only for the immediate transaction; administrative functions are not available.");
//SUPPLIERS
define("SUPPLIERS_NONE","No suppliers loaded.");
//SEARCH
define("SEARCHING_FOR","Searching for");
define("SEARCH_IN","in");
define("SEARCH_NORES","No results found for search");
define("SEARCH_USEBOX","Use the box under For to search for something!");
define("SEARCH_ON","Search On");
define("SEARCH_FOR","for");
define("SEARCH_OVERRIDES","Overrides");
define("SEARCH_RESULTS","Results");

//VOUCHERS
define("VCH_MSG1","Expiry is less than date.");
define("VCH_MSG2","Date is empty.");
define("VCH_MSG3","Expiry is empty.");
define("VCH_MSG4","Voucher saved.");
define("VCH_MSG5","Are you sure you want to delete this voucher?");
define("VCH_ID","ID");
define("VCH_SID","Sale ID");
define("VCH_CODE","Code");
define("VCH_DATE","Date");
define("VCH_CID","Customer ID");
define("VCH_RCPT","Recipient");
define("VCH_COST","Cost");
define("VCH_VALUE","Value");
define("VCH_EXPIRY","Expiry");
define("VCH_CUSTCON","Customer Contact");
define("VCH_RCPTCON","Recipient Contact");
define("VCH_VLIST","Vouchers list");
define("VCH_PRINT","PRINT");
define("VCH_PRNTVCH","Print voucher");
define("VCH_EDITVCH","Edit voucher");

//VOUCHERPRINT
define("VP_QPV","QuartzPOS Voucher");
define("VP_DIE","For the voucher window you need to have a valid voucher number. ");
define("VP_PRINTTITLE","Print. Hotkey Alt-p");
define("VP_CLOSETITLE","Close. Hotkey Alt-c");
define("VP_GV","Gift Voucher");

define("VP_VTO","Value To");
define("VP_VALID","Valid until");
define("VP_DATEISS","Date Issued");
define("VP_INITVAL","Initial Value");
define("VP_VCHID","Voucher ID");
//VOID
define("VOID_TITLE","QuartzPOS Void Sale");
define("VOID_DEL","That sale deleted now.");
define("VOID_HEAD","Void Sale");
define("VOID_HELP1","Click the VOID button below only if you wish to void this sale! A log of this is kept, but the sale is deleted from the system.");
define("VOID_FAIL","Failed to find sale with that id.");
define("VOID_ITMTOT","Item Total");
define("VOID_VWTRANTITLE","View transaction");
define("VOID_HELP2","The sale id is not a valid sale!");
define("VOID_HELP3","No sale id passed.");
// Company details
define("COMPANY_NAME","Company Name");
define("COMPANY_ADDRESS1","Company Address 1");
define("COMPANY_ADDRESS2","Company Address 2");
define("COMPANY_ADDRESS3","Company Address 3");
define("COMPANY_PHONE1","Company Phone 1");
define("COMPANY_PHONE2","Company Phone 2");
define("COMPANY_FAX","Company Fax");
define("COMPANY_EMAIL","Company Email");
define("COMPANY_WEBSITE","Company Website");
define("COMPANY_TAX_NO","Company Tax No.");
define("COMPANY_SETUP","Company Setup");

define("TXT_FREEPHONE","Freephone");
define("TXT_FAX","Fax");
define("TXT_CELLULAR","Mobile");

define("TXT_WEBSITE","Website");
define("SALES_HEADER1","Sales Header 1");
define("SALES_HEADER2","Sales Header 2");
define("SALES_FOOTER1","Sales Footer 1");
define("SALES_FOOTER2","Sales Footer 2");
define("RECEIPT_LOGO","Receipt Logo");
define("QUOTE_FOOTER","Quote Footer");
?>
