<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com

    

  Released under the GNU General Public License
*/
// In the languages directory there must be a directory named for the language, and a file in that directory with same name (and case!) followed by suffix .php

$cwd=getcwd(); // current working directory
chdir('languages'); // go to languages directory in order to parse languages
$languages = array();
$directories = glob( '*' , GLOB_ONLYDIR); // read the current directory to see if we have subdirectories
foreach($directories as $i=>$d){
    if(file_exists($d.'/'.$d.'.php')){ // check the subdirectories for the required langauge file
        $languages[]=$d;
    }
}
chdir($cwd); //change back to current working directory
if(!isset($_SESSION['language'])){
    
    require_once(POS_DEFAULT_LANGUAGE ."/".POS_DEFAULT_LANGUAGE. ".php");
} else {
    if(file_exists($_SESSION['language']."/".$_SESSION['language'].".php")){
    require_once($_SESSION['language']."/".$_SESSION['language'].".php");
    } else {
        require_once(POS_DEFAULT_LANGUAGE ."/".POS_DEFAULT_LANGUAGE. ".php");
    }
}


?>
