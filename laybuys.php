<?php

/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */

//if (!isset($_SESSION)){
//    session_start();
//}
// if (!isset($_SESSION['admin'])) {
//     header("Location:admin.php");
// }
require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");

if (!class_exists('FB')) {
    require_once 'consoleLogging.php';
}
require_once 'functions.php';
////if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION,"Session=");
////if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_GET,"GET=");
//if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST,"POST=");
// CREATE TABLE IF NOT EXISTS `laybuys` (
//   `id` int(11) NOT NULL AUTO_INCREMENT,
//   `saleid` int(11) NOT NULL,
//   `balance` float NOT NULL COMMENT 'Balance owing',
//   `pmts` text COMMENT 'Payment records',
//   `duedate` date DEFAULT NULL COMMENT 'Date the laybuy expires',
//   PRIMARY KEY (`id`))
if (isset($_POST['submitlby'])) {
    unset($_POST['submitlby']);
    foreach ($_POST as $k => $v) {
        $idarr = explode('-', $k);
        $ids = $idarr[1];
        $field = $idarr[0];
        if ($field == 'pmts') {
            $val = serialize($v);
        } else {
            $val = $v;
        }
        $sql = "UPDATE laybuys SET $field='$val' WHERE id=$ids";
        //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql, 'laybuys.php post sql=');
        $db->query($sql);
    }
}
if (isset($list)) {
    $_GET['list'] = $list;
}
if (isset($_GET['list'])) {
    $sql = "SELECT laybuys.id, customers.first_name,customers.last_name,laybuys.sale_id, laybuys.balance,laybuys.pmts,laybuys.duedate,laybuys.notes
FROM laybuys
JOIN sales ON laybuys.sale_id = sales.id
JOIN customers ON sales.customer_id = customers.id ";
    if ($_GET['list'] != 'all' && is_numeric($_GET['list'])) {
        $sql.="WHERE sale_id = '" . $db->clean($_GET['list']) . "'";
    } else if ($_GET['list'] == 'all') {
        // no need to do anything
    } else { //sent garbage, let's make it up from the last insert
        $xsql = "SELECT MAX(sale_id) FROM laybuys";
        $lastid = $db->QPResults($xsql);
        ////if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($lastid, 'laybuys.php last sale id');
        $_GET['list'] = $lastid['MAX(sale_id)'];
        $sql.="WHERE sale_id = '" . $db->clean($_GET['list']) . "'";
    }
    $liststr = "&list=" . $_GET['list'];

    // eg Array ( [0] => 20131008-16:19:23 [1] => 320 [2] => Array ( [0] => 20131008-16:19:30 [1] => 320 ) [3] => Array ( [0] => 20131008-16:20:21 [1] => 320 ) )
    // needs to be :
    // (([0]=payment date [1]=payment amount),([0]=payment date [1]=payment amount))
    //print $row['saleid']."=".$row['balance']." ".print_r()."<br>";
    //  print_r($pmtsua);
    //print'<td>'.$row['saleid'].'</td><td>'.$row['balance'].'</td><td>';
    $url='';

if($_SERVER['SCRIPT_NAME'] ==="/admin.php"){ $url = "admin.php?action=laybuys&"; } else { $url = "laybuys.php?"; }
    $output = '<form name="laybuys" action="'.$url.'submit=true' . $liststr . '" method="post">';
    $output .= '<div class="admin_content">';

$output .= '<table name="laybuys" id="laybuys" cellspacing="0" style="width:800px;" title="Laybuys" cellspacing="0"><thead class="laybuys-thead">';

$output .= '<th class="laybuys-th" width="30px">'.LB_ID.'</th>';
$output .= '<th class="laybuys-th" width="60px">'.LB_FIRSTNAME.'</th>';
$output .= '<th class="laybuys-th" width="60px">'.LB_LASTNAME.'</th>';
$output .= '<th class="laybuys-th" width="30px">'.LB_SALEID.'</th>';

$output .= '<th class="laybuys-th" width="30px">'.LB_BALANCE.'</th>';
$output .= '<th class="laybuys-th" width="90px">'.LB_PAYMENTS.'</th>';
$output .= '<th class="laybuys-th">'.LB_DUEDATE.'</th>';
$output .= '<th class="laybuys-th">'.LB_NOTES.'</th></tr></thead><tbody>';

    //$tbn = "laybuys";
    //$tbid = "laybuys";
    //$tbs = array('width' => '800px');
    //$tbp = array('title' => LB_TITLE, 'cellspacing' => '0');
    //$props = array($tbn, $tbid, $tbs, $tbp);
    //
    //$data['header'] = array(
    //    LB_ID => array('width' => '50px'),
    //    LB_FIRSTNAME => array('width' => '50px', 'fieldtype' => array('disabled')),
    //    LB_LASTNAME => array('width' => '50px', 'fieldtype' => array('disabled')),
    //    LB_SALEID => array('width' => '50px', 'fieldtype' => array('disabled')),
    //    LB_BALANCE => array('width' => '50px', 'fieldtype' => array('disabled')),
    //    LB_PAYMENTS => array('width' => '50px', 'fieldtype' => array('textarea', 'disabled')),
    //    LB_DUEDATE => array('width' => '50px', 'fieldtype' => array('date')),
    //    LB_NOTES => array('width' => '50px', 'fieldtype' => array('textarea')),
    //);
    //$data['footer'] = array(
    //    'message' => LB_MESSAGE,
    //    'style' => 'padding:10px;color:#111;',
    //);
    //$data['formname'] = "laybuysfrm";

    $data = $db->QPComplete($sql);
    $sel = array();

    foreach ($data as $key => $value) {
         $output .= '<tr>';
         if ($value['pmts']) {
            $arr = unserialize($value['pmts']);

            $ars="";
            foreach($arr as $k=>$v){
                $ars .= implode(',', $v);$ars .=" \r\n ";
            }

            $data[$key]['pmts'] = $ars;
        }
        if ($value['id']) {
            $sel[] = $value['id'];
        }
        foreach($value as $r=>$d){
            $output .= '<td class="tvalue">';
            switch ($r){
                case 'id';
                case 'first_name';
                case 'last_name';
                case 'sale_id';
                case 'balance';
                    $output.='<div id="'.$r.'-'.$value['id'].'" ><a target="_blank" href="admin.php?action=reports&id='.$value['sale_id'].'">'.$d.'</a></div>';
                    break;
                case 'pmts';
                    $output.='<div id="'.$r.'-'.$value['id'].'" >'.$ars.'</div>';
                    break;
                case 'duedate';
                     $output.='<input type="date" name="'.$r.'-'.$value['id'].'" id="'.$r.'-'.$value['id'].'" value="'.$d.'">';
                    break;
                 case 'notes';
                     $output.='<textarea name="'.$r.'-'.$value['id'].'" id="'.$r.'-'.$value['id'].'">'.$d.'</textarea>';
                    break;
                   default;
                    break;

            }
            $output .= '</td>';

        }
        $output .= '</tr>';
    }
        $output .="</tbody></table>";

$output .= '<div style="padding:10px;color:#111;">'.LB_MESSAGE.'</div>';
    //$output.=buildtable($props, $data, true, 'laybuys');
    $output.="<input type='submit' value='".LB_SAVE."' name='submitlby'>";
} else {
    $output = LB_NOFOUND;
}


$output.='</div>';
$output.='</form><br />';
if (!isset($list)) { //only allow a pay down from the pos screen, as the admin interface sets $list and does not have capability to accept payment
    if (!empty($sel)) {
        // create a select system that contains the IDs of the laybuys, send the ID to the parent of the container, to intiate a payment line
        $output .="<p>".LB_SELECT."</p>";
        $output .="<select name='payoff' id='payoff'>";
        foreach ($sel as $k => $v) {
            $output.="<option value='$v'>$v</option>";
        }
        $output .="</select>";
        $output .="<button name='paydown' id='paydown' type='button' onclick='getId();'>".LB_CHOOSE."</button>";
    }
    $headscripts = "<script type='text/javascript'>function getId(){
    var Id = document.getElementById('payoff').value;
   console.log('Id='+Id);
    var data=new Array();
    data[0]=Id;
    parent.IPC('lp', data);
    }</script>";
}
$extrascripts = ' jQuery(function() {console.log("datepicker read");
    jQuery( ".datepick" ).datepicker(
    { dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true });
   console.log("datepicked");
        });
        ';
include 'template.php';
print $output;
?>
