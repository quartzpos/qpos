<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */
// Locations handler
if (!isset($_SESSION))
    session_start();
if (!isset($_SESSION['admin'])) {
    header("Location:admin.php");
}
require_once 'helpers.php';
require_once 'functions.php';
if(isset($_POST['submit_location'])){
    // handle the posted fields
    foreach ($_POST as $key => $val) {
        $_POST[$key] = $db->clean($_POST[$key]);
    }
}
if(isset($_GET)){
    // handle the posted fields
    foreach ($_GET as $key => $val) {
        $_GET[$key] = $db->clean($_GET[$key]);
    }
}
if(isset($_GET['item'])){
    $_POST['item'] = $_GET['item'];
}
function form_items(){

            global $db;
            $result = $db->QPComplete("select id,item_number, item_name from items");

            $output = 'Item <select name="item" onchange="getStock();"><option value="-1">---</option>';
            foreach($result as $k=>$v){
                 if (isset($_POST['item']) && $_POST['item'] == $v['id']) { $item = ' SELECTED '; } else { $item= ' ';}
                 $output .= '<option value="'.$v['id'].'"'.$item.' >'.$v['item_number'].'  '.$v['item_name'].'</option>';

            }
            $output .= '</select>';
            return $output;
        }

function qtyStock($items_id, $location_id = 0, $mode = "REMAINS"){
            // retrieve the stock quantity from the items table, subtract ALL quantities from the locations table, to return the expected (not enforced) quantity
            // edit the location stock possibly
            // MODES = REMAINS, UPDATE
            global $db;
            $retarr = Array();
            if(is_numeric($items_id)){
            $itemqty = $db->QPResults("SELECT quantity FROM items WHERE id=".$items_id);
            $locs = $db->QPComplete("SELECT quantity FROM items_locations WHERE items_id=".$items_id);
            $count = 0;
            if($locs){
            foreach($locs as $k=>$v){
                $count += $v['quantity'];
            }
            }
            switch ($mode){
                case 'REMAINS': // return the remaining qty
                    $retarr['itemqty'] = $itemqty;
                    $retarr['remains'] = $itemqty - $count;
                    break;
                case 'UPDATE':
                   // if()
                        break;
            }
            return $retarr;
        }
}
?>
<div class="admin_content">
<?php
if (isset($_GET['edit'])) {
    $sql= "select * from items_locations where id=" . $_GET['edit'];
    $rows = $db->QPComplete($sql);
    ?>
        <div id="location_form"><form action="admin.php?action=locations" enctype="multipart/form-data" method="post">
            <input type="hidden" name="location_id" value="<?php echo $rows[0]['id']; ?>">
            <TABLE>

                <TR><TD><?php echo TXT_CATEGORY; ?></TD><TD><input type="text" name="category" size="30" value="<?php echo htmlspecialchars($row[1]); ?>"> </TD></TR>
                <TD><?php echo TXT_CODE; ?></TD><TD><input type="text" name="ccode" value="<?php echo htmlspecialchars($row[3]); ?>" title="<?php echo CODE_MESSAGE; ?>"> </TD></TR>
                <TR><TD><?php echo TXT_IMAGE; ?></TD><TD><input type="file" id="category_image" name="category_image" title="<?php echo IMAGE_SIZE_MESSAGE; ?>"> </TD></TR>
                </TABLE>
            <input type="submit" name="submit_location" id="submitlocation" value="<?php echo TXT_SAVE; ?>">
        </div>
    <?php
    if (isset($row['2'])) {
        echo " <div id='cat_image'><label id='imagelabel'>".TXT_CURRENTLY.": " . $row['2'] . "</label>";

        echo'&nbsp;&nbsp;<img src="' . $row['2'] . '" id="image" class="category-image" style="max-width:400px;" />';

        //insert a button to clear the image to default
        ?>
                            <input type="button" value="Clear" onclick="defaultimage();" />
                            <input type="hidden" name="imagedefault" id="imagedefault" value="" />
                            <script type="text/javascript">
                                function defaultimage() {
                                    document.getElementById('imagedefault').value = "1";
                                    document.getElementById('imagelabel').innerText = "<?php echo CATEGORY_PNG; ?>";
                                    document.getElementById('image').src = "<?php echo $themepath;?>images/categories/categories.png";

                                }
                            </script>
                            </form></div>
                            <?php
                        }
                        ?>




        <?php
    }  else {
        ?>
        <form action="admin.php?action=locations" enctype="multipart/form-data" method="POST">
            <table>

                <TR>
                    <TD><?php echo TXT_ITEM; ?></TD>
                    <TD><?php echo form_items(); ?></TD>
                </TR>
                 <TR>
                    <TD><?php echo TXT_LOCATION; ?></TD>
                    <TD><textarea name="location" id="location" title="<?php echo TITLE_LOCATION; ?>"></textarea></TD>
                </TR>
                <TR>
                    <TD><?php echo TXT_IMAGE; ?></TD>
                    <TD><input type="file" name="category_image" id="category_image" title="<?php echo IMAGE_SIZE_MESSAGE; ?>"></TD>
                </TR>
                <TR>
                    <TD><input type="submit" name="submitcategory" value="<?php echo TXT_ADD_CATEGORY; ?>"></TD>
                    </TR>
            </table>
        </form>
        <br>
    <?php
    $sql = "select * from categories order by category ";
    $result = $db->query($sql);
    $res = $db->query($sql);
    $rowchk = $db->fetchRow($res);
    if (!empty($rowchk[1])) {
        ?>
            <table cellspacing="0">
                <TR><TH colspan="2" width="400"><?php echo TXT_CATEGORY; ?></TH><TH><?php echo TXT_EDIT; ?></TH><TH><?php echo TXT_DELETE; ?></TH></TR>
            <?php
            while ($row = $db->fetchRow($result)) {
                if (empty($row[2])) {
                    $row[2] = $themepath . "images/categories/category.png";
                }
                ?>
                    <TR><TD width="<?php echo CATEGORY_IMG_SIZE; ?>" class="tvalue"><img width="<?php echo CATEGORY_IMG_SIZE; ?>" height="<?php echo CATEGORY_IMG_SIZE; ?>" src="<?php echo $row[2]; ?>"></TD><TD class="btvalue"><a href="admin.php?action=products&category=<?php echo $row[0]; ?>"><?php echo htmlspecialchars($row[1]); ?></a></TD><TD class="tvalue" align="center"><a href="admin.php?action=categories&edit=<?php echo $row[0]; ?>"><img src="<?php echo $themepath; ?>images/edit.png" title="<?php echo TXT_EDIT." ".htmlspecialchars($row[1]); ?>" /></a></TD><TD class="tvalue" align="center"><a href="admin.php?action=categories&delete=<?php echo $row[0]; ?>"><img src="<?php echo $themepath; ?>images/delete.png" title="<?php echo TXT_DELETE." ".htmlspecialchars($row[1]); ?>" /></a></TD></TR>
                    <?php
                }
                ?>
            </table>
                        <?php
                    } else {
                        echo CATEGORY_UNLOADED;
                    }
                    if (isset($output)) {
                        echo $output;
                    }
                }
                ?>
</div>
