<?php
//Login
if (!isset($_SESSION)){$session_start();}
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION, "LOGIN php included, Session=");
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST, "Login.php POST=");
$ref = debug_backtrace();
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($ref[0]['file'], "Login.php caller = ");

if (isset($_POST['userlogin']) || isset($_POST['adminlogin']) ) {
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION, "LOGIN php: userlogin session=");

        $sql = "select * from users where username='" . trim($_POST['username']) . "' and password='" . md5(trim($_POST['userpassword'])) . "'";

    if($row = $db->QPComplete($sql)){
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($row," login row=");
    //IF LOGIN OK

        if(isset($row[0]['id'])){
        $_SESSION['user'] = $row[0]['id'];
        $_SESSION['admin'] = $row[0]['admin'];
        $_SESSION['system'] = $row[0]['system'];
        $_SESSION['sales'] = $row[0]['sales'];
        $_SESSION['reports'] = $row[0]['reports'];
        $_SESSION['stock'] = $row[0]['stock'];
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION, "LOGIN php, set session, Session=");
        }
    } else {
        echo "Failure to login with those credentials.<br/>";
    }



    if (isset($_SESSION['user'])) {
        if ($_SESSION['user'] == 'admin') {
            $_SESSION['username'] = 'Admin';
        } else {
            $result = $db->query("select * from users where id='" . trim($_SESSION['user']) . "'");
            //IF LOGIN OK
            if ($result) {
                $row = $db->fetchRow($result);

                $_SESSION['username'] = $row[1] . ' ' . $row[2];
               //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION, "LOGIN php, set session username , Session=");
            }
        }
    }
}

if (isset($_COOKIE["quartzpos"])) {
    $tillpersist = $_COOKIE["quartzpos"];
    if (isset($_SESSION['user']))
        $_SESSION['till'] = $_COOKIE["quartzpos"];
}
if (!isset($_SESSION['till']) && !isset($_SESSION['tillcheck']) && empty($tillpersist)) {
    header("Location:settill.php?notset=1");
}

if (isset($_SESSION['user']) && isset($_SESSION['till'])) {
    $userid = $_SESSION['user'];
    if ($userid == "admin") {
        $userid = 0;
    }
    $date = date('YmdHis');
    if (!isset($_SESSION['trans'])) {
        $_SESSION['id'] = ($date . '-' . $userid . '-' . $_SESSION['till']);
       //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION, "LOGIN php sessid setter; set sessionid TRANS, Session=");
    }

    $_SESSION['trans'] = $_SESSION['id'];

   //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION, "LOGIN php, set session TRANS, Session=");
}

//if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION,"Session vars=");
// make the session id Ymd date . userid . till#. last transid+1
function loginnow($params = NULL) {
    $path = '';
    if (isset($pathex))
        $path = $pathex;

    include("dbconfig.php");
    require_once("languages/languages.php");

    require_once("database.php");
    $db = new database($dbhost, $dbuser, $dbpassword, $dbname);
    require_once 'functions.php';

    $res = $db->QPResults("SELECT value FROM system WHERE name='theme'");
    if (empty($res['value'])) {
        $res['value'] = "flat";
    }
    $themepath = $path . 'qpthemes/' . $res['value'] . '/';
    ?>
    <div id="loginformdiv">
        <center><b class="blt"><?php echo LOGIN_WELCOME; ?></b>
    <?php
    if ($params == 'installed') {
        echo"<b>".LOGIN_INSTALL_SUCCESS."</b>";
    }
    ?>
            <form action="" method="POST" class="loginform" autocomplete="off">
                <table align="center" class="tlogin">
                    <TR>
                        <TD align="right" width="40%"><?php echo LOGIN_USER; ?></TD>
                        <TD width="60%"><input type="text" name="username" id="username" value="" title="<?php echo LOGIN_USERNAME; ?>"></TD>
                    </TR>
                    <TR>
                        <TD align="right" width="40%"><?php echo LOGIN_PASSWORD; ?></TD>
                        <TD width="60%"><input type="password" name="userpassword" id="userpassword" autocomplete="off" value="" title="<?php echo LOGIN_USERPASS;?>"></TD>
                    </TR>
    <?php if ($params != 'super') { ?><TR>
                            <TD align="right" width="40%"><?php echo TILL_NUMBER; ?></TD>
                            <TD width="60%"><input type="input" name="till" value="<?php
        if (isset($_SESSION['till'])) {
            $tillpersist = $_SESSION['till'];
        } else {
            if (isset($_COOKIE['quartzpos'])) {
                $tillpersist = $_COOKIE['quartzpos'];
            }
        }
        if (isset($tillpersist))
            echo $tillpersist;
        ?>"
                                                   disabled title="<?php echo LOGIN_TILLSET;?>"></TD>
                        </TR>
                        <TR>
                            <td style="text-align:right"><img src ="<?php echo $themepath; ?>images/sale.png" title="Sales login" style="vertical-align: bottom;" /></td>
                            <TD><input type="submit" name="userlogin" value="<?php echo LOGIN_SUBMIT; ?>" onclick='this.form.action = "pos.php?action=login";' title="<?php echo LOGIN_POSACCESS;?>"/></TD>
                        </TR>
                        <tr>
                            <td  style="text-align:right"><img src ="<?php echo $themepath; ?>images/boss.png" title="Admin login" style="vertical-align: bottom;"/></td>
                            <td ><input type="submit" name="adminlogin" value="<?php echo TXT_ADMINISTRATION; ?>"
                                        onclick='this.form.action = "admin.php?action=login";'title="<?php echo LOGIN_ADMINACCESS;?>"/></td>
                        </tr><?php
                //this is the  end of the check for params super - coming from the super.php
            } else {
        ?>
                        <tr>
                            <td  style="text-align:right"><img src ="<?php echo $themepath; ?>images/boss.png" title="Admin login" style="vertical-align: bottom;"/></td>
                            <td ><input type="submit" name="adminlogin" value="<?php echo TXT_ADMINISTRATION; ?>"
                                        onclick='this.form.action = "super.php?action=login";' title="<?php echo LOGIN_ADMINLOGIN;?>"/></td>
                        </tr>


    <?php }
    ?>
                </table>
            </form></center>
        <script type="text/javascript"> document.getElementById("username").focus();</script>
    </div>
<?php }
?>
