<?php
if (!isset($_SESSION))
    session_start();
require_once("config.php");
require_once("languages/languages.php");
require_once 'consoleLogging.php';
require_once 'dailytotals.php';
require_once("database.php");


require_once 'functions.php';
// we can run the backup maintenance by CURL on a cronjob
// example: curl -F 'backup=1' http://quartzpos.com/maintenance.php
if (isset($_POST['backup']) && $_POST['backup'] === "1") {

    function backup_db($dbhost = 'localhost', $dbuser, $dbpass, $dbname) {
        $cwd = getcwd() . '/';
        $backupFile = $cwd . BACKUP_FILE_PATH . $dbname . date("Y-m-d-H-i") . '.sql.gz'; // full filesystem path for bash script
        $fileDownload = BACKUP_FILE_PATH . $dbname . date("Y-m-d-H-i") . '.sql.gz'; // downloadable path
        if (!file_exists($backupFile)) { //only write if not existing
            $command = "mysqldump --opt -h $dbhost -u $dbuser --password=$dbpass $dbname | gzip > $backupFile";
            $check = system($command);
        }
        if (file_exists($backupFile)) {
            return $fileDownload;
        } else {
            return array($command);
        }
    }

    $backup = backup_db($dbhost, $dbuser, $dbpassword, $dbname);
    if (!is_array($backup)) {
        $msg = MAINT_BACKUP_SUCCESS.":<a href='" . $backup. "' >". $backup."</a>";
    } else {
        $msg = MAINT_BACKUP_FAIL.":<br>" . $backup[0];
    }
}
if (!isset($_SESSION['admin']) && !isset($_POST['backup'])) {
    header("Location:admin.php");
}
if (isset($_POST['clean']) && $_POST['clean'] === "1") {
    $cmsg = '';
    $alltbls = $db->query("show tables");
    while ($table = $db->fetchAssoc($alltbls)) {
        foreach ($table as $dba => $tablename) {
            if($db->query("OPTIMIZE TABLE `$tablename`"))
            { $cmsg .=MAINT_BACKUP_SUCCESS." $tablename. ";

            } else {
            $cmsg.=MAINT_BACKUP_FAIL." $tablename. ";
            }
        }
    }
}
if (isset($_POST['inprunst']) && $_POST['inprunst'] === "1") {
    $stmsg = '';
    header('Location:admin.php?action=maintenance&dt_rebuild=1');
        $stmsg = ST_MSG;

}
// we can run the clearing maintenance by CURL on a cronjob
// example: curl -F 'clear=1' http://quartzpos.com/maintenance.php
if (isset($_POST['clear']) && $_POST['clear'] === "1") {
    $clrmsg = '';
    $sql="DELETE sales, sales_items FROM sales LEFT JOIN sales_items ON sales_items.sale_id=sales.id WHERE sales.state<>'Quote' AND sales.paid_with IS NULL";
    $del = $db->query($sql);
    if($del){
        $clrmsg .=MNT_DB_CLR;
    } else {
        $clrmsg .=MNT_DB_CLR_FAIL;
    }
}
?>
<script type="text/javascript">
    function runbackup() {
       console.log("runbackup");
        jQuery("#backup").val(1);//set the value of hidden input
        jQuery("#submit").click();//force immediate form submit
    }
    function runclean() {
       console.log("runclean");
        jQuery("#clean").val(1);//set the value of hidden input
        jQuery("#submit").click();//force immediate form submit
    }
     function runst() {
       console.log("runst");
        jQuery("#runst").val(1);//set the value of hidden input
        jQuery("#submit").click();//force immediate form submit
    }
      function runclear() {
       console.log("runclear");
        jQuery("#clear").val(1);//set the value of hidden input
        jQuery("#submit").click();//force immediate form submit
    }
</script>

<div class="admin_content">
    <form action="admin.php?action=maintenance" name="maintform" id="maintform" method="post">
        <table >
            <?php
                                    if (isset($_SESSION['system']) && $_SESSION['system']>3) { ?>
            <tr><td><a href="pma/index.php" title="<?php echo MNT_EDITOR; ?>" target="_blank"><input type="button" value="<?php echo MNT_EDITOR_NAME;?>" /></a></td>
                <td></td><td><?php echo TXT_USERNAME.':'. $dbuser.' | '.LOGIN_PASSWORD.':'.$dbpassword; ?></td></tr> <?php } ?>
            <?php if (isset($_SESSION['admin']) && $_SESSION['admin']>1) { ?>
            <tr><td><input type="button" value="<?php echo MNT_BACKUP_NAME;?>" onclick="runbackup();"/><input type="hidden" value="" name="backup" id="backup" /></td>
                <td></td><td><?php
                    if (isset($msg)) {
                        echo $msg;
                    }
                    ?></td></tr>
            <tr><td><input type="button" value="<?php echo MNT_OPTIMIZE_NAME;?>" onclick="runclean();"/><input type="hidden" value="" name="clean" id="clean" /></td>
                <td></td><td><?php
                    if (isset($cmsg)) {
                        echo $cmsg;
                    }
                    ?></td></tr>
            <tr><td><a href="admin.php?action=maintenance&dt_rebuild=1&verbose=1"><input type="button" value="<?php echo MNT_SALES_REBUILD;?>" /></a></td>
                <td></td><td><?php
                    if (isset($out)) {
                        echo $out;
                    }
                    ?></td></tr>
            <tr><td><input type="button" value="<?php echo MNT_CLEAR_BAD;?>" onclick="runclear();"/><input type="hidden" value="" name="clear" id="clear" /></td>
                <td></td><td><?php echo MNT_HELP_MSG;?>

                    <?php
                    if (isset($clrmsg)) {
                        echo $clrmsg;
                    }
                    ?></td></tr>
            <tr><td ><input type="submit" name="submitmaint" id="submit" value="<?php echo TXT_SUBMIT;?>"></td></tr>
            <?php } ?>
        </table>
    </form>

</div>
