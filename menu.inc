<!-- Clickable Nav -->
<?php if (isset($_SESSION['username'])): ?>
		<div class="click-nav">
			<ul id="nav">
				<li>
					<a class="clicker">System</a>
					<ul id="menu1">
						<!--<li><a onclick="showhide('menu1');" href="#">Dashboard</a></li>
						<li><a onclick="showhide('menu1');" href="#">Settings</a></li>
						<li><a onclick="showhide('menu1');" href="#">Privacy</a></li>
						<li><a onclick="showhide('menu1');" href="#">Help</a></li>-->
                        <li><a onclick="showhide('menu1');" href="pos.php?action=logout"><?php echo TXT_LOGOUT; ?></a><span class="hotkeylabel">Alt-6</span></li>
					</ul>
					
				</li>
				<li>
					<a class="clicker">Functions</a>
					<ul id="menu2">
						
                        <li><a href="#" onclick="showhide('menu2');showhide('vouchers','TRUE');">View Gift Vouchers </a><span class="hotkeylabel">Alt-2</span></li>
						<li><a href="#" onclick="showhide('menu2');showhide('super','TRUE');">Supervisor</a><span class="hotkeylabel">Alt-s</span></li>
						<li><a href="#" onclick="showhide('menu2');showhide('laybuydiv','TRUE');">Laybuy payment</a><span class="hotkeylabel">Alt-3</span></li>
						<li><a href="#" onClick="showhide('menu2');reload('sessframe');showhide('sessions','TRUE')">Resume</a><span class="hotkeylabel">Alt-4</span></li>
						
						<li><a href="#" onClick="showhide('menu2');javascript:loadXMLDoc('', 'saveSession'); suspend();">Suspend</a><span class="hotkeylabel">Alt-5</span></li>
                        <li><a href="#" onClick="showhide('menu2'); shiftPLfocus();">PriceLevels</a><span class="hotkeylabel">Alt-p</span></li>
                        <li><a href="#" onClick="calcS();disabledConfirm_exit = true;doQuote();checkout();">Quote</a><span class="hotkeylabel">Alt-q</span></li>
						<li><a href="#" onClick="calcS();disabledConfirm_exit = true;checkout();">Checkout</a><span class="hotkeylabel">Alt-z</span></li>
					</ul>
				</li>
			</ul>
		</div>
		<?php endif; ?>
		<!-- /Clickable Nav -->