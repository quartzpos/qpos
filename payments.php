<?php
/*
 * payments.php
 *
 * Copyright 2013 onyx <onyx@onyxlaptop>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
if (!isset($_SESSION))
    session_start();
require_once('config.php');
require_once("languages/languages.php");

require_once('database.php');
require ('functions.php');
require_once 'rounding.php';
require('consoleLogging.php');
$path = '';
if (isset($pathex))
    $path = $pathex;

$res = $db->QPResults("SELECT value FROM system WHERE name='theme'");
$themepath = $path . 'qpthemes/' . $res['value'] . '/';
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("Arriving at payments.php REQUEST=", $_REQUEST);
if(isset($_GET['iframe'])){
    $iframe=TRUE;
} else {
    $iframe=FALSE;
}
//Logout
if (isset($_GET['action']) && $_GET['action'] == "logout") {
    if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_REQUEST,"payments.php line 47 REQUEST after logout");
    if (isset($_SESSION['admin'])) {
        session_destroy();
        header("Location:admin.php");
    } else {
        session_destroy();
        header("Location:payments.php");
    }
}
if (isset($_POST['id'])) {
    $_GET['id'] = $_POST['id'];
}
if (!isset($_GET['id'])) {
    die("No valid sale id given.");
}
$laybuys = Array();
if (isset($_POST['laybuyfrm'])) {
    $_GET['laybuy'] = $_POST['laybuyfrm'];
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_GET['laybuy'], "payments.php set GETlaybuy");
}
if (isset($_GET['laybuy'])) {
    $laybuystopay = $_GET['laybuy'];

    $laybuystmparr = explode(",", $laybuystopay);
    foreach ($laybuystmparr as $key => $val) {
        $split = explode(":", $val);
       if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($val, "payments.php val of laybuy=");
        $laybuys[] = Array($split[0] => $split[1]);
       if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($laybuys, "payments.php laybuys=");
    }
}
$id = $_GET['id'];
$smallcoin = $db->QPResults("SELECT value FROM system WHERE name='smallcoin'");
            $smallcoin = $smallcoin['value'];
$sql = "SELECT * FROM sales WHERE id='$id'";

$row = $db->QPResults($sql);
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($row, "Sale data=");
if(!isset($row['sale_total_cost'])){ die ('Payments.php has experience a failure in talking to the database. Please report this with date and time and a copy of the database backup. Last SQL='.$sql);}
$cost = $row['sale_total_cost'];
$custid = $row['customer_id'];

if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST, 'payments post vars');
$pmt = Array();
$msg = '';
$block = 0;
$prev = 0;
$rcpt = '';
if (!empty($row['paid_with'])) {
    // there is already a payment for this id
    $msg.=PAY_ERR_PREV;

    $pmtp = unserialize(base64_decode($row['paid_with']));
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($pmtp, 'previous pmt');
    $prev = 1;
    foreach ($pmtp as $key => $val) {
        $_POST[$key] = $val;
    }
    $_POST['remainder'] = $_POST['total'];
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST, 'mod post vars');
    $sql = "UPDATE `sales` SET  `paid_with` =NULL WHERE `id` ='" . $id . "'"; // remove the existing payment, as expecting changes
    $result = $db->query($sql);
}


if (!empty($_POST)) {
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST, 'payments post not empty');
   if(isset($_POST['submit'])){
      if($_POST['submit'] ==='SmallReceipt'){ $rcpt="small"; } else {$rcpt='a4';}
   }
    if (isset($_POST['admin'])){ $_GET['admin'] = $_POST['admin']; }
    if (!empty($_POST['account']) && empty($_POST['accountref'])) {
        $msg.="<div class='red'>".PAY_REF.".</div>";
        $block = 1;
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("blocked due to account ref not there");
    }
    if (!empty($_POST['comment'])){
      $_POST['comment']= str_replace(',', '-', $_POST['comment']);
    }
    if (!empty($_POST['voucherref']))
        $_POST['voucherref'] = str_replace(',', '-', $_POST['voucherref']); //remove commas as they can cause issues in the export file
    if (!empty($_POST['accountref']))
        $_POST['accountref'] = str_replace(',', '-', $_POST['accountref']);
    if (!empty($_POST['laybuynotes'])) {
        $notes = $_POST['laybuynotes'];
    } else {
        $notes = '';
    }
    if (!empty($_POST['voucher']) && empty($_POST['voucherref'])) {
        $msg.="<div class='red'>".PAY_VREF.".</div>";
        $block = 1;
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("blocked due to voucher ref not there");
    }
    if (!empty($_POST['laybuy']) && is_numeric($_POST['laybuy'])) {
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST['laybuy'], "Laybuy=");
        $laybuyamt = -1 * trim($_POST['laybuy']);
        if ($laybuyamt < 0) {

            $res = make_laybuy_payment($laybuyamt, null, null, $id, $custid, 'check', null, $notes, false); //this begins a new laybuy or extends it ... not paying it down
            if (!empty($res)) {
                $failmsg = $res;
                $prev = 1;
            }
        }//end   if ($laybuyamt < 0)
    }
    if (empty($prev)) {
        $remallow='0.01';
        if(isset($_POST['smallcoin']) && !empty($_POST['smallcoin'])){
            $remallow=$_POST['smallcoin']; // remainder allowance to allow small inconsequential values through
        }
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST['remainder'], "payments.php post remainder line 149");
        if (floatval($_POST['remainder']) < $remallow) { //valid payment completed
           if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log(floatval($_POST['remainder']), 'remains:');
            if (floatval($_POST['remainder']) < 0 && abs(floatval($_POST['remainder']))>=$remallow){
                $changeraw= abs(floatval($_POST['remainder']));
                 if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($changeraw, "Changeraw=");
                $change=getRounding($changeraw);
                if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($change, "Change=");
                $change= number_format($change, 2);
                if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($change, "Change=");
                $changemsg = PAY_CHANGE."<hr><strong>$" .$change . "</strong>";
               if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($remallow, 'remallow');
            }
            foreach ($_POST as $key => $val) {

                if (!empty($val) && $key!="admin") {
                    $pmt[$key] = $val;
                }
               if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($pmt, 'Pmt array=');
            }
           // $cost = 0;
//send payment array $pmt to database if not blocked
            if ($block == 0 && $prev == 0 && !empty($pmt)) {
                $endmsg = "<br>".PAY_MADE." $" . $_POST['total'] . ". ";
                $pmmt = base64_encode(serialize($pmt));// this was changed to avoid issue charcter encoding http://davidwalsh.name/php-serialize-unserialize-issues
                $sql = "UPDATE sales SET  `paid_with` =  '" . $pmmt . "', `state` = 'completed', `payment_comment` ='".$db->clean($_POST['comment'])."' WHERE `id` ='" . $id . "'";
                $result = $db->query($sql);

                //do the laybuy payment down
                foreach ($laybuys as $k => $v) { //print_r($v);
                    foreach ($v as $key => $value) {
                        make_laybuy_payment($value, null, $key, $id, $custid, false, null, null, true);
                    }


                }
                if (isset($endmsg)) {
                    echo $endmsg;
                    if (isset($changemsg) && !empty($changemsg)) {
                        ?>
                        <link href="<?php echo $path; ?>js/plugins/css/jsmodal-light.css" rel="stylesheet"  type="text/css">
                        <script type="text/javascript" language="javascript" src="<?php echo $path; ?>js/plugins/js/jsmodal-1.0d.js"></script>
                        <script>
                            function closepmts() {
                                window.location = "sale.php?id=<?php echo $id; ?>&rcpt=<?php echo $rcpt; ?>&closewin=1
                                <?php if(isset($_GET['admin'])){echo'&admin='.$_GET['admin'];}
                                if(isset($_GET['laybuy'])){echo'&laybuy='.$_GET['laybuy'];}
                                ?>";
                            }
                            document.write('<div id="modal-1"></div>');
                            document.getElementById('modal-1').onclick = function () {
                                Modal.open({
                                    content: '<?php echo $changemsg; ?>',
                                    width: '15%',
                                    hideclose: true, // Hides the close-modal graphic
                                    closeCallback: function () {
                                        closepmts()
                                    }
                                    //closeAfter: 60 // Define this number in seconds. //TODO: make this a configurable option for admin
                                });
                            }
                        </script>
                        <?php
                    }
                    ?><script type="text/javascript">
                        if (document.getElementById('modal-1')) {
                            document.getElementById('modal-1').click();
                        } else {
                            window.location = "sale.php?id=<?php echo $id; ?>&rcpt=<?php echo $rcpt; ?>&closewin=1<?php if(isset($_GET['admin'])){echo'&admin='.$_GET['admin'];} if(isset($_GET['laybuy'])){echo'&laybuy='.$_GET['laybuy'];} ?>";
                        }
                    </script>
                    <?php
                }
            }
        } else {
            $msg.="<br>".PAY_INSUFF.": $<span class='amount'>" . floatval($_POST['total']) . "</span>";
        }
    }
    if((isset($_POST['A4Receipt']) || isset($_POST['SmallReceipt'])) && isset($_POST['cost'])){
        $cost = $_POST['cost'];
    }
}


?>
<html>
    <head>
        <title><?php echo PAY_TITLE;?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8">
        <link href="<?php echo $themepath; ?>pos.css" rel="StyleSheet" type="text/css">
        <script type="text/javascript" src="js/jquery.min.js"></script>


        <script language="javascript" src="js/general.js"></script>
        <script language="javascript">
                    $(document).ready(function () {


                        $('.price').blur(function () {
                            var sum = 0;
                            var cost =<?php echo isset($cost) ? $cost : '0'; ?>;
                           console.log("payments.php .price cost=" + cost);
                            $('.price').each(function () {
                                if (Number($(this).val()) !== 0) {
                                    sum += Number($(this).val());
                                   console.log(this.id + sum);
                                }
                            });
                            var remain = cost - sum;
                            remain = remain.toFixed(2);
                            $('#total').val(sum.toFixed(2));
                            $('#remainder').val(remain);
                            cashRound(remain);
                        });

                        //var rem= document.getElementById("remainder").value;
                        //cashRound(rem);

                        document.getElementById('cash').focus();
                            document.getElementById('eft').focus();


                    });

                    function closed() {

                        window.location = "sale.php?id=<?php echo $id; ?>&closewin=1<?php
                        if(isset($_GET['admin'])){echo'&admin='.$_GET['admin'];}
                        if(isset($_GET['laybuy'])){echo'&laybuy='.$_GET['laybuy'];}
                        ?>";
                    }
        </script>
        <script language="javascript" src="js/payments.js"></script>



    </head>
    <body class="pmtbody">
<div class="functions" style=" margin:0 auto;padding-bottom: 10px;padding-top:10px;border:1px solid black;background:#ccc;">
                    <?php if (isset($_SESSION['sales']) && $_SESSION['sales']>2){  //do void only if manager or better
        ?><a href="void.php?sale=<?php echo $id;
                    if($iframe){echo'&iframe=1';}
                    ?>"><span class="void"><input type="button" value="<?php echo PAY_VOID;?>" /></span></a><?php } ?>
                <a href="pos.php?editsale=<?php echo $id; ?>" onclick=" parent.window.location = 'pos.php?editsale=<?php echo $id; ?>';"><span class="editsale"><input type="button" value="<?php echo PAY_EDIT;?>" /></span></a>
    </div>
        <div id="payments">

            <?php

            if ($cost || $block == 1 || $prev == 1) {
                if ($cost >= 0) {
                    $msg.="<br>".PAY_OWED.": $<span class='amount'>" . $cost . "</span>";
                } else {
                    $msg.="<br><p class='red credit'>".PAY_CREDIT.": $<span class='amount'>" . $cost . "</span></p>";
                }
                if (isset($msg)) {
                    echo $msg;
                }

                if (isset($failmsg)) {
                    echo '<br>' . $failmsg . '<br>';
                }
                ?>
                <form name="payment" id="payment" action="payments.php" method="POST">

                    <table id="pmtform">
                        <th width="200px">Type</th><th>Amount</th><th>&lArr;</th><th>Ref</th>
                        <tr class="pmtr"><td class="pmtd" style="background:url(<?php echo $themepath; ?>images/eftpos.png) no-repeat top right;height:32px;"><?php echo PAY_EFT;?></td><td class="pmtd"><input name="eft" id="eft" class="price" autocomplete="off" type="text" onkeypress="return isCurrencyKey(event);" value="<?php echo (isset($_POST['eft'])) ? $_POST['eft'] : ''; ?>"/></td><td><input type="button" id="eftall" onclick="allt('eft');"  autocomplete="off"  value="<?php echo PAY_ALL;?>" title="<?php echo PAY_EFTTITLE;?>"/></td></tr>
                        <tr class="pmtr"><td class="pmtd" style="background:url(<?php echo $themepath; ?>images/visa.png) no-repeat top right;height:32px;"><?php echo PAY_VISA;?></td><td class="pmtd"><input name="visa" id="visa" type="text" autocomplete="off" class="price" onkeypress="return isCurrencyKey(event);" value="<?php echo (isset($_POST['visa'])) ? $_POST['visa'] : ''; ?>" /></td><td><input type="button" id="visaall"  autocomplete="off"  onclick="allt('visa');" value="<?php echo PAY_ALL;?>" title="<?php echo PAY_VTITLE;?>"/></td></tr>
                        <tr class="pmtr"><td class="pmtd" style="background:url(<?php echo $themepath; ?>images/amex.png) no-repeat top right;height:32px;"><?php echo PAY_AMEX;?></td><td class="pmtd"><input name="amex" id="amex" type="text" autocomplete="off" class="price"  onkeypress="return isCurrencyKey(event);" value="<?php echo (isset($_POST['amex'])) ? $_POST['amex'] : ''; ?>"/></td><td><input type="button" id="amexall"  autocomplete="off"  onclick="allt('amex');" value="<?php echo PAY_ALL;?>" title="<?php echo PAY_ATITLE;?>"/></td></tr>
                        <tr class="pmtr"><td class="pmtd"style="background:url(<?php echo $themepath; ?>images/mastercard.png) no-repeat top right;height:32px;"><?php echo PAY_MC;?></td><td class="pmtd"><input name="mastercard" id="mastercard" autocomplete="off"class="price" type="text" onkeypress="return isCurrencyKey(event);" value="<?php echo (isset($_POST['mastercard'])) ? $_POST['mastercard'] : ''; ?>"/></td><td><input  autocomplete="off"  type="button" id="mastercardall" onclick="allt('mastercard');" value="<?php echo PAY_ALL;?>" title="<?php echo PAY_MTITLE;?>"/></td></tr>
                        <tr class="pmtr"><td class="pmtd"style="background:url(<?php echo $themepath; ?>images/voucher.png) no-repeat top right;height:32px;"><?php echo PAY_VOUCHER;?></td><td class="pmtd"><input name="voucher" id="voucher" autocomplete="off" class="price" type="text" onkeypress="return isCurrencyKey(event);" value="<?php echo (isset($_POST['voucher'])) ? $_POST['voucher'] : ''; ?>" /></td><td><input type="button" id="voucherall" onclick="allt('voucher');" value="<?php echo PAY_ALL;?>" title="<?php echo PAY_VOTITLE;?>"/></td><td class="pmtd"><input name="voucherref"  autocomplete="off"  type="text" value="<?php echo (isset($_POST['voucherref'])) ? $_POST['voucherref'] : ''; ?>" /></td></tr>
                        <tr class="pmtr"><td class="pmtd"style="background:url(<?php echo $themepath; ?>images/account.png) no-repeat top right;height:32px;"><?php echo PAY_ACCOUNT;?></td><td class="pmtd"><input name="account" id="account" autocomplete="off" class="price" type="text" onkeypress="return isCurrencyKey(event);" value="<?php echo (isset($_POST['account'])) ? $_POST['account'] : ''; ?>"/></td><td><input type="button" id="accountall" onclick="allt('account');" value="<?php echo PAY_ALL;?>" title="<?php echo PAY_ACTITLE;?>"/></td><td class="pmtd"><input name="accountref" type="text"  value="<?php echo (isset($_POST['accountref'])) ? $_POST['accountref'] : ''; ?>"/></td></tr>
                        <tr class="pmtr"><td class="pmtd"style="background:url(<?php echo $themepath; ?>images/cash.png) no-repeat top right;height:32px;"><?php echo PAY_CASH;?></td><td class="pmtd"><input name="cash" id="cash" class="price" type="text" autocomplete="off" onkeypress="return isCurrencyKey(event);"value="<?php echo (isset($_POST['cash'])) ? $_POST['cash'] : ''; ?>" /></td><td><input type="button" id="cashall" onclick="allt('cash');" value="<?php echo PAY_ALL;?>" title="<?php echo PAY_CTITLE;?>"/></td><td><label>&Lt; <?php echo PAY_ROUNDED;?>:$</label><label id="cashlabel"><?php print getRounding($cost, 0);?></label></td></tr>
                        <!-- bye bye cheque -->
                        <tr class="pmtr"><td class="pmtd" style="background:url(<?php echo $themepath; ?>images/laybuy.png) no-repeat top right;height:32px;"><?php echo PAY_LAYBUY;?></td><td class="pmtd"><input name="laybuy" id="laybuy" <?php if(isset($_GET['laybuy'])) { echo ' disabled="disabled" '; }?>class="price" type="text" onkeypress="return isCurrencyKey(event);" autocomplete="off" value="<?php echo (isset($_POST['laybuy'])) ? $_POST['laybuy'] : ''; ?>" /></td><td><input  <?php if(isset($_GET['laybuy'])) { echo ' disabled="disabled" '; }?> type="button" id="laybuyall" onclick="allt('laybuy');" value="<?php echo PAY_ALL;?>" title="<?php echo PAY_LTITLE;?>" /></td><td class="pmtd"><input name="laybuynotes"  autocomplete="off" type="text" value="<?php echo (isset($_POST['laybuynotes'])) ? $_POST['laybuynotes'] : ''; ?>" title="<?php echo PAY_LAYTITLE;?>"/></td></tr>
                        <tr class="pmtr"><td class="pmtd" style="background:url(<?php echo $themepath; ?>images/chat.png) no-repeat top right;height:32px;"><?php echo PAY_COMMENTS;?></td><td class="pmtd" colspan="3"><textarea name="comment" id="pmtcomment" class="pmtcom" title="<?php echo PAY_COMTITLE;?>"><?php echo (isset($_POST['comment'])) ? $_POST['comment'] : ''; ?></textarea></td></tr>
                        <tr></tr>
                        <tr class="pmtr"><td class="pmtd"><?php echo PAY_REMAIN;?></td><td class="pmtd"><input class="remainp" name="remainder"  id="remainder" type="text" readonly  value="<?php echo $cost; ?>" /></td></tr>
                        <tr class="pmtr"><td class="pmtd"><?php echo PAY_TOTAL;?></td><td class="pmtd"><input class="totalp" name="total" id="total" type="text" readonly value="0"/></td></tr>

                    </table>
    <?php
    if (!empty($laybuystopay)) {
        echo "<input type='hidden' name='laybuyfrm' id='laybuyfrm' value='$laybuystopay' />";
    }
     if(isset($_GET['admin'])){ ?><input type="hidden" name="admin" value="<?php echo $_GET['admin']; ?>" /><?php } ?>
                    <input type="hidden" name="id" value="<?php echo $id; ?>" /><input type="hidden" id="cost" name="cost" value="<?php echo $cost; ?>" />
                    <input type="hidden" name="smallcoin" id="smallcoin" value="<?php echo $smallcoin; ?>"/>
                    <input type="submit" name="submit" value="<?php echo PAY_SUBMIT;?>" id="submit" title="<?php echo PAY_SUBTITLE;?>"/>
                    <input type="submit" name="submit" value="<?php echo PAY_SUBMITR;?>" id="submitr" title="<?php echo PAY_SUBTITLER;?>"/>
                </form>
                <?php
            }
            if (isset($failmsg)) {
                echo '<br>' . $failmsg;
            }
            ?>
        </div>

    </body></html>
<?php

function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function make_laybuy_payment($amount, $date = null, $id, $sale_id, $customer_id = null, $new = false, $duedate = null, $notes = null, $paydown = false) {
    include('dbconfig.php');
    $db = new database($dbhost, $dbuser, $dbpassword, $dbname);
    // make a payment to a laybuy - a new or current laybuy
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($amount, "make laybuy payment amount=");
    if (!is_numeric($amount) || ((($amount * -1) < 0.01) && $paydown === false) || ((($amount ) < 0.01) && $paydown === true)) {
        $fail = PAY_ERR_PAY;
       if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($fail, "Insert laybuys fail=");
        return $fail;
    }
    if (!isset($date)) {
        $date = date('Y-m-d H:i:s');
    }
    if (!is_numeric($sale_id) && $paydown === false) {
        $fail = PAY_ERR_SID;
       if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($fail, "Insert laybuys fail=");
        return $fail;
    }
    if (!is_numeric($customer_id)) {
        $fail = PAY_ERR_CID;
       if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($fail, "Insert laybuys fail=");
        return $fail;
    }
    if (get_system_variable('laybuy_period', $db) && empty($duedate)) {
      $ndate = new DateTime();

        $dd = get_system_variable('laybuy_period', $db) + $ndate->getTimestamp();
        $duedate= (string) date("Y-m-d H:i:s", $dd);
         if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($duedate,"laybuy period setting=");
    }
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("laybuy all values good");
    //assuming all values are good, we can now proceed
    if (!$new || $new == 'check') { // existing laybuy
        if (!empty($id)) { //get current values
            $sql = "SELECT * FROM laybuys WHERE id='" . $id . "'";
            $result = $db->QPResults($sql);
        } else {

            $sql = "SELECT * FROM laybuys WHERE sale_id='" . $sale_id . "'";
            $result = $db->QPResults($sql);
        }


        if (!$result) {
            $balance = $amount;
            $pmtsarray[] = array($date, $amount);
            $arr = serialize($pmtsarray);
            $sql = "INSERT INTO laybuys (sale_id,pmts,balance,duedate,notes) VALUES(";
            $sql .= "'$sale_id','$arr','$balance','$duedate','$notes')";
           if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql, "Insert laybuys sql=");
            $result = $db->query($sql);
            if (!$result) {
                $fail = PAY_ERR_LAY;
                return $fail;
            }
            $revsql="DELETE FROM laybuys WHERE sale_id= $sale_id";
            $db->query("INSERT INTO rewind (sql_to_run, sale_id) VALUES ('$revsql',$sale_id");
            return;
        }
        //there is a result, so we can update that record
        $balance = $result['balance'];
        if (!$id) {
            $id = $result['id'];
        }
        $duedate = $result['duedate'];
        $pmts = $result['pmts'];
        $pmtsarray = unserialize($pmts);
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($id, "Payments.php existing laybuy found, adding sale id=");
        // modify now
        $balance = $balance + $amount;
        $pmtsarray[] = array($date, $amount);
        $arr = serialize($pmtsarray);
        $sql = "UPDATE laybuys SET pmts='$arr',balance='$balance' WHERE id='$id'";
        $result = $db->query($sql);
        if ($balance > 0) {
            echo PAY_MSG1_LAY." : $ $balance  - ".PAY_MSG2_LAY."=$id.<br>";
        }
        if (!$result) {
            $fail = PAY_ERR_ULAY;
            if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql, "make laybuy payment amount failure to update sql=");
            return $fail;
        }

        $revsql="UPDATE laybuys SET balance=balance-$amount WHERE sale_id= $sale_id";
                $db->query("INSERT INTO rewind (sql_to_run, sale_id) VALUES ('$revsql',$sale_id");
        //$revsql="UPDATE laybuys SET pmts='$revarr' WHERE sale_id= $sale_id";
        //        $db->query("INSERT INTO rewind (sql_to_run, sale_id) VALUES ('$revsql',$sale_id");
        return;
    }
}
?>
