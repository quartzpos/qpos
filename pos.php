<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
*/


    session_start();
if(!file_exists('dbconfig.php') || !file_exists('config.php')){
    header("Location: install.php");
}
require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");
require_once 'functions.php';
require 'consoleLogging.php';

if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION,"pos.php Session=");
//Logout
if (isset($_GET['action']) && $_GET['action'] == "logout") {
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION,"pos.php logout Session=");
//Logout
    session_destroy();
    header("Location:pos.php");
}

require_once 'login.php';

require_once 'posscripts.php';

$title = 'QuartzPos Frontend';
require_once 'template.php';
?>


<body onload='setfoc("prodcodeDiv");'>

<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
    <TR height="20">
        <TD><?php $location="pos.php"; require_once("header.php"); ?></TD>
    </TR>
    <TR>
        <TD valign="top">
            <?php
            if (!isset($_SESSION['user'])) {

    loginnow();
//MAIN CONTENT TABLE
} else {
                ?>
<div  id="adminlink"><?php if (isset($_SESSION['admin']) && $_SESSION['admin']>0 || $_SESSION['admin']=='admin') { ?><a
                            href="admin.php?action=login"><?php echo TXT_ADMINISTRATION; ?></a><?php } ?><a
                        href="pos.php?action=logout"><?php echo TXT_LOGOUT; ?></a><input type="hidden" id="admincheck" name="admincheck" value="<?php
                        if (isset( $_SESSION['admin'] ) && $_SESSION['admin'] >1 || $_SESSION['admin']=='admin' ){ echo 'admin'; }
                        ?>"/></div>
                </div>
                <!-- main table -->
                <table width="100%" height="100%" cellspacing="0" border="0" id="saletable">
                    <tr>
                        <td valign="top" height="230">
                            <table id="fe_sales" width="100%" cellspacing="0">
                                <tr>
                                    <td valign="top">

                                        <table width="100%">

                                            <tr>
                                                <td>
                                                    <form action="processsale.php" method="post" name="frm_sale"
                                                          id="frm_sale" autocomplete="off">
                                                        <input type="hidden" name="quote" id="quote" value="" />

                                                        <div id="sales_items_customer">
                                                            <input type="hidden" name="customer_id" id="customer_id"
                                                                   value="1"/></div>
                                                            <div id="sales_items_hidden"><input type="hidden" name="user" id="userid"
                                                                   value="<?php echo $_SESSION['user']; ?>"/>
                                                            <input type="hidden" name="trans"
                                                                   value="<?php echo $_SESSION['trans']; ?>"/>
                                                            <input type="hidden" name="till"
                                                                   value="<?php echo $_SESSION['till']; ?>"/>
                                                            <div id='pls'>
                                                                <?php echo listpls($db); ?>
                                                            </div>
                                                            </div>
                                                        <div id="sales_items">
                                                            <table width="100%" id="tbl_sitems" cellspacing="0">
                                                                <thead><tr class="slstr">
                                                                    <th class="firstth" width="57%"
                                                                        align="left"><?php echo TXT_ITEM; ?></th>
                                                                    <th class="midth" width="10%"
                                                                        align="right"><?php echo TXT_QTY; ?></th>
                                                                    <th class="midth" width="7%"
                                                                        align="right"><?php echo TXT_TAX; ?>%
                                                                    </th>
                                                                    <th class="midth" width="10%"
                                                                        align="right"><?php echo TXT_PRICE; ?></th>
                                                                    <th class="midth" width="10%"
                                                                        align="right"><?php echo TXT_LINE; ?></th>
                                                                    <th class="lastth" width="6%"
                                                                        align="center"><?php echo '- X'; ?></th>
                                                                    </tr></thead>
                                                                    <tbody></tbody>
                                                            </table>
                                                        </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="sessions" class="iframe">
                                            <div id="closesess" onclick="showhide('sessions', 'FALSE')"><img
                                                    src="<?php echo $themepath; ?>images/close.png" title="<?php echo CLOSE_SESSIONS_WINDOW; ?>"/></div>
                                            <iframe id="sessframe" src="sessions.php"></iframe>
                                            <p><?php echo SESSIONS_DESC; ?></p></div>
                                        <div id="super" class="iframe">
                                            <div id="closesuper" onclick="showhide('super', 'FALSE')"><img src="<?php echo $themepath; ?>images/close.png"
                                                                                                  title="<?php echo CLOSE_SUPERVISOR_WINDOW; ?>"/>
                                            </div>
                                            <iframe id="superframe" src="super.php"></iframe>
                                        </div>
                                        <div id="comments" class="iframe">
                                            <div class="wintitle"><?php echo TXT_COMMENTS; ?></div>
                                            <div id="closecomm" onclick="showhide('comments', 'FALSE')"><img
                                                    src="<?php echo $themepath; ?>images/close.png" title="<?php echo CLOSE_COMMENTS_WINDOW; ?>"/></div>
                                            <?php echo COMMENTS_MESSAGE; ?><br>
                                            <textarea id="comment" name="comment"></textarea>

                                        </div>
                                        <div id="custcomments" class="iframe">
                                            <div class="wintitle"><?php echo CUSTOMER_COMMENTS; ?></div>
                                            <div id="closecustcomm" onclick="showhide('custcomments', 'FALSE')"><img
                                                    src="<?php echo $themepath; ?>images/close.png" title="<?php echo CLOSE_CUSTCOMMENTS_WINDOW; ?>"/>
                                            </div><?php echo INVOICE_MESSAGE; ?><br>
                                            <textarea id="custcomment" name="custcomment"
                                                      onblur="swapval('custcomment', 'cc')"></textarea>

                                        </div>
                                        <div id="totals">
                                            <table>
                                                <TR>
                                                    <TD class="scurrency1"><?php echo TXT_TAX; ?>
                                                        :<?php echo CURRENCY; ?><input type="text" name="tax" id="vtax"
                                                                                       class="sale_value" value="0"
                                                                                       readonly></TD>
                                                </tr>
                                                <tr>
                                                    <TD class="scurrency1"><?php echo TXT_SUBTOTAL; ?>
                                                        :<?php echo CURRENCY; ?><input type="text" name="subtotal"
                                                                                       id="vsubtotal" class="sale_value"
                                                                                       value="0" readonly></TD>
                                                </TR>
                                                <TR>
                                                    <td class="scurrency2"><?php echo TXT_TOTAL; ?>
                                                        :<?php echo CURRENCY; ?><input type="text" name="total"
                                                                                       id="vtotal" class="sale_value"
                                                                                       value="0" readonly></TD>
                                                </TR>
                                                <TR>
                                                    <td class="scurrency2"><?php echo TXT_DISCOUNT; ?>
                                                        :<?php echo CURRENCY; ?><input type="text"
                                                                                       id="vdiscount" class="sale_value"
                                                                                       value="0" readonly title="<?php echo DISCOUNT_MESSAGE; ?>"></TD>
                                                </TR>
                                            </table>
                                    </td>
                                </tr>
                            </table>
                            </div>
                            <input type="hidden" name="editsaleid" value="" id="editsaleid" />
                            </form>
                        </td>
                        <td width="392" class="kbtd">
                            <INPUT id="prodcodeDiv" TYPE="text" onkeypress="return isProdKey(event)" tabindex=1 title=<?php echo POS_HK_ITEM;?>
                                   value="" onblur="
		    clearfn('prodcodeDiv');clearKeyCounter();"/><div class="catbutton"><img class="clearbtn" tabindex=2 src="<?php echo $themepath; ?>images/delete.png"
                                                                                           title="<?php echo CLEAR_PRODCODE; ?>" onclick="clearfn('prodcodeDiv');"/><img tabindex=4 class=" clearbtn reveal" src="<?php echo $themepath; ?>images/categories/category.png" title="<?php echo CLICK_CATEGORIES; ?>"  onclick="showhide('catitems', 'TRUE');"/></div>
<!--                            <INPUT id="prodcodeButton" tabindex=3 TYPE="button" onClick="getVal()"
                                   value="">-->
                            <!-- CUSTOMER INFO -->
                            <TABLE cellspacing="0" cellpadding="0" class="custinfo">
                                <TR>
                                    <TD>
                                        <div id="customerDiv0" onclick="showCustomerDiv(1)">&nbsp;</div>
                                        <div id="customerDiv1">
                                            <input type="hidden" name="pricelevel" id="pricelevel"/>

                                            <div id="customer_menu"><a
                                                    href="javascript:showCustomerDiv(2)"><?php echo ADD_NEW_CUSTOMER; ?></a>
                                                | <a href="javascript:showCustomerDiv(0)"><?php echo TXT_CLOSE; ?></a>
                                            </div>
                                            <table>
                                                <tr>
                                                    <td><input type="text" id="findcustomertext"></td>
                                                    <td><input type="button" value="<?php echo TXT_FIND; ?>"
                                                               onClick="javascript:loadXMLDoc('', 'getCustomers')"></td>
                                                </tr>
                                            </table>
                                            <table id="findcustomerstable">
                                                <tr>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            <div id="findCustomersDiv"></div>
                                        </div>
                                        <div id="customerDiv2">
                                            <div id="customer_menu"><a
                                                    href="javascript:showCustomerDiv(0)"><?php echo TXT_CLOSE; ?></a>
                                            </div>
                                            <!-- Add Customer -->
                                            <form name="fe_addcustomer_form">
                                                <table>
                                                    <TR>
                                                        <TD>Trade Client</TD>
                                                        <TD><input type="checkbox" name="trade" id="tradechk"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD><?php echo TXT_FIRSTNAME; ?></TD>
                                                        <TD><input type="text" name="firstname" size="40"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD><?php echo TXT_LASTNAME; ?></TD>
                                                        <TD><input type="text" name="lastname" size="40"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD><?php echo TXT_ACCOUNT_NUMBER; ?></TD>
                                                        <TD><input type="text" name="account_number" size="30"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD><?php echo TXT_ADDRESS; ?></TD>
                                                        <TD><input type="text" size="60" name="address"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD><?php echo TXT_CITY; ?></TD>
                                                        <TD><input type="text" size="40" name="city"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD><?php echo TXT_PCODE; ?></TD>
                                                        <TD><input type="text" size="20" name="pcode"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD><?php echo TXT_STATE; ?></TD>
                                                        <TD><input type="text" size="40" name="state"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD><?php echo TXT_COUNTRY; ?></TD>
                                                        <TD><input type="text" size="50" name="country"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD><?php echo TXT_PHONE; ?></TD>
                                                        <TD><input type="text" size="20" name="phone_number"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD><?php echo TXT_EMAIL; ?></TD>
                                                        <TD><input type="text" size="60" name="email"></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD valign="top"><?php echo TXT_COMMENTS; ?></TD>
                                                        <TD><textarea rows="5" cols="50" name="comments"></textarea>
                                                        </TD>
                                                    </TR>
                                                    <TR>
                                                        <TD colspan="2"><input type="button"
                                                                               value="<?php echo CUSTOMER_SUBMIT; ?>"
                                                                               onClick="javascript:loadXMLDoc('', 'addCustomer')">
                                                        </TD>
                                                    </TR>
                                                </table>
                                            </form>
                                        </div>
                                    </TD>
                                    <TD valign="top"><INPUT id="customerButton" TYPE="button" tabindex=4
                                                            onClick="showCustomerDiv(1)"
                                                            value="<?php echo TXT_CUSTOMER; ?>" title="<?php echo CHOOSE_CUSTOMER; ?>">
                                    </TD>
                                    <TD valign="top">
                                    </TD>
                                </TR>
                            </TABLE>
                            <!-- END CUSTOMER INFO -->
                            <!-- KEYBOARD -->

                            <table cellspacing="0">
                                <tr>
                                    <td class="keybn" tabindex=5
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '1')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">1
                                    </td>
                                    <td tabindex=6 class="keybn"
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '2')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">2
                                    </td>
                                    <td tabindex=7 class="keybn"
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '3')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">3
                                    </td>
                                    <td tabindex=8 class="keybn"
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '4')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">4
                                    </td>
                                    <td class="keybn" tabindex=9
                                        onClick="keyboardUP(document.getElementById('tbl_sitems'))"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'"><?php echo UP; ?>
                                    </td>
                                    <td tabindex=19 class="keybv" id="checkout" onClick="calcS();disabledConfirm_exit = true;checkout()" onMouseDown="this.className = 'keybv_click'" onMouseUp="this.className = 'keybv'" rowspan="2" title="<?php echo CHECKOUT_KEY; ?>"><?php echo CHECKOUT; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="keybn" tabindex=10
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '5')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">5
                                    </td>
                                    <td tabindex=11 class="keybn"
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '6')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">6
                                    </td>
                                    <td tabindex=12 class="keybn"
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '7')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">7
                                    </td>
                                    <td tabindex=13 class="keybn"
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '8')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">8
                                    </td>
                                    <td class="keybn" onClick="keyboardDOWN(document.getElementById('tbl_sitems'))"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'"><?php echo DOWN; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="keybn" tabindex=14
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '9')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">9
                                    </td>
                                    <td tabindex=15 class="keybn"
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '0')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">0
                                    </td>
                                    <td tabindex=15 class="keybn"
                                        onClick="setValue(setel,document.getElementById('tbl_sitems'), '.')"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'">.
                                    </td>
                                    <td tabindex=16 class="keybn"
                                        onClick="keyboardDEL(document.getElementById('tbl_sitems'))"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'"
                                        title="<?php echo DEL_MESSAGE; ?>"><?php echo DEL; ?>
                                    </td>
                                    <td tabindex=17 class="keybn"
                                        onClick="keyboardCLEAR(setel,document.getElementById('tbl_sitems'))"
                                        onMouseDown="this.className = 'keybn_click'"
                                        onMouseUp="this.className = 'keybn'"
                                        title="<?php echo CLEAR_MESSAGE; ?>"><?php echo CLEAR; ?>
                                    </td>
                                    <td tabindex=18 class="keybh" onClick="calcS()"
                                        onMouseDown="this.className = 'keybh_click'"
                                        onMouseUp="this.className = 'keybh'" colspan="1"
                                        title="<?php echo CALCULATE_MESSAGE; ?>"><?php echo ENTER; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td tabindex=20 class="keybh"
                                        onClick="javascript:loadXMLDoc('', 'saveSession'); suspend();"
                                        onMouseDown="this.className = 'keybh_click'"
                                        onMouseUp="this.className = 'keybh'" colspan="2"
                                        title="<?php echo SUSPEND_MESSAGE; ?>"><?php echo SUSPEND; ?>
                                    </td>
                                    <td tabindex=21 class="keybh" onClick="reload('sessframe');showhide('sessions', 'TRUE')"
                                        onMouseDown="this.className = 'keybh_click'"
                                        onMouseUp="this.className = 'keybh'" colspan="2"
                                        title="<?php echo RESUME_MESSAGE; ?>"><?php echo TXT_RESUME; ?>
                                    </td>

                                    <td tabindex=22 class="keybh" onclick="showhide('super', 'TRUE');"
                                        onMouseDown="this.className = 'keybh_click'"
                                        onMouseUp="this.className = 'keybh'" colspan="3"
                                        title="<?php echo SUPERVISOR_MESSAGE; ?>"><?php echo TXT_SUPERVISOR; ?>
                                    </td>

                                </tr>
                            </table>
                            <!-- END KEYBOARD -->

                        </td>

                    </tr>


                </table>
                <div id="catitems" class="catitems iframe">
                    <div id="closecat" onclick="showhide('catitems', 'FALSE')"><img src="<?php echo $themepath; ?>images/close.png"
                                                                           title="<?php echo CLOSE_CATEGORIES_WINDOW; ?>"/></div>
                    <!-- CATEGORIES AND ITEMS -->
                    <table width="100%" height="100%" cellspacing="0">
                        <tr>
                            <td class="cats" height="100%" valign="top">
                                <div id="fe_categories">
                                    <?php
                                    $sql = "select * from categories order by category";
                                    $result = $db->query($sql);
                                    while ($row = $db->fetchRow($result)) {
                                        $row[1] = str_ireplace('/', ' , ', $row[1]);
                                        if(!isset($row[2]) || empty($row[2])){
                                            $row[2]=$themepath."images/categories.png";
                                        }
                                        ?>
                                        <table class="tabcategory"
                                               onClick="javascript:loadXMLDoc('getItems.php?category=<?php echo $row[0]; ?>', 'getItems')">
                                            <tr>
                                                <td><img
                                                        title="<?php echo CLICK_TO_SEE.htmlspecialchars($row[1]); ?>"
                                                        src="<?php echo $row[2]; ?>"></td>
                                            </tr>
                                            <tr>
                                                <td class="catitem"><?php echo htmlspecialchars($row[1]); ?></td>
                                            </tr>
                                        </table>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </td>
                            <td height="100%" valign="top">
                                <div id="fe_items">&nbsp;</div>
                            </td>
                        </tr>
                    </table>
                    <!-- END CATEGORIES AND ITEMS -->
                </div>
                <div id="pmtouts" class="iframe">
                    <div id="closepmtouts" onclick="showhide('pmtouts','FALSE')"><img src="<?php echo $themepath; ?>images/close.png"
                                                                              title="<?php echo CLOSE_PAYMENTS_WINDOW; ?>"/>
                    </div><span id="pmotext"><b><?php echo ADMINMENU_PAYMENTS; ?>:</b><iframe id="pframe" src="pmtout.php"></iframe>
                </div>
                 <div id="vouchers" class="iframe">
                    <div id="closevouchers" onclick="showhide('vouchers', 'FALSE')"><img src="<?php echo $themepath; ?>images/close.png"
                                                                              title="<?php echo CLOSE_VOUCHERS_WINDOW; ?>"/>
                    </div><span id="vouchtext"><b><?php echo ADMINMENU_VOUCHERS; ?>:</b><iframe id="vframe" src="vouchers.php"></iframe>
                </div>

                  <div id="laybuydiv" class="iframe">
                    <div id="closelaybuy" onclick="showhide('laybuydiv', 'FALSE')"><img src="<?php echo $themepath; ?>images/close.png"
                                                                              title="<?php echo CLOSE_LAYBUYS_WINDOW; ?>"/>
                    </div><span id="laybuystext"><b><?php echo ADMINMENU_LAYBUYS; ?>:</b><iframe id="lframe" src="laybuys.php?list=all"></iframe>
                </div>
<?php
//END MAIN CONTENT TABLE
            }
            $check = true;
            include('pmtout.php');
            ?>
        </TD>
    </TR>


    <TR height="16">
        <td>
            <div id="footer"><?php if (isset($_SESSION['user'])) { ?>
                    <div class="padleft">
                         <div class="comms">
                    <table>
                        <tr>
                            <th width="100%" onclick="showhide('custcomments', 'TRUE');"><?php echo CUSTOMER_COMMENTS; ?></th>
                        </tr>
                        <tr>
                            <td><label id="cc"></label></td>
                        </tr>
                    </table>
                </div>
                      <div style="text-align:left;">  <img class="reveal" src="<?php echo $themepath; ?>images/chat.png" title="<?php echo CLICK_COMMENTS; ?>"
                         onclick="showhide('comments', 'TRUE');"/><img class="reveal" src="<?php echo $themepath; ?>images/chatb.png"
                                                               title="<?php echo CLICK_CUSTCOMMENTS; ?>"
                                                               onclick="showhide('custcomments', 'TRUE');"/></div>
                    <?php if (isset($pmtouts) && $pmtouts == '1') { ?> <img class="reveal pmtoutimg"
                                                                            src="<?php echo $themepath; ?>images/warning.gif"
                                                                            title="<?php echo CLICK_PAYMENTS; ?>"
                                                                            onclick="showhide('pmtouts', 'TRUE');"/><?php
                    }?>
                        </div>
               <?php }
                require_once("footer.php");
                ?></div>
        </TD>
    </TR>
</table>
<input type="hidden" id="laybuyitem" value="<?php echo getSystemvalue('laybuy_item'); ?>" />
</body>
</html>
