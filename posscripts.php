<?php

// the javascripts to be included in just pos.php - the pos frontend
$autocomplete = $db->QPResults("SELECT value FROM system WHERE name='autocomplete'");
if (!empty($autocomplete)) {
    $autocomplete = $autocomplete['value'];
} else {
    $autocomplete = '1';
}
$startscripts = '
jQuery(".clicker").click(function(){
   var disp = jQuery(this).next("ul").css("display");
   if (disp =="none"){  jQuery(this).next("ul").show("slow")} else { jQuery(this).next("ul").hide("slow")}
   } );

		window.onbeforeunload = function() { //this function only allows exiting the page at appropriate time

		    if (disabledConfirm_exit === true) {

			return;
		    }
		    if (hasData()) {
			return "'.PS_VOID.'";
		    } else {
			return;
		    }
		}
setInterval("startTime()", 1000);
		';
if (isset($_GET['parent'])) {
    $addurl = "";
    if (isset($_GET['editsale'])) {
        $addurl = "?editsale=" . $_GET['editsale'];
    }
    $startscripts .= 'parent.window.location="pos.php' . $addurl . '";';
}
if ($autocomplete == '1') {
    $startscripts .= '$("#prodcodeDiv").autocomplete({
		    source: "autocomplete.php?functioncall=prodcode",
		    minLength: 3,
                    delay: 500,

                    focus: function( event, ui ) {
                  //console.log("received focus on autocomplete");
                        if(keycounter > 11) { firePCD(); }
                    },
		    open: function(event, ui) {
			$(this).autocomplete("widget")
				.find("ui-menu-item-alternate")
				.removeClass("ui-menu-item-alternate")
				.end()
				.find("li.ui-menu-item:odd a")
				.addClass("ui-menu-item-alternate");
		    },

		    close: function(event, ui) {

			getVal();

		    }

		});
                $.ui.autocomplete.prototype._renderMenu=
                     function( ul, items ) {
                        var that = this;
                        $.each( items, function( index, item ) {
                         that._renderItemData( ul, item );
                         });
                         $( ul ).find( "li:odd" ).addClass( "odd" );
                    };
';
}

$startscripts .= '
                    function hasData() {
		   someThing = document.getElementById("vtotal");


		    if (someThing.value != "0") {
			return true;
		    }

		    return false;
		}
		$(".inp2").on("input", function(){
    alert("'.PS_INPCH.'");
});

	   ';
if (isset($_GET['additem'])) {
    $startscripts .= 'document.getElementById("prodcodeDiv").value="' . $_GET['additem'] . ' :: testitem";
        getVal();';
    // with this you are able to quickly test an addition to the pos screen by passing additem=itemname to the GET params of the url
}
if (isset($_GET['editsale'])) {
    $startscripts .= 'console.log("editsale=' . intval($_GET['editsale']) . '");saleedit(' . intval($_GET['editsale']) . ');';
}
$hkscripts = '

document.addEventListener("keydown", function(event) {
  if (event.altKey && String.fromCharCode(event.keyCode) === "1") {
   //console.log("you pressed alt-1");
    event.preventDefault();
    event.stopPropagation();
    rn=getSelectedIndex(document.getElementById("tbl_sitems"));
    if (rn != "undefined") {
   //console.log("key capture rn="+rn);
   setfoc("itemsqty"+rn);
  //console.log("alt+ 1 clicked");
   }
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && String.fromCharCode(event.keyCode) === "2") {
   //console.log("you pressed alt-2");
    event.preventDefault();
    event.stopPropagation();
    showhide("vouchers","TRUE");
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && String.fromCharCode(event.keyCode) === "3") {
   //console.log("you pressed alt-3");
    event.preventDefault();
    event.stopPropagation();showhide("laybuydiv","TRUE");
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && String.fromCharCode(event.keyCode) === "4") {
   //console.log("you pressed alt-4");
    event.preventDefault();
    event.stopPropagation();
    reload("sessframe");
    showhide("sessions","TRUE");
  //console.log("alt+ 4 clicked");
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && String.fromCharCode(event.keyCode) === "5") {
   //console.log("you pressed alt-5");
    event.preventDefault();
    event.stopPropagation();
    javascript:loadXMLDoc("", "saveSession");
   suspend();
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && String.fromCharCode(event.keyCode) === "6") {
   //console.log("you pressed alt-5");
    event.preventDefault();
    event.stopPropagation();

  //console.log("alt+ 6 clicked");window.location="pos.php?action=logout";
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && (String.fromCharCode(event.keyCode) === "z"||String.fromCharCode(event.keyCode) === "Z")) {
   //console.log("you pressed alt-z");
    event.preventDefault();
    event.stopPropagation();

  //console.log("alt+ z clicked");
   calcS();disabledConfirm_exit = true;checkout();
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && (String.fromCharCode(event.keyCode) === "S"||String.fromCharCode(event.keyCode) === "s")) {
   //console.log("you pressed alt-s");
    event.preventDefault();
    event.stopPropagation();

  //console.log("alt+ s clicked");
   showhide("super","TRUE");
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && (String.fromCharCode(event.keyCode) === "P"||String.fromCharCode(event.keyCode) === "p")) {
   //console.log("you pressed alt-p");
    event.preventDefault();
    event.stopPropagation();

  //console.log("alt+ p clicked");
   shiftPLfocus();
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && (String.fromCharCode(event.keyCode) === "o"||String.fromCharCode(event.keyCode) === "O")) {
   //console.log("you pressed alt-o");
    event.preventDefault();
    event.stopPropagation();

  //console.log("alt+ o clicked");
   closePL();
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && (String.fromCharCode(event.keyCode) === "q"||String.fromCharCode(event.keyCode) === "Q")) {
   //console.log("you pressed alt-q");
    event.preventDefault();
    event.stopPropagation();

  //console.log("alt+ q clicked");
   calcS();disabledConfirm_exit = true;doQuote();checkout();
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && (String.fromCharCode(event.keyCode) === "i"||String.fromCharCode(event.keyCode) === "I")) {
   //console.log("you pressed alt-i");
    event.preventDefault();
    event.stopPropagation();

  //console.log("alt+ i clicked");
   setfoc("prodcodeDiv");
  }
}, true);
';

?>
