<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */

if (!isset($_SESSION))
    session_start();
if (!isset($_SESSION['admin'])) {
    header("Location:admin.php");
}
$db->query("ALTER TABLE `pricelevel` CHANGE `markup` `markup` VARCHAR(4) NULL DEFAULT NULL COMMENT 'As a percentage';");
//Add category
if (isset($_POST['submitpricelevel'])) {
    if (!isset($_POST['name'])) {
        echo PL_NMRQ."<br>";
    } else {
        $sql = "SELECT * FROM pricelevel WHERE name = '".$_POST['name']."'";
        $nameres = $db->QPResults($sql);
        if(empty($nameres['name'])){
        if (isset($_POST['taxinc']) && $_POST['taxinc'] == 'on') {
            $taxinc = 1;
        } else {
            $taxinc = 0;
        }
        $markup = NULL;
        if (isset($_POST['markup'])) {

                $markup = $db->clean($_POST['markup']);
                //if ($markup <= 0){ //to allow discounts ie negative  this is commented
                //    $markup = NULL;
                //}

        }
        $idsql = "SELECT uid from pricelevel ORDER BY uid DESC LIMIT 1";
        $uidres = $db->QPResults($idsql);
        $newuid = $uidres['uid'] + 1;
        $sql = "insert into pricelevel(uid, name,taxinc,markup,base,security_level) values($newuid, '" . $_POST['name'] . "','" . $taxinc . "','" . $markup . "','" . $_POST['base'] . "','" . $_POST['perms'] . "')";
        $db->query($sql);

    } else {
        $htm = PL_PLM1." '".$_POST['name']."' ".PL_PLM2.": <a href='admin.php?action=pricelevels&edit=".$nameres['uid']."'>".$_POST['name']."</a><br>";
        print warn($htm);
    }
}}

//Edit category
if (isset($_POST['submiteditpricelevel'])) {

    $markup = NULL;
    if (isset($_POST['markup'])) {

            $markup = $db->clean($_POST['markup']);


    }
    if (isset($_POST['taxinc'])) {
        $taxinc = ($_POST['taxinc'] == 'on') ? '1' : '0';
    } else {
        $taxinc = '0';
    }
    if (isset($_POST['uid'])) {
        if (is_numeric($_POST['uid'])) {
            $uid = $_POST['uid'];
            //if ($uid <= 0) {
            //    $uid = 1;
            //}
        }
    }

    $sql = "select * from pricelevel where uid=" . $_POST['currentuid'];
    $result = $db->query($sql);
    while ($row = $db->fetchRow($result)) {
        if ($row[1] != $_POST['name'] && $uid != $_POST['currentuid']) {
            $msg = PL_UPID."<br>";
        }
    }
    if (!isset($msg)) {
        $sql = "update pricelevel set uid='" . $uid . "',name='" . $_POST['name'] . "', taxinc=$taxinc, markup='$markup', base='" . $_POST['base'] . "',security_level='" . $_POST['perms'] . "' where uid=" . $_POST['currentuid'];

        if(!$db->query($sql)){echo "Failure to update SQL: $sql"; }
    } else {
        print warn($msg);
    }
}

//Delete pricelevel
if (isset($_GET['delete'])) {
    $sql = "delete from pricelevel where uid=" . $_GET['delete'];
    $db->query($sql);
}
?>
<script type="text/javascript">
    function confirmdelete(pl, nm) {
        op = confirm("<?php echo PL_DPL;?> '" + nm + "'?");
        if (op)
            document.location.href = "admin.php?action=pricelevels&delete=" + pl;
    }
</script>
<div class="admin_content">
<?php
if (isset($_GET['edit'])) {
    $result = $db->query("select * from pricelevel where uid=" . $_GET['edit']);
    $row = $db->fetchRow($result);
    $checked = "";
    if ($row[2] == 1)
        $checked = "checked";
    ?>
        <form action="admin.php?action=pricelevels" enctype="multipart/form-data" method="post">

            <TABLE><tr><td><?php echo PL_ID;?></td><td><input type="text" name="uid" value="<?php echo $row[0]; ?>"><input type="hidden" name="currentuid" value="<?php echo $row[0]; ?>"></td></tr>
                <TR><TD><?php echo PL_NAME;?></TD><TD><input type="text" name="name" size="30" value="<?php echo htmlspecialchars($row[1]); ?>"> </td></tr>
                <TR><TD><?php echo PL_TAX;?></TD><TD><input type="checkbox" name="taxinc" size="30"  <?php echo $checked; ?> </td></TR>
                <TR><TD><?php echo PL_MARKUP;?></TD><TD><input type="text" name="markup" size="2"  value="<?php echo htmlspecialchars($row[3]); ?>" title="<?php echo PL_MARKUP_TITLE;?>"> </td></TR>
                <TR><TD><?php echo PL_BASE;?></TD><TD><select name='base' id='base' >
                            <option value="total_cost" <?php echo ($row[4] == "total_cost") ? 'selected' : ''; ?> ><?php echo PL_RETAIL;?></option>
                            <option value="trade_price" <?php echo ($row[4] == "trade_price") ? 'selected' : ''; ?>><?php echo PL_TRADE;?></option>
                            <option value="wholesale" <?php echo ($row[4] == "wholesale") ? 'selected' : ''; ?>><?php echo PL_WHOLE;?></option>
                        </select></td></TR>
                <TR><TD><?php echo PL_STOCK;?></td><td><select name="perms" id="perms">
  <option value="0" <?php echo ($row[5]=='0')?'selected':''; ?>><?php echo PL_SUSP;?></option>
  <option value="1" <?php echo ($row[5]=='1')?'selected':''; ?>><?php echo PL_STAFF;?></option>
  <option value="2"<?php echo ($row[5]=='2')?'selected':''; ?>><?php echo PL_SUPER;?></option>
  <option value="3"<?php echo ($row[5]=='3')?'selected':''; ?>><?php echo PL_MGR;?></option>
  <option value="4"<?php echo ($row[5]=='4')?'selected':''; ?>><?php echo PL_ADMIN;?></option>
</select><td><tr>
                <tr><td><input type="submit" name="submiteditpricelevel" value="<?php echo TXT_SAVE; ?>"></TD></TR>
            </TABLE>
            <p><?php echo PL_IDMSG;?></p>
        </form>
    <?php
}
else {
    ?>
        <form action="admin.php?action=pricelevels" enctype="multipart/form-data" method="POST">
            <table>
                <TR><TD><?php echo PL_ADD;?></td></TR>
                <TR><TD><?php echo PL_NAME;?></TD><TD><input type="text" name="name" size="30"> </td></TR>
                <TR><TD><?php echo PL_TAX;?></TD><TD><input type="checkbox" name="taxinc" size="30"> </td></TR>
                <TR><TD><?php echo PL_MARKUP;?></TD><TD><input type="text" name="markup" size="2"> </td></TR>
                <TR><TD><?php echo PL_BASE;?></TD><TD><select name='base' id='base' >
                            <option value="total_cost" selected ><?php echo PL_RETAIL;?></option>
                            <option value="trade_price"><?php echo PL_TRADE;?></option>
                            <option value="wholesale" ><?php echo PL_WHOLE;?></option>
                        </select></td></TR>
                <TR><TD><?php echo PL_PERM;?></TD><TD><select name="perms" id="perms">
  <option value="0" ><?php echo PL_SUSP;?></option>
  <option value="1"><?php echo PL_STAFF;?></option>
  <option value="2"><?php echo PL_SUPER;?></option>
  <option value="3"><?php echo PL_MGR;?></option>
  <option value="4"><?php echo PL_ADMIN;?></option>
</select></td></TR>
                <tr><TD><input type="submit" name="submitpricelevel" value="<?php echo PL_SUBMIT;?>"></TD></tr>
            </table>
        </form>
        <br>
        <table cellspacing="0">
            <TR><th><?php echo PL_ID;?></th><TH><?php echo PL_NAME;?></TH><th><?php echo PL_TAX;?></th><th><?php echo PL_MARKUP;?>%</th><th><?php echo PL_BASE;?></th><th><?php echo PL_PERM;?><TH><?php echo TXT_EDIT; ?></TH><TH><?php echo TXT_DELETE; ?></TH></TR>
                        <?php
                        $sql = "select * from pricelevel";
                        $result = $db->query($sql);
                        while ($row = $db->fetchRow($result)) {
                            ?>
                <TR><TD class="btvalue"><?php echo $row[0]; ?></TD>
                    <TD class="btvalue"><?php echo htmlspecialchars($row[1]); ?></TD>
                    <TD class="btvalue" style="text-align:right;"><?php echo ($row[2] == '1') ? POSITIVE : NEGATIVE; ?></TD>
                    <TD class="btvalue" style="text-align:right;"><?php echo htmlspecialchars($row[3]); ?></TD>
                    <TD class="btvalue" style="text-align:right;"><?php echo htmlspecialchars($row[4]); ?></TD>
                    <TD class="btvalue" style="text-align:right;"><?php echo htmlspecialchars($row[5]); ?></TD>
                    <TD class="tvalue" align="center"><a href="admin.php?action=pricelevels&edit=<?php echo $row[0]; ?>"><img src="<?php echo $themepath; ?>images/edit.png" title="<?php echo TXT_EDIT.' '.htmlspecialchars($row[1]); ?>" /></a></TD><TD class="tvalue" align="center"><img src="<?php echo $themepath; ?>images/delete.png" title="<?php echo TXT_DELETE.' '.htmlspecialchars($row[1]); ?>" onclick="confirmdelete('<?php echo $row[0]; ?>', '<?php echo $row[1]; ?>');"/></TD></TR>
        <?php
    }
    ?>
        </table><p><?php echo PL_HELP1;?><br><?php echo PL_HELP2;?></p><p><?php echo PL_HELP3;?> <a href="admin.php?action=users" title="<?php echo PL_USERTITLE;?>" target="_blank"><?php echo PL_HELP4;?></a> <?php echo PL_HELP5;?></p>
    <?php
}
?>
</div>
