<?php
if (!isset($_SESSION)) {
    session_start();
}
require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");

require 'consoleLogging.php';
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION, "Processsale php Session=");
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_GET, "GET=");
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST, "POST=");
//Logout
//if (isset($_GET['action']) && $_GET['action'] == "logout") {
//    if (isset($_SESSION['admin'])) {
//        session_destroy();
//        header("Location:admin.php");
//    } else {
//        session_destroy();
//        header("Location:processsale.php");
//    }
//}
$title = 'Process sale';
$extrascripts = '
  $("#somediv").dialog({
            width: 700,
            height: 700,
            modal: true,
            close: function () {
                $("#thedialog").attr("src", "about:blank");
            }
        });
';


$debugsale = TRUE;
$err = '';
//Submit sale
if (isset($_POST['total']) && isset($_POST['till']) && isset($_POST['trans']) && isset($_POST['user'])) {
    //print_r($_POST);
    if (isset($_POST['customer_id'])) {
        $customer_id = $_POST['customer_id'];
    } else {
        $customer_id = 1;
    }
    $editsaleid = 0;
    if (isset($_POST['editsaleid'])) {
        $editsaleid = $_POST['editsaleid'];
    }
    $itemid = '0';
    $subtotal = $_POST['subtotal'];
    $total = $_POST['total'];
    $tax = $_POST['tax'];
    $items = $_POST['items'];
    $itemsname = $_POST['items_name'];
    if (isset($_POST['itemsmeta'])) {
        $itemsmeta = $_POST['itemsmeta'];
    }
    $itemsqty = $_POST['itemsqty'];
    $itemstax = $_POST['itemstax']; //
    $itemstotaltax = $_POST['itemstotaltax'];
    $itemsbprice = $_POST['itemsbprice'];
    $itemsuprice = $_POST['itemsuprice']; //
    $itemsprice = $_POST['itemsprice']; //
    $itemstotalprice = $_POST['itemstotalprice'];
    $salecomment = $db->clean($_POST['comment']);
    $custcomment = $db->clean($_POST['custcomment']);
    $quote = $_POST['quote'];
    if (empty($quote)) {
        $status = 'Processing';
    } else {
        $status = 'Quote';
    }
    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($quote,"Quote=");
    $user = $_POST['user']; //$_SESSION['user'];
    $till = $_POST['till'];
    $trans = $_POST['trans'];
    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($user, "User  for the trasnsaction");
    if ($debugsale) {
        $transmitted = array_merge($_POST, $_SESSION);
        $log = serialize($transmitted);
        $sql = "INSERT INTO logs (transaction, transmitted,user_id) VALUES ('$trans','$log','$user')";
        $db->query($sql);
    }
    //TODO: handle edit of sale elegantly instead of creating a new one
    if (empty($editsaleid)) {
        $sql = "insert into sales(date, time, customer_id, sale_sub_total, sale_total_cost, items_purchased, sold_by,till,comment,customer_comment, session,state) values ('" . date("Y-m-d") . "','" . date("H:i:s") . "'," . $customer_id . ",'" . $subtotal . "','" . $total . "'," . (sizeof($items)) . "," . ($user) . "," . $till . ",'" . $salecomment . "','" . $custcomment . "','" . $trans . "','$status')";
        $result = $db->query($sql);
        if (!$result) {

            $err.="SQL failure: $sql";
        } else {
            $sale_id = $db->insertId($db->getConnection());

            if ($debugsale) {
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("process sale begin revsql to remove sales");
                $revsql = "DELETE FROM sales WHERE id = $sale_id";
                $db->query("INSERT INTO rewind (sql_to_run, sale_id) VALUES ('$revsql',$sale_id)");
            }


//Insert each item for this sale

            $count = 1;
            $laybuystopay = "";
            for ($i = 0; $i < sizeof($items); $i++) {
                $sql = "SELECT id FROM items WHERE item_number='" . $items[$i] . "'";
                $result = $db->query($sql);
                if (!$result) {

                    $err.="SQL failure: $sql";
                } else {
                    while ($row = $db->fetchRow($result)) {
                        $itemid = $row[0];
                    }
                }
                //when editing a sale we can delete all these items, reversing the quantity 
                if ($itemsqty[$i] != 0 && (!empty($itemid))){
                $sql = "insert into sales_items(sale_id, item_id, quantity_purchased, item_unit_price, item_trade_price, item_tax_percent, item_total_tax,  item_total_cost, item_name, item_description) values(" . $sale_id . "," . $itemid . "," . $itemsqty[$i] . ",'" . $itemsuprice[$i] . "','" . $itemsbprice[$i] . "','" . $itemstax[$i] . "','" . $itemstotaltax[$i] . "','" . $itemstotalprice[$i] . "','" . $items[$i] . "','" . $itemsname[$i] . "')";
                $result = $db->query($sql);
                if (!$result) {
                    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql, "Failure to run sql=");
                    $err = "SQL failure: $sql";
                } else {
                    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("process sale begin revsql to remove items");
                    $revsql = "DELETE FROM sales_items WHERE sale_id = $sale_id";
                    $db->query("INSERT INTO rewind (sql_to_run, sale_id) VALUES ('$revsql',$sale_id)");

                    $sql = "SELECT quantity FROM items WHERE item_number='" . $items[$i] . "'";
                    $qty = $db->QPResults($sql)['quantity'];

                    $qty = $qty - $itemsqty[$i];
                    $sql = "UPDATE items SET quantity='$qty' WHERE item_number='" . $items[$i] . "'";
                    $db->query($sql);
                    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql,"Updating stock");

                    $revsql = "UPDATE items SET `quantity`=`quantity` +" . $itemsqty[$i];
                    $db->query("INSERT INTO rewind (sql_to_run, sale_id) VALUES ('$revsql',$sale_id)");
                }
			}
                //check if the item is a gift voucher
                $sql = "SELECT value FROM system WHERE name='vouchers'";
                $vch = $db->QPResults($sql);
                if (isset($vch['value']) && !empty($vch['value'])) {
                    $vcharr = unserialize($vch['value']);
                    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($vcharr, "processsale.php vcharr=");
                    if (in_array($items[$i], $vcharr)) {

                        //add voucher to the vouchers table
                        $date = date('Y-m-d'); //current date formatted for mysql
                        $defaultexpiry = date("Y-m-d", strtotime("+1 year"));
                        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($defaultexpiry, " processsale.php expiry=");
                        $code = $trans . 'vc-' . $count; // TODO or we could make this an administrator -customizable pattern?
                        //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($code, "processsale.php Set voucher = ");
                        $sql = "INSERT INTO vouchers(sale_id,code,date,customer_id,purchase_value,current_value,expiry)values($sale_id,'$code','$date','$customer_id',$itemstotalprice[$i],$itemstotalprice[$i],'$defaultexpiry')";
                        //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($itemstotalprice[$i],"Voucher price=");
                        $db->query($sql);
                        $revsql = "DELETE FROM vouchers WHERE sale_id= $sale_id";
                        $db->query("INSERT INTO rewind (sql_to_run, sale_id) VALUES ('$revsql',$sale_id");

                        $count++;
                    }
                }

                //check for the item being a laybuy
                if (!empty($itemsmeta[$i])) {
                    $arr = explode("=", $itemsmeta[$i]);
                    $laybuystopay.=$arr[1] . ':' . $itemstotalprice[$i] . ',';
                    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($arr, "processsale.php has found meta data for item, ");
                }
            }
        }
    } else { // $editsaleid is valid
        $sql = "UPDATE sales SET date='" . date("Y-m-d") . "',time='" . date("H:i:s") . "',customer_id=" . $customer_id . ",sale_sub_total='" . $subtotal . "',sale_total_cost='" . $total . "',items_purchased=" . (sizeof($items)) . ",sold_by=" . ($user) . ",till=" . $till . ",comment='" . $salecomment . "',customer_comment='" . $custcomment . "',session='" . $trans . "',state='$status' WHERE id=$editsaleid";
        $db->query($sql);
        //Insert each item for this sale

        $count = 1;
        $laybuystopay = "";

        // clear the sales_items and vouchers first before re-inserting
        $sql = "DELETE FROM sales_items WHERE sale_id=$editsaleid";
        $db->query($sql);
        $sql = "DELETE FROM vouchers WHERE sale_id= $editsaleid";
        $db->query($sql);

        for ($i = 0; $i < sizeof($items); $i++) {
            $sql = "SELECT id FROM items WHERE item_number='" . $items[$i] . "'";
            $result = $db->query($sql);
            if (!$result) {

                $err.="SQL failure: $sql";
            } else {
                while ($row = $db->fetchRow($result)) {
                    $itemid = $row[0];
                }
            }
            //when editing a sale we can delete all these items, reversing the quantity 

            $sql = "insert into sales_items(sale_id, item_id, quantity_purchased, item_unit_price, item_trade_price, item_tax_percent, item_total_tax,  item_total_cost, item_name, item_description) values(" . $editsaleid . "," . $itemid . "," . $itemsqty[$i] . ",'" . $itemsuprice[$i] . "','" . $itemsbprice[$i] . "','" . $itemstax[$i] . "','" . $itemstotaltax[$i] . "','" . $itemstotalprice[$i] . "','" . $items[$i] . "','" . $itemsname[$i] . "')";
            $result = $db->query($sql);
            if (!$result) {
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql, "Failure to run sql=");
                $err = "SQL failure: $sql";
            } else {
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("process sale begin revsql to remove items");
                $revsql = "DELETE FROM sales_items WHERE sale_id = $editsaleid";
                $db->query("INSERT INTO rewind (sql_to_run, sale_id) VALUES ('$revsql',$editsaleid)");

                $sql = "SELECT quantity FROM items WHERE item_number='" . $items[$i] . "'";
                $qty = $db->QPResults($sql)['quantity'];

                $qty = $qty - $itemsqty[$i];
                $sql = "UPDATE items SET quantity='$qty' WHERE item_number='" . $items[$i] . "'";
                $db->query($sql);
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql,"Updating stock");

                $revsql = "UPDATE items SET `quantity`=`quantity` +" . $itemsqty[$i];
                $db->query("INSERT INTO rewind (sql_to_run, sale_id) VALUES ('$revsql',$editsaleid)");
            }
            //check if the item is a gift voucher
            $sql = "SELECT value FROM system WHERE name='vouchers'";
            $vch = $db->QPResults($sql);
            if (isset($vch['value']) && !empty($vch['value'])) {
                $vcharr = unserialize($vch['value']);
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($vcharr, "processsale.php vcharr=");
                if (in_array($items[$i], $vcharr)) {

                    //add voucher to the vouchers table
                    $date = date('Y-m-d'); //current date formatted for mysql
                    $defaultexpiry = date("Y-m-d", strtotime("+1 year"));
                    if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($defaultexpiry, " processsale.php expiry=");
                    $code = $trans . 'vc-' . $count; // TODO or we could make this an administrator -customizable pattern?
                    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($code, "processsale.php Set voucher = ");
                    $sql = "INSERT INTO vouchers(sale_id,code,date,customer_id,purchase_value,current_value,expiry)values($editsaleid,'$code','$date','$customer_id',$itemstotalprice[$i],$itemstotalprice[$i],'$defaultexpiry')";
                    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($itemstotalprice[$i],"Voucher price=");
                    $db->query($sql);
                    $revsql = "DELETE FROM vouchers WHERE sale_id= $editsaleid";
                    $db->query("INSERT INTO rewind (sql_to_run, sale_id) VALUES ('$revsql',$editsaleid");

                    $count++;
                }
            }

            //check for the item being a laybuy
            if (!empty($itemsmeta[$i])) {
                $arr = explode("=", $itemsmeta[$i]);
                $laybuystopay.=$arr[1] . ':' . $itemstotalprice[$i] . ',';
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($arr, "processsale.php has found meta data for item, ");
            }
        }
        $sale_id = $editsaleid; //for the following output
    }//end of editsale
    unset($_POST);
    if (!empty($quote)) {
        header('Location:sale.php?quote=1&closewin=1&id=' . $sale_id);
    }
    if (empty($err)) {
        require_once 'template.php';
        ?>
        <body>
            <div id="somediv" title="Payment Processing">

                <iframe id="thedialog" width="700" height="700" style="margin: 0 auto;display: block;"></iframe>

            </div>


            <script language="javascript">
                $("#thedialog").attr('src', "payments.php?id=<?php
        echo $sale_id;
        if (!empty($laybuystopay)) {
            $laybuystopay = rtrim($laybuystopay, ","); //strip last comma for tidiness
            echo "&laybuy=" . $laybuystopay;
        }
        ?>");</script>

        </body>
        <?php
    } // check for err
    else {
        echo $err;
    }
} else {
    //the post variables are not set! throw error and return to pos
    require_once 'template.php';
    ?>
    <div class="warn">
        <h2>
            <?php echo PS_HELP1;?>
        </h2>
        <p>
            <?php echo PS_HELP2;?><a href="pos.php?action=logout"><button><?php echo PS_HELP3;?></button>
            </a>
            <br>
        </p>


    </div>

    <?php
    $now = date('Ymdhm');
    $out = PS_HELP4. $now . ".<br>".PS_HELP5."<br>";
    $out .="<table border='1'><th>Key</th><th>Variable</th>";
    foreach ($_POST as $k => $v) {
        if (!is_array($v)) {
            $out.="<tr><td>" . $k . "</td><td>" . $v . "</td></tr>";
        } else {
            $out.="<tr><td>" . $k . "</td><td>" . displayTree($v) . "</td></tr>";
        }
    }
    $out.="</table>";
    $filename = "QPerror" . $now . ".htm";
    $fh = fopen($filename, 'w') or die("Can't open error file:" . $filename);
    $chk = fwrite($fh, $out);
    fclose($fh);
    if ($chk) {
        echo"<br><br>".PS_HELP6 . $filename . "<br>";
    } else {
        echo"<br><br>".PS_HELP7."<br>";
    }
}

function displayTree($var) {
    $output = "";
    $newline = "\n";
    foreach ($var as $key => $value) {
        if (is_array($value) || is_object($value)) {
            $value = $newline . "<ul>" . displayTree($value) . "</ul>";
        }

        if (is_array($var)) {
            if (!stripos($value, "<li class=")) {
                $output .= "<li class=\"file\">" . $value . "</li>" . $newline;
            } else {
                $output .= $value . $newline;
            }
        } else { // is_object
            if (!stripos($value, "<li class=")) {
                $value = "<ul><li class=\"file\">" . $value . "</li></ul>" . $newline;
            }

            $output .= "<li class=\"folder\">" . $key . $value . "</li>" . $newline;
        }
    }

    return $output;
}
?>
