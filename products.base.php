<?php
/*
Quartzpos, Open Source Point-Of-Sale System
http://Quartzpos.com



Released under the GNU General Public License
*/
if (!isset($_SESSION))
				session_start();
if (!isset($_SESSION['admin'])) {
				header("Location:admin.php");
}
require_once 'helpers.php';


// cleans up bad chars in item_number

// movement sql = SELECT i.quantity_purchased,i.item_name FROM sales_items i inner join sales s on i.sale_id=s.id WHERE s.date BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW()
$output = "";
$tax    = $db->QPResults("SELECT value FROM system WHERE name='GST_rate'");
if (empty($tax['value'])) {
				$tax['value'] = "15";
}
if (isset($_POST['tax_percent'])) {
				$tax['value'] = $_POST['tax_percent'];
}
//Add new item
if (isset($_POST['submit_item'])) {
				$sql = "SELECT * FROM items WHERE item_name='" . $db->clean($_POST['item_name']) . "' OR item_number='" . $db->clean($_POST['item_number']) . "'";
				$res = $db->QPResults($sql);
				if (!isset($res['id']) || empty($res['id'])) {
								//Upload image file
								$item_image = "images/items/";
								if (isset($_FILES['item_image']['name']) && image_upload($_FILES['item_image'], $item_image)) {
												$item_image .= $_FILES['item_image']['name'];
								} else {
												$item_image .= "item.png";
								}
								if (empty($_POST['reorder_level'])){
												$_POST['reorder_level'] = 0;}
								if (empty($_POST['quantity'])){
												$_POST['quantity'] = 1;}
								if (empty($_POST['description'])){
												$_POST['description'] = $_POST['item_name'];}
								if (empty($_POST['wholesale_price'])){
									 $_POST['wholesale_price'] =  $_POST['total_cost']*.5;
								 }
								 if (empty( $_POST['trade_price'])){
									 $_POST['trade_price'] =  $_POST['total_cost']*.75;
								 }

								//insert item in the database
								$_POST  = db_clean($_POST, $db);
								$sql    = "insert into items(item_name, item_number, description, brand_id, category_id, supplier_id, trade_price, wholesale, unit_price, supplier_item_number, tax_percent, total_cost, quantity, reorder_level, image) values(
'" . $_POST['item_name'] . "',
'" . $_POST['item_number'] . "',
'" . $_POST['description'] . "',
" . $_POST['brand_id'] . ",
" . $_POST['category_id'] . ",
" . $_POST['supplier_id'] . ",
'" . $_POST['trade_price'] . "',
    '" . $_POST['wholesale_price'] . "',
'" . $_POST['unit_price'] . "',
'" . $_POST['supplier_item_number'] . "',
'" . $_POST['tax_percent'] . "',
'" . $_POST['total_cost'] . "',
" . $_POST['quantity'] . ",
" . $_POST['reorder_level'] . ",
'" . $item_image . "')";
								$result = $db->query($sql);
								if (!$result) {
												echo "Failure to add item SQL=$sql";
								} else{
									echo "Added item: ". $_POST['item_name']."<br/>";
									//require last insert id
										$item_id = $db->insertId($db->getConnection());
									if(isset($_POST['date']) && !empty($_POST['date'])){


										$sql="insert into items_aging (items_id,date,quantity,description) VALUES ($item_id,'".$db->clean($_POST['date'])."',".$db->clean($_POST['quantity']).",'Adding product into database')";
										if(!$db->query($sql)){
											echo "Failure to load into items_aging table : $sql<br/>";
										}

									}
									//add to barcodes maybe
									if(isset($_POST['barcode'])){
											$sql="insert into barcodes(barcode,prodcode) VALUES ('".$db->clean($_POST['barcode'])."','".$db->clean($_POST['item_number'])."')";
										if(!$db->query($sql)){
											echo "Failure to load into barcodes table : $sql<br/>";
										}
									}


								}
				} else {
								$output           = "<br>An item already exists with that name or number.";
								$_GET['add_item'] = 1;
				}
}
//Edit an item
if (isset($_POST['edit_item'])) {
				if (empty($_POST['description']))
								$_POST['description'] = $_POST['item_name'];
				//Upload image file
				// but if the image is NOT updated it needs to be ignored
				$item_image = "images/items/";
				$imgsql     = "";
				if (isset($_POST['imagedefault']) && $_POST['imagedefault'] == "1") {
								$item_image .= "item.png";
								$imgsql = ", image='" . $item_image . "'";
				}
				if (isset($_FILES['item_image']['name']) && image_upload($_FILES['item_image'], $item_image)) {
								$item_image .= $_FILES['item_image']['name'];
								$imgsql = ", image='" . $item_image . "'";
				}
				$_POST = db_clean($_POST, $db);
				//Save values
				$sql   = "update items set item_name = '" . $_POST['item_name'] . "',
item_number = '" . $_POST['item_number'] . "',
description = '" . $_POST['description'] . "',
brand_id = " . $_POST['brand_id'] . ",
category_id = " . $_POST['category_id'] . ",
supplier_id = " . $_POST['supplier_id'] . ",
trade_price = '" . $_POST['trade_price'] . "',
    wholesale = '" . $_POST['wholesale_price'] . "',
unit_price = '" . $_POST['unit_price'] . "',
supplier_item_number = '" . $_POST['supplier_item_number'] . "',
tax_percent = '" . $_POST['tax_percent'] . "',
total_cost = '" . $_POST['total_cost'] . "',
quantity = " . $_POST['quantity'] . ",
reorder_level = " . $_POST['reorder_level'] . $imgsql . " where id=" . $_POST['item_id'];
				$db->query($sql);

				if(isset($_POST['date']) && !empty($_POST['date'])){
										//require item id
										$item_id = $_POST['item_id'];
										//check if there is an entry in aging for this item, if so ignore, otherwise add
										$sql = "SELECT * from items_aging where items_id = ".$item_id;
										$result= $db->QPComplete($sql);
										if(!isset($result[0])){
											$sql="insert into items_aging (items_id,date,quantity,description) VALUES ($item_id,'".$db->clean($_POST['date'])."',".$db->clean($_POST['quantity']).",'Editing product.')";
											if(!$db->query($sql)){
											echo "Failure to load into items_aging table : $sql<br/>";
											}
										}


									}
}
//Delete item
if (isset($_GET['delete'])) {
				$sql = "delete from items where id=" . $_GET['delete'];
				$db->query($sql);
}
?>
<div class="admin_content">
<?php
//Add a new item
if (isset($_GET['add_item']) || isset($_GET['edit_item'])) {
				//Get the items values that we want to edit
				if (isset($_GET['edit_item'])) {
								$row = $db->QPResults("select * from items where id=" . $_GET['edit_item']);
				}
?>
<script language="JavaScript">
  function setPrice(t){
    var uprice = parseFloat(document.forms['frm_new_item'].elements['unit_price'].value);
    var taxperc = parseFloat(document.forms['frm_new_item'].elements['tax_percent'].value);
    var total = parseFloat(document.forms['frm_new_item'].elements['total_cost'].value);
    //When we change the unit price or tax percent fields
    if(t==1){
      total = toCurrency(uprice*(1+taxperc/100));
      document.forms['frm_new_item'].elements['total_cost'].value = total;
    }
    //When we change the total price field
    if(t==2){
      uprice = toCurrency(total/(1+taxperc/100));
      document.forms['frm_new_item'].elements['unit_price'].value = uprice;
    }
  }
	function barcode(prodcode){
  showhide('barcodes','TRUE');
 console.log("Show barcodes div, product="+prodcode);
  node = document.getElementById("ifcontainer");
  while (node.hasChildNodes()) {
    node.removeChild(node.lastChild);
}
  var el = document.createElement("iframe");
el.setAttribute('id', 'barcodeiframe');
node.appendChild(el);
el.setAttribute('src', "barcodes.php?prodcode="+prodcode);

  }
</script>

<?php
$bcs= FALSE;
if (isset($_GET['edit_item'])) {
// check for existence of barcodes, and use appropriate image
$sql="SELECT * FROM barcodes WHERE prodcode='".$row['item_number']."'";
$bcs = $db->QPResults($sql);

if($bcs && !empty($bcs['barcode'])){
	$bcsimage="barcode.png";
} else {
	$bcsimage="barcode_off.png";
}
?>
<form name="frm_new_item" action="admin.php?action=products" enctype="multipart/form-data" method="POST">
<?php
				if (isset($_GET['edit_item'])) {
								echo '<input type="hidden" name="item_id" value="' . htmlspecialchars($row['id']) . '">';
				}
				if (isset($_GET['add_item']) && $_GET['add_item'] == 1) {
								echo TXT_ADJUST_ITEM;
				}
?>
<table>
<TR><TD><?php
				echo TXT_ITEM_NAME;
?></TD><TD><input type="text" size="30" name="item_name" required pattern="[A-Za-z0-9 _$.-*+#]{3,80}"
title="A human-meaningful name for this item. Maximum 80 alphanumeric characters, spaces and underscores, minimum 3."<?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['item_name']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['item_name']))
								echo 'value="' . $_POST['item_name'] . '"';
?>></TD></TR>
<TR><TD><?php
				echo TXT_ITEM_NUMBER;
?></TD><TD><input type="text" size="20" name="item_number" required pattern="[A-Za-z0-9_]{3,80}"
						title="A unique identifier for the system to keep a track of this item. Maximum 80 alphanumeric characters, minimum 3." <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['item_number']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['item_number']))
								echo 'value="' . $_POST['item_number'] . '"';
?>></TD></TR>
<TR><TD><?php
				echo TXT_BARCODES;
?></TD><TD><?php if (isset($_GET['add_item'])){ ?>
<input type="text" size="30" name="barcode" <?php
if (isset($_POST['barcode'])){echo 'value="' . $_POST['barcode'] . '"';}
								?> title="Enter a single barcode." />
								<?php } else { ?>

<img src="<?php	echo $themepath;echo "images/".$bcsimage;?>" alt="Barcode" Title="Barcode" onclick="barcode('<?php echo $row['item_number'];?>')" />
<?php } ?>
				</TD></TR>
<TR><TD><?php
				echo TXT_CATEGORY;
?></TD>
<TD>
<select name="category_id"><OPTION value="0">---</OPTION>
<?php
				$result = $db->query("select id,category from categories order by category");
				while ($categories = $db->fetchRow($result)) {
?><option value="<?php
								echo $categories[0];
?>" <?php
								if (isset($_GET['edit_item']) && $row['category_id'] == $categories[0])
												echo "SELECTED";
								if (isset($_GET['add_item']) && isset($_POST['supplier_id']) && $_POST['category_id'] == $categories[0])
												echo "SELECTED";
?>><?php
								echo $categories[1];
?></option><?php
				}
?>
</select>
</TD></TR>
<TR><TD><?php
				echo TXT_BRAND;
?></TD>
<TD>
<select name="brand_id"><OPTION value="0">---</OPTION>
<?php
				$result = $db->query("select id,brand from brands order by brand");
				while ($brands = $db->fetchRow($result)) {
?><option value="<?php
								echo $brands[0];
?>" <?php
								if (isset($_GET['edit_item']) && $row['brand_id'] == $brands[0])
												echo "SELECTED";
								if (isset($_GET['add_item']) && isset($_POST['brand_id']) && $_POST['brand_id'] == $brands[0])
												echo "SELECTED";
?>><?php
								echo $brands[1];
?></option><?php
				}
?>
</select>
</TD></TR>
<TR><TD><?php
				echo TXT_SUPPLIER;
?></TD>
<TD>
<select name="supplier_id"><OPTION value="0">---</OPTION>
<?php
				$result = $db->query("select id,supplier from suppliers order by supplier");
				while ($suppliers = $db->fetchRow($result)) {
?><option value="<?php
								echo $suppliers[0];
?>" <?php
								if (isset($_GET['edit_item']) && $row['supplier_id'] == $suppliers[0])
												echo "SELECTED";
								if (isset($_GET['add_item']) && isset($_POST['supplier_id']) && $_POST['supplier_id'] == $suppliers[0])
												echo "SELECTED";
?>><?php
								echo $suppliers[1];
?></option><?php
				}
?>

</select>
</TD></TR>
<TR><TD><?php
				echo TXT_SUPPLIER_ITEM_NUMBER;
?></TD><TD><input type="text" size="30" name="supplier_item_number" <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['supplier_item_number']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['supplier_item_number']))
								echo 'value="' . $_POST['supplier_item_number'] . '"';
?>></TD></TR>
<TR><TD><?php
				echo TXT_DATE;
?></TD><TD><input type="date" size="12" name="date" id="date" value="<?php
			echo isset($_POST['date'])? $_POST['date']:'';
?>"></TD></TR>
<TR><TD><?php
				echo TXT_TRADE_PRICE;
?></TD><TD><input type="number" step="0.01" name="trade_price" <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['trade_price']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['trade_price']))
								echo 'value="' . $_POST['trade_price'] . '"';
?>></TD></TR>
<TR><TD><?php
				echo TXT_WHOLESALE_PRICE;
?></TD><TD><input type="number" step="0.01" name="wholesale_price" <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['wholesale']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['wholesale_price']))
								echo 'value="' . $_POST['wholesale_price'] . '"';
?> ></TD></TR>
<TR><TD><?php
				echo TXT_UNIT_PRICE;
?></TD><TD><input type="number" step="0.01" name="unit_price" required <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['unit_price']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['unit_price']))
								echo 'value="' . $_POST['unit_price'] . '"';
?>></TD></TR>
<TR><TD><?php
				echo TXT_TAX_PERCENT;
?></TD><TD><input type="number" step="0.01" name="tax_percent" <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['tax_percent']) . '"';
				if (isset($_GET['add_item']))
								echo 'value="' . $tax['value'] . '"';
?>></TD></TR>
<TR><TD><?php
				echo TXT_SELLING_PRICE;
?></TD><TD><input type="number" step="0.01" name="total_cost" required <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['total_cost']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['total_cost']))
								echo 'value="' . $_POST['total_cost'] . '"';
?> ></TD></TR>
<TR><TD><?php
				echo TXT_STOCK;
?></TD><TD><input type="number" step="0.01" name="quantity"  <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['quantity']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['quantity']))
								echo 'value="' . $_POST['quantity'] . '"';
?>></TD></TR>
<TR><TD><?php
				echo TXT_REORDER_LEVEL;
?></TD><TD><input type="number" step="0.01" name="reorder_level" <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['reorder_level']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['reorder_level']))
								echo 'value="' . $_POST['reorder_level'] . '"';
?>></TD></TR>
<TR><TD><?php
				echo TXT_ITEM_IMAGE;
?></TD><TD><input type="file" name="item_image" id="item_image"><?php
				if (isset($row['image'])) {
								echo "<label id='imagelabel'>" . TXT_CURRENTLY . " : " . $row['image'] . "</label>";
				}
?><?php
				if (isset($row['image']))
								echo '&nbsp;&nbsp;<img src="' . $row['image'] . '" id="image" class="product-image" style="max-width:400px;" />';
				if (isset($_GET['edit_item'])) {
								//insert a button to clear the image to default
?>
        <input type="button" value="Clear" onclick="defaultimage();" />
        <input type="hidden" name="imagedefault" id="imagedefault" value="" />
        <script type="text/javascript">
            function defaultimage(){
        document.getElementById('imagedefault').value="1";
        document.getElementById('imagelabel').innerText="Default : item.png";
        document.getElementById('image').src="images/items/item.png";
    }
        </script>
        <?php
				}
?></TD></TR>
<TR><TD valign="top"><?php
				echo TXT_DESCRIPTION;
?></TD>
<TD>
<textarea rows="4" cols="40" name="description"><?php
				if (isset($_GET['edit_item']))
								echo htmlspecialchars($row['description']);
				if (isset($_GET['add_item']) && isset($_POST['description']))
								echo $_POST['description'];
?></textarea>
</TD></TR>
<TR><TD><input type="submit" <?php
				if (isset($_GET['edit_item'])){
								echo 'name="edit_item" value="' . TXT_SAVE . '"';
				}
				else{
								echo 'name="submit_item" value="' . ADD_ITEM . '"';
				}
?>></TD></TR>
</table>
</form>
 <?php
				if (isset($_GET['edit_item'])) {
					//put aging on screen
					$sql="SELECT * FROM items_aging WHERE items_id=".$db->clean($_GET['edit_item']);
					if($result=$db->QPComplete($sql)){
?>
<h4>Aging entries</h4>
<table>
	<th>Date</th>
	<th>Quantity</th>
	<th>Description</th>


<?php
foreach ($result as $k=>$v){
	echo '<tr>';
	echo '<td>'.$v['date'].'</td>';
	echo '<td align="center">'.$v['quantity'].'</td>';
	echo '<td>'.$v['description'].'</td>';
	echo '</tr>';
}
echo '</table><br/>';


 } //close aging  ?>
<a href="admin.php?action=reports&report=move&item=<?php
								echo $_GET['edit_item'];
?>" ><input type="button" value="<?php
								echo TXT_MOVEMENT;
?>"></a>
 <div id="barcodes" class="barcodediv iframe">
                    <div id="closebar" onclick="showhide('barcodes','FALSE')"><img src="<?php
				echo $themepath;
?>images/close.png"
                                                                           title="Close the barcodes window"/></div>
                                                                          <div id="ifcontainer" style="width:90%;"></div></div>


<?php
				}
} else {
				//List items
	// handle get items from paging javscript in general.js
	if (isset($_GET['prodcode']) ){ $_GET['find_prodcode'] = $_GET['prodcode']; }
	if (isset($_GET['category']) ){ $_GET['find_category'] = $_GET['category']; }
	if (isset($_GET['brand']) ){ $_GET['find_brand'] = $_GET['brand']; }
	if (isset($_GET['find_prodcode'])){ $_POST['find_prodcode'] = $db->clean($_GET['find_prodcode']); }
	if (isset($_GET['find_category'])){ $_POST['find_category'] = $db->clean($_GET['find_category']); }
	if (isset($_GET['find_brand'])){ $_POST['find_brand'] = $db->clean($_GET['find_brand']); }


?>

<table cellspacing="0">
<TR><TH colspan="12" align="left">
<form action="admin.php?action=products" method="POST">
<b><?php
				echo TXT_CATEGORY;
?>:</b>
<select name="find_category"><option value="0">---</option>
<?php
				$result = $db->query("select id,category from categories order by category");
				while ($row = $db->fetchRow($result)) {
?><option value="<?php
								echo $row[0];
?>" <?php
								if ((isset($_GET['category']) && $_GET['category'] == $row[0]) || (isset($_POST['find_category']) && $_POST['find_category'] == $row[0]))
												echo "SELECTED";
?>><?php
								echo $row[1];
?></option><?php
				}
?>
</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b><?php
				echo TXT_BRAND;
?>:</b>
<select name="find_brand"><option value="0">---</option>
<?php
				$result = $db->query("select id,brand from brands order by brand");
				while ($row = $db->fetchRow($result)) {
?><option value="<?php
								echo $row[0];
?>" <?php
								if ((isset($_GET['brand']) && $_GET['brand'] == $row[0]) || (isset($_POST['find_brand']) && $_POST['find_brand'] == $row[0]))
												echo "SELECTED";
?>><?php
								echo $row[1];
?></option><?php
				}
?>
</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="text" name="find_prodcode" placeholder="Item number to search" onclick="this.value=''" <?php if (isset($_POST['find_prodcode'])){ echo 'value="'.$_POST['find_prodcode'].'"'; } ?>/>
<input list="qty" name="row_quantity" id="row_quantity" placeholder="Report rows"/ <?php
				if (isset($_POST['row_quantity'])) {
								echo 'value="' . $_POST['row_quantity'] . '"';
				}
?>>
<datalist id="qty">
    <option value="10">
    <option value="20">
    <option value="50">
    <option value="100">
    <option value="500">
</datalist>
<input type="submit" name="find_items" value="<?php
				echo TXT_FIND_ITEMS;
?>">
<input type="button" value="<?php
				echo ADD_NEW_ITEM;
?>" onclick="document.location.href='admin.php?action=products&add_item'">
</form>
</TH></TR>
<?php
				$sql = "select SQL_CALC_FOUND_ROWS id, item_name, item_number, description, clearance, brand_id, category_id, supplier_id, trade_price, wholesale, unit_price, supplier_item_number, tax_percent, total_cost, quantity, reorder_level, image from items";
				// If we have selected a category or a brand or product code
				$q   = 0;
				if (isset($_POST['find_category']) && !empty($_POST['find_category'])){
								$q++;
								$_GET['category']=$_POST['find_category'];
				}
				if (isset($_POST['find_brand']) && !empty($_POST['find_brand'])){
								$q++;
								$_GET['brand']=$_POST['find_brand'];
				}
				if (isset($_POST['find_prodcode']) && !empty($_POST['find_prodcode'])){
								$q++;
				}
				if ($q > 1) {
								$sql .= " where";
								if (isset($_POST['find_category']) && $_POST['find_category'] != "0") {
												$sql .= " (category_id=" . $_POST['find_category'].")";
												if ($q > 1) {
												$sql .= " AND ";
								}
								}

								if (isset($_POST['find_brand']) && $_POST['find_brand'] != "0") {
												$sql .= " (brand_id=" . $_POST['find_brand'].")";
												if ($q > 1) {
												$sql .= " AND ";
								}
								}
								if (isset($_POST['find_prodcode']) && !empty($_POST['find_prodcode'])) {
								//implement a breaking down of search terms for fuzzy logic, explodes find_prodcode if it has any other characters in it
								$text = $_POST['find_prodcode'];
								$searchterms = "";
								$scount = 0;
								$exploded = multiexplode(array(",",".","|",":"," "),$text);
								if(count($exploded)>1){
									//we have a multi part  search term
									foreach($exploded as $exp=>$val){
										if($scount !==0){ $searchterms .=" OR ";}
										$searchterms.=  " ( item_number LIKE '%" . $val . "%'";
										$searchterms .= " OR description LIKE '%" . $val . "%')";
										$scount ++;
									}
									} else {
										//single word
													$sql .= "  (item_number LIKE '%" . $_POST['find_prodcode'] . "%'";
												$sql .= " OR description LIKE '%" . $_POST['find_prodcode'] . "%')";
									}
								} else {
									//rtrim the last AND
									$sql = rtrim($sql,'AND ');
								}


								if (isset($searchterms) && !empty($searchterms)){
									$sql .=" ( ". $searchterms. " ) "; //so we have an exploded search term
									}
				}

				if (isset($_GET['category']) && $q==1) {
								$sql .= " where category_id=" . $_GET['category'];
				}
				if (isset($_GET['brand']) && $q==1) {
								$sql .= " where brand_id=" . $_GET['brand'];
				}
				if (isset($_POST['find_prodcode']) && !empty($_POST['find_prodcode']) && $q==1) { //otherwise use regular search

												$sql .= "  WHERE (item_number LIKE '%" . $_POST['find_prodcode'] . "%'";
												$sql .= " OR description LIKE '%" . $_POST['find_prodcode'] . "%')";
								}
				//Ten (10) items per page default
				if (isset($_GET['row_quantity']) && is_numeric($_GET['row_quantity']) && $_GET['row_quantity'] > 0) {
								$_POST['row_quantity'] = $_GET['row_quantity'];
				}
				$ipp = ITEMS_PER_PAGE; // ipp will be the number of report rows passed
				if (isset($_POST['row_quantity']) && !empty($_POST['row_quantity'])) {
								if (is_numeric($_POST['row_quantity']) && $_POST['row_quantity'] > 0) {
												$ipp = $_POST['row_quantity'];
								}
				}
				$sql .= " ORDER BY item_name ";
				$sql .= " limit " . (isset($_GET['page']) ? ($_GET['page'] - 1) * $ipp : 0) . "," . $ipp;
				//ChromePhp::log($sql,"products.php search sql=");
				//echo $sql;
				if(!$result = $db->QPComplete($sql)){
					echo "Either there are no results, or that search has an error:".$sql." .<br/>";
				}

				//The total of items that this query would return without the limit clause
				$found_rows      = $db->query("SELECT FOUND_ROWS()");
				$total_num_items = $db->fetchRow($found_rows);
				$npages          = ceil($total_num_items[0] / $ipp);
				if (!empty($npages)) {
?>
<TR><TH colspan="6" align="left">&nbsp;&nbsp;page<?php
								echo (isset($_GET['page']) ? $_GET['page'] : 1);
								echo TXT_OF . $npages;
?></TH>
<TH colspan="6" align="right">
<div id="pageset">&nbsp;</div>
<?php
								if ($npages > 1) {
?>
<script language="javascript">setPages(<?php
												echo $npages;
?>,<?php
												echo (isset($_GET['page']) ? $_GET['page'] : 1);
												echo "," . $ipp;
												$passstr="";
												if(isset($_POST['find_prodcode']) && !empty($_POST['find_prodcode'])){ $passstr.="&find_prodcode=".$_POST['find_prodcode']; }
												if(isset($_POST['find_category']) && !empty($_POST['find_category'])) { $passstr.="&find_category=".$_POST['find_category']; }
												if(isset($_POST['find_brand']) && !empty($_POST['find_brand'])){ $passstr.="&find_brand=".$_POST['find_brand']; }
												if(!empty($passstr)) { echo ",'".$passstr."'";}
?>);</script>
<?php
								}
?>
</TH></TR>
<TR><TH width="24%" colspan="2"><?php
								echo TXT_ITEM_NAME;
?></TH>
<TH width="34px" align="center"><img src="<?php
								echo $themepath;
?>images/barcode.png" alt="Barcode" Title="Barcode" /></th><TH width="100" align="left"><?php
								echo TXT_ITEM_NUMBER;
?></TH>
<TH width="5%" align="left"><?php
								echo TXT_CLEARANCE;
?></TH>
<TH width="11%" align="left"><?php
								echo TXT_BRAND;
?></TH>
<TH width="11%" align="left"><?php
								echo TXT_CATEGORY;
?></TH>
<TH width="5%"><?php
								echo TXT_TRADE_PRICE;
?></TH>
<TH width="5%"><?php
								echo TXT_WHOLESALE_PRICE;
?></TH>
<TH width="5%"><?php
								echo TXT_UNIT_PRICE;
?></TH>
<TH width="3%"><?php
								echo TXT_TAX_PERCENT;
?></TH>
<TH width="5%"><?php
								echo TXT_SELLING_PRICE;
?></TH>
<TH width="5%"><?php
								echo TXT_STOCK;
?></TH>
<TH width="5%"><?php
								echo TXT_EDIT;
?></TH>
<TH width="5%"><?php
								echo TXT_DELETE;
?></TH>
</TR>
<?php
if (isset($result)){
								foreach ($result as $row) {
												$rcategory = $db->query("select category from categories where id=" . $row['category_id']);
												$rbrand    = $db->query("select brand from brands where id=" . $row['brand_id']);
												$category  = $db->fetchRow($rcategory);
												$brand     = $db->fetchRow($rbrand);
												if (empty($row['image'])) {
																$row['image'] = $themepath . "images/products.png";
												}
?>
<script language="JavaScript">
  function delete_item(item){
    op = confirm("Do you really want to delete this item?");
    if(op)document.location.href="admin.php?action=products&delete=" + item;
  }
  function barcode(prodcode){
  showhide('barcodes','TRUE');
 console.log("Show barcodes div, product="+prodcode);
  node = document.getElementById("ifcontainer");
  while (node.hasChildNodes()) {
    node.removeChild(node.lastChild);
}
  var el = document.createElement("iframe");
el.setAttribute('id', 'barcodeiframe');
node.appendChild(el);
el.setAttribute('src', "barcodes.php?prodcode="+prodcode);

  }
  function toggle_clearance(prodcode){
	     $.ajax({
            type: "GET",
            url: "getItems.php",
            data: "clearitem=" + prodcode,
            dataType: "json",
            cache: false,
            success: function (result) {
				console.log("Toggle clearance on item="+prodcode);
            },
            error: function () {
                console.log("Failure to toggle clearance on item="+prodcode);

            },
        });
	}
</script>
<TR>
<TD width="5%" class="tvalue"><img width="<?php
												echo ITEM_IMG_SIZE;
?>" height="<?php
												echo ITEM_IMG_SIZE;
?>" src="<?php
												echo $row['image'];
?>"></TD>
<TD width="16%" class="btvalue"><?php
												echo htmlspecialchars($row['item_name']);
?></TD>
<?php
// check for existence of barcodes, and use appropriate image
$sql="SELECT * FROM barcodes WHERE prodcode='".$row['item_number']."'";
$bcs = $db->QPResults($sql);

if($bcs && !empty($bcs['barcode'])){
	$bcsimage="barcode.png";
} else {
	$bcsimage="barcode_off.png";
}
?>
<Td width="34px" align="center" class="btvalue"><img src="<?php
												echo $themepath;
												echo "images/".$bcsimage."\"";
?> alt="Barcode" Title="Barcode" onclick="barcode('<?php
												echo $row['item_number'];
?>')" /></td>
<TD width="7%" class="tvalue"><?php
												echo htmlspecialchars($row['item_number']);
?></TD>
<TD width="5%" class="tvalue"><?php
												echo   '<input type="checkbox" id="clear-'.$row['item_number'].'" name="clear-'.$row['item_number'].'"';
												if ($row['clearance']){echo ' checked '; }
												echo ' onclick="toggle_clearance(\''.$row['item_number'].'\')" ';
												echo ' title="Clearance item if ticked:'.$row['item_number'].'"';
												echo '>';


?></TD>
<TD width="11%" class="tvalue"><?php
												if (isset($brand[0]))
																echo htmlspecialchars($brand[0]);
												else
																echo "-";
?></TD>
<TD width="11%" class="tvalue"><?php
												if (isset($category[0]))
																echo htmlspecialchars($category[0]);
												else
																echo "-";
?></TD>
<TD width="5%" align="center" class="tvalue"><?php
												echo htmlspecialchars($row['trade_price']); //price
?></TD>
<TD width="5%" align="center" class="tvalue"><?php
												echo htmlspecialchars(money_formatter( $row['wholesale'])); //wholesale price
?></TD>
<TD width="5%" align="center" class="tvalue"><?php
												echo htmlspecialchars(money_formatter( $row['unit_price'])); //unit price
?></TD>
<TD width="3%" align="center" class="tvalue"><?php
												echo htmlspecialchars($row['tax_percent']); //tax
?></TD>
<TD width="5%" align="center" class="tvalue"><?php
												echo htmlspecialchars($row['total_cost']); //retail
?></TD>
<TD width="5%" align="center" class="tvalue"><?php
												echo htmlspecialchars($row['quantity']); //qty
?></TD>
<TD width="5%" align="center" class="tvalue"><a href="admin.php?action=products&edit_item=<?php
												echo $row['id'];
?>"><img src="<?php
												echo $themepath;
?>images/edit.png" title="Edit <?php
												echo htmlspecialchars($row['item_number']);
?>"></a></TD>
<TD width="5%" align="center" class="tvalue"><a href="Javascript:delete_item(<?php
												echo $row['id'];
?>)"><img src="<?php
												echo $themepath;
?>images/delete.png" title="Delete <?php
												echo htmlspecialchars($row['item_number']);
?>"></a></TD>
</TR>
<?php
								}
								}
				} else { //there are no products
								$output = "No products loaded.";
				}
?>
</table>
<?php
				if (isset($output)) {
								echo $output;
				}
?>
  <div id="barcodes" class="barcodediv iframe">
                    <div id="closebar" onclick="showhide('barcodes','FALSE')"><img src="<?php
				echo $themepath;
?>images/close.png"
                                                                           title="Close the barcodes window"/></div>
                                                                          <div id="ifcontainer" style="width:90%;"></div></div>
<?php
}
?>
</div>
