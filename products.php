<?php
/*
Quartzpos, Open Source Point-Of-Sale System
http://Quartzpos.com



Released under the GNU General Public License
*/
if (!isset($_SESSION))
				session_start();
if (!isset($_SESSION['admin'])) {
				header("Location:admin.php");
}
require_once 'helpers.php';

$db->query("ALTER TABLE `items` ADD `markup` DECIMAL(4,2) NULL AFTER `total_cost`");
$db->query("CREATE TABLE IF NOT EXISTS `items_locations` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) NOT NULL,
  `location` text DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `date_stored` date DEFAULT NULL,
  `stored_user_id` int(8) DEFAULT NULL,
  `date_modified` date DEFAULT NULL,
  `modified_user_id` int(8) DEFAULT NULL,
  `notes` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
$output = "";
$tax    = $db->QPResults("SELECT value FROM system WHERE name='GST_rate'");
if (empty($tax['value'])) {
				$tax['value'] = "15";
}
if (isset($_POST['tax_percent'])) {
				$tax['value'] = $_POST['tax_percent'];
}
//Add new item
if (isset($_POST['submit_item'])) {
				include_once'products_add.php';
}
//Edit an item
if (isset($_POST['edit_item'])) {
			include_once'products_edit.php';
}
//Delete item
if (isset($_GET['delete'])) {
				$sql = "delete from items where id=" . $_GET['delete'];
				$db->query($sql);
}
?>
<div class="admin_content">
<?php
//Add a new item
if (isset($_GET['stocktake']) ) {
			include_once 'products_stocktake.php';
}
elseif (isset($_GET['add_item']) || isset($_GET['edit_item'])) {
			include_once 'products_form.php';
} else {
				include_once'products_list.php';
}
?>
</div>
