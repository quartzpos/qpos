<?php
//add product
$sql = "SELECT * FROM items WHERE item_name='" . $db->clean($_POST['item_name']) . "' OR item_number='" . $db->clean($_POST['item_number']) . "'";
$res = $db->QPResults($sql);
if (!isset($res['id']) || empty($res['id'])) {
    //Upload image file
    $item_image = "images/items/";
    if (isset($_FILES['item_image']['name']) && image_upload($_FILES['item_image'], $item_image)) {
        $item_image.= $_FILES['item_image']['name'];
    } else {
        $item_image.= "item.png";
    }
    if (empty($_POST['reorder_level'])) {
        $_POST['reorder_level'] = 0;
    }
    if (empty($_POST['quantity'])) {
        $_POST['quantity'] = 1;
    }
    if (empty($_POST['description'])) {
        $_POST['description'] = $_POST['item_name'];
    }
    if (empty($_POST['wholesale_price'])) {
        $_POST['wholesale_price'] = $_POST['total_cost'] * .5;
    }
    if (empty($_POST['trade_price'])) {
        $_POST['trade_price'] = $_POST['total_cost'] * .75;
    }
    //insert item in the database
    $_POST = db_clean($_POST, $db);
    $sql = "insert into items(item_name, item_number, description, brand_id, category_id, supplier_id, trade_price, wholesale, unit_price, supplier_item_number, tax_percent, total_cost, quantity, reorder_level, image) values(
'" . $_POST['item_name'] . "',
'" . $_POST['item_number'] . "',
'" . $_POST['description'] . "',
" . $_POST['brand_id'] . ",
" . $_POST['category_id'] . ",
" . $_POST['supplier_id'] . ",
'" . $_POST['trade_price'] . "',
    '" . $_POST['wholesale_price'] . "',
'" . $_POST['unit_price'] . "',
'" . $_POST['supplier_item_number'] . "',
'" . $_POST['tax_percent'] . "',
'" . $_POST['total_cost'] . "',
" . $_POST['quantity'] . ",
" . $_POST['reorder_level'] . ",
'" . $item_image . "')";
    $result = $db->query($sql);
    if (!$result) {
        echo "Failure to add item SQL=$sql";
    } else {
        echo "Added item: " . $_POST['item_name'] . "<br/>";
        $item_id = $db->insertId($db->getConnection());//require last insert id
        if (isset($_POST['date']) && !empty($_POST['date'])) {


            $sql = "insert into items_aging (items_id,date,quantity,description) VALUES ($item_id,'" . $db->clean($_POST['date']) . "'," . $db->clean($_POST['quantity']) . ",'Adding product into database')";
            if (!$db->query($sql)) {
                echo "Failure to load into items_aging table : $sql<br/>";
            }
        }
        // check for existence of dimensions and add
        // set defaults
        $net_weight_kg=0;
        $gross_weight_kg=0;
        $net_height_m=0;
        $gross_height_m=0;
        $net_width_m=0;
        $gross_width_m=0;
        $gross_depth_m=0;
        $net_depth_m=0;
        $supplier_url="";
        $packaging_type="";
        $style="";
        $colour="";
        $materials="";
        $manufacture_components="";
        $certifications="";
        $hazards="";
        $awards="";
        $website_url="";
        $deleted_date=0;
        $author="";
        $creation_user_id="";
        $deletion_user_id="";

        if (isset($_POST['net_weight_kg']) && !empty($_POST['net_weight_kg'])) {
            $net_weight_kg = $db->clean($_POST['net_weight_kg']);
        }
        if (isset($_POST['gross_weight_kg']) && !empty($_POST['gross_weight_kg'])) {
            $gross_weight_kg = $db->clean($_POST['gross_weight_kg']);
        }
        if (isset($_POST['net_height_m']) && !empty($_POST['net_height_m'])) {
            $net_height_m = $db->clean($_POST['net_height_m']);
        }
        if (isset($_POST['gross_height_m']) && !empty($_POST['gross_height_m'])) {
            $gross_height_m = $db->clean($_POST['gross_height_m']);
        }
         if (isset($_POST['net_width_m']) && !empty($_POST['net_width_m'])) {
            $net_width_m = $db->clean($_POST['net_width_m']);
        }
         if (isset($_POST['gross_width_m']) && !empty($_POST['gross_width_m'])) {
            $gross_width_m = $db->clean($_POST['gross_width_m']);
        }
        if (isset($_POST['gross_depth_m']) && !empty($_POST['gross_depth_m'])) {
            $gross_depth_m = $db->clean($_POST['gross_depth_m']);
        }
         if (isset($_POST['net_depth_m']) && !empty($_POST['net_depth_m'])) {
            $net_depth_m = $db->clean($_POST['net_depth_m']);
        }
        if (isset($_POST['supplier_url']) && !empty($_POST['supplier_url'])) {
            $supplier_url = $db->clean($_POST['supplier_url']);
        }
        if (isset($_POST['packaging_type']) && !empty($_POST['packaging_type'])) {
            $packaging_type = $db->clean($_POST['packaging_type']);
        }
        if (isset($_POST['style']) && !empty($_POST['style'])) {
            $style = $db->clean($_POST['style']);
        }
        if (isset($_POST['colour']) && !empty($_POST['colour'])) {
            $colour = $db->clean($_POST['colour']);
        }
        if (isset($_POST['materials']) && !empty($_POST['materials'])) {
            $materials = $db->clean($_POST['materials']);
        }
        if (isset($_POST['manufacture_components']) && !empty($_POST['manufacture_components'])) {
            $manufacture_components = $db->clean($_POST['manufacture_components']);
        }
        if (isset($_POST['certifications']) && !empty($_POST['certifications'])) {
            $certifications = $db->clean($_POST['certifications']);
        }
        if (isset($_POST['hazards']) && !empty($_POST['hazards'])) {
            $hazards = $db->clean($_POST['hazards']);
        }
        if (isset($_POST['awards']) && !empty($_POST['awards'])) {
            $awards = $db->clean($_POST['awards']);
        }
        if (isset($_POST['website_url']) && !empty($_POST['website_url'])) {
            $website_url = $db->clean($_POST['website_url']);
        }
        if (isset($_POST['deleted_date']) && !empty($_POST['deleted_date'])) {
            $deleted_date = $db->clean($_POST['deleted_date']);
        }
        if (isset($_POST['author']) && !empty($_POST['author'])) {
            $author = $db->clean($_POST['author']);
        }
        if (isset($_POST['creation_user_id']) && !empty($_POST['creation_user_id'])) {
            $creation_user_id = $db->clean($_POST['creation_user_id']);
        }
        if (isset($_POST['deletion_user_id']) && !empty($_POST['deletion_user_id'])) {
            $deletion_user_id = $db->clean($_POST['deletion_user_id']);
        }
         $sql = "INSERT INTO `items_dimensions` (`items_id`, `net_weight_kg`, `gross_weight_kg`, `net_height_m`, `gross_height_m`, `net_width_m`, `gross_width_m`, `gross_depth_m`, `net_depth_m`, `supplier_url`, `packaging_type`, `style`, `colour`, `materials`, `manufacture_components`, `certifications`, `hazards`, `awards`, `website_url`, `deleted_date`, `author`, `creation_user_id`, `deletion_user_id`) VALUES ( '$item_id', '$net_weight_kg', '$gross_weight_kg', '$net_height_m', '$gross_height_m', '$net_width_m', '$gross_width_m', '$gross_depth_m', '$net_depth_m', '$supplier_url', '$packaging_type', '$colour', '$colour', '$materials', '$manufacture_components', '$certifications', '$hazards', '$awards', '$website_url', '$deleted_date', '$author', '$creation_user_id', '$deletion_user_id');";
            if (!$db->query($sql)) {
                echo "Failure to load into items_dimensions table : $sql<br/>";
            }
    }
} else {
    $output = "<br>An item already exists with that name or number.";
    $_GET['add_item'] = 1;
}
?>
