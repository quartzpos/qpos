<?php
	//Get the items values that we want to edit
				if (isset($_GET['edit_item'])) {
								$row = $db->QPResults("select * from items where id=" . $_GET['edit_item']);
				}
?>
<script language="JavaScript">
  function setPrice(t){
    var uprice = parseFloat(document.forms['frm_new_item'].elements['unit_price'].value);
    var taxperc = parseFloat(document.forms['frm_new_item'].elements['tax_percent'].value);
    var total = parseFloat(document.forms['frm_new_item'].elements['total_cost'].value);
    //When we change the unit price or tax percent fields
    if(t==1){
      total = toCurrency(uprice*(1+taxperc/100));
      document.forms['frm_new_item'].elements['total_cost'].value = total;
    }
    //When we change the total price field
    if(t==2){
      uprice = toCurrency(total/(1+taxperc/100));
      document.forms['frm_new_item'].elements['unit_price'].value = uprice;
    }
  }
	function barcode(prodcode){
  showhide('barcodes','TRUE');
 console.log("Show barcodes div, product="+prodcode);
  node = document.getElementById("ifcontainer");
  while (node.hasChildNodes()) {
    node.removeChild(node.lastChild);
}
  var el = document.createElement("iframe");
el.setAttribute('id', 'barcodeiframe');
node.appendChild(el);
el.setAttribute('src', "barcodes.php?prodcode="+prodcode);

  }
</script>

<form name="frm_new_item" action="admin.php?action=products" enctype="multipart/form-data" method="POST">
<?php
				if (isset($_GET['edit_item'])) {
								echo '<input type="hidden" name="item_id" value="' . htmlspecialchars($row['id']) . '">';
				}
				if (isset($_GET['add_item']) && $_GET['add_item'] == 1) {
								echo TXT_ADJUST_ITEM;
				}
?>
<table style="width:100%;">
<TR><TD><?php
				echo TXT_ITEM_NAME;
?></TD><TD><input type="text" size="30" name="item_name" required charset="utf-8" pattern="[A-Za-z0-9 _.\-*+#$]{3,80}"
title="A human-meaningful name for this item. Maximum 80 alphanumeric characters."
<?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['item_name']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['item_name']))
								echo 'value="' . $_POST['item_name'] . '"';
?>></TD>
<TD><?php
				echo TXT_ITEM_NUMBER;
?></TD><TD><input type="text" size="30" name="item_number" required pattern="[A-Za-z0-9_]{3,80}"
						title="A unique identifier for the system to keep a track of this item. Maximum 80 alphanumeric characters, minimum 3."

<?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['item_number']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['item_number']))
								echo 'value="' . $_POST['item_number'] . '"';
?>></TD></TR>
<TR><TD valign="top"><?php
				echo TXT_DESCRIPTION;
?></TD>
<TD>
<textarea rows="4"  name="description" style="width:90%"><?php
				if (isset($_GET['edit_item']))
								echo htmlspecialchars($row['description']);
				if (isset($_GET['add_item']) && isset($_POST['description']))
								echo $_POST['description'];
?></textarea></TD>
<td>

			<?php

				echo TXT_ITEM_IMAGE;
?></td><td><input type="file" name="item_image" title="Load a new image for this item." id="item_image">

<?php
				if (isset($row['image'])) {
								echo "<label id='imagelabel'>" . TXT_CURRENTLY . " : " . $row['image'] . "</label>";
				}
?><?php
				if (isset($row['image']))
								echo '<img src="' . $row['image'] . '" id="image" class="product-image" style="max-width:400px;" />';
				if (isset($_GET['edit_item'])) {
								//insert a button to clear the image to default
?>
        <input type="button" value="Clear" onclick="defaultimage();" />
        <input type="hidden" name="imagedefault" id="imagedefault" value="" />
        <script type="text/javascript">
            function defaultimage(){
        document.getElementById('imagedefault').value="1";
        document.getElementById('imagelabel').innerText="Default : item.png";
        document.getElementById('image').src="images/items/item.png";
    }
        </script>
        <?php
				}
?>

</td>
</TR>
<TR><TD><?php
				echo TXT_CATEGORY;
?></TD>
<TD>
<select name="category_id" title="Select the category for this item."><OPTION value="0">---</OPTION>
<?php
				$result = $db->query("select id,category from categories order by category");
				while ($categories = $db->fetchRow($result)) {
?><option value="<?php
								echo $categories[0];
?>" <?php
								if (isset($_GET['edit_item']) && $row['category_id'] == $categories[0])
												echo "SELECTED";
								if (isset($_GET['add_item']) && isset($_POST['supplier_id']) && $_POST['category_id'] == $categories[0])
												echo "SELECTED";
?>><?php
								echo $categories[1];
?></option><?php
				}
?>
</select>
</TD>
<TD><?php
				echo TXT_BRAND;
?></TD>
<TD>
<select name="brand_id" title="Select the brand for this item."><OPTION value="0">---</OPTION>
<?php
				$result = $db->query("select id,brand from brands order by brand");
				while ($brands = $db->fetchRow($result)) {
?><option value="<?php
								echo $brands[0];
?>" <?php
								if (isset($_GET['edit_item']) && $row['brand_id'] == $brands[0])
												echo "SELECTED";
								if (isset($_GET['add_item']) && isset($_POST['brand_id']) && $_POST['brand_id'] == $brands[0])
												echo "SELECTED";
?>><?php
								echo $brands[1];
?></option><?php
				}
?>
</select>
</TD></TR>
<TR><TD><?php
				echo TXT_SUPPLIER;
?></TD>
<TD>
<select name="supplier_id" title="Select the supplier for this item."><OPTION value="0">---</OPTION>
<?php
				$result = $db->query("select id,supplier from suppliers order by supplier");
				while ($suppliers = $db->fetchRow($result)) {
?><option value="<?php
								echo $suppliers[0];
?>" <?php
								if (isset($_GET['edit_item']) && $row['supplier_id'] == $suppliers[0])
												echo "SELECTED";
								if (isset($_GET['add_item']) && isset($_POST['supplier_id']) && $_POST['supplier_id'] == $suppliers[0])
												echo "SELECTED";
?>><?php
								echo $suppliers[1];
?></option><?php
				}
?>

</select>
</TD>
<TD><?php
				echo TXT_SUPPLIER_ITEM_NUMBER;
?></TD><TD><input type="text" size="30" name="supplier_item_number" title="The supplier's SKU for this item. Maximum 50 alphanumeric characters."<?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['supplier_item_number']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['supplier_item_number']))
								echo 'value="' . $_POST['supplier_item_number'] . '"';
?>></TD></TR>

<tr>
			<td>
				<button type="button" onclick="showhide('locationdiv',true)"><?php echo TXT_LOCATIONS; ?></button>
			</td>
			<td>
			<div id="locationdiv" style="display:none">
				<table id="locations">
					<thead>
						<tr>
						<th><?php echo TXT_LOCATION; ?></th>
						<th><?php echo TXT_QTY; ?></th>
						<th><?php echo TXT_NOTES; ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						// get all the locations, display them, link to their page
						if (isset($_GET['edit_item'])) {
							echo getLocations($_GET['edit_item']);
						} else {
							echo '<tr><td>'.NO_LOCATIONS_MSG.'</td></tr>';
						}

						?>
					</tbody>
				</table>

			</div>
</td>
			<td>
				<?php
				$barcodesmsg = NO_BARCODES_MSG;
				$bctitle = NO_BARCODES_TITLE;
							if (isset($_GET['edit_item'])) {
								$res=getBarcodes($row['item_number']);
								//check valid barcode, change ui prompting
							if(!empty($res[0]['barcode'])){
								$barcodesmsg=TXT_BARCODES;
								$bctitle=BARCODES_TITLE;
							}
						}

						?>
				<button type="button" onclick="barcode('<?php echo $row['item_number']; ?>')" title="<?php echo $bctitle; ?>"><?php echo $barcodesmsg; ?></button>
			</td>
</td>
	</tr>
	<tr><td>
				<?php echo TXT_SUPPLIER_URL; ?>
			</td>
			<td><input type="text" size="50" name="supplier_url" id="supplier_url" title="<?php echo TITLE_SUPPLIER_URL; ?>" value="<?php
			echo isset($_POST['supplier_url'])? $_POST['supplier_url']:'';
?>"></td>
			<td>
				<?php echo TXT_WEBSITE_URL; ?>
			</td>
			<td><input type="text" size="30" name="website_url" id="website_url" title="<?php echo TITLE_WEBSITE_URL; ?>" value="<?php
			echo isset($_POST['website_url'])? $_POST['website_url']:'';
?>"></td>

	</tr>
</table>

	<button type="button" class="collapsible" title="<?php echo TITLE_PRICING; ?>"><?php echo TXT_PRICING; ?></button>
<div class="content">
	<table width="100%">
		<tr>
			<td>
				<?php	echo TXT_SELLING_PRICE;?>
			</td>
			<td>
				<input type="number" step="0.01" name="total_cost" title="The retail price for this item." required <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['total_cost']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['total_cost']))
								echo 'value="' . $_POST['total_cost'] . '"';
?> onchange="setPrice(2)">
			</td>
			<td>
				<?php	echo TXT_TRADE_PRICE;?>
			</td>
			<td>
				<input type="number" step="0.01" name="trade_price" title="The trade price for this item."<?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['trade_price']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['trade_price']))
								echo 'value="' . $_POST['trade_price'] . '"';
?>>
			</td>
		</tr>
		<tr>
			<td>
				<?php				echo TXT_WHOLESALE_PRICE;?>
			</td>
			<td>
				<input type="number" step="0.01" name="wholesale_price" title="The wholesale price for this item." <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['wholesale']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['wholesale_price']))
								echo 'value="' . $_POST['wholesale_price'] . '"';
?> onchange="setPrice(1)">
			</td>
			<td>
				<?php	echo TXT_UNIT_PRICE;?>
			</td>
			<td>
				<input type="number" step="0.01" name="unit_price" title="The unit cost price for this item from the supplier."<?php
				if (isset($_GET['edit_item'])){
								echo 'value="' . htmlspecialchars($row['unit_price']) . '"';
				}
				if (isset($_GET['add_item']) && isset($_POST['unit_price'])){
								echo 'value="' . $_POST['unit_price'] . '"';
				}
?> onchange="setPrice(1)" >
			</td>
		</tr>
		<tr>
			<td>
				<?php echo TXT_TAX_PERCENT;?>
			</td>
			<td>
					<input type="number" step="0.01" name="tax_percent" title="The typical tax rate applying to this item."<?php
				if (isset($_GET['edit_item'])){
								echo 'value="' . htmlspecialchars($row['tax_percent']) . '"';
				}
				if (isset($_GET['add_item'])){
								echo 'value="' . $tax['value'] . '"';
				}
?> onchange="setPrice(1)">
			</td>
						<td>
				<?php echo TXT_MARKUP;?>
			</td>
			<td>
					<input type="number" step="0.01" name="markup" title="The cost to retail markup for this item, excluding tax."<?php
				if (isset($_GET['edit_item'])){
								echo 'value="' . htmlspecialchars($row['markup']) . '"';
				}
				if (isset($_GET['add_item'])){
								echo 'value=""';
				}
?> onchange="setPrice(3)">
			</td>
		</tr>

	</table>

</div>
<button type="button" class="collapsible" title="<?php echo TITLE_STOCK_CONTROL; ?>"><?php echo TXT_STOCK_CONTROL; ?></button>
<div class="content">
<table style="width:100%">
	<tr>
			<td>
		<?php
				echo TXT_REORDER_LEVEL;
?>
	</td>
	<td>
	<input type="number" step="0.01" name="reorder_level" title="The reorder level for this item."<?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['reorder_level']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['reorder_level']))
								echo 'value="' . $_POST['reorder_level'] . '"';
?>>
	</td>
		<td>
		<?php
				echo TXT_STOCK;
?>
		</td>
		<td>
		<input type="number" step="0.01" name="quantity" title="The quantity of stock on hand." <?php
				if (isset($_GET['edit_item']))
								echo 'value="' . htmlspecialchars($row['quantity']) . '"';
				if (isset($_GET['add_item']) && isset($_POST['quantity']))
								echo 'value="' . $_POST['quantity'] . '"';
?>>
		</td>
	</tr>
	<tr>

	<td>
		<?php
				echo TXT_DATE;
?>
		</td>
		<td>
		<input type="date" size="12" name="date" id="date" title="Select the date of loading for this item." value="<?php
			echo isset($_POST['date'])? $_POST['date']:'';
?>">
		</td>
	</tr>
</table>


		<?php
				if (isset($_GET['edit_item'])) {
					//put aging on screen
					$sql="SELECT * FROM items_aging WHERE items_id=".$db->clean($_GET['edit_item']);
					if($result=$db->QPComplete($sql)){
?>
<h4><?php echo TXT_AGEING_ENTRIES; ?></h4>
<table>
	<th><?php echo TXT_DATE; ?></th>
	<th><?php echo TXT_QUANTITY; ?></th>
	<th><?php echo TXT_DESCRIPTION; ?></th>


<?php
foreach ($result as $k=>$v){
	echo '<tr>';
	echo '<td>'.$v['date'].'</td>';
	echo '<td align="center">'.$v['quantity'].'</td>';
	echo '<td>'.$v['description'].'</td>';
	echo '</tr>';
}
echo '</table><br/>';


 } //close aging  ?>
<a href="admin.php?action=reports&report=move&item=<?php
								echo $_GET['edit_item'];
?>" target="_blank"><input type="button" value="<?php
								echo TXT_MOVEMENT;
?>"></a>
<?php
				} ?>
	</p>
</div>
<button type="button" class="collapsible" title="<?php echo TITLE_DIMENSIONS; ?>"><?php echo TXT_DIMENSIONS; ?></button>
<div class="content">
<table style="width:100%">

		<tr>
			<td>
				<?php echo TXT_NET_WEIGHT; ?>
			</td>
			<td><input type="text" size="30" name="net_weight" id="net_weight" title="<?php echo TITLE_NET_WEIGHT; ?>" value="<?php
			echo isset($_POST['net_weight'])? $_POST['net_weight']:'';
?>"></td>
			<td>
				<?php echo TXT_GROSS_WEIGHT; ?>
			</td>
			<td><input type="text" size="30" name="gross_weight" id="gross_weight" title="<?php echo TITLE_GROSS_WEIGHT; ?>" value="<?php
			echo isset($_POST['gross_weight'])? $_POST['gross_weight']:'';
?>"></td>
	</tr>
		<tr>
			<td>
				<?php echo TXT_NET_HEIGHT; ?>
			</td>
			<td><input type="text" size="30" name="net_height" id="net_height" title="<?php echo TITLE_NET_HEIGHT; ?>" value="<?php
			echo isset($_POST['net_height'])? $_POST['net_height']:'';
?>"></td>
			<td>
				<?php echo TXT_GROSS_HEIGHT; ?>
			</td>
			<td><input type="text" size="30" name="gross_height" id="gross_height" title="<?php echo TITLE_GROSS_HEIGHT; ?>" value="<?php
			echo isset($_POST['gross_height'])? $_POST['gross_height']:'';
?>"></td>
	</tr>
			<tr>
			<td>
				<?php echo TXT_NET_WIDTH; ?>
			</td>
			<td><input type="text" size="30" name="net_width" id="net_width" title="<?php echo TITLE_NET_WIDTH; ?>" value="<?php
			echo isset($_POST['net_width'])? $_POST['net_width']:'';
?>"></td>
			<td>
				<?php echo TXT_GROSS_WIDTH; ?>
			</td>
			<td><input type="text" size="30" name="gross_width" id="gross_width" title="<?php echo TITLE_GROSS_WIDTH; ?>" value="<?php
			echo isset($_POST['gross_width'])? $_POST['gross_width']:'';
?>"></td>
	</tr>
				<tr>
			<td>
				<?php echo TXT_NET_DEPTH; ?>
			</td>
			<td><input type="text" size="30" name="net_depth" id="net_depth" title="<?php echo TITLE_NET_DEPTH; ?>" value="<?php
			echo isset($_POST['net_depth'])? $_POST['net_depth']:'';
?>"></td>
			<td>
				<?php echo TXT_GROSS_DEPTH; ?>
			</td>
			<td><input type="text" size="30" name="gross_depth" id="gross_depth" title="<?php echo TITLE_GROSS_DEPTH; ?>" value="<?php
			echo isset($_POST['gross_depth'])? $_POST['gross_depth']:'';
?>"></td>
	</tr>
</table>
</div>

<button type="button" class="collapsible" title="<?php echo TITLE_STYLECOL; ?>"><?php echo TXT_STYLE; ?></button>
<div class="content">
<table style="width:100%">

		<tr>
			<td>
				<?php echo TXT_STYLE; ?>
			</td>
			<td><input type="text" size="30" name="style" id="net_style" title="<?php echo TITLE_STYLE; ?>" value="<?php
			echo isset($_POST['style'])? $_POST['style']:'';
?>"></td>
			<td>
				<?php echo TXT_COLOUR; ?>
			</td>
			<td><input type="text" size="30" name="colour" id="colour" title="<?php echo TITLE_COLOUR; ?>" value="<?php
			echo isset($_POST['colour'])? $_POST['colour']:'';
?>"></td>
	</tr>
		<tr>
			<td>
				<?php echo TXT_MATERIALS; ?>
			</td>
			<td><textarea name="materials" id="materials" title="<?php echo TITLE_MATERIALS; ?>" style="width:90%"><?php
			echo isset($_POST['materials'])? $_POST['materials']:'';
?></textarea></td>
			<td>
				<?php echo TXT_MANUFACTURE_COMPONENTS; ?>
			</td>
			<td><textarea name="manufacture_components" id="manufacture_components" title="<?php echo TITLE_MANUFACTURE_COMPONENTS; ?>" style="width:90%"><?php
			echo isset($_POST['manufacture_components'])? $_POST['manufacture_components']:'';
?></textarea></td>
	</tr>
			<tr>
			<td>
				<?php echo TXT_PACKAGING_TYPE; ?>
			</td>
				<td><textarea name="packaging_type" id="packaging_type" title="<?php echo TITLE_PACKAGING_TYPE; ?>" style="width:90%"><?php
			echo isset($_POST['packaging_type'])? $_POST['packaging_type']:'';
?></textarea></td>
			<td>
				<?php echo TXT_CERTIFICATIONS; ?>
			</td>
				<td><textarea name="certifications" id="certifications" title="<?php echo TITLE_CERTIFICATIONS; ?>" style="width:90%"><?php
			echo isset($_POST['certifications'])? $_POST['certifications']:'';
?></textarea></td>
	</tr>
				<tr>
			<td>
				<?php echo TXT_AWARDS; ?>
			<td><textarea name="awards" id="awards" title="<?php echo TITLE_AWARDS; ?>" style="width:90%"><?php
			echo isset($_POST['awards'])? $_POST['awards']:'';
?></textarea></td>
			<td>
				<?php echo TXT_HAZARDS; ?>
			</td>
			<td><textarea name="hazards" id="hazards" title="<?php echo TITLE_HAZARDS; ?>" style="width:90%"><?php
			echo isset($_POST['hazards'])? $_POST['hazards']:'';
?></textarea></td>
	</tr>
</table>
</div>

<button type="button" class="collapsible" title="<?php echo TITLE_SYSTEM; ?>"><?php echo TXT_SYSTEM; ?></button>
<div class="content">

</div>

<input type="submit" <?php
				if (isset($_GET['edit_item'])){
								echo 'name="edit_item" value="' . TXT_SAVE . '"';
				}
				else{
								echo 'name="submit_item" value="' . ADD_ITEM . '"';
				}
?>>

</form>
<script type="application/x-javascript">

var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}
</script>
 <div id="barcodes" class="barcodediv iframe">
                    <div id="closebar" onclick="showhide('barcodes','FALSE')"><img src="<?php
				echo $themepath;
?>images/close.png"
                                                                           title="Close the barcodes window"/></div>
                                                                          <div id="ifcontainer" style="width:90%;"></div></div>
</div>
