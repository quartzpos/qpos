<?php
//List items
	// handle get items from paging javscript in general.js
	if (isset($_GET['prodcode']) ){ $_GET['find_prodcode'] = $_GET['prodcode']; }
	if (isset($_GET['category']) ){ $_GET['find_category'] = $_GET['category']; }
	if (isset($_GET['brand']) ){ $_GET['find_brand'] = $_GET['brand']; }
	if (isset($_GET['find_prodcode'])){ $_POST['find_prodcode'] = $db->clean($_GET['find_prodcode']); }
	if (isset($_GET['find_category'])){ $_POST['find_category'] = $db->clean($_GET['find_category']); }
	if (isset($_GET['find_brand'])){ $_POST['find_brand'] = $db->clean($_GET['find_brand']); }


?>

<table cellspacing="0">
<TR><TH colspan="12" align="left">
<form action="admin.php?action=products" method="POST">
<b><?php
				echo TXT_CATEGORY;
?>:</b>
<select name="find_category"><option value="0">---</option>
<?php
				$result = $db->query("select id,category from categories order by category");
				while ($row = $db->fetchRow($result)) {
?><option value="<?php
								echo $row[0];
?>" <?php
								if ((isset($_GET['category']) && $_GET['category'] == $row[0]) || (isset($_POST['find_category']) && $_POST['find_category'] == $row[0]))
												echo "SELECTED";
?>><?php
								echo $row[1];
?></option><?php
				}
?>
</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b><?php
				echo TXT_BRAND;
?>:</b>
<select name="find_brand"><option value="0">---</option>
<?php
				$result = $db->query("select id,brand from brands order by brand");
				while ($row = $db->fetchRow($result)) {
?><option value="<?php
								echo $row[0];
?>" <?php
								if ((isset($_GET['brand']) && $_GET['brand'] == $row[0]) || (isset($_POST['find_brand']) && $_POST['find_brand'] == $row[0]))
												echo "SELECTED";
?>><?php
								echo $row[1];
?></option><?php
				}
?>
</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="text" name="find_prodcode" placeholder="Item number to search" onclick="this.value=''" <?php if (isset($_POST['find_prodcode'])){ echo 'value="'.$_POST['find_prodcode'].'"'; } ?>/>
<input list="qty" name="row_quantity" id="row_quantity" placeholder="Report rows"/ <?php
				if (isset($_POST['row_quantity'])) {
								echo 'value="' . $_POST['row_quantity'] . '"';
				}
?>>
<datalist id="qty">
    <option value="10">
    <option value="20">
    <option value="50">
    <option value="100">
    <option value="500">
</datalist>
<input type="submit" name="find_items" value="<?php
				echo TXT_FIND_ITEMS;
?>">
<input type="button" value="<?php
				echo ADD_NEW_ITEM;
?>" onclick="document.location.href='admin.php?action=products&add_item'">
</form>
</TH></TR>
<?php
				$sql = "select SQL_CALC_FOUND_ROWS id, item_name, item_number, description, clearance, brand_id, category_id, supplier_id, trade_price, wholesale, unit_price, supplier_item_number, tax_percent, total_cost, quantity, reorder_level, image from items";
				// If we have selected a category or a brand or product code
				$q   = 0;
				if (isset($_POST['find_category']) && !empty($_POST['find_category'])){
								$q++;
								$_GET['category']=$_POST['find_category'];
				}
				if (isset($_POST['find_brand']) && !empty($_POST['find_brand'])){
								$q++;
								$_GET['brand']=$_POST['find_brand'];
				}
				if (isset($_POST['find_prodcode']) && !empty($_POST['find_prodcode'])){
								$q++;
				}
				if ($q > 1) {
								$sql .= " where";
								if (isset($_POST['find_category']) && $_POST['find_category'] != "0") {
												$sql .= " (category_id=" . $_POST['find_category'].")";
												if ($q > 1) {
												$sql .= " AND ";
								}
								}

								if (isset($_POST['find_brand']) && $_POST['find_brand'] != "0") {
												$sql .= " (brand_id=" . $_POST['find_brand'].")";
												if ($q > 1) {
												$sql .= " AND ";
								}
								}
								if (isset($_POST['find_prodcode']) && !empty($_POST['find_prodcode'])) {
								//implement a breaking down of search terms for fuzzy logic, explodes find_prodcode if it has any other characters in it
								$text = $_POST['find_prodcode'];
								$searchterms = "";
								$scount = 0;
								$exploded = multiexplode(array(",",".","|",":"," "),$text);
								if(count($exploded)>1){
									//we have a multi part  search term
									foreach($exploded as $exp=>$val){
										if($scount !==0){ $searchterms .=" OR ";}
										$searchterms.=  " ( item_number LIKE '%" . $val . "%'";
										$searchterms .= " OR description LIKE '%" . $val . "%')";
										$scount ++;
									}
									} else {
										//single word
													$sql .= "  (item_number LIKE '%" . $_POST['find_prodcode'] . "%'";
												$sql .= " OR description LIKE '%" . $_POST['find_prodcode'] . "%')";
									}
								} else {
									//rtrim the last AND
									$sql = rtrim($sql,'AND ');
								}


								if (isset($searchterms) && !empty($searchterms)){
									$sql .=" ( ". $searchterms. " ) "; //so we have an exploded search term
									}
				}

				if (isset($_GET['category']) && $q==1) {
								$sql .= " where category_id=" . $_GET['category'];
				}
				if (isset($_GET['brand']) && $q==1) {
								$sql .= " where brand_id=" . $_GET['brand'];
				}
				if (isset($_POST['find_prodcode']) && !empty($_POST['find_prodcode']) && $q==1) { //otherwise use regular search

												$sql .= "  WHERE (item_number LIKE '%" . $_POST['find_prodcode'] . "%'";
												$sql .= " OR description LIKE '%" . $_POST['find_prodcode'] . "%')";
								}
				//Ten (10) items per page default
				if (isset($_GET['row_quantity']) && is_numeric($_GET['row_quantity']) && $_GET['row_quantity'] > 0) {
								$_POST['row_quantity'] = $_GET['row_quantity'];
				}
				$ipp = ITEMS_PER_PAGE; // ipp will be the number of report rows passed
				if (isset($_POST['row_quantity']) && !empty($_POST['row_quantity'])) {
								if (is_numeric($_POST['row_quantity']) && $_POST['row_quantity'] > 0) {
												$ipp = $_POST['row_quantity'];
								}
				}
				$sql .= " ORDER BY item_name ";
				$sql .= " limit " . (isset($_GET['page']) ? ($_GET['page'] - 1) * $ipp : 0) . "," . $ipp;
				//ChromePhp::log($sql,"products.php search sql=");
				//echo $sql;
				if(!$result = $db->QPComplete($sql)){
					echo "Either there are no results, or that search has an error:".$sql." .<br/>";
				}

				//The total of items that this query would return without the limit clause
				$found_rows      = $db->query("SELECT FOUND_ROWS()");
				$total_num_items = $db->fetchRow($found_rows);
				$npages          = ceil($total_num_items[0] / $ipp);
				if (!empty($npages)) {
?>
<TR><TH colspan="6" align="left">&nbsp;&nbsp;page<?php
								echo (isset($_GET['page']) ? $_GET['page'] : 1);
								echo TXT_OF . $npages;
?></TH>
<TH colspan="6" align="right">
<div id="pageset">&nbsp;</div>
<?php
								if ($npages > 1) {
?>
<script language="javascript">setPages(<?php
												echo $npages;
?>,<?php
												echo (isset($_GET['page']) ? $_GET['page'] : 1);
												echo "," . $ipp;
												$passstr="";
												if(isset($_POST['find_prodcode']) && !empty($_POST['find_prodcode'])){ $passstr.="&find_prodcode=".$_POST['find_prodcode']; }
												if(isset($_POST['find_category']) && !empty($_POST['find_category'])) { $passstr.="&find_category=".$_POST['find_category']; }
												if(isset($_POST['find_brand']) && !empty($_POST['find_brand'])){ $passstr.="&find_brand=".$_POST['find_brand']; }
												if(!empty($passstr)) { echo ",'".$passstr."'";}
?>);</script>
<?php
								}
?>
</TH></TR>
<TR><TH width="24%" colspan="2"><?php
								echo TXT_ITEM_NAME;
?></TH>
<TH width="34px" align="center"><img src="<?php
								echo $themepath;
?>images/barcode.png" alt="Barcode" Title="Barcode" /></th><TH width="100" align="left"><?php
								echo TXT_ITEM_NUMBER;
?></TH>
<TH width="5%" align="left"><?php
								echo TXT_CLEARANCE;
?></TH>
<TH width="11%" align="left"><?php
								echo TXT_BRAND;
?></TH>
<TH width="11%" align="left"><?php
								echo TXT_CATEGORY;
?></TH>
<TH width="5%"><?php
								echo TXT_TRADE_PRICE;
?></TH>
<TH width="5%"><?php
								echo TXT_WHOLESALE_PRICE;
?></TH>
<TH width="5%"><?php
								echo TXT_UNIT_PRICE;
?></TH>
<TH width="3%"><?php
								echo TXT_TAX_PERCENT;
?></TH>
<TH width="5%"><?php
								echo TXT_SELLING_PRICE;
?></TH>
<TH width="5%"><?php
								echo TXT_STOCK;
?></TH>
<TH width="5%"><?php
								echo TXT_EDIT;
?></TH>
<TH width="5%"><?php
								echo TXT_DELETE;
?></TH>
</TR>
<?php
if (isset($result)){
								foreach ($result as $row) {
												$rcategory = $db->query("select category from categories where id=" . $row['category_id']);
												$rbrand    = $db->query("select brand from brands where id=" . $row['brand_id']);
												$category  = $db->fetchRow($rcategory);
												$brand     = $db->fetchRow($rbrand);
												if (empty($row['image'])) {
																$row['image'] = $themepath . "images/products.png";
												}
?>
<script language="JavaScript">
  function delete_item(item){
    op = confirm("Do you really want to delete this item?");
    if(op)document.location.href="admin.php?action=products&delete=" + item;
  }
  function barcode(prodcode){
  showhide('barcodes','TRUE');
 console.log("Show barcodes div, product="+prodcode);
  node = document.getElementById("ifcontainer");
  while (node.hasChildNodes()) {
    node.removeChild(node.lastChild);
}
  var el = document.createElement("iframe");
el.setAttribute('id', 'barcodeiframe');
node.appendChild(el);
el.setAttribute('src', "barcodes.php?prodcode="+prodcode);

  }
  function toggle_clearance(prodcode){
	     $.ajax({
            type: "GET",
            url: "getItems.php",
            data: "clearitem=" + prodcode,
            dataType: "json",
            cache: false,
            success: function (result) {
				console.log("Toggle clearance on item="+prodcode);
            },
            error: function () {
                console.log("Failure to toggle clearance on item="+prodcode);

            },
        });
	}
</script>
<TR>
<TD width="5%" class="tvalue"><img width="<?php
												echo ITEM_IMG_SIZE;
?>" height="<?php
												echo ITEM_IMG_SIZE;
?>" src="<?php
												echo $row['image'];
?>"></TD>
<TD width="16%" class="btvalue"><?php
												echo htmlspecialchars($row['item_name']);
?></TD>
<?php
// check for existence of barcodes, and use appropriate image
$sql="SELECT * FROM barcodes WHERE prodcode='".$row['item_number']."' AND ORD(barcode) >0";
$bcs = $db->QPResults($sql);

if($bcs && !empty($bcs['barcode'])){
	$bcsimage="barcode.png";
} else {
	$bcsimage="barcode_off.png";
}
?>
<Td width="34px" align="center" class="btvalue"><img src="<?php
												echo $themepath;
												echo "images/".$bcsimage."\"";
?> alt="Barcode" Title="Barcode" onclick="barcode('<?php
												echo $row['item_number'];
?>')" /></td>
<TD width="7%" class="tvalue"><?php
												echo htmlspecialchars($row['item_number']);
?></TD>
<TD width="5%" class="tvalue"><?php
												echo   '<input type="checkbox" id="clear-'.$row['item_number'].'" name="clear-'.$row['item_number'].'"';
												if ($row['clearance']){echo ' checked '; }
												echo ' onclick="toggle_clearance(\''.$row['item_number'].'\')" ';
												echo ' title="Clearance item if ticked:'.$row['item_number'].'"';
												echo '>';


?></TD>
<TD width="11%" class="tvalue"><?php
												if (isset($brand[0]))
																echo htmlspecialchars($brand[0]);
												else
																echo "-";
?></TD>
<TD width="11%" class="tvalue"><?php
												if (isset($category[0]))
																echo htmlspecialchars($category[0]);
												else
																echo "-";
?></TD>
<TD width="5%" align="center" class="tvalue"><?php
												echo htmlspecialchars($row['trade_price']); //price
?></TD>
<TD width="5%" align="center" class="tvalue"><?php
												echo htmlspecialchars(money_formatter( $row['wholesale'])); //wholesale price
?></TD>
<TD width="5%" align="center" class="tvalue"><?php
												echo htmlspecialchars(money_formatter( $row['unit_price'])); //unit price
?></TD>
<TD width="3%" align="center" class="tvalue"><?php
												echo htmlspecialchars($row['tax_percent']); //tax
?></TD>
<TD width="5%" align="center" class="tvalue"><?php
												echo htmlspecialchars($row['total_cost']); //retail
?></TD>
<TD width="5%" align="center" class="tvalue"><?php
												echo htmlspecialchars($row['quantity']); //qty
?></TD>
<TD width="5%" align="center" class="tvalue"><a href="admin.php?action=products&edit_item=<?php
												echo $row['id'];
?>"><img src="<?php
												echo $themepath;
?>images/edit.png" title="Edit <?php
												echo htmlspecialchars($row['item_number']);
?>"></a></TD>
<TD width="5%" align="center" class="tvalue"><a href="Javascript:delete_item(<?php
												echo $row['id'];
?>)"><img src="<?php
												echo $themepath;
?>images/delete.png" title="Delete <?php
												echo htmlspecialchars($row['item_number']);
?>"></a></TD>
</TR>
<?php
								}
								}
				} else { //there are no products
								$output = "No products loaded.";
				}
?>
</table>
<?php
				if (isset($output)) {
								echo $output;
				}
?>
  <div id="barcodes" class="barcodediv iframe">
                    <div id="closebar" onclick="showhide('barcodes','FALSE')"><img src="<?php
				echo $themepath;
?>images/close.png"
                                                                           title="Close the barcodes window"/></div>
                                                                          <div id="ifcontainer" style="width:90%;"></div></div>
