<?php
/*
Quartzpos, Open Source Point-Of-Sale System
http://Quartzpos.com



Released under the GNU General Public License
*/

if(!isset($_SESSION))session_start();
if(!isset($_SESSION['admin'])){
header("Location:admin.php");
}
require_once("config.php");
if (defined('DEBUG_CONSOLE') && DEBUG_CONSOLE){
    require 'consoleLogging.php';

    ChromePhp::log($_REQUEST, "ADMIN php: starting REQUEST=");

    ChromePhp::log($_GET, "ADMIN php: GET=");

    ChromePhp::log($_POST, "ADMIN php: POST=");

    ChromePhp::log($_SESSION, "ADMIN php: starting Session=");
    }
require_once("languages/languages.php");

require_once("database.php");
require_once("functions.php");
require_once ('helpers.php');
?>

<?php
global $db;
$sql="SELECT i.id,i.item_name,i.description,d.location,b.barcode, c.category, br.brand,s.supplier ,i.total_cost,i.unit_price, i.quantity FROM items i LEFT JOIN items_dimensions d on d.items_id=i.id LEFT JOIN barcodes b on b.prodcode=i.item_number LEFT join categories c ON c.id = i.category_id left join brands br on br.id = i.brand_id left join suppliers s on s.id=i.supplier_id order by i.item_name";
$result=$db->QPComplete($sql);
$output='';

if($result){
	$output .='<tr><th>ID</th><th>Name</th><th>Description</th><th>Location</th><th>Barcode</th><th>Category</th><th>Brand</th><th>Supplier</th><th>Retail</th><th>Cost</th><th>Qty</th></tr><tbody>';
	foreach($result as $k=>$v){
		//$v is the array of the row
		$output .= '<tr>';
				foreach($v as $key=>$av){
					$output .='<td class="grid">'.$av.'</td>';
				}
				$output .= '<tr>';
	}
echo '<table cellspacing="0">'.$output.'</tbody></table>';
}

?>
$(document).ready(function(){
	$('#data_table').Tabledit({
		deleteButton: false,
		editButton: false,   		
		columns: {
		  identifier: [0, 'id'],                    
		  editable: [[1, 'name'], [2, 'description'], [3, 'location'], [4, 'barcode'], [5, 'category'],[6, 'brand'],[7, 'supplier'],[8, 'retail'],[9, 'cost'],[10, 'qty']]
		},
		hideIdentifier: true,
		url: 'live_edit.php'		
	});
});