<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */
if (!isset($_SESSION))
    session_start();
require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");
require_once("functions.php");
$timezone = $db->QPResults("SELECT value FROM system WHERE name='timezone'")['value'];
if (!isset($_SESSION['admin'])) {
    header("Location:admin.php");
}
$header = '';
$get = '';


foreach ($_GET as $key => $value) {
    $get.=$key . '=' . $value . '&';
}
if (isset($_POST['date1']) && $_POST['date1'] != '' && isset($_POST['date2']) && $_POST['date2'] != '' && $_POST['date1'] > $_POST['date2']) {
    print" From date is greater than To date. Sorry, but the time machine is broken ;-) <br>";
}


//$dsurl = "dailysummary.php?date=" . date("Y-m-d");   // build daily summary url for today
$menulink=Array(); //sales menu
$imenulink=Array(); // items menu
$files = glob('reports/*.menu', GLOB_BRACE);
foreach($files as $file) {
  include_once( $file );
}
?>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  border-radius: 4px;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: #555;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1;}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>


<div class="admin_content">
   <div id="nav">
    <ul>
   <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">Sales</a>
    <div class="dropdown-content">
     <?php foreach($menulink as $k=>$link){
    echo '<a href="'.$link['href'].'" target="'.$link['target'].'" title="'.$link['title'].'">'.$link['text'].'</a>';
} ?>
    </div>
  </li>
  <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">Items</a>
    <div class="dropdown-content">
      <?php foreach($imenulink as $k=>$link){
    echo '<a href="'.$link['href'].'" target="'.$link['target'].'" title="'.$link['title'].'">'.$link['text'].'</a>';
}
?>
    </div>
  </li>
</ul>
</div>

    <?php
    $subtitle = "Reports";
    if(isset($_GET['id']) && !isset($_GET['report'])){$_GET['report'] = 'trans';} //set default report when passed just an id number
    $reportfile='reports/'.$_GET['report'].'.php';
    if(is_file($reportfile)){
        include_once($reportfile);
    }
    if (isset($_GET['report'])) {

        //these functions are used to build the UI form

        function form_start($get){
            return  '<b>Choose options:</b><br><br><form action="admin.php?'.$get.'" method="POST" name="form_report1" autocomplete="off">';
        }
        function form_date1(){
            if(isset($_POST['submit_report1'])){ $pdate =  $_POST['date1']; } else { $pdate = '';}
            $output = 'Date <input type="text" size="12" id="date1" name="date1" autocomplete="off" value="' .$pdate . '">';
            return $output;
        }
        function form_date2(){
            if(isset($_POST['submit_report1'])){ $pdate =  $_POST['date2']; } else { $pdate = '';}
            $output = 'to <input type="text" size="12" id="date2" name="date2" autocomplete="off" value="'.$pdate.'">';
            return $output;
        }
        function form_user(){
            global $db;
            $result = $db->QPComplete("select id, first_name, last_name from users");

            $output = '&nbsp;&nbsp;&nbsp;User <select name="user"><option value="-1">---</option>';
            foreach($result as $k=>$v){
                 if (isset($_POST['user']) && $_POST['user'] == $v['id']) { $user = ' SELECTED '; } else { $user= ' ';}
                 $output .= '<option value="'.$v['id'].'"'.$user.' >'.$v['first_name'].'  '.$v['last_name'].'</option>';

            }
            $output .= '</select>';
            return $output;
        }
        function form_month(){
          if (isset($_POST['months'])){ $month= $_POST['months']; } else { $month = 1; }
            return '||&nbsp;Months<input type="number"  name="months" id="months"  min="1" max="12" value="'.$month.'">&nbsp;';

        }
        function form_item(){
            global $db;
            $result = $db->QPComplete("select id,item_number, item_name from items");
            if(isset($_GET['item']) && !(isset($_POST['item']))){$_POST['item']=$db->clean($_GET['item']);}
            $output = '&nbsp;&nbsp;&nbsp;Item <select name="item"><option value="-1">---</option>';
            foreach($result as $k=>$v){
                 if (isset($_POST['item']) && $_POST['item'] == $v['id']) { $item = ' SELECTED '; } else { $item= ' ';}
                 $output .= '<option value="'.$v['id'].'"'.$item.' >'.$v['item_number'].'  '.$v['item_name'].'</option>';

            }
            $output .= '</select>';
            return $output;
        }
        function form_cats(){
            global $db;
            $result = $db->QPComplete("select * from categories ORDER BY category ASC");

            $output = 'Category <select name="category"><option value="-1">---</option>';
            foreach($result as $k=>$v){
                 if (isset($_POST['category']) && $_POST['category'] == $v['id']) { $item = ' SELECTED '; } else { $item= ' ';}
                 $output .= '<option value="'.$v['id'].'"'.$item.' >'.$v['category'].'</option>';

            }
            $output .= '</select>';
            return $output;
        }
        function form_till(){
            global $db;
            $result = $db->QPComplete("select distinct till from sales");

            $output = 'Till <select name="till"><option value="-1">---</option>';
            foreach($result as $k=>$v){
                 if (isset($_POST['till']) && $_POST['till'] == $v['till']) { $item = ' SELECTED '; } else { $item= ' ';}
                 $output .= '<option value="'.$v['till'].'"'.$item.' >'.$v['till'].'</option>';

            }
            $output .= '</select>';
            return $output;
        }
        function form_end(){
            return '&nbsp;<input type="submit" value="Submit" name="submit_report1"><br></form>';
        }
        function overview($get){
            if(isset($_POST['submit_report2'])){ $pdate1 =  $_POST['date1']; } else { $pdate1 = '';}
            if(isset($_POST['submit_report2'])){ $pdate2 =  $_POST['date2']; } else { $pdate2 = '';}
            if(isset($_POST['submit_report2'])){ $nodays =  $_POST['nodays']; } else { $nodays = '';}
            $output = '<div class="admin_content"><div id="chartdiv" style="height:400px;width:1600px; "></div>
                     <form action="admin.php?'.$get.'" method="POST" name="form_report1">
                Start Date <input type="text" size="12" id="date1" name="date1" autocomplete="off" value="'.$pdate1.'">
                Finish Date <input type="text" size="12" id="date2" name="date2" autocomplete="off" value="'.$pdate2.'">
                 or #Days<input type="number" size="12" id="nodays" name="nodays" autocomplete="off" value="'.$nodays.'">
                <input type="submit" value="Plot" name="submit_report2"></form>
             </div>';

             return $output;
        }
        ?>
        <div id="hdr_report"><h2><?php echo $subtitle; ?></h2>
            <?php
            // flexible way to build the form according to the report's included php file
                if(isset($formstart) && $formstart) { echo form_start($get); }
                if(isset($formdate1) && $formdate1) { echo form_date1(); }
                if(isset($formdate2) && $formdate2) { echo form_date2(); }
                if(isset($formuser) && $formuser) { echo form_user(); }
                if(isset($formmonth) && $formmonth) { echo form_month(); }
                if(isset($formitem) && $formitem) { echo form_item(); }
                if(isset($formcats) && $formcats) { echo form_cats(); }
                if(isset($formtill) && $formtill) { echo form_till(); }
                if(isset($formend) && $formend) { echo form_end(); }
                if(isset($overview) && $overview) { echo overview($get); }

            }?>
        </div>
    <?php
            if(isset($transactions) && $transactions) { trans_res(); }
    //---------------------------------------------------------------------------------------------
    //  clean up some get and post params
    $from = 0;
    $to = 30;
    if (isset($_GET['from'])) {

        if (is_numeric($_GET['from']) && $_GET['from'] > -1) {
            $from = $db->clean($_GET['from']);
        } else {
            $from = 0;
        }
    }
    if (isset($_GET['to'])) {

        if (is_numeric($_GET['to']) && $_GET['to'] > -1) {
            $to = $db->clean($_GET['to']);
        } else {
            $to = 30;
        }
    }
    if (isset($_GET['report'])) {
        $report = $_GET['report'];

        if (isset($_GET['date1'])){
            $_POST['date1'] = $_GET['date1'];}
        if (isset($_GET['date2'])){
            $_POST['date2'] = $_GET['date2'];}
        if (isset($_GET['user'])){
            $_POST['user'] = $_GET['user'];}
        if (isset($_GET['till'])){
            $_POST['till'] = $_GET['till'];}
//---------------------------------------------------------------------------------------------

    if(isset($formhandle) && $formhandle) { $moddata = form_handle(); }// with a form handler in every included report we can build the modification data required
    $mod = '';$total = 0;
    if(isset($increport) && $increport){ include_once ('reports/includes/'.$increport.'report.php'); }

        } // end isset get report
        ?>
    <div id="dailysum" class="dailysum iframe">
        <div id="closedailysum" onclick="showhide('dailysum', 'FALSE')"><img src="<?php echo $themepath; ?>images/close.png"
                                                                             title="Close the Daily Summary window"/></div>
        <span id="dsumtext"><b>Daily Summary:</b><iframe id="dsframe" ></iframe>
    </div>
</div>
</body>
</html>
