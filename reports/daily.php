<?php
$subtitle = "Daily report";
$formstart = 1; // start the form
$formdate1 = 1; // first date field
$formdate2 = 0; // second date field
$formtill = 1; // till field
$formend = 1; // end the form
$formhandle = 1; // process the form as per following function
$increport='trans'; // use the transaction report format
function form_handle(){
		$retarr = Array(); global $timezone;
				$nz_time = new DateTime(null, new DateTimezone($timezone));
                if (isset($_POST['date1']) && $_POST['date1'] != '') {
                    $retarr['datemod'] = $_POST['date1'];
                } else {
                    $retarr['datemod'] = $nz_time->format('Y-m-d');
                }


                if (isset($_POST['till']) && $_POST['till'] > 0) {
                    $retarr['tillmod'] = $_POST['till'];

                }
                if (isset($_POST['user']) && $_POST['user'] > -1) {
                    $retarr['usermod'] = $_POST['user'];

                }
				return $retarr;
}
