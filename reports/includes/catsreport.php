<?php
$sql = 'SELECT i.quantity_purchased,i.item_unit_price,i.item_name,i.item_description,s.date,s.id FROM sales_items i inner join sales s on i.sale_id=s.id inner join items it on i.item_id=it.id inner join categories c on it.category_id=c.id WHERE ';
if (isset($moddata['date2mod']) && isset($moddata['datemod'])) {
    $datestring = 's.date BETWEEN "' . $moddata['datemod'] . '" AND "' . $moddata['date2mod'] . '"';
} else if (isset($moddata['date2mod']) && !isset($moddata['datemod'])) {
    if (isset($moddata['month'])) {
        $datestring = '(s.date BETWEEN SUBDATE(CURDATE(), INTERVAL ' . $moddata['month'] . ' MONTH) AND "' . $moddata['date2mod'] . '")';
    }
} else if (isset($moddata['datemod']) && !empty($moddata['datemod'])) {
    $datestring = '(s.date BETWEEN "' . $moddata['datemod'] . '" AND NOW())';
} else {
    $datestring = '(s.date BETWEEN SUBDATE(CURDATE(), INTERVAL ' . $moddata['month'] . ' MONTH) AND NOW())';
}
if (isset($_GET['item'])) {
    $_POST['item'] = $_GET['item'];
}
if (isset($_POST['item']) && $_POST['item'] != '-1' && is_numeric($_POST['item'])) {
    $datestring.= ' AND i.item_id = ' . $_POST['item'];
}
if (isset($_POST['category'])) {
    $datestring.= " AND c.id=" . $db->clean($_POST['category']);
}
$sql = $sql . $datestring . ' ORDER BY i.item_description ASC';
//echo $sql;
$result = $db->query($sql);
if (!empty($result)) {
    $translist = '<table cellspacing="4"><th>Sold</th><th>Price</th><th>Item Number<th>Item</th><th>Date</th><th>Subtotal</th>';
    $grandtotal = 0;
    $qtypur = 0;
    while ($irow = $db->fetchAssoc($result)) {
        $subt = $irow['quantity_purchased'] * $irow['item_unit_price'];
        $qtypur+= $irow['quantity_purchased'];
        $grandtotal+= $subt;
        $translist.= "<tr><td style='text-align:right;'><a href='admin.php?action=reports&report=trans&id=" . $irow['id'] . "' title='Transaction" . $irow['id'] . "' target='_blank'>" . $irow['quantity_purchased'] . "</a></td><td style='text-align:right;'>" . $irow['item_unit_price'] . "</td><td style='text-align:center;'>" . $irow['item_name'] . "</td><td>" . $irow['item_description'] . "</td><td>" . $irow['date'] . "</td><td style='text-align:right;'>" . $subt . "</td></tr>";
    }
    echo '<h2>Total sold:' . $qtypur . ' at value:$' . $grandtotal . '</h2>';
    echo $translist . "</table>";
} else {
    echo "Failure to retrieve results of SQL: " . $sql;
}
?>