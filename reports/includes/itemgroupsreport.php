<?php
$mod = '';
$total = 0;
$arr = Array();
$transactions = Array();
// build mod based upon above mod values
if (isset($moddata['date2mod']) && isset($moddata['datemod'])) {
    $datestring = '(s.date BETWEEN "' . $moddata['datemod'] . '" AND "' . $moddata['date2mod'] . '")';
} else if (isset($moddata['date2mod']) && !isset($moddata['datemod'])) {
    if (isset($moddata['month'])) {
        $datestring = '(s.date BETWEEN SUBDATE(CURDATE(), INTERVAL ' . $moddata['month'] . ' MONTH) AND "' . $moddata['date2mod'] . '")';
    }
} else if (isset($moddata['datemod']) && !empty($moddata['datemod'])) {
    $datestring = '(s.date BETWEEN "' . $moddata['datemod'] . '" AND NOW())';
} else {
    $datestring = '(s.date BETWEEN SUBDATE(CURDATE(), INTERVAL ' . $moddata['month'] . ' MONTH) AND NOW())';
}
if (isset($moddata['category'])) {
    $datestring.= " AND c.id=" . $moddata['category'];

}
$sql = "SELECT i.quantity_purchased as sold,i.item_unit_price as price,it.id as item_id,i.item_name as name,i.item_description,s.sale_total_cost,s.time,s.date,s.id as sale_id, s.sold_by, c.category as category FROM sales_items i inner join sales s on i.sale_id=s.id inner join items it on i.item_id=it.id inner join categories c on it.category_id=c.id WHERE ";
$sql = $sql . $datestring . ' ORDER BY c.category ASC,i.item_name ASC';
//echo $sql;
$catcount = 0;

$result = $db->QPComplete($sql);

if ($result) {
    //build array where items are tallied
    $items = array();
    foreach ($result as $k=>$v){
        if(isset($items[$v['item_id']]['name'])){ // have one already
            $items[$v['item_id']]['tally'] += $v['sold'];
            $items[$v['item_id']]['total'] +=$v['sold']*$v['price'];
        } else { // build that item record
            $items[$v['item_id']] =Array();
            $items[$v['item_id']]['item_id']=$v['item_id'];
            $items[$v['item_id']]['name']=$v['name'];
            $items[$v['item_id']]['tally']= intval ($v['sold']);
            $items[$v['item_id']]['category'] = $v['category'];
            $items[$v['item_id']]['description']= $v['item_description'];
            $items[$v['item_id']]['total']=$v['sold']*$v['price'];
        }

    }


    $cat=''; $catarr= Array();
     //get category totals
     foreach($items as $k=>$v){

        if( $cat != $v['category'] ){
            $cat = $v['category'];
            $catarr[$cat]['cat']=$cat;

            $catarr[$cat]['items'][] = $v;
            $catarr[$cat]['tally']=$v['tally'];
            $catarr[$cat]['total']=$v['total'];
        } else {
            $catarr[$cat]['tally']+=$v['tally'];
            $catarr[$cat]['total']+=$v['total'];
            $catarr[$cat]['items'][] = $v;
        }
     }


         echo ' <table cellspacing="0"><th>Category</th><th style="text-align: left;">Sold</th><th style="text-align: right;">Value</th>';
   foreach( $catarr as $k=> $v){
    //var_dump($v);
    echo '<tr><td class="tvalue">'.$v['cat'].'</td><td class="tvalue">'.$v['tally'].'</td><td class="tvalue">'.$v['total'].'</td>';
    echo '</tr>';

   }
   echo '</table><br>';


   echo ' <table cellspacing="0"><th>Category</th><th style="text-align: left;">Item</th><th style="text-align: left;">Description</th><th style="text-align: left;">Sold</th><th style="text-align: right;">Value</th><th>Edit</th>';
   foreach( $items as $k=> $v){
    echo '<tr><td class="tvalue">'.$v['category'].'</td><td class="tvalue">'.$v['name'].'</td><td class="tvalue">'.$v['description'].'</td><td class="tvalue">'.$v['tally'].'</td><td class="tvalue">'.$v['total'].'</td>';
    echo '<td width="5%" align="center" class="tvalue"><a target="_blank" href="admin.php?action=products&edit_item='.$v['item_id'].'"><img src="'.$themepath.'images/edit.png" title="Edit '.$v['name'].'"></a></td></tr>';

   }
   echo '</table>';

} else {
    echo "Failed: ".$sql;
}
?>