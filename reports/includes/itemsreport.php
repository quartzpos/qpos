<?php
$mod = '';
$total = 0;
$arr = Array();
$transactions = Array();
// build mod based upon above mod values
if (isset($moddata['datemod'])) $mod = " WHERE date='" . $moddata['datemod'] . "'";
$sql = "SELECT * FROM sales_items JOIN sales ON sales_items.sale_id=sales.id" . (isset($mod) ? $mod : '') . " ORDER BY sales_items.item_name";
//echo $sql;
$result = $db->QPComplete($sql);
if ($result) {
    echo ' <table cellspacing="4"><th style="text-align: left;">Item</th><th style="text-align: left;">Item number<th style="text-align: left;">Sold</th><th style="text-align: right;">Transactions</th>';
    foreach ($result as $k => $irow) {
        if (isset($arr[$irow['item_id']])) { // count the quantity purchased - the $arr[item_id] array contains the item quantities
            $arr[$irow['item_id']] = $arr[$irow['item_id']] + $irow['quantity_purchased'];
        } else {
            $arr[$irow['item_id']] = $irow['quantity_purchased'];
        }
        $transactions[] = $irow;
    }
    foreach ($arr as $key => $val) {
        $sql = "SELECT * FROM items WHERE id='" . $key . "'";
        if (!$res = $db->QPComplete($sql)) {
            echo "Failure with SQL:" . $sql;
        } else {
            $row = $res[0];
            $item = $row['item_name'];
            $prodcode = $row['item_number'];
            $translist = '<td>
<img src="' . $themepath . 'images/play.png" title="Show transaction details" onclick="showhide(\'transactions' . $row['id'] . '\',\'TRUE\');"/>
<div id="transactions' . $row['id'] . '" class="items" style="padding:30px; display:none;z-index:' . (99 + $row['id']) . '">
<div id="closeitem" onclick="showhide(\'transactions' . $row['id'] . '\',\'FALSE\');">
<img src="' . $themepath . 'images/close.png" title="Close the transaction window"/>
</div><table class="translist"><th>Sale ID</th><th>Total</th><th>Date</th><th>Time</th><th>User</th><th style="text-align:right">Qty</th>
';
            foreach ($transactions as $transaction) {
                if ($transaction['item_id'] == $row['id']) {
                    $userdat = get_userdata($transaction['sold_by'], $db);
                    if (!empty($userdat)) {
                        $translist.= "<tr><td><a href='admin.php?action=reports&report=trans&id=" . $transaction['sale_id'] . "' title='Transaction" . $transaction['sale_id'] . "' target='_blank'>" . $transaction['sale_id'] . "</a></td><td>$" . $transaction['sale_total_cost'] . "</td><td>" . $transaction['date'] . "</td><td>" . $transaction['time'] . "</td><td>" . $userdat['username'] . "</td><td style='text-align:right'>" . $transaction['quantity_purchased'] . "</td></tr>";
                    } else {
                        $translist.= "<tr><td><a href='admin.php?action=reports&report=trans&id=" . $transaction['sale_id'] . "' title='Transaction" . $transaction['sale_id'] . "' target='_blank'>" . $transaction['sale_id'] . "</a></td><td>$" . $transaction['sale_total_cost'] . "</td><td>" . $transaction['date'] . "</td><td>" . $transaction['time'] . "</td><td>Deleted user</td><td style='text-align:right'>" . $transaction['quantity_purchased'] . "</td></tr>";
                    }
                }
            }
            $translist.= "</table></div>";
            echo '<tr><td>' . $item . '</td><td>' . $prodcode . '</td><td>' . $val . '</td>' . $translist . '</tr>';
        }
    }
    echo '</table>';
    echo '<br>The items report is how many of an item has been sold in the period selected - default TODAY. It is ordered by Product code.<br>';
}
?>