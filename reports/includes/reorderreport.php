<?php
global $db;
$sort = 's.supplier';$sortAD='ASC'; $sortlink="DESC";  //defaults for sorting
if(isset($_GET['sort'])){
switch ($_GET['sort']){
	case 'supplier':
		$sort='s.supplier';
		break;
	case 'brand':
		$sort='br.brand';
		break;
	case 'name':
		$sort='i.item_name';
		break;
	case 'category':
		$sort='c.category';
		break;
}
}
if(isset($_GET['sortlink'])){
	if($_GET['sortlink']==="DESC"){$sortAD="DESC"; $sortlink="ASC";} else {$sortAD="ASC";$sortlink="DESC"; }
}
$sql="SELECT i.id,i.item_name,i.description,d.location,b.barcode, c.category, br.brand,s.supplier ,i.total_cost,i.unit_price,i.reorder_level, i.quantity FROM items i LEFT JOIN items_dimensions d on d.items_id=i.id LEFT JOIN barcodes b on b.prodcode=i.item_number LEFT join categories c ON c.id = i.category_id left join brands br on br.id = i.brand_id left join suppliers s on s.id=i.supplier_id  WHERE  ( i.quantity <= i.reorder_level AND i.reorder_level > 0) order by ".$sort." ".$sortAD;
//echo $sql;
$result=$db->QPComplete($sql);
$output='';

if($result){
	$output .='<tr><th><a href="admin.php?action=reports&report=reorder&sortlink='.$sortlink.'&sort=name" target="_self">Name</a></th><th><a href="admin.php?action=reports&report=reorder&sortlink='.$sortlink.'&sort=category" target="_self">Category</th><th><a href="admin.php?action=reports&report=reorder&sortlink='.$sortlink.'&sort=brand" target="_self">Brand</a></th><th><a href="admin.php?action=reports&report=reorder&sortlink='.$sortlink.'&sort=supplier" target="_self">Supplier</a></th><th>Retail</th><th>Cost</th><th>ReOrd</th><th>Qty</th><th>Edit</th></tr><tbody>';
	foreach($result as $k=>$v){
		//$v is the array of the row
		$output .= "<tr>";
				foreach($v as $key=>$av){
					$warn = 0; $style=''; $rmsg='';
					if(intval( $v['reorder_level']) > intval( $v['quantity'])){ $warn = 3;}
					if(intval( $v['reorder_level']) == intval( $v['quantity'])){ $warn = 2;}
					if(intval( $v['reorder_level']) < intval( $v['quantity'])){ $warn = 1;}
					if(intval( $v['reorder_level'])*1.1 < intval( $v['quantity'])){ $warn = 0;}
					if(intval( $v['reorder_level']) == 0 ){ $warn = 0;}
					//if($key=="reorder_level"){
						switch ($warn){
							case 3:
								$style = "font-weight:bold;color:red;background:white;";

								break;
							case 2:
								$style = "font-weight:normal;color:black;background:white;";

								break;
							case 1:
								$style = "font-weight:normal;color:black;background:green;";

								break;
							case 0:
								$style ='';
								break;
						}

					//}
				switch ($key){ // limit the columns shown in table
					case 'item_name':
					case 'category':
					case 'brand':
					case 'supplier':
					case 'total_cost':
					case 'unit_price':
					case 'reorder_level':
					case 'quantity':
						$output .='<td class="grid" style="'.$style.'">'.$av.'</td>';
					break;

				}
				}
				$output .= '<td width="5%" align="center" class="tvalue"><a target="_blank" href="admin.php?action=products&edit_item='.$v['id'].'"><img src="'.$themepath.'images/edit.png" title="Edit '.$v['item_name'].'"></a></td>';
				$output .= '<tr>';
	}
echo '<table cellspacing="0">'.$output.'</tbody></table>';
echo '<style>@media print {
html body * { display:none;}

.admin_content { display:block;}}</style>';
}
?>