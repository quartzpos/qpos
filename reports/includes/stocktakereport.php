<?php
global $db;
$sql="SELECT i.id,i.item_name,i.description,d.location,b.barcode, c.category, br.brand,s.supplier ,i.total_cost,i.unit_price,i.reorder_level, i.quantity FROM items i LEFT JOIN items_dimensions d on d.items_id=i.id LEFT JOIN barcodes b on b.prodcode=i.item_number LEFT join categories c ON c.id = i.category_id left join brands br on br.id = i.brand_id left join suppliers s on s.id=i.supplier_id order by i.item_name";
$result=$db->QPComplete($sql);
$output='';

if($result){
	$output .='<tr><th>ID</th><th>Name</th><th>Description</th><th>Location</th><th>Barcode</th><th>Category</th><th>Brand</th><th>Supplier</th><th>Retail</th><th>Cost</th><th>ReOrd</th><th>Qty</th><th>Edit</th></tr><tbody>';
	foreach($result as $k=>$v){
		//$v is the array of the row
		$output .= "<tr>";
				foreach($v as $key=>$av){
					$warn = 0; $style=''; $rmsg='';
					if(intval( $v['reorder_level']) > intval( $v['quantity'])){ $warn = 3;}
					if(intval( $v['reorder_level']) == intval( $v['quantity'])){ $warn = 2;}
					if(intval( $v['reorder_level']) < intval( $v['quantity'])){ $warn = 1;}
					if(intval( $v['reorder_level'])*1.1 < intval( $v['quantity'])){ $warn = 0;}
					if(intval( $v['reorder_level']) == 0 ){ $warn = 0;}
					//if($key=="reorder_level"){
						switch ($warn){
							case 3:
								$style = "font-weight:bold;color:red;background:white;";
								
								break;
							case 2:
								$style = "font-weight:normal;color:black;background:white;";

								break;
							case 1:
								$style = "font-weight:normal;color:black;background:green;";

								break;
							case 0:
								$style ='';
								break;
						}

					//}

					$output .='<td class="grid" style="'.$style.'">'.$av.'</td>';
				}
				$output .= '<td width="5%" align="center" class="tvalue"><a target="_blank" href="admin.php?action=products&edit_item='.$v['id'].'"><img src="'.$themepath.'images/edit.png" title="Edit '.$v['item_name'].'"></a></td>';
				$output .= '<tr>';
	}
echo '<table cellspacing="0">'.$output.'</tbody></table>';
}
?>