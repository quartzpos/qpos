<?php
$subtitle = "Item sales report";
$formstart = 1; // start the form
$formdate1 = 1; // first date field
$formdate2 = 0; // second date field
$formuser = 0; // user chooser
$formtill = 0; // till field
$formend = 1; // end the form
$formhandle = 1;
$increport = 'items';
function form_handle(){
	$retarr = Array(); global $timezone;
				$nz_time = new DateTime(null, new DateTimezone($timezone));
                if (isset($_POST['date1']) && $_POST['date1'] != '') {
                    $retarr['datemod'] = $_POST['date1'];
                } else {
                    $retarr['datemod'] = $nz_time->format('Y-m-d');
                }
return $retarr;
}