<?php
$subtitle="Movement";
$formstart = 1; // start the form
$formdate1 = 1; // first date field
$formdate2 = 1; // second date field
$formuser = 0; // user chooser
$formmonth = 1; // months field
$formitem = 1; // items field
$formtill = 0; // till field
$formend = 1; // end the form
$formhandle=1;
$increport = 'move';
function form_handle(){
	$retarr = Array();
	 if (isset($_POST['date1']) && !empty($_POST['date1'] )) {
                        $retarr['datemod']  = $_POST['date1'];
                    } else if(isset($_POST['months'])){
                        $retarr['month']  = $_POST['months'];
                    } else {
						$retarr['month']  = 1;
					}
                     if (isset($_POST['date2']) && !empty($_POST['date2'])) {
                        $retarr['date2mod'] = $_POST['date2'];
                    }
					return $retarr;
}