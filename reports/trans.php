<?php
$subtitle = "Transaction report";
$formstart = 1; // start the form
$formdate1 = 1; // first date field
$formdate2 = 1; // second date field
$formuser = 1; // user chooser
$formtill = 1; // till field
$formend = 1; // end the form
$formhandle = 1;
$transactions = 1; // switch for the transactions includer
$increport='trans'; // use the transaction report format
function form_handle(){
	$retarr = Array();
	if (isset($_POST['date1']) && !empty($_POST['date1'])) {
                    $retarr['datemod'] = $_POST['date1'];
                }
                if (isset($_POST['date2']) && !empty($_POST['date2'])) {
                    $retarr['date2mod'] = $_POST['date2'];
                }

                if (isset($_POST['till']) && $_POST['till'] > 0) {
                    $retarr['till']= $_POST['till'];
                }
                if (isset($_POST['user']) && $_POST['user'] > -1) {
                    $retarr['user'] = $_POST['user'];
                }
				return $retarr;
}
function trans_res(){
	global $db;
    if (isset($_GET['id']) && is_numeric($_GET['id'])) {
        $id = $db->clean($_GET['id']);
        if (is_numeric($id)) {
            $mod = " WHERE sale_id =$id";
            $sql = "SELECT * FROM sales WHERE id=$id";
            $result = $db->query($sql);

            while ($row = $db->fetchAssoc($result)) {
                if ($row['sold_by'] == 0) {
                    $user = "Admin";
                } else {
                    $sqlu = "SELECT * FROM users WHERE id = " . $row['sold_by'];

                    $resultu = $db->query($sqlu);

                    if ($resultu) {
                        while ($rowu = $db->fetchAssoc($resultu)) {

                            $user = "<a href='admin.php?action=users&edit=" . $rowu['id'] . "' title='Edit " . $rowu['username'] . "'>" . $rowu['first_name'] . " " . $rowu['last_name'] . "</a>"; // create link to edit the user
                        }
                    }
                }
				if(isset($user)){
                $transtitle = "Transaction $id from " . $row['date'] . " by " . $user;
			} else{
				$transtitle = "Transaction $id from " . $row['date'] ." (User deleted)";
			}
                $sql = "SELECT * FROM customers WHERE id='" . $row['customer_id'] . "'";
                $resultc = $db->query($sql);

                while ($rowc = $db->fetchAssoc($resultc)) {
                    $customer = "<a href='admin.php?action=clientss&edit=" . $row['customer_id'] . "' title='Edit " . $rowc['first_name'] . $rowc['last_name'] . "'>" . $rowc['first_name'] . " " . $rowc['last_name'] . "</a>";
                }
            }
        }
        $sql = "SELECT * FROM sales_items" . $mod;

        $result = $db->query($sql);
        if ($result) {
            if (!empty($transtitle))
                print "<h4>$transtitle for $customer</h4>";
            ?>
            <table cellspacing="4"><?php
                if (empty($mod)) {
                    ?>
                    <th>Transaction</th><th>Date</th><th>Time</th>Payment</th>
                    <?php
                }
                ?>
                <th>Item</th><th>Quantity</th><th>Item Total</th><th>Till</th>
                <?php
                // `id`, `sale_id`, `item_id`, `quantity_purchased`, `item_unit_price`, `item_trade_price`, `item_tax_percent`, `item_total_tax`, `item_total_cost`

                while ($row = $db->fetchAssoc($result)) {
                    $item = '';
                    $itemsql = "SELECT * FROM items WHERE id='" . $row['item_id']."'";
                    $iresult = $db->query($itemsql);
                    while ($irow = $db->fetchAssoc($iresult)) {
                        $item = "<a href='admin.php?action=products&edit_item=" . $irow['id'] . "' title='Edit " . $irow['item_name'] . "'>" . $irow['item_name'] . "</a>"; // create link to edit the item
                        ?><TR>
                                            <?php
                                            $id = $row['sale_id'];
                                            $sql = "SELECT * FROM sales WHERE id=" . $id;

                                            $cresult = $db->query($sql);
                                            if ($cresult) {
                                                while ($crow = $db->fetchAssoc($cresult)) {
                                                    $till = $crow['till'];
                                                    $comments = $crow['comment'];
                                                    $custcomment = $crow['customer_comment'];
													$pmtcomment = $crow['payment_comment'];
                                                }
                                            }

                                            if (empty($mod)) {
                                                $ssql = "SELECT date FROM sales WHERE id=" . $row['sale_id'];
                                                $sdate = '';
                                                $trans = '';
                                                $stime = '';
                                                $sresult = $db->query($ssql);
                                                if ($sresult) {
                                                    while ($srow = $db->fetchAssoc($sresult)) {
                                                        $sdate = $srow['date'];
                                                        $stime = date("H:i:s", $srow['time']);
                                                    }
                                                }
                                                $trans = '<a href="admin.php?action=reports' . $header . '&subreport=trans&report=trans&id=' . $row['sale_id'] . '" title="View transaction ' . $row['sale_id'] . '">' . $row['sale_id'] . '</a>';
                                                $payment = '<script type="text/javascript">window.open("payments.php?id=<?php echo $sale_id; ?>","","width=600,height=450,toolbars=0,location=0,titlebar=0,left=450");</script>';
                                                ?>
                                <td align="center"><?php echo $trans; ?></td>
                                <td align="center"><?php echo $sdate; ?></td>
                                <td align="center"><?php echo $stime; ?></td>

                                <td align="center"><?php echo $payment; ?></td>
                                <?php
                            }
                            ?>
                            <TD align="center"><?php echo $item; // item                          ?></TD><TD align="center"><?php echo $row['quantity_purchased']; // qty                          ?></TD><TD align="right"><?php echo $row['item_total_cost']; //total                           ?></TD>
                            <?php
                            if (isset($till))
                                echo '<td>' . $till . '</td>';
                            ?>
                        </TR>
                        <?php
                    }
                }
                ?></table>
            <?php
			echo '<div id="transcomments">';
            if (isset($comments)){
                echo '<span class="comments"><h4>Comments:</h4> ' . $comments . '</span>';
            }
            if (isset($custcomment)){
                echo '<span class="comments"><h4>Customer Comments:</h4> ' . $custcomment . '</span>';
            }
			if (isset($pmtcomment)){
                echo '<span class="comments"><h4>Payment Comments:</h4> ' . $pmtcomment . '</span>';
			}
			echo '</div><br />';
            ?><script type="text/javascript">function receipt(rcpt) {
                        window.open("sale.php?id=<?php echo $id; ?>&rcpt="+rcpt, "", "width=680,height=700,toolbars=0,location=0,titlebar=0,left=20");
                    }
            </script>
            <input type="button" value="A4 Receipt" onclick="receipt('a4');"><input type="button" value="Small Receipt" onclick="receipt('small');">
            <a href="void.php?sale=<?php echo $id; ?>"><span class="void"><input type="button" value="Void Sale" /></span></a>
                <!--<a href="pos.php?editsale=<?php //echo $id; ?>"><span class="editsale"><input type="button" value="Edit Sale" /></span></a> -->
            <?php
        }
    } //end get $id
}