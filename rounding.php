<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */

function getRounding($val = '0', $coin = 0) {
    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($val, "Rounding.php val=");
    global $db;
    $smallcoin = $db->QPResults("SELECT value FROM system WHERE name='smallcoin'");
    $smallcoin = $smallcoin['value'];
    $roundup = $db->QPResults("SELECT value FROM system WHERE name='roundup'");
    $roundup = $roundup['value'];
    $rounddown = $db->QPResults("SELECT value FROM system WHERE name='rounddown'");
    $rounddown = $rounddown['value'];
    $newval = '0';
//set swedish rounding defaults if values are not set
    if (!isset($smallcoin) || empty($smallcoin)) {
        $smallcoin = '.10';
    }
    if (!isset($roundup) || empty($roundup)) {
        $roundup = '6,7,8,9';
    }
    if (!isset($rounddown) || empty($rounddown)) {
        $rounddown = '1,2,3,4,5';
    }
    if (!empty($val)) {
        //make sure $val is a string
        $val=  strval(number_format($val, 2));
        $last = $smallcoin[strlen($smallcoin) - 1];

        $lastval = $val[strlen($val) - 1];

        $downcheck = strpos($rounddown, $lastval);
        $upcheck = strpos($roundup, $lastval);
        if ($downcheck !== false) {
            //then round down
            $newval = rounddown($val, $lastval, $last);
         
        } elseif ($upcheck !== false) {
            //then round up
            $newval = roundup($val, $lastval, $smallcoin);
        } else {
            $newval = $val;
        }
       // if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($newval, "Rounding.php in not empty val, newval=");
    }
    if (!empty($coin)) { // return the denomination of coinage for payments.php javascript
        $newval = $smallcoin;
        //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($val, "Rounding.php not empty coin newval=");
    }
     //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($newval, "Rounding.php in not empty val, newval=");
    return floatval($newval);
}

function rounddown($val, $lastval, $last) {
    $ret = substr($val, 0, -1);
    $ret .=$last;
    return $ret;
}

function roundup($val, $lastval, $smallcoin) {
    $last = $smallcoin[strlen($smallcoin) - 1];
    $ret = rounddown($val, $lastval, $last);
    $ret = floatval($ret) + floatval($smallcoin);
    return $ret;
}

if (isset($_GET['val']) || isset($_GET['coin'])) { //only come here from a get call for the xml
    header('Content-Type:text/xml; charset="utf8"');
    if (!isset($_SESSION))
        session_start();
    require_once("config.php");
    require_once("database.php");

//require_once 'consoleLogging.php';

    if (isset($_GET['val'])) {
        $val = $_GET['val'];
    } else {
        $val = '0';
    }
    if (isset($_GET['coin'])) {
        $coin = $_GET['coin'];
    } else {
        $coin = '0';
    }
    $newval = getRounding($val, $coin);
//if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($newval, "Rounding.php newval=");
    echo '<?xml version="1.0" encoding="utf8" ?>';
    ?>

    <value><?php echo $newval; ?></value>
<?php } ?>