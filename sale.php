<?php
// the printable invoice
if (!isset($_SESSION))
    session_start();


include("config.php");
include("consoleLogging.php");
require_once("languages/languages.php");
require_once("database.php");
include'rounding.php';
include_once'functions.php';
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_REQUEST, "sale.php request=");
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION, "sale.php session=");
$admin=NULL;
if(isset($_GET['quote'])){
    $quote=TRUE;
} else{
    $quote=FALSE;
}
$laybuydetails='';
$credmsg='';
if(isset($_GET['laybuy'])){
    $larr=explode(':',$_GET['laybuy']);
    $lb=$db->clean($larr[0]);
    $sql="SELECT * FROM laybuys where id=$lb";
    $result = $db->QPResults($sql);
    if(isset($result['id'])){
      $laybuy = $result['id'];
      if($result['balance'] > 0){ $credmsg=LB_CREDIT;}
      $laybuydetails .='<u>'.LB_DETAILS.'</u><br/>'.PAY_MSG1_LAY.'=$'.$result['balance'].'&nbsp;'.$credmsg.'<br />';
      $laybuydetails .=LB_DUEDATE.': '.$result['duedate'].'<br />';
      $laybuydetails .=LB_NOTES.': '.$result['notes'].'<br />';
    } else {
      echo "Failure in SQL:".$sql;
    }
} else{
    $laybuy=FALSE;
}
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    if(!isset($_GET['vouchers'])){//send to the editvouchers page if there is vouchers that are not assigned fully
    $sql="SELECT * FROM vouchers WHERE sale_id='".$db->clean($id)."'";
    $results=$db->QPComplete($sql);
    if(!empty($results)){
        header('Location: editvoucher.php?action=edit&edit=' . $id . '&sale=1&id='. $id);
    }
    }

    $sql = "SELECT sale_total_cost, paid_with FROM sales WHERE id='$id'";
    $result = $db->query($sql);
    $row = $db->fetchAssoc($result);

    if (empty($row['paid_with']) && !$quote) {
       //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($row['paid_with'], 'paid');
        header('Location: payments.php?id=' . $id . '&sale=1');
    }

    $invoicedb = "SELECT * FROM system WHERE name LIKE 'invoice_%'";
    $invarr = $db->QPComplete($invoicedb); //$invar contains the invoice associated fields to build upon.
    $form = array();
   //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($invarr, "sale.php Invarr=");
    //in the database 'system table, there is an invoice field value and it correponds to another system table row
    //eg. invoice_field10 = {company_tax_no}
    //so the code gets the company_tax_no row in system table, and loads it's value
    foreach ($invarr as $key => $val) {
        if (!empty($val['value'])) {
            //there is a value in the field, parse it to see if it is another system field reference
            $field = sscanf($val['value'], "{%s", $fieldname); //retrieves the value between curly braces
           //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($fieldname, "fieldname=");
            if (isset($fieldname) && !empty($fieldname)) { //we have an actual field name
                $fieldtitle = str_replace('}', '', $fieldname);
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($fieldtitle, "Found field");
                $newval = $db->QPResults("SELECT value FROM system WHERE name='$fieldtitle'")['value'];
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($newval, "newvalue=");
                if(!empty($newval)){ $form[$fieldtitle] = $newval; }
            }
            $form[$val['name']] = $val['value'];
        }
    }
    $sql="SELECT value FROM system WHERE name='sales_header1'";
    $sales_header1=$db->QPResults($sql)['value'];
    $sql="SELECT value FROM system WHERE name='sales_header2'";
    $sales_header2=$db->QPResults($sql)['value'];
      $sql="SELECT value FROM system WHERE name='sales_footer1'";
    $sales_footer1=$db->QPResults($sql)['value'];
    $sql="SELECT value FROM system WHERE name='sales_footer2'";
    $sales_footer2=$db->QPResults($sql)['value'];
    $sql="SELECT value FROM system WHERE name='quote_footer'";
    $quote_footer=$db->QPResults($sql)['value'];
   //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($form, " sale.php Form=");
}
if(isset($_GET['rcpt'])){

  if($_GET['rcpt']==="small"){ include_once"sale_receipt.php";} else { include_once"sale_a4.php"; }
}
?>

