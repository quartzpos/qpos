<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8">
        <title>QuartzPOS Invoice</title>
        <style>

            *{font-family:arial, helvetica, sans-serif;font-size:12px;}
            @media print{
                .noprint, .noprint *,#top{display:none !important;}

            }
            small{font-size:10px;color:#788;}
            body{margin:0px;background:#fff;padding:2em;}
            .saletable { color:#333; font-weight:bold; }
            #top {background:#9cf;border-bottom:solid 1px #248;}
            #menu td{border-left:solid 1px #788;width:60px;cursor:hand;
                     text-align:center;color:#248;font-weight:bold;}
            #sale_info{margin-top:10px;min-height: 220px;}
            #sale_info td{}
            #total b{color:#922;font-size:14px;}
            #logo img{max-width:300px; }
            #logo {padding:1.5em;display:inline-block; vertical-align:top;}
            .right{text-align:right;float:right;}
            #details{ display: inline-block;}
            #sale_header{width:100%; min-height:250px; border-bottom: 1px solid #333;}
            .light{background: #ccc; color:#000; }
            #client{border: 1px solid #666; padding:10px;max-width: 300px;margin-top: 10px;}
        </style>
        <script language="javascript">
            var closewin =<?php if (isset($_GET['closewin'])) {
             echo $_GET['closewin'].';';
                } else {
                 echo '0;';
                 }
                 ?>
              var admin =<?php if (isset($_GET['admin'])) {
             echo $_GET['admin'].';';
             $admin=1;
                } else {
                 echo '0;';
                 }
?>
            function closed() {
               console.log("close function");
               if(closewin ===1 && admin ===1){
                   this.window.close();
               }
                if (closewin === 0) {
                    window.close();
                } else {
                    parent.window.location = "pos.php?action=logout";
                }
            }
             function focus(){
                 document.getElementById("print").focus();
             }
             document.addEventListener("keydown", function(event) {
  if (event.altKey && (String.fromCharCode(event.keyCode) === "P"||String.fromCharCode(event.keyCode) === "p")) {
   console.log("you pressed alt-p");
    event.preventDefault();
    event.stopPropagation();

  console.log("alt+ p clicked");
   window.print();
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && (String.fromCharCode(event.keyCode) === "c"||String.fromCharCode(event.keyCode) === "C")) {
   console.log("you pressed alt-c");
    event.preventDefault();
    event.stopPropagation();

  console.log("alt+ c clicked");
   closed();
  }
}, true);
        </script>
    </head>
    <body onload="focus()">
        <?php
        if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
            die('For the sale window you need to have a valid sale transaction number. it should take the form of sale.php?id=3456');
        } else {


            $sql = "select * from sales where id=" . $_GET['id'];

            $result = $db->query($sql);
            if($result){
            $sales_info = $db->fetchAssoc($result);
            } else {
                die("sale.php died with sql= $sql");
            }
            if(!$quote){
             $pmtp = unserialize(base64_decode($sales_info['paid_with']));
             // this was changed to avoid issue character encoding http://davidwalsh.name/php-serialize-unserialize-issues
             $change=getRounding(abs($pmtp['remainder']));
             $changeout=  number_format($change,2);
             unset($pmtp['remainder']);
             unset($pmtp['id']);
             unset($pmtp['total']);
             unset($pmtp['cost']);
             unset($pmtp['smallcoin']);

             $payment="Payment received=";
             if(!empty($pmtp)){
             foreach ($pmtp as $key => $val) {
                 if($key==="voucherref" || $key==="accountref"){
                     $payment.=strtoupper($key)." : ". $val.". ";
                 } else {
                       $payment.=strtoupper($key)." : $".money_formatter((double)$val).". ";
                 }
              }
             }
              $payment.="Change given : $".$changeout;
            }
            $customer = $db->QPResults("SELECT * FROM customers WHERE id='" . $sales_info['customer_id'] . "'");
            $classzebra = "light";
            ?>
            <table id="top" width="100%" cellpadding="0">
                <tr>
                    <td>&nbsp;</td>
                    <td align="right">
                        <input type="button" onClick="window.print()" id="print" value="Print"  title="Print. Hotkey Alt-p"/>
                        <?php if(!isset($admin)){ ?>
                        <input type="button" id="close" onClick="closed()" value="Close" title="Close. Hotkey Alt-c"/>
                        <?php } ?>
                    </td>
                </tr>
            </table>
            <div id="sale_header"><?php
                if (isset($form['receipt_logo'])){
                echo "<div id='logo'><img src='" . $form['receipt_logo'] . "' /></div>";}
                ?><div id="details" class="right">
                <?php
                if($quote){
                    $form['invoice_title']='<h2>'.TXT_QUOTE.'</h2>';
                }
                echo(isset($form['invoice_title'])) ? $form['invoice_title'] : '<h2>Tax Invoice</h2>';
                echo "<br>";
                echo(isset($form['invoice_taxtitle']) && isset($form['company_tax_no'])) ? $form['invoice_taxtitle'] . ' ' . $form['company_tax_no'] : '';
                echo "<br>";
                echo(isset($form['company_details_name'])) ? $form['company_details_name'] : '';
                echo "<br>";
                echo(isset($form['company_details_address1'])) ? $form['company_details_address1'] : '';
                echo "<br>";
                echo(isset($form['company_details_address2'])) ? $form['company_details_address2'] : '';
                echo "<br>";
                echo(isset($form['company_details_address3'])) ? $form['company_details_address3'] : '';
                echo "<br>";
                echo(isset($form['company_details_phone1'])) ? TXT_PHONE.':'.$form['company_details_phone1'] : '';
                echo "<br>";
                echo(isset($form['company_details_phone2'])) ? TXT_FREEPHONE.':'.$form['company_details_phone2'] : '';
                echo "<br>";
                echo(isset($form['company_details_fax'])) ? TXT_FAX.':'.$form['company_details_fax'] : '';
                echo "<br>";
                echo(isset($form['company_details_email'])) ? TXT_EMAIL.':'.$form['company_details_email'] : '';
                echo "<br>";
                echo(isset($form['company_details_website'])) ? TXT_WEBSITE.':'.$form['company_details_website'] : '';
                ?></div>

                <div id="client"><?php
                                    echo $customer['first_name'] . ' ' . $customer['last_name'] . ' - Ac#: ' . $customer['account_number'] . '<br>';
                                    echo $customer['address'] . '<br>' . $customer['city'] . ' ' . $customer['pcode'] . ' ' . $customer['state'] . '<br>';
                                    echo $customer['country'] . '.<br>';
                                    ?></div>
            </div>
        <div id="header1" style="margin-top:10px;"><?php if (isset($sales_header1)){ echo $sales_header1; }?></div>
                <div id="header2"><?php if (isset($sales_header2)){ echo $sales_header2; }?></div>
            <div id="sale_info">

                <small>Date/Time : <?php echo $sales_info['date'] . ' ' . $sales_info['time']; ?>&nbsp;&nbsp;Transaction # <?php echo $sales_info['session']; ?>&nbsp;&nbsp;Sale ID # <?php echo $sales_info['id']; ?></small>
                <hr>
                <table width="100%" cellspacing="0"><th class="saletable" align="left">Item code</th><th class="saletable">Description</th><th class="saletable">Qty</th><th class="saletable">Unit Cost</th><th class="saletable" align="right">Item total cost</th>
    <?php
    $sql = "select sales_items.item_name, sales_items.item_description, item_number, quantity_purchased, item_total_cost, item_unit_price from sales_items join items on sales_items.item_id=items.id where sale_id=" . $_GET['id'];
    $result = $db->query($sql);
    if (!$result) {
                die("sale.php died with sql= $sql");
            }
    while ($sales_items = $db->fetchAssoc($result)) {
        ?><tr class="<?php echo $classzebra; ?>"><td align="left"><?php echo $sales_items['item_number']; ?></td><td align="center"><?php echo $sales_items['item_description'] . '&nbsp;'; ?></td><td width="40" align="center"><?php echo $sales_items['quantity_purchased']; ?></td><td width="50" align="center"><?php echo $sales_items['item_unit_price']; ?></td><td width="50" align="right"><?php echo money_formatter( $sales_items['item_total_cost']); ?></td></tr><?php
        if ($classzebra == 'light') {
            $classzebra = '';
        } else {
            $classzebra = 'light';
        }
    }
    ?>
                </table>
            </div>
            <?php if (!empty($laybuydetails)){
              echo '<div id="laybuydetails">'.$laybuydetails.'</div>';
            } ?>
            <hr>
            <table width="100%" cellpadding=0">
                <tr>
                    <td></td>    <td align="right">Sub-total: </td><td align="right" width="7em"><b><?php echo money_formatter( $sales_info['sale_sub_total']); ?></td>
                </tr>
                <tr>
                    <td></td>    <td align="right">Tax: </td><td align="right" width="7em"><b><?php echo money_formatter( $sales_info['sale_total_cost'] - $sales_info['sale_sub_total']); ?></td>
                </tr><tr>
                    <td></td>    <td id="total" align="right">Total: </td><td align="right" width="7em"><b><?php echo money_formatter( $sales_info['sale_total_cost']); ?></b></td>
                </tr>
            </table>
            <div id="paymentreceived" style="border-top:1px solid black;border-bottom:1px solid black; padding-top: 10px;padding-bottom:10px;"><?php echo (isset($payment))?$payment:''; ?></div>
            <div id="comment"><?php if (!empty($sales_info['customer_comment'])) echo $sales_info['customer_comment']; ?></div>
            <div id="sale_footer">
                <div id="sale_footer1"><?php if (isset($sales_footer1)) echo $sales_footer1; ?></div>
                <div id="sale_footer2"><?php if (isset($sales_footer2)) echo $sales_footer2; ?></div>
                <div id="quote_footer"><?php if (isset($quote_footer) && $quote) echo $quote_footer; ?></div>
            </div>
<?php } ?>
    </body>
</html>