<?php

if (!isset($_SESSION))
    session_start();
require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");

require 'consoleLogging.php';
if (!isset($_POST) && isset($_GET))
    $_POST = $_GET;
//if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_GET,"sale edit GET=");
if (isset($_GET['id'])) {
    $sql = "SELECT * FROM sales WHERE id=" . $_GET['id'];
    $row = $db->QPResults($sql);

    $sql = "SELECT * FROM sales_items WHERE sale_id=" . $_GET['id'];
    $details = $db->QPComplete($sql);


    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($details,"Details=");
    $str = "";
    foreach ($details as $k => $v) {
        $x = "<item><prodcode>" . $v['item_name'] . "</prodcode>";
        $x.="<qty>" . $v['quantity_purchased'] . "</qty>";
        $x.="<tax>" . $v['item_tax_percent'] . "</tax>";
        $x.="<uprice>" . $v['item_unit_price'] . "</uprice>";
        $x.="<price>" . $v['item_total_cost'] . "</price>";
        $x.="</item>";
        $str.=$x;
    }

    $row['details'] = $str;
    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($str, "sale string=");


    $body = '<?xml version="1.0" encoding="utf8" ?> <session>';
    foreach ($row as $key => $val) {
        if (empty($val) && $val != 0)
            $val = " ";
        $body.= "<$key>$val</$key>";
    }
    $body.= "<name>" . $row['session'] . "</name>"; // see  general.js function sessionResume
    $body.= "<custid>" . $row['customer_id'] . "</custid>";
    $body.= "<userid>" . $row['sold_by'] . "</userid>";
    $body.= "<changed>" . $row['date'] . " " . $row['time'] . "</changed>";
    $body.= '</session>';
    //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($body, "XML body from sale edit=");
    header('Content-Type:text/xml; charset="utf8"');
    echo $body;
}
?>