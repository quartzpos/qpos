<?php
/*
 * processsale.php
 *
 * The purpose of this file is to take the user session, and save it to the sessions table of the database, for later retrieval
 * Copyright 2013 onyx <onyx@onyxlaptop>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
if (!isset($_SESSION))
    session_start();
require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");

require 'consoleLogging.php';
if(!isset($_POST) && isset($_GET)) $_POST=$_GET;
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE) { ChromePhp::log($_GET,"session resume GET="); }
if(isset($_GET['id'])){
$sql=" SELECT * FROM sessions WHERE id=". $_GET['id'] ." ";
 $result=$db->query($sql);
 $row = $db->fetchAssoc($result);
// $row =  (array) $row;
//if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($row,"row=");
if(isset($row['details']) && !empty($row['details'])){
  $details=unserialize($row['details']);
 //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($details,"Details=");
  $str="";
  foreach($details as $k=>$v){
   $x="<item><prodcode>".$v[0]."</prodcode>";
    $x.="<qty>".$v[1]."</qty>";
    $x.="<tax>".$v[2]."</tax>";
    $x.="<uprice>".$v[3]."</uprice>";
    $x.="<price>".$v[4]."</price>";
    $x.="</item>";
    $str.=$x;
  }
  
  $row['details']=$str;
 if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE){ ChromePhp::log($str, "Resume string="); }
}

$body= '<?xml version="1.0" encoding="utf8" ?> <session>';
foreach($row as $key=>$val){
if(empty($val) && $val !=0) $val=" ";
$body.=  "<$key>$val</$key>";
}
$body.= '</session>'; 
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE) { ChromePhp::log($body, "XML body from session resume="); }
header('Content-Type:text/xml; charset="utf8"');
echo $body;

}

?>