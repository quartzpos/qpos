<?php
/*
 * processsale.php
 *
 * The purpose of this file is to take the user session, and save it to the sessions table of the database, for later retrieval
 * Copyright 2013 onyx <onyx@onyxlaptop>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
if (!isset($_SESSION))
    session_start();
require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");

require 'consoleLogging.php';
if(empty($_POST)) $_POST=$_GET;
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_GET);
if (isset($_POST['userid'] )){
      $sql=" SELECT * FROM users WHERE id='".$_POST['userid'] ."'";
      $result=$db->query($sql);
      $row = $db->fetchAssoc($result);
      $_SESSION['system'] = $row['system'];
      }
if (isset($_SESSION['system'])){
$_POST['userlevel']=$_SESSION['system'];
}else{
$_POST['userlevel']=1; //assume staff userlevel if none passed
}
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_POST,"post=");


$details=(!empty($_POST['details']))?$_POST['details']:'';

$now = date('Y-m-d H:i:s');
$sql=" SELECT id FROM sessions WHERE name='". $_POST['name'] . "'";
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql,"select session sql=");

$result=$db->query($sql);
 $row = $db->fetchAssoc($result);
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($row,"result=");
if (empty($row)){
//YYYY-MM-DD HH:MM:SS
$sql = "insert into sessions(name,userid,userlevel,changed,till,custid,details,comment,customer_comment) values('" . $_POST['name'] . "'," . $_POST['userid'] . "," .$_POST['userlevel'] . ",'"  . $now. "','" . $_POST['till'] . "','" . $_POST['custid'] . "','" . $_POST['details'] . "','".$db->clean($_POST['comment'])."','".$db->clean($_POST['custcomments'])."')";
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql,"insert session sql=");
$result = $db->query($sql);
$id = $db->insertId($db->getConnection());
} else {
  $sql = "UPDATE sessions set userid='" . $_POST['userid'] . "', userlevel='".$_POST['userlevel'] ."', changed='" . $now. "',till='" . $_POST['till'] . "',custid='" . $_POST['custid'] . "',details='" . $_POST['details'] . "',comment='".$db->clean($_POST['comment'])."',customer_comment='".$db->clean($_POST['custcomments'])."' WHERE name='".$_POST['name']."'";
 //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql,"update session sql=");
$results = $db->query($sql);
 $row = $db->fetchAssoc($result);
$id =$row['id']; // retrieved from the select query above

}
$db->close();
header('Content-Type:text/xml; charset="utf8"');
echo '<?xml version="1.0" encoding="utf8" ?>
';

if ($result) {
    ?>
    <session>
        <id><?php echo $id; ?></id>
        <name><?php echo $_POST['name']; ?></name>
       
    </session>
    <?php
}
?>