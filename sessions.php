<?php
/*
 * processsale.php
 *
 * The purpose of this file is to retrieve the user's sessions from the database
 * Copyright 2013 onyx <onyx@onyxlaptop>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
if (!isset($_SESSION))
    session_start();
require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");
    $path='';
    if(isset($pathex))$path=$pathex;
    $res = $db->QPResults("SELECT value FROM system WHERE name='theme'");
    $themepath = $path.'qpthemes/'.$res['value'].'/';
require 'consoleLogging.php';
//if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log("Sessions.php running in iframe");
//if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_GET);
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION,"sessions.php session=");
$now = date('Y-m-d H:i:s');
if(isset($_GET['del'])){
    if(!is_numeric($_GET['del'])){
	die("Bad 'del' parameter passed to sessions.php");
    }
  $sql=" DELETE FROM sessions WHERE id =".$db->clean($_GET['del']);
  $result=$db->query($sql);
  if(!$result){if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql,"Failed SQL=");}
}
$usersql='';
if(isset($_SESSION['user'])) $usersql = "WHERE userid='".$_SESSION['user']."'";
if (isset($_SESSION['admin']) && $_SESSION['admin'] > 1) $usersql = "WHERE userlevel<='".$_SESSION['admin']."'"; //users with ADMIN perms of 2 or more can restore a session from a lower privileged user
$sql=" SELECT * FROM sessions ".$usersql." ORDER BY changed DESC ";
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($sql,"select session sql=");
$class="trodd";
$result=$db->query($sql);
if(!empty($result)){
?>
  <html>
    <head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<link href="<?php echo $themepath; ?>pos.css" rel="stylesheet"  type="text/css">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script language="javascript" src="js/general.js"></script>
	<script type="text/javascript">
	function unresume(x){
	  window.location="sessions.php?del="+x;
	}
	</script>
	</head>
	<body class="blank">
	<?php
echo '<table id="sessionstable">';
echo '<th>'.TXT_SESSION.'</th><th>'.TXT_USER.'</th><th>'.TXT_CUSTOMER.'</th><th>'.TXT_TILL.'</th><th>'.TXT_CHANGED.'</th><th>'.TXT_RESUME.'</th><th>'.TXT_DELETE.'</th>';
while ($row = $db->fetchAssoc($result)){
if($row['userid'] !=0){
  $sql="SELECT * FROM users WHERE id='".$row['userid']."'";
  $resultu=$db->query($sql);
  $rowu = $db->fetchAssoc($resultu);
  $user=$rowu['username'];
  } else {
    $user="Admin";
  }
  $sql="SELECT * FROM customers WHERE id='".$row['custid']."'";

  if($rowu = $db->QPComplete($sql)){
  $customer=$rowu[0]['first_name'].' '.$rowu[0]['last_name'];
  }

echo '<tr class="'.$class.'"><td>'.$row['name'].'</td><td>'.$user.'</td><td>'.$customer.'</td><td>'.$row['till'].'</td><td>'.$row['changed'].'</td><td><input type="button" value="RESUME" onclick="resume('.$row['id'].')" /><td><input type="button" value="X" onclick="unresume('.$row['id'].')" /></td></tr>';
if($class=="trodd"){ $class="treven"; } else {$class="trodd";} //alternate the row colours
}
echo '</table></body></html>';
}

?>
