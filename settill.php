<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com

  

  Released under the GNU General Public License
 */
require 'consoleLogging.php';
if (!isset($_SESSION)){

session_start();}
require_once("config.php");
require_once("languages/languages.php");
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log(" SETTILL php, start Session.");
require_once("database.php");
$path='';
    if(isset($pathex))$path=$pathex;
    $res = $db->QPResults("SELECT value FROM system WHERE name='theme'");
    if(empty($res['value'])){$res['value']="flat";}
    $themepath = $path.'qpthemes/'.$res['value'].'/';

$msg="";
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE){
    ChromePhp::log($_GET,"GET=");
    ChromePhp::log($_POST,"SETTILL php, POST=");
    ChromePhp::log($_SESSION," SETTILL php, Session=");
}
$_SESSION['tillcheck']=1;
require_once 'login.php';
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION,"SETTILL php, set do cookies, Session=");
if (isset($_POST['till'])){
	$_POST['till']= intval($_POST['till']);
	if($_POST['till'] <1){ $_POST['till'] = 0; $msg="Till must be above zero."; $_GET['action']=null;}
	}
if (isset($_POST['till']) && intval($_POST['till'])> 0  && isset($_SESSION['admin'])) {
 $_SESSION['till'] = $_POST['till'];
    setcookie("quartzpos", $_POST['till'], time() + 5184000);
   if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION,"SETTILL php, setcookie");
} else {
    if (isset($_COOKIE["quartzpos"])) {
        $_SESSION['till'] = $_COOKIE["quartzpos"];
        setcookie("quartzpos", $_SESSION['till'], time() + 5184000);
        if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION,"SETTILLphp, set cookie, Session=");
    }
}

?> <html>
	<head>
	<title> <?php echo QP_TILL_SET;?> </title> <meta http - equiv = "Content-Type"
content = "text/html; charset=utf8">
	<link href = "<?php echo $themepath; ?>pos.css"
rel = "StyleSheet"
type = "text/css">
	<script type = "text/javascript"
src = "<?php echo $path; ?>js/jquery.min.js"> </script>

<script type = "text/javascript"
src = "<?php echo $path; ?>js/jquery-ui.min.js"> </script> <script type = "text/javascript"
language = "javascript"
src = "<?php echo $path; ?>js/general.js"> </script>

<link href = "<?php echo $themepath; ?>jqui/jquery-ui.min.css"
rel = "stylesheet"
type = "text/css">

	</head> <body class = "admin">

	<table width = "100%"
height = "100%"
cellspacing = "0"
cellpadding = "0"
border = "0">
	<TR height = "20"> <TD> <?php require_once("header.php"); ?> </TD></TR>
	<TR> <TD valign = "top">
	<?php
             if (!empty($msg)){
             echo "  <div class='admin_content'>".$msg."</div><br>";
             }
                    if (!isset($_GET['action'])) {
                        ?> <div class = 'admin_content'>
	<center> <b class = "blt"> <?php 
                if (isset($_GET['notset']) && $_GET['notset']==1) echo SET_TILL;
                
                echo SUPER_LOGIN; 
                ?> </b></center>
	<form action = "settill.php?action=reset"
method = "POST"
class = "loginform">
	<table align = "center"
class = "tlogin">
	<TR> <TD align = "right"
width = "40%"> <?php echo LOGIN_USER; ?> </TD><TD width="60%"><input type="text" name="username" autocomplete="off"></TD> </TR> <TR> <TD align = "right"
width = "40%"> <?php echo LOGIN_PASSWORD; ?> </TD><TD width="60%"><input type="password" name="userpassword"  autocomplete="off"></TD> </TR> <TR> <TD align = "right"
width = "40%"> <?php echo TILL_NUMBER; ?> </TD><TD width="60%"><input type="input" name="till" onkeypress="isNumberKey(event)" title=<?php echo NUMERIC_VALUES; ?> value="0"></TD> </TR> <TR> <TD colspan = "2"
align = "center"> <input type = "submit"
name = "userlogin"
value = "Set till"> </TD></TR>
	</table> </form> <center> <p> <?php echo SET_TILL_NO; ?> <br> <?php echo BROWSER_WARN; ?> </p></center>
	</div>
<?php
                } else {
                if(isset($_SESSION['user']) && $_SESSION['till']> 0){
                    echo "<div class='admin_content'>".TILL_SET." ".$_SESSION['till'] .".<br><br><a href='pos.php'><input type='button' name='sales' value='". LOGIN_SUBMIT."'></a><a href='admin.php'><input type='button' name='admin' value='". TXT_ADMINISTRATION."'></a></div>";
                    } elseif(!isset($_SESSION['user'])) {
                    echo "<div class='admin_content'>".AUTH_FAILURE." <a href='settill.php?notset=1'>".TRY_AGAIN."</a>.</div>";
                    }
                }
		
		if(file_exists("install.php")){
		    // the install.php is a security hazard if it stays in existing installation
		    echo "<div id='warn' style='border:1px solid black;border-radius:6px;'>".INSTALL_WARNING."</div>";
		}
                ?> </td></tr>
</table> </body> </html>
