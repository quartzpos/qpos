<?php
if (!isset($_SESSION))
    session_start();
require_once("config.php");
require_once("languages/languages.php");
require_once 'consoleLogging.php';
require_once("database.php");
require_once 'helpers.php';
if (!isset($_SESSION['admin'])) {
    header("Location:admin.php");
}
$export = '';
$sql = "SELECT * FROM system WHERE name='export_type'";
$result = $db->query($sql);
while ($row = $db->fetchAssoc($result)) {
    $export = $row['value'];
}
$import = '';
$sql = "SELECT * FROM system WHERE name='import_type'";
$result = $db->query($sql);
while ($row = $db->fetchAssoc($result)) {
    $import = $row['value'];
}
if (isset($_POST['submitsetup'])) {
    $configfile = '<?php

/**************************************
Configuration file written by setup.php
***************************************/

//Administrator login
$adminname = "' . $adminname . '";
$adminpassword = "' . $adminpassword . '";


//GENERAL VALUES
define("QUARTZPOS_VERSION","Diamond");
// Jan - Garnet
// Feb - Amethyst
// Mar - Aquamarine
// Apr - Diamond
// May - Emerald
// Jun - Pearl
// Jul - Ruby
// Aug - Sardonyx
//Sep - Sapphire
// Oct - Opal
// Nov - Topaz
// Dec - Turquoise
define("POS_DEFAULT_LANGUAGE","' . $_POST['default_language'] . '");
define("CATEGORY_IMG_SIZE","32");
define("ITEM_IMG_SIZE","32");
define("CURRENCY","' . $_POST['currency'] . ' ");
define("ITEMS_PER_PAGE","10");
define("IMPORT_FILE_PATH", "");
define("EXPORT_FILE_PATH", "export/"); // a directory with trailing slash
define("BACKUP_FILE_PATH", "backup/"); // a directory with trailing slash
require_once "dbconfig.php";
define("DEBUG_CONSOLE", FALSE);
?>
';
    $dbconfig = '<?php

/*
 Keep the database connection parameters here for QuartzPOS
 */

$dbhost = "' . $dbhost . '";
$dbuser = "' . $dbuser . '";
$dbpassword = "' . $dbpassword . '";
$dbname = "' . $dbname . '";
?>
';
//Create configuration files
    $file = fopen("config.php", "w+") or die("Failed to create config file!");
    fwrite($file, $configfile);
    fclose($file);
    $file = fopen("dbconfig.php", "w+") or die("Failed to create config file!");
    fwrite($file, $dbconfig);
    fclose($file);
}
//if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_REQUEST, "setup.php request=");
if (isset($_POST['export'])) {
    $export = $_POST['export'];
    if ($export == 'none')
        $export = '';
    $export = $db->clean($export);
    $db->query("UPDATE system SET value='$export' WHERE name='export_type'");
}
if (isset($_POST['import'])) {
    $import = $_POST['import'];
    if ($import == 'none')
        $import = '';
    $import = $db->clean($import);
    $db->query("UPDATE system SET value='$import' WHERE name='import_type'");
}
if (isset($_POST['vouchers'])) {
    $vch = $_POST['vouchers'];
    $myArray = explode(',', $vch);
    $vch = serialize($myArray);
    $db->query("UPDATE system SET value='$vch' WHERE name='vouchers'");
} else {
    $vch = $db->QPResults("SELECT value FROM system WHERE name='vouchers'")['value'];
}
if (isset($_POST['laybuys'])) {
    $lyby = $_POST['laybuys'];
    $db->query("UPDATE system SET value='$lyby' WHERE name='laybuy_item'");
} else {
    $lyby = $db->QPResults("SELECT value FROM system WHERE name='laybuy_item'")['value'];
}

if (isset($_POST['laybuysperiod'])) {
    $laybuysperiod = $_POST['laybuysperiod'];
    $db->query("UPDATE system SET value='$laybuysperiod' WHERE name='laybuy_period'");
} else {
    $laybuysperiod = $db->QPResults("SELECT value FROM system WHERE name='laybuy_period'")['value'];
}
if (isset($_POST['rounddown'])) {
    $rounddown = $_POST['rounddown'];
    $db->query("UPDATE system SET value='$rounddown' WHERE name='rounddown'");
} else {
    $rounddown = $db->QPResults("SELECT value FROM system WHERE name='rounddown'")['value'];
}
if (isset($_POST['smallcoin'])) {
    $smallcoin = $_POST['smallcoin'];
    $db->query("UPDATE system SET value='$smallcoin' WHERE name='smallcoin'");
} else {
    $smallcoin = $db->QPResults("SELECT value FROM system WHERE name='smallcoin'")['value'];
}
if (isset($_POST['roundup'])) {
    $roundup = $_POST['roundup'];
    $db->query("UPDATE system SET value='$roundup' WHERE name='roundup'");
} else {
    $roundup = $db->QPResults("SELECT value FROM system WHERE name='roundup'")['value'];
}
if (isset($_POST['autocomplete'])) {
    $autocomplete = $_POST['autocomplete'];
    $db->query("UPDATE system SET value='$autocomplete' WHERE name='autocomplete'");
} else {
    $autocomplete = $db->QPResults("SELECT value FROM system WHERE name='autocomplete'")['value'];
}
if (isset($_POST['timezone'])) {
    $timezone = $_POST['timezone'];
    $db->query("UPDATE system SET value='$timezone' WHERE name='timezone'");
} else {
    $timezone = $db->QPResults("SELECT value FROM system WHERE name='timezone'")['value'];
}
if (isset($_POST['theme'])) {
    $theme = $_POST['theme'];
    $db->query("UPDATE system SET value='$theme' WHERE name='theme'");
    ?><meta http-equiv="refresh" content="1"><?php // changes theme immediately
} else {
    $theme = $db->QPResults("SELECT value FROM system WHERE name='theme'")['value'];
}
  $checked = "";
    if ( $autocomplete =='1'){$checked = "checked";}
if (isset($vch)) {
    $varr = unserialize($vch);
    $vch = implode(',', $varr); //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($vch,"setup.php vch=");
}

//ennumerate the themes folder to list themes for select box
chdir('qpthemes');
$themelist = glob("*", GLOB_ONLYDIR);
chdir('..');
$themesel="<select name='theme' title='".SETUP_CTTITLE."'>";

foreach($themelist as $tk=>$tv){
    $themesel.="<option value='".$tv."'";
    if (isset($theme) && $tv===$theme){ $themesel.=" SELECTED ";}
    $themesel.=">".ucfirst($tv)."</option>";
}
$themesel.="</select>";
?>

<div class="admin_content">
    <form action="admin.php?action=setup" method="post">
        <table >
            <tr><td><?php echo SETUP_CO;?>: </td>
                <td><img src="<?php echo $themepath; ?>images/setup.png" title="<?php echo SETUP_TITLE;?>" onclick="showhide('cosetup','TRUE')" /></td>
            </tr>
            <tr><td><?php echo TXT_LANGUAGE;?>: </td><td>
                    <select name="default_language">
<?php
foreach ($languages as $language_name => $language_file) {
    ?><option value="<?php echo $language_file; ?>" <?php if ($language_file == POS_DEFAULT_LANGUAGE) echo "SELECTED"; ?>><?php echo ucfirst($language_file); ?></option><?php
                        }
                        ?>
                    </select></td></tr>
            <tr><td><?php echo SETUP_TIMEZONE;?>: </td><td>
                    <select name="timezone">
<?php
$tzs=  get_QP_timezones();
foreach ($tzs as $k => $v) {
    foreach($v as $key=> $val){
    ?><option value="<?php echo $key; ?>" <?php if ($key == $timezone) echo "SELECTED"; ?>><?php echo $key; ?></option><?php
}}
                        ?>
                    </select></td><td><?php echo SETUP_TZHELP;?></td></tr>
            <tr><td><?php echo SETUP_EXPORT;?>: </td><td>
                    <select name="export" title="<?php echo SETUP_EXTITLE;?>">
                        <option value="none"<?php if ($export == '') echo "SELECTED"; ?>>---</option>
                        <option value="wildcard"<?php if ($export == 'wildcard') echo "SELECTED"; ?>>Wildcard</option>
                        <option value="quartz" <?php if ($export == 'quartz') echo "SELECTED"; ?>>QuartzBooks</option>

                    </select></td>
                <td><img src="<?php echo $themepath; ?>images/setup.png" title="Configure export options" onclick="showhide('export','TRUE')" /></td></tr>
            <tr><td><?php echo SETUP_IMPORT;?>: </td><td>
                    <select name="import" title="<?php echo SETUP_IMTITLE;?>">
                        <option value="none"<?php if ($import == '') echo "SELECTED"; ?>>---</option>
                        <option value="wildcard"<?php if ($import == 'wildcard') echo "SELECTED"; ?>>Wildcard</option>
                        <option value="quartz" <?php if ($import == 'quartz') echo "SELECTED"; ?>>QuartzBooks</option>

                    </select></td>
                <td><img src="<?php echo $themepath; ?>images/setup.png" title="Configure import options" onclick="showhide('import','TRUE')" /></td></tr>
            <tr><td><?php echo SETUP_CURRENCY;?>: </td><td><input type="text" name="currency" style= "width:90px;" title="<?php echo SETUP_CURTITLE;?>" value="<?php echo htmlspecialchars(CURRENCY); ?>"></td></tr>
            <tr><td><?php echo SETUP_CASH;?>: </td><td><input type="text" name="rounddown" style= "width:80px;" title="<?php echo SETUP_CSHTITLE1;?>" value="<?php echo htmlspecialchars($rounddown); ?>">&darr;
                    <input type="text" name="roundup" style= "width:80px;" title="<?php echo SETUP_CSHTITLE2;?>" value="<?php echo htmlspecialchars($roundup); ?>">&uarr;<input type="text" name="smallcoin" style= "width:80px;" title="<?php echo SETUP_CSHTITLE3;?>" value="<?php echo htmlspecialchars($smallcoin); ?>"><?php echo SETUP_DOLLARS;?></td><td><?php echo SETUP_CSHHELP;?></td></tr>
            <tr><td><?php echo SETUP_AUTOCOMP;?>:</td><td><input type="hidden" name="autocomplete" value="0" />
<input name="autocomplete" class="checkme" type="checkbox" <?php echo $checked; ?> value="1"></td><td><?php echo SETUP_ACHELP;?></td></tr>
            <tr><td><?php echo SETUP_VCHR;?></td><td><input type="text" name="vouchers" value="<?php echo (isset($vch)) ? $vch : ''; ?>"></td><td><?php echo SETUP_VCHRHELP;?></td></tr>
            <tr><td><?php echo SETUP_LAYBUYS;?>:</td><td><input type="text" name="laybuys" value="<?php echo (isset($lyby)) ? $lyby : ''; ?>"></td><td><?php echo SETUP_LYBYHELP1;?></td><td><input type="text" name="laybuysperiod" value="<?php echo (isset($laybuysperiod)) ? $laybuysperiod : ''; ?>"></td><td><?php echo SETUP_LYBYHELP2;?></td></tr>
            <tr><td><?php echo SETUP_THEME;?>:</td><td><?php echo $themesel; ?></td><td><?php echo SETUP_THMEHELP;?></td></tr>
            <tr><td><?php echo SETUP_TILL;?> #:</td><td><a href="settill.php" title="<?php echo SETUP_TILLTITLE;?>" target="_blank"><input type="button" value="Set Till Number" /></a></td></tr>

            <tr><td><?php echo TXT_SAVE;?></td><td ><input type="submit" name="submitsetup" id="submit" value="<?php echo SETUP_SAVE;?>"></td></tr>

        </table>
    </form>
    <div id="export" class="iframe">
        <div id="closeexport" onclick="showhide('export','FALSE')"><img src="<?php echo $themepath; ?>images/close.png"
                                                                title="<?php echo SETUP_EWTITLE;?>"/>
        </div>
        <iframe id="exportframe" src="exportconfig.php"></iframe>
    </div>
    <div id="import" class="iframe">
        <div id="closeimport" onclick="showhide('import','FALSE')"><img src="<?php echo $themepath; ?>images/close.png"
                                                                title="<?php echo SETUP_IWTITLE;?>"/>
        </div>
        <iframe id="importframe" src="importconfig.php"></iframe>
    </div>
    <div id="cosetup" class="iframe">
        <div id="closecosetup" onclick="showhide('cosetup','FALSE')"><img src="<?php echo $themepath; ?>images/close.png"
                                                                  title="<?php echo SETUP_CWTITLE;?>"/>
        </div>
        <iframe id="cosetupframe" src="companysetup.php"></iframe>
    </div>
</div>
