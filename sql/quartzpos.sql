
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+12:00";


--
-- Database: `quartzpos`
--
CREATE DATABASE IF NOT EXISTS `quartzpos` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `quartzpos`;

-- --------------------------------------------------------

--
-- Table structure for table `barcodes`
--

DROP TABLE IF EXISTS `barcodes`;
CREATE TABLE `barcodes` (
  `barcode` varchar(254) NOT NULL,
  `prodcode` varchar(254) NOT NULL,
  `uid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands` (
  `id` int(8) NOT NULL,
  `brand` varchar(30) NOT NULL,
  `code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(8) NOT NULL,
  `category` varchar(30) NOT NULL,
  `image` varchar(80) DEFAULT NULL,
  `code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(8) NOT NULL,
  `first_name` varchar(75) NOT NULL,
  `last_name` varchar(75) NOT NULL,
  `account_number` varchar(30) DEFAULT NULL,
  `address` varchar(80) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `pcode` varchar(30) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `phone_number` varchar(25) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `comments` tinytext DEFAULT NULL,
  `pricelevel` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `account_number`, `address`, `city`, `pcode`, `state`, `country`, `phone_number`, `email`, `comments`, `pricelevel`) VALUES
(1, 'Cash', 'Sale', '1', '', '', '', '', '', '', '', '', 1),
(2, 'Trade', 'Customer', '2', '', '', '', '', '', '', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

DROP TABLE IF EXISTS `discounts`;
CREATE TABLE `discounts` (
  `id` int(8) NOT NULL,
  `item_id` int(8) NOT NULL,
  `percent_off` int(11) NOT NULL,
  `comment` blob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hkmatrix`
--

DROP TABLE IF EXISTS `hkmatrix`;
CREATE TABLE `hkmatrix` (
  `uid` int(11) NOT NULL,
  `custgroup` varchar(4) DEFAULT NULL,
  `refnum` varchar(10) DEFAULT NULL,
  `prodgroup` varchar(254) DEFAULT NULL,
  `supplier` varchar(254) DEFAULT NULL,
  `prodcode` varchar(16) DEFAULT NULL,
  `matrix` varchar(1) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `contract` decimal(10,2) DEFAULT NULL,
  `trade` decimal(10,2) DEFAULT NULL,
  `retail` decimal(10,2) DEFAULT NULL,
  `date_on` date DEFAULT NULL,
  `date_off` date DEFAULT NULL,
  `break1` decimal(5,2) DEFAULT NULL,
  `break2` decimal(5,2) DEFAULT NULL,
  `break3` decimal(5,2) DEFAULT NULL,
  `break4` decimal(5,2) DEFAULT NULL,
  `break5` decimal(5,2) DEFAULT NULL,
  `break6` decimal(5,2) DEFAULT NULL,
  `break7` decimal(5,2) DEFAULT NULL,
  `price1` decimal(10,2) DEFAULT NULL,
  `price2` decimal(10,2) DEFAULT NULL,
  `price3` decimal(10,2) DEFAULT NULL,
  `price4` decimal(10,2) DEFAULT NULL,
  `price5` decimal(10,2) DEFAULT NULL,
  `price6` decimal(10,2) DEFAULT NULL,
  `price7` decimal(10,2) DEFAULT NULL,
  `text` varchar(254) DEFAULT NULL,
  `type` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(8) NOT NULL,
  `item_name` varchar(80) NOT NULL,
  `item_number` varchar(80) DEFAULT NULL,
  `description` tinytext DEFAULT NULL,
  `clearance` tinyint(1) NOT NULL DEFAULT 0,
  `brand_id` int(8) NOT NULL,
  `category_id` int(8) NOT NULL,
  `supplier_id` int(8) NOT NULL,
  `trade_price` decimal(10,2) NOT NULL,
  `wholesale` decimal(10,2) DEFAULT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `supplier_item_number` varchar(50) DEFAULT NULL,
  `tax_percent` varchar(10) NOT NULL,
  `total_cost` decimal(10,2) NOT NULL,
  `quantity` int(8) DEFAULT NULL,
  `reorder_level` int(8) DEFAULT NULL,
  `image` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `items_aging`
--

DROP TABLE IF EXISTS `items_aging`;
CREATE TABLE `items_aging` (
  `id` int(11) NOT NULL,
  `items_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `quantity` int(8) NOT NULL DEFAULT 1,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `laybuys`
--

DROP TABLE IF EXISTS `laybuys`;
CREATE TABLE `laybuys` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `balance` float NOT NULL COMMENT 'Balance owing',
  `pmts` text DEFAULT NULL COMMENT 'Payment records',
  `duedate` date DEFAULT NULL COMMENT 'Date the layby expires',
  `notes` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `laybys`
--

DROP TABLE IF EXISTS `laybys`;
CREATE TABLE `laybys` (
  `id` int(11) NOT NULL,
  `saleid` int(11) NOT NULL,
  `balance` float NOT NULL COMMENT 'Balance owing',
  `pmts` text DEFAULT NULL COMMENT 'Payment records',
  `duedate` date DEFAULT NULL COMMENT 'Date the layby expires'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) NOT NULL,
  `transaction` varchar(20) NOT NULL,
  `transmitted` text DEFAULT NULL,
  `user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pricelevel`
--

DROP TABLE IF EXISTS `pricelevel`;
CREATE TABLE `pricelevel` (
  `uid` int(3) NOT NULL,
  `name` varchar(254) NOT NULL COMMENT 'Name of pricing scheme',
  `taxinc` smallint(1) NOT NULL DEFAULT 1 COMMENT 'Tax included',
  `markup` smallint(4) DEFAULT NULL COMMENT 'As a percentage',
  `base` varchar(32) NOT NULL DEFAULT 'total_cost',
  `security_level` int(2) NOT NULL DEFAULT 0 COMMENT 'What sales security level is required to choose this pricelevel'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rewind`
--

DROP TABLE IF EXISTS `rewind`;
CREATE TABLE `rewind` (
  `id` int(10) NOT NULL,
  `sale_id` int(10) NOT NULL,
  `sql_to_run` text DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales` (
  `id` int(8) NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `time` time NOT NULL,
  `customer_id` int(8) DEFAULT NULL,
  `sale_sub_total` varchar(30) NOT NULL,
  `sale_total_cost` varchar(30) NOT NULL,
  `paid_with` text DEFAULT NULL,
  `items_purchased` int(8) NOT NULL,
  `sold_by` int(8) NOT NULL,
  `till` int(3) NOT NULL DEFAULT 1,
  `comment` text DEFAULT NULL,
  `customer_comment` text DEFAULT NULL,
  `session` varchar(255) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_items`
--

DROP TABLE IF EXISTS `sales_items`;
CREATE TABLE `sales_items` (
  `id` int(8) NOT NULL,
  `sale_id` int(8) NOT NULL,
  `item_id` int(8) NOT NULL,
  `quantity_purchased` int(8) NOT NULL,
  `item_unit_price` varchar(30) NOT NULL,
  `item_trade_price` varchar(30) NOT NULL,
  `item_tax_percent` varchar(10) NOT NULL,
  `item_total_tax` varchar(30) NOT NULL,
  `item_total_cost` varchar(30) NOT NULL,
  `item_name` varchar(255) DEFAULT NULL COMMENT 'Item name where the item is not in database',
  `item_description` text DEFAULT NULL COMMENT 'Item description where the item is different from the database'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_totals`
--

DROP TABLE IF EXISTS `sales_totals`;
CREATE TABLE `sales_totals` (
  `id` int(15) NOT NULL,
  `date` date NOT NULL,
  `sale_total` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `userlevel` int(2) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `till` int(11) DEFAULT NULL,
  `custid` int(11) DEFAULT NULL,
  `details` mediumtext DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `customer_comment` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers` (
  `id` int(8) NOT NULL,
  `supplier` varchar(80) NOT NULL,
  `address` varchar(80) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `pcode` varchar(30) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `phone_number` varchar(25) DEFAULT NULL,
  `contact` varchar(60) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `comments` tinytext DEFAULT NULL,
  `code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `system`
--

DROP TABLE IF EXISTS `system`;
CREATE TABLE `system` (
  `id` int(5) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system`
--

INSERT INTO `system` (`id`, `name`, `value`) VALUES
(1, 'export', '1'),
(2, 'export_type', ''),
(3, 'import_type', ''),
(4, 'import_username', ''),
(5, 'import_password', ''),
(6, 'import_sambapath', ''),
(7, 'import_datapath', ''),
(8, 'export_username', ''),
(9, 'export_password', ''),
(10, 'export_hostpath', ''),
(11, 'export_filename', ''),
(12, 'invoice_title', '<strong><h3>Tax Invoice</h3></strong>'),
(13, 'invoice_taxtitle', 'GST# '),
(14, 'invoice_field1', '{company_details_name}'),
(15, 'invoice_field2', '{company_details_address1}'),
(16, 'invoice_field3', '{company_details_address2}'),
(17, 'invoice_field4', '{company_details_address3}'),
(24, 'company_details_name', ''),
(25, 'company_details_address1', ''),
(26, 'company_details_address2', ''),
(27, 'company_details_address3', ''),
(28, 'company_details_phone1', ''),
(29, 'company_details_phone2', ''),
(30, 'company_details_fax', ''),
(31, 'company_details_email', ''),
(32, 'company_details_website', ''),
(18, 'invoice_field5', '{company_details_phone1}'),
(19, 'invoice_field6', '{company_details_phone2}'),
(20, 'invoice_field7', '{company_details_fax}'),
(21, 'invoice_field8', '{company_details_email}'),
(22, 'invoice_field9', '{company_details_website}'),
(23, 'invoice_field10', '{company_tax_no}'),
(33, 'company_tax_no', ''),
(34, 'sales_header1', ''),
(35, 'sales_header2', ''),
(36, 'receipt_logo', ''),
(37, 'invoice_image', '{receipt_logo}'),
(38, 'vouchers', 'a:1:{i:0;s:11:\"GIFTVOUCHER\";}'),
(39, 'laybuy_period', '15768000'),
(40, 'laybuy_item', 'ZZZ'),
(41, 'GST_rate', '15'),
(42, 'import_qb_server', 'localhost'),
(44, 'import_qb_user', ''),
(45, 'import_qb_pass', ''),
(46, 'import_qb_database', 'quartzbooks'),
(47, 'import_qb_prefix', '0_'),
(48, 'theme', 'flat'),
(49, 'rounddown', '1,2,3,4,5'),
(50, 'roundup', '6,7,8,9'),
(51, 'smallcoin', '.10'),
(53, 'autocomplete', '1'),
(54, 'sales_footer1', '** This business is 100% KIWI Family owned and operated**<br> Thank you for shopping with us. A RECEIPT must accompany all exchanges , refunds or credit claims. Please refer to our terms of trade, as displayed in the shop.'),
(55, 'sales_footer2', 'If your purchase is unsuitable we MAY at OUR DISCRETION accept the goods back and issue a store credit.These items have to be return unused and in the original packaging. <br> Management  decision is final.'),
(56, 'export_hostdir', ''),
(57, 'quote_footer', 'This quote is valid for two weeks and is dependent upon stock of the items being available. E & OE.'),
(58, 'timezone', 'Pacific/Auckland');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(8) NOT NULL,
  `first_name` varchar(75) NOT NULL,
  `last_name` varchar(75) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(60) NOT NULL,
  `language` varchar(254) NOT NULL DEFAULT 'english',
  `type` int(2) NOT NULL DEFAULT 1,
  `sales` int(2) NOT NULL DEFAULT 1,
  `system` int(2) NOT NULL DEFAULT 0,
  `admin` int(2) NOT NULL DEFAULT 0,
  `reports` int(2) NOT NULL DEFAULT 0,
  `stock` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `password`, `language`, `type`, `sales`, `system`, `admin`, `reports`, `stock`) VALUES
(1, 'admin', 'istrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'english', 4, 4, 4, 4, 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users_type`
--

DROP TABLE IF EXISTS `users_type`;
CREATE TABLE `users_type` (
  `uid` int(11) NOT NULL,
  `name` varchar(254) NOT NULL COMMENT 'Name of user type',
  `sales` int(2) NOT NULL DEFAULT 0 COMMENT 'Level of privilege within Sales screen',
  `system` int(2) NOT NULL DEFAULT 0 COMMENT 'Level of privilege in system setup screens',
  `admin` int(2) NOT NULL DEFAULT 0 COMMENT 'Level of privilege as an administrator',
  `reports` int(2) NOT NULL DEFAULT 0 COMMENT 'Level of privilege to access reports',
  `stock` int(2) NOT NULL DEFAULT 0 COMMENT 'Level of privilege to adjust stock'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `voids`
--

DROP TABLE IF EXISTS `voids`;
CREATE TABLE `voids` (
  `id` int(10) NOT NULL,
  `value` float NOT NULL,
  `user_id` int(10) NOT NULL,
  `till` int(3) NOT NULL,
  `time` datetime NOT NULL,
  `date` date NOT NULL,
  `salesdetails` text NOT NULL,
  `itemsdetails` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

DROP TABLE IF EXISTS `vouchers`;
CREATE TABLE `vouchers` (
  `id` int(5) NOT NULL,
  `sale_id` int(10) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `customer_id` int(5) DEFAULT NULL,
  `recipient_name` varchar(255) DEFAULT NULL,
  `purchase_value` decimal(10,2) NOT NULL,
  `current_value` decimal(10,2) NOT NULL,
  `expiry` date NOT NULL,
  `customer_contact` text DEFAULT NULL,
  `recipient_contact` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barcodes`
--
ALTER TABLE `barcodes`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hkmatrix`
--
ALTER TABLE `hkmatrix`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `item_number` (`item_number`);

--
-- Indexes for table `items_aging`
--
ALTER TABLE `items_aging`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laybuys`
--
ALTER TABLE `laybuys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`);

--
-- Indexes for table `laybys`
--
ALTER TABLE `laybys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricelevel`
--
ALTER TABLE `pricelevel`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `rewind`
--
ALTER TABLE `rewind`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_items`
--
ALTER TABLE `sales_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_totals`
--
ALTER TABLE `sales_totals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `system`
--
ALTER TABLE `system`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_type`
--
ALTER TABLE `users_type`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `voids`
--
ALTER TABLE `voids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `code` (`code`),
  ADD KEY `sale_id` (`sale_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barcodes`
--
ALTER TABLE `barcodes`
  MODIFY `uid` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hkmatrix`
--
ALTER TABLE `hkmatrix`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items_aging`
--
ALTER TABLE `items_aging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laybuys`
--
ALTER TABLE `laybuys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laybys`
--
ALTER TABLE `laybys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rewind`
--
ALTER TABLE `rewind`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_items`
--
ALTER TABLE `sales_items`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_totals`
--
ALTER TABLE `sales_totals`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system`
--
ALTER TABLE `system`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `users_type`
--
ALTER TABLE `users_type`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `voids`
--
ALTER TABLE `voids`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
COMMIT;

