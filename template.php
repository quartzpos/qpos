    <?php
    // this file is the insertion of the header for the POS page, bringing in predefined CSS+ JS
    $path='';
    if(isset($pathex))$path=$pathex;
    if(!isset($db)){
		require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");}
    if(is_object($db)) {
        $res = $db->QPResults("SELECT value FROM system WHERE name='theme'");
        } else {
            $res=Array();$res['value']="flat";
        }
        $themepath = $path.'qpthemes/'.$res['value'].'/';
    ?>
<!DOCTYPE html>
<html><head>

<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php if (isset($title)) echo $title; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<link href="<?php echo $themepath; ?>pos.css" rel="stylesheet"  type="text/css"><link href="<?php echo $themepath; ?>menu.css" rel="stylesheet"  type="text/css">
	<link rel="icon" href="<?php echo $path; ?>favicon.ico" type="image/x-icon">
<?php if (isset($extracss)) echo $extracss; ?>
	<script type="text/javascript" src="<?php echo $path; ?>js/jquery.min.js"></script>

	<script type="text/javascript" src="<?php echo $path; ?>js/jquery-ui.min.js"></script>

	<link href="<?php echo $themepath; ?>jqui/jquery-ui.min.css" rel="stylesheet" type="text/css">
         <script type="text/javascript" src="<?php echo $path; ?>js/ajax.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $path; ?>js/general.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo $path; ?>js/waitforexists.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo $path; ?>js/jsmodal-1.0d.min.js"></script>
	<?php if (isset($headscripts)) echo $headscripts; ?>
	<script type="text/javascript" language="javascript">jQuery(document).ready(function() {
	  <?php
      if (isset($startscripts)) echo $startscripts;
      if (isset($hkscripts)) echo $hkscripts;
      ?>

	});
      <?php if (isset($extrascripts)) echo $extrascripts; ?>


</script>
 <script type="text/javascript" language="javascript" src="<?php echo $path; ?>js/getVal.js"></script>

</head>
