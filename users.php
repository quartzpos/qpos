<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com

    

  Released under the GNU General Public License
*/

if(!isset($_SESSION))session_start();
if(!isset($_SESSION['admin'])){
header("Location:admin.php");
}
require_once("languages/languages.php");
//Add a new user
if(isset($_POST['submituser'])){
$sql="SELECT * FROM users WHERE username = '".$db->clean($_POST['username']) ."'";

$result = $db->query($sql);
$row = $db->fetchAssoc($result);
if (!isset($row['username'])){
$sql = "insert into users(first_name, last_name, username, password, type,sales,system,admin,reports,stock, language) values ('" .$db->clean($_POST['firstname']) ."','" .$db->clean($_POST['lastname']) ."','" .$db->clean($_POST['username']) ."','" .md5(trim($_POST['password'])) ."','".$_POST['type']."','".$_POST['sales']."','".$_POST['system']."','".$_POST['admin']."','".$_POST['reports']."','".$_POST['stock']."','".$_POST['language']."')";
$db->query($sql);
$msg=USER_ADDED." ".$_POST['username']."<br/>";
} else {
$msg = USER_UNABLE.$_POST['username']."<br/>";
$_GET = NULL;
$_POST = NULL;

}
}

//Edit user data
if(isset($_POST['edituser'])){
$sql = "update users set type='".$_POST['type']."',  sales='".$_POST['sales']."', system='".$_POST['system']."', language='".$_POST['language']."', admin='".$_POST['admin']."', reports='".$_POST['reports']."',stock='".$_POST['reports']."',first_name='"  .$db->clean($_POST['firstname'])  ."', last_name='"  .$db->clean($_POST['lastname']) ."', username='" .$db->clean($_POST['username'])  ."' " .($_POST['password']=="" ? "" : ",password='" .md5(trim($_POST['password'])) ."', language='" .$db->clean($_POST['language'])  ."' ") ."where id=" .$_POST['user_id'];
$db->query($sql);
}

//Delete an user
if(isset($_GET['delete'])){
$sql = "delete from users where id=" .$_GET['delete'];
$db->query($sql);
}

?>

<div class="admin_content">

<script language="JavaScript">
  function validate_form(frm){
    if(frm.firstname.value=="" || frm.lastname.value=="" || frm.username.value=="" || frm.password.value=="" || frm.password.value != frm.confirm_password.value){
      alert(<?php echo VERIFY_FIELDS;?>);
      return false;
    }
    else return true;
  }
  function validate_edit_form(frm){
    if(frm.firstname.value=="" || frm.lastname.value=="" || frm.username.value=="" || frm.password.value != frm.confirm_password.value){
      alert(<?php echo VERIFY_FIELDS;?> );
      return false;
    }
    else return true;
  }
</script>

<?php


if(isset($_GET['edit'])){
$sql = "select * from users where id=" .$_GET['edit'];
$result = $db->query($sql);
$row = $db->fetchAssoc($result);
?>
<form action="admin.php?action=users" onsubmit="return validate_edit_form(this);" method="POST" autocomplete="off">
<input type="hidden" name="user_id" value="<?php echo $_GET['edit']; ?>">
<table>
<TR><TD><?php echo TXT_FIRSTNAME; ?></TD><TD><input type="text" name="firstname" autocomplete="off" size="40" value="<?php echo htmlspecialchars($row['first_name']); ?>"></TD></TR>
<TR><TD><?php echo TXT_LASTNAME; ?></TD><TD><input type="text" name="lastname" autocomplete="off" size="40" value="<?php echo htmlspecialchars($row['last_name']); ?>"></TD></TR>
<TR><TD><?php echo TXT_USERNAME; ?></TD><TD><input type="text" name="username" autocomplete="off" value="<?php echo htmlspecialchars($row['username']); ?>"></TD></TR>
<TR><TD><?php echo LOGIN_PASSWORD; ?></TD><TD><input type="password" autocomplete="off" name="password">(*)</TD></TR>
<TR><TD><?php echo LOGIN_CONFIRM_PASSWORD; ?></TD><TD><input type="password" autocomplete="off" name="confirm_password"></TD></TR>
<TR><TD colspan="2"><small>(*)<?php echo EDIT_PASSWORD_INFO; ?></small></TD></TR>
<TR><TD><?php echo TXT_LANGUAGE; ?></TD><TD><select name="language" id="language">
<?php 
foreach ($languages as $k=>$v){
    //$languages comes from the languages.php file
    echo '<option value="'.$v.'" ';
    if ($row['language']==$v){ echo 'selected';}
    echo '>'.ucfirst($v).'</option>';
}
?>
</select><td><tr>
</TD></TR>
<TR><TD>User type</td><td><select name="type" id="type">
  <option value="0" <?php echo ($row['type']=='0')?'selected':''; ?>>Suspended</option>
  <option value="1" <?php echo ($row['type']=='1')?'selected':''; ?>>Staff</option>
  <option value="2"<?php echo ($row['type']=='2')?'selected':''; ?>>Supervisor</option>
  <option value="3"<?php echo ($row['type']=='3')?'selected':''; ?>>Manager</option>
  <option value="4"<?php echo ($row['type']=='4')?'selected':''; ?>>Administrator</option>
</select><td><tr>
<TR><TD>Sales</td><td><select name="sales" id="sales">
  <option value="0" <?php echo ($row['sales']=='0')?'selected':''; ?>>Suspended</option>
  <option value="1" <?php echo ($row['sales']=='1')?'selected':''; ?>>Staff</option>
  <option value="2"<?php echo ($row['sales']=='2')?'selected':''; ?>>Supervisor</option>
  <option value="3"<?php echo ($row['sales']=='3')?'selected':''; ?>>Manager</option>
  <option value="4"<?php echo ($row['sales']=='4')?'selected':''; ?>>Administrator</option>
</select><td><tr>
<TR><TD>System</td><td><select name="system" id="system">
  <option value="0" <?php echo ($row['system']=='0')?'selected':''; ?>>Suspended</option>
  <option value="1" <?php echo ($row['system']=='1')?'selected':''; ?>>Staff</option>
  <option value="2"<?php echo ($row['system']=='2')?'selected':''; ?>>Supervisor</option>
  <option value="3"<?php echo ($row['system']=='3')?'selected':''; ?>>Manager</option>
  <option value="4"<?php echo ($row['system']=='4')?'selected':''; ?>>Administrator</option>
</select><td><tr>
<TR><TD>Admin</td><td><select name="admin" id="admin">
  <option value="0" <?php echo ($row['admin']=='0')?'selected':''; ?>>Suspended</option>
  <option value="1" <?php echo ($row['admin']=='1')?'selected':''; ?>>Staff</option>
  <option value="2"<?php echo ($row['admin']=='2')?'selected':''; ?>>Supervisor</option>
  <option value="3"<?php echo ($row['admin']=='3')?'selected':''; ?>>Manager</option>
  <option value="4"<?php echo ($row['admin']=='4')?'selected':''; ?>>Administrator</option>
</select><td><tr>
<TR><TD>Reports</td><td><select name="reports" id="reports">
  <option value="0" <?php echo ($row['reports']=='0')?'selected':''; ?>>Suspended</option>
  <option value="1" <?php echo ($row['reports']=='1')?'selected':''; ?>>Staff</option>
  <option value="2"<?php echo ($row['reports']=='2')?'selected':''; ?>>Supervisor</option>
  <option value="3"<?php echo ($row['reports']=='3')?'selected':''; ?>>Manager</option>
  <option value="4"<?php echo ($row['reports']=='4')?'selected':''; ?>>Administrator</option>
</select><td><tr>
<TR><TD>Stock</td><td><select name="stock" id="stock">
  <option value="0" <?php echo ($row['stock']=='0')?'selected':''; ?>>Suspended</option>
  <option value="1" <?php echo ($row['stock']=='1')?'selected':''; ?>>Staff</option>
  <option value="2"<?php echo ($row['stock']=='2')?'selected':''; ?>>Supervisor</option>
  <option value="3"<?php echo ($row['stock']=='3')?'selected':''; ?>>Manager</option>
  <option value="4"<?php echo ($row['stock']=='4')?'selected':''; ?>>Administrator</option>
</select><td><tr>

<TR><TD colspan="2"><input type="submit" name="edituser" value="<?php echo TXT_SAVE ?>"></TD></TR>
</table>
</form>
<?php
}
else{
?>
<form action="admin.php?action=users" onsubmit="return validate_form(this);" method="POST" autocomplete="off">
<?php
if (isset($msg)) echo $msg.'<br>';
echo "<strong><u>".ADD_NEW_USER."</u></strong>"; ?>:<br>
<table>
<TR><TD><?php echo TXT_FIRSTNAME; ?></TD><TD><input type="text" name="firstname" autocomplete="off" size="40"></TD></TR>
<TR><TD><?php echo TXT_LASTNAME; ?></TD><TD><input type="text" name="lastname" autocomplete="off" size="40"></TD></TR>
<TR><TD><?php echo TXT_USERNAME; ?></TD><TD><input type="text" name="username" autocomplete="off" ></TD></TR>
<TR><TD><?php echo LOGIN_PASSWORD; ?></TD><TD><input type="password" name="password" autocomplete="off"></TD></TR>
<TR><TD><?php echo LOGIN_CONFIRM_PASSWORD; ?></TD><TD><input type="password" name="confirm_password" autocomplete="off"></TD></TR>
<TR><TD><?php echo TXT_LANGUAGE; ?></TD><TD><select name="language" id="language">
<?php 
foreach ($languages as $k=>$v){
    //$languages comes from the languages.php file
    echo '<option value="'.$v.'" ';
    //if ($row['language']==$v){ echo 'selected';}
    echo '>'.ucfirst($v).'</option>';
}
?>
</select><td><tr>
<TR><TD>User type</td><td><select name="type" id="type">
<option value="0" >Suspended</option>
  <option value="1" default selected>Staff</option>
  <option value="2">Supervisor</option>
  <option value="3">Manager</option>
  <option value="4">Administrator</option>
</select><td><tr>

<TR><TD>Sales</td><td><select name="sales" id="sales">
  <option value="0" >Suspended</option>
  <option value="1" default selected>Staff</option>
  <option value="2">Supervisor</option>
  <option value="3">Manager</option>
  <option value="4">Administrator</option>
</select><td><tr>
<TR><TD>System</td><td><select name="system" id="system">
  <option value="0" >Suspended</option>
  <option value="1">Staff</option>
  <option value="2">Supervisor</option>
  <option value="3">Manager</option>
  <option value="4">Administrator</option>
</select><td><tr>
<TR><TD>Admin</td><td><select name="admin" id="admin">
  <option value="0" >Suspended</option>
  <option value="1">Staff</option>
  <option value="2">Supervisor</option>
  <option value="3">Manager</option>
  <option value="4">Administrator</option>
</select><td><tr>
<TR><TD>Reports</td><td><select name="reports" id="reports">
  <option value="0" >Suspended</option>
  <option value="1">Staff</option>
  <option value="2">Supervisor</option>
  <option value="3">Manager</option>
  <option value="4">Administrator</option>
</select><td><tr>
<TR><TD>Stock</td><td><select name="stock" id="stock">
  <option value="0" >Suspended</option>
  <option value="1">Staff</option>
  <option value="2">Supervisor</option>
  <option value="3">Manager</option>
  <option value="4">Administrator</option>
</select><td><tr>
<TR><TD colspan="2"><input type="submit" name="submituser" value="<?php echo USER_SUBMIT ?>"></TD></TR>
</table>
</form>
<br>
<script language="JavaScript">
  function delete_user(user){
    op = confirm("<?php echo CONFIRM_DELETE_USER; ?>");
    if(op)document.location.href="admin.php?action=users&delete="+user;
  }
</script>
<table cellspacing="0">
<TR><TH width="150"><?php echo TXT_USERNAME; ?></TH><TH width="400"><?php echo TXT_NAME; ?></TH><th><?php echo TXT_LANGUAGE; ?></th><th><?php echo TXT_TYPE; ?></th><th><?php echo TXT_SALES; ?></th><th><?php echo TXT_SYSTEM; ?></th><th><?php echo TXT_ADMIN; ?></th><th><?php echo TXT_REPORTS; ?></th><th><?php echo TXT_STOCK; ?></th><TH><?php echo TXT_EDIT; ?></TH><TH><?php echo TXT_DELETE; ?></TH></TR>
<?php
$sql = "select * from users";
$result = $db->query($sql);
while($row = $db->fetchAssoc($result)){
?><TR><TD class="tvalue" align="center"><?php echo htmlspecialchars($row['username']); ?></TD>
<TD class="tvalue" align="center"><?php echo htmlspecialchars($row['first_name'] ." " .$row['last_name']); ?></TD>
<TD class="tvalue" align="center"><?php echo ucfirst($row['language']); ?></TD>
<td class="tvalue" align="center"><?php echo $row['type']; ?></td>
<td class="tvalue" align="center"><?php echo $row['sales']; ?></td>
<td class="tvalue" align="center"><?php echo $row['system']; ?></td>
<td class="tvalue" align="center"><?php echo $row['admin']; ?></td>
<td class="tvalue" align="center"><?php echo $row['reports']; ?></td>
<td class="tvalue" align="center"><?php echo $row['stock']; ?></td>
<TD class="tvalue" align="center"><a href="admin.php?action=users&edit=<?php echo $row['id']; ?>"><img src="<?php echo $themepath; ?>images/edit.png" title="Edit <?php echo htmlspecialchars($row['first_name'] ." " .$row['last_name']); ?>" /></a></TD><TD class="tvalue" align="center"><a href="javascript:delete_user(<?php echo $row['id']; ?>)"><img src="<?php echo $themepath; ?>images/delete.png" title="Delete <?php echo htmlspecialchars($row['first_name'] ." " .$row['last_name']); ?>" /></a></TD>
</TR><?php
}
?>
<TR><TD></TD></TR>
</table><br>
<u><strong><?php echo TXT_HELPKEY; ?>:</strong></u>
<table><tr><th><?php echo TXT_INDEX; ?></th><th><?php echo TXT_PERMISSION_LEVEL; ?></th></tr>
  <tr><td class="tvalue" align="center">0</td><td class="tvalue" align="center">Suspended</td></tr>
<tr><td class="tvalue" align="center">1</td><td class="tvalue" align="center">Staff</td></tr>
<tr><td class="tvalue" align="center">2</td><td class="tvalue" align="center">Supervisor</td></tr>
<tr><td class="tvalue" align="center">3</td><td class="tvalue" align="center">Manager</td></tr>
<tr><td class="tvalue" align="center">4</td><td class="tvalue" align="center">Administrator</td></tr>
</table>
<?php
}
?>
</div>
