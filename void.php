<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */


session_start();


require_once("config.php");
require_once("languages/languages.php");

require_once("database.php");
require_once 'functions.php';
require 'consoleLogging.php';
$timezone = $db->QPResults("SELECT value FROM system WHERE name='timezone'")['value'];
require_once 'login.php';
if ($_SESSION['sales'] < 3) {
    header('Location: pos.php');
}
$title = VOID_TITLE;
$extrascripts = "function getParentUrl() {
    var isInIframe = (parent !== window),
        parentUrl = null;

    if (isInIframe) {
        parentUrl = document.referrer;
    }

    return parentUrl;
}";
require_once 'template.php';

$ref = debug_backtrace();
if (isset($ref[0]['file']) && strpos($ref[0]['file'], 'admin')) {

}
if (isset($_GET['iframe'])) {
    $iframe = TRUE;
    $url = "pmtout.php";
} else {
    $iframe = FALSE;
    $url = "pos.php?parent=1";
}
if (!isset($_SESSION['user'])) {

    loginnow();
}
if (isset($_POST['voidsale']) && isset($_POST['saleid'])) {
    //void form submitted
    if(isset($_POST['ifchk'])){ // the submitted form now carries the data to determine if we are in an iframe from pmtout, so handle the close button accordingly
        $ck=  strpos($_POST['ifchk'], 'out');

        if($ck>0){
            $url="pmtout.php";
        }
    }
    $itemsdetails = "";
    $id = $_POST['saleid'];
    $sql = "SELECT * FROM sales WHERE id='$id'";
    $result = $db->QPResults($sql);
    if (isset($result['sale_total_cost'])) {
        $value = $result['sale_total_cost'];
        $saledetails = implode("|", $result);
        $sql = "SELECT * FROM sales_items WHERE sale_id='$id'";
        $result = $db->QPComplete($sql);

        foreach ($result as $k => $v) {
            $qty = $v['quantity_purchased'];
            $item = $v['item_id'];
            $sql = "SELECT quantity FROM items WHERE id=$item";
            $qresult = $db->QPResults($sql);
            $qty = $qty + $qresult['quantity'];
            $sql = "UPDATE items SET quantity=$qty WHERE id=$item";
            $db->query($sql);
            $itemsdetails .= "##" . implode("|", $v);
        }
        //print $saledetails . "<br>" . $itemsdetails;
        $nz_time = new DateTime(null, new DateTimezone($timezone));

        $date = $nz_time->format('Y-m-d');
        $time = $date . " " . $nz_time->format('H:m:s');
        $till = $_SESSION['till'];
        $user = $_SESSION['user'];
        $sql = "INSERT INTO voids (user_id,value,till,time,date,salesdetails,itemsdetails) VALUES ('$user','$value','$till','$time','$date','$saledetails','$itemsdetails')";
        $db->query($sql);
        $sql = "DELETE FROM sales WHERE id='$id'";
        $db->query($sql);
        $sql = "DELETE FROM sales_items WHERE sale_id='$id'";
        $db->query($sql);
    }
    echo VOID_DEL."<a href='" . $url . "'><input type='button' value='".TXT_CLOSE."' /></a>";
}
if (isset($_GET['sale'])) {
    ?>

    <div id="void">
        <h3><?php echo VOID_HEAD;?></h3>

        <p><?php echo VOID_HELP1;?></p><br>
        <form name="voidform" action="void.php" method="post">
            <?php
            $id = $_GET['sale'];
            if (is_numeric($id)) {
                $mod = " WHERE sale_id =$id";
                $sql = "SELECT * FROM sales WHERE id=$id";
                $result = $db->query($sql);
                $check = $db->QPResults($sql);
                if (!isset($check['id'])) {
                    echo VOID_FAIL." <a href='" . $url . "'><input type='button' value='".TXT_CLOSE."' /></a>";
                }
                while ($row = $db->fetchAssoc($result)) {
                    if ($row['sold_by'] == 0) {
                        $user = "Admin";
                    } else {
                        $sqlu = "SELECT * FROM users WHERE id = " . $row['sold_by'];

                        $resultu = $db->query($sqlu);

                        if ($resultu) {
                            while ($rowu = $db->fetchAssoc($resultu)) {

                                $user = "<a href='#'  href='admin.php?action=users&edit=" . $rowu['id'] . "' title='". TXT_EDIT.' '. $rowu['username'] . "'>" . $rowu['first_name'] . " " . $rowu['last_name'] . "</a>"; // create link to edit the user
                            }
                        }
                    }

                    $transtitle = TXT_TRANSACT." ".$id. " ".TXT_FROM. " ". $row['date'] . " ".TXT_BY." " . $user;
                    $sql = "SELECT * FROM customers WHERE id='" . $row['customer_id'] . "'";
                    $resultc = $db->query($sql);

                    while ($rowc = $db->fetchAssoc($resultc)) {
                        $customer = "<a href='#'  href='admin.php?action=clientss&edit=" . $row['customer_id'] . "' title='". TXT_EDIT.' '. $rowc['first_name'] . $rowc['last_name'] . "'>" . $rowc['first_name'] . " " . $rowc['last_name'] . "</a>";
                    }
                }

                $sql = "SELECT * FROM sales_items" . $mod;

                $result = $db->query($sql);
                if ($result && isset($check['id'])) {
                    if (!empty($transtitle))
                        print "<h4>$transtitle ".TXT_FOR." $customer</h4>";
                    ?>
                    <table cellspacing="4"><?php
                        if (empty($mod)) {
                            ?>
                            <th><?php echo TXT_TRANSACT;?></th>
                            <th><?php echo TXT_DATE;?></th>
                            <th><?php echo TXT_TIME;?></th>
                            <th><?php echo TXT_PAYMENT;?></th>
                            <?php
                        }
                        ?>
                        <th><?php echo TXT_ITEM;?></th>
                        <th><?php echo TXT_QUANTITY;?></th>
                        <th><?php echo VOID_ITMTOT;?></th>
                        <th><?php echo TXT_TILL;?></th>
                        <?php
                        // `id`, `sale_id`, `item_id`, `quantity_purchased`, `item_unit_price`, `item_trade_price`, `item_tax_percent`, `item_total_tax`, `item_total_cost`

                        while ($row = $db->fetchAssoc($result)) {
                            $item = '';
                            $itemsql = "SELECT * FROM items WHERE id=" . $row['item_id'];
                            $iresult = $db->query($itemsql);
                            while ($irow = $db->fetchAssoc($iresult)) {
                                $item = "<a href='#'  href='admin.php?action=products&edit_item=" . $irow['id'] . "' title='" .TXT_EDIT.' '. $irow['item_name'] . "'>" . $irow['item_name'] . "</a>"; // create link to edit the item
                                ?><TR>
                                                    <?php
                                                    $id = $row['sale_id'];
                                                    $sql = "SELECT * FROM sales WHERE id=" . $id;

                                                    $cresult = $db->query($sql);
                                                    if ($cresult) {
                                                        while ($crow = $db->fetchAssoc($cresult)) {
                                                            $till = $crow['till'];
                                                            $comments = $crow['comment'];
                                                            $custcomment = $crow['customer_comment'];
                                                        }
                                                    }

                                                    if (empty($mod)) {
                                                        $ssql = "SELECT date FROM sales WHERE id=" . $row['sale_id'];
                                                        $sdate = '';
                                                        $trans = '';
                                                        $stime = '';
                                                        $sresult = $db->query($ssql);
                                                        if ($sresult) {
                                                            while ($srow = $db->fetchAssoc($sresult)) {
                                                                $sdate = $srow['date'];
                                                                $stime = date("H:i:s", $srow['time']);
                                                            }
                                                        }
                                                        $trans = '<a href="admin.php?action=reports' . $header . '&subreport=trans&id=' . $row['sale_id'] . '" title="' .VOID_VWTRANTITLE.' '. $row['sale_id'] . '">' . $row['sale_id'] . '</a>';
                                                        $payment = '<script type="text/javascript">window.open("payments.php?id=<?php echo $sale_id; ?>","","width=600,height=450,toolbars=0,location=0,titlebar=0,left=450");</script>';
                                                        ?>
                                        <td align="center"><?php echo $trans; ?></td>
                                        <td align="center"><?php echo $sdate; ?></td>
                                        <td align="center"><?php echo $stime; ?></td>

                                        <td align="center"><?php echo $payment; ?></td>
                                        <?php
                                    }
                                    ?>
                                    <TD align="center"><?php echo $item; // item                           ?></TD><TD align="center"><?php echo $row['quantity_purchased']; // qty                           ?></TD><TD align="center"><?php echo $row['item_total_cost']; //total                            ?></TD>
                                    <?php
                                    if (isset($till))
                                        echo '<td>' . $till . '</td>';
                                    ?>
                                </TR>
                                <?php
                            }
                        }
                        ?></table>
                    <?php
                    if (isset($comments))
                        echo '<span class="comments"><h4>'.TXT_COMMENTS.':</h4> ' . $comments . '</span>';
                    if (isset($custcomment))
                        echo '<span class="comments"><h4>'.CUSTOMER_COMMENTS.':</h4> ' . $custcomment . '</span>';
                    ?>
                    <?php
                }
                if (isset($check['id'])) {
                    echo '<input type="hidden" value="'.$url.'" name="ifchk" />';
                    echo '<input type="hidden" value="' . $id . '" name="saleid" />';
                    echo '<br><input type="submit" value="Void Sale" name="voidsale" />';
                    echo "<a href='" . $url . "&editsale=".$id."'><input type='button' value='Cancel' /></a>";
                }
            } else {
                print VOID_HELP2."<a href='" . $url . "'><input type='button' value='".TXT_CLOSE."' /></a>";
            }
            ?>

        </form>

    </div>

    <?php
} elseif (!isset($_POST['voidsale'])) {
    print VOID_HELP3."<a href='" . $url . "'><input type='button' value='".TXT_CLOSE."' /></a>";
}
?>
