<?php
if (!isset($_SESSION))
    session_start();


include("config.php");
include("consoleLogging.php");
require_once("languages/languages.php");
require_once("database.php");
include'rounding.php';
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_REQUEST, "sale.php request=");
if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($_SESSION, "sale.php session=");
$admin=NULL;
   $invoicedb = "SELECT * FROM system WHERE name LIKE 'invoice_%'";
    $invarr = $db->QPComplete($invoicedb); //$invar contains the invoice associated fields to build upon.
    $form = array();
   //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($invarr, "sale.php Invarr=");
    //in the database 'system table, there is an invoice field value and it correponds to another system table row
    //eg. invoice_field10 = {company_tax_no}
    //so the code gets the company_tax_no row in system table, and loads it's value
    foreach ($invarr as $key => $val) {
        if (!empty($val['value'])) {
            //there is a value in the field, parse it to see if it is another system field reference
            $field = sscanf($val['value'], "{%s", $fieldname); //retrieves the value between curly braces
           //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($fieldname, "fieldname=");
            if (isset($fieldname) && !empty($fieldname)) { //we have an actual field name
                $fieldtitle = str_replace('}', '', $fieldname);
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($fieldtitle, "Found field");
                $newval = $db->QPResults("SELECT value FROM system WHERE name='$fieldtitle'")['value'];
                //if(defined('DEBUG_CONSOLE') && DEBUG_CONSOLE)ChromePhp::log($newval, "newvalue=");
                $form[$fieldtitle] = $newval;
            }
            $form[$val['name']] = $val['value'];
        }
    }
    $sql="SELECT value FROM system WHERE name='sales_header1'";
    $sales_header1=$db->QPResults($sql)['value'];
    $sql="SELECT value FROM system WHERE name='sales_header2'";
    $sales_header2=$db->QPResults($sql)['value'];
      $sql="SELECT value FROM system WHERE name='sales_footer1'";
    $sales_footer1=$db->QPResults($sql)['value'];
    $sql="SELECT value FROM system WHERE name='sales_footer2'";
    $sales_footer2=$db->QPResults($sql)['value'];
    $sql="SELECT value FROM system WHERE name='quote_footer'";
    $quote_footer=$db->QPResults($sql)['value'];

?><html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8">
        <title><?php echo VP_QPV;?></title>
        <style>

            *{font-family:arial, helvetica, sans-serif;font-size:12px;}
            
            @media print{
                .noprint, .noprint *,#top{display:none !important;}

            }
            h1{font-size:36pt;}
            h2{font-size:24pt;}
            small{font-size:10px;color:#788;}
            body{margin:0px;background:#fff;padding:2em;}
            .saletable { color:#333; font-weight:bold; }
            #top {background:#9cf;border-bottom:solid 1px #248;}
            #menu td{border-left:solid 1px #788;width:60px;cursor:hand;
                     text-align:center;color:#248;font-weight:bold;}
            #sale_info{margin-top:10px;min-height: 220px;}
            #sale_info td{}
            #total b{color:#922;font-size:14px;}
            #logo img{max-width:300px; }
            #logo {padding:1.5em;display:inline-block; vertical-align:top;}
            .right{text-align:right;float:right;}
            #details{ display: inline-block;}
            #sale_header{width:100%; min-height:200px; border-bottom: 1px solid #333;}
            .light{background: #ccc; color:#000; }
            #client{border: 1px solid #666; padding:10px;max-width: 300px;margin-top: 10px;}
        </style>
        <script language="javascript">
            var closewin =<?php if (isset($_GET['closewin'])) {
             echo $_GET['closewin'].';';
                } else {
                 echo '0;';
                 } 
                 ?>
              var admin =<?php if (isset($_GET['admin'])) {
             echo $_GET['admin'].';';
             $admin=$_GET['admin'];
                } else {
                 echo '0;';
                 } 
?>
            function closed() {
               console.log("close function");
               if(closewin ===1 && admin ===1){
                   this.window.close();
               }
                if (closewin === 0) {
                    window.close();
                } else {
                    parent.window.location = "pos.php?action=logout";
                }
            }
             function focus(){
                 document.getElementById("print").focus();
             }    
             document.addEventListener("keydown", function(event) {
  if (event.altKey && (String.fromCharCode(event.keyCode) === "P"||String.fromCharCode(event.keyCode) === "p")) {
   console.log("you pressed alt-p");
    event.preventDefault();
    event.stopPropagation();
     
  console.log("alt+ p clicked");
   window.print();
  }
}, true);
document.addEventListener("keydown", function(event) {
  if (event.altKey && (String.fromCharCode(event.keyCode) === "c"||String.fromCharCode(event.keyCode) === "C")) {
   console.log("you pressed alt-c");
    event.preventDefault();
    event.stopPropagation();
     
  console.log("alt+ c clicked");
   closed();
  }
}, true);
        </script>
    </head>
    <body onload="focus()">
        <?php
        if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
            die(VP_DIE);
        } else {


            $sql = "select * from vouchers where id=" . $_GET['id'];
           
            $result = $db->query($sql);
            if($result){
            $voucher_info = $db->fetchAssoc($result);
            } else {
                die("voucherprint.php died with sql= $sql");
            }
           
            $customer = $db->QPResults("SELECT * FROM customers WHERE id='" . $voucher_info['customer_id'] . "'");
            $classzebra = "light";
            ?>
            <table id="top" width="100%" cellpadding="0">
                <tr>
                    <td>&nbsp;</td>
                    <td align="right">
                        <input type="button" onClick="window.print()" id="print" value="Print"  title="<?php echo VP_PRINTTITLE;?>"/> 
                        
                        <input type="button" id="close" onClick="closed()" value="Close" title="<?php echo VP_CLOSETITLE;?>"/>
                        
                    </td>
                </tr>
            </table>
            <div id="sale_header"><?php
                if (isset($form['receipt_logo'])){
                echo "<div id='logo'><img src='" . $form['receipt_logo'] . "' /></div>";}
                ?><div id="details" class="right">
                <?php
                
               
                echo(isset($form['company_details_name'])) ? $form['company_details_name'] : '';
                echo "<br>";
                echo(isset($form['company_details_address1'])) ? $form['company_details_address1'] : '';
                echo "<br>";
                echo(isset($form['company_details_address2'])) ? $form['company_details_address2'] : '';
                echo "<br>";
                echo(isset($form['company_details_address3'])) ? $form['company_details_address3'] : '';
                echo "<br>";
                echo(isset($form['company_details_phone1'])) ? TXT_PHONE.':'.$form['company_details_phone1'] : '';
                echo "<br>";
                echo(isset($form['company_details_phone2'])) ? TXT_FREEPHONE.':'.$form['company_details_phone2'] : '';
                echo "<br>";
                echo(isset($form['company_details_fax'])) ? TXT_FAX.':'.$form['company_details_fax'] : '';
                echo "<br>";
                echo(isset($form['company_details_email'])) ? TXT_EMAIL.':'.$form['company_details_email'] : '';
                echo "<br>";
                echo(isset($form['company_details_website'])) ? TXT_WEBSITE.':'.$form['company_details_website'] : '';
                ?></div>
                
                
            </div>
        <div id="header1" style="margin-top:10px;"><?php if (isset($sales_header1)){ echo $sales_header1; }?></div>
                <div id="header2"><?php if (isset($sales_header2)){ echo $sales_header2; }?></div>
            <div id="sale_info" style="text-align:center;">
                <h1><?php echo VP_GV;?></h1>
                <div id="from"><?php
                                    echo VP_FROM.": ". $customer['first_name'] . ' ' . $customer['last_name'];
                                    if (isset($voucher_info['recipient_name']) && !empty($voucher_info['recipient_name'])){
                                        echo "<br>".VP_TO.": ".$voucher_info['recipient_name'];
                                    } else {
                                        echo "<br>";
                                    }
                                    ?></div>
                
                
           
                <div id="voucherdetails">
                    
                    <h2><?php echo VP_VTO;?>: $<?php print $voucher_info['current_value']; ?></h2><br>
                    <p><?php echo VP_VALID;?>: <?php print $voucher_info['expiry']; ?></p>
                    <small><?php echo VP_DATEISS;?>: <?php echo $voucher_info['date']; ?>&nbsp;|&nbsp;<?php echo VP_INITVAL;?>: $<?php echo $voucher_info['purchase_value']; ?>&nbsp;|&nbsp;<?php echo VP_VCHID;?># <?php echo $voucher_info['id']; ?></small>
                </div> </div>    
            <hr>
          
            <div id="comment"><?php if (!empty($voucher_info['customer_comment'])) echo $voucher_info['customer_comment']; ?></div>
            <div id="sale_footer">
                <div id="sale_footer1"><?php if (isset($sales_footer1)) echo $sales_footer1; ?></div>
                <div id="sale_footer2"><?php if (isset($sales_footer2)) echo $sales_footer2; ?></div>
                
            </div>
<?php } ?>
    </body>
</html>
