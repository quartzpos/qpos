<?php
/*
  Quartzpos, Open Source Point-Of-Sale System
  http://Quartzpos.com



  Released under the GNU General Public License
 */

if (!isset($_SESSION)) {
    session_start();
}

require_once("config.php");
require_once("languages/languages.php");
require_once 'database.php';
require_once 'consoleLogging.php';
$ref = debug_backtrace();
if(isset($ref[0]['file']) && strpos($ref[0]['file'], 'admin')){
    $url="admin.php?action=vouchers";
    $edit=TRUE;
    $printurl="admin=1";
} else {
    $url="vouchers.php";
    $edit=FALSE;
    $printurl="admin=0";
}

if (isset($_POST['submit'])) {
    if (isset($_POST['deleted'])) {
        foreach ($_POST as $k => $v) {

            $pos = strpos($k, 'delete_');
            if ($pos !== FALSE) {
                $vidarr = explode('_', $k);

                $vid = $vidarr[1];
                $sql = "DELETE FROM vouchers WHERE id='$vid'";
                //$db->query($sql);
                unset($_POST['delete_' . $vid]); //unset so it doesn't get caught up in the next sequence below
            }
        }
        unset($_POST['deleted']);
    }
    unset($_POST['submit']);
}
if(isset($_POST['editvoucher'])){
    //th edit voucher form is submitted
    $error=0; $msg='';
    $id=$_POST['voucher_id'];
    $code=$db->clean($_POST['code']);
    $date=$db->clean($_POST['date']);
    $value=$db->clean($_POST['current_value']);
    $expiry=$db->clean($_POST['expiry']);
    $cc=$db->clean($_POST['customer_contact']);
    $rc=$db->clean($_POST['recipient_contact']);
    if($expiry<$date){$msg.= VCH_MSG1; $error=1;}
    if(empty($date)){$msg.= VCH_MSG2; $error=1;}
    if(empty($expiry)){$msg.= VCH_MSG3; $error=1;}
            if(empty($error)){
                $sql="UPDATE vouchers SET code='$code',date='$date',current_value='$value',expiry='$expiry',customer_contact='$cc',recipient_contact='$rc' WHERE id='$id'";
                $set=$db->query($sql);
                if($set){
                    $msg.=VCH_MSG4;
                }
            }
    
}
$extrascripts = ' jQuery(function() {console.log("datepicker read");
    jQuery( ".datepick" ).datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true });
        });
        function deletevoucher(val){
        document.getElementById("deleted").value = "";
            if (!window.confirm("'.VCH_MSG5.'")){
                 document.getElementById("delete_"+val).checked = false;
            } else{
             document.getElementById("deleted").value = "1";
            }
        }
';

require_once('functions.php');
require_once('template.php');
?><body>
    <div class="admin_content">
<?php
//echo $ref[0]['file'];
if (isset($msg)){
    print $msg."<br>";
}
if (isset($_GET['edit'])) {
    $sql = "select * from vouchers where id=" . intval($_GET['edit']);
    $v = $db->QPResults($sql);
    ?>
            <form action="<?php echo $url;?>" method="POST">
                <table>
                    <tr><td><?php echo VCH_ID;?></td><td style="  width: 50px;"><?php echo intval($_GET['edit']); ?></td></tr> 
                    <tr><td><?php echo VCH_SID;?></td><td ><?php echo $v['sale_id']; ?> </td></tr>
                    <tr><td><?php echo VCH_CODE;?></td><td><input type="text" name="code" value="<?php echo $v['code']; ?>" class="inp1"/></td></tr>
                    <tr><td><?php echo VCH_DATE;?></td>   <td ><input type="text" name="date" value="<?php echo $v['date']; ?>" class="datepick inp1" /></td></tr>
                    <tr><td><?php echo VCH_CID;?></td><td><?php echo $v['customer_id']; ?></td></tr>
                    <tr><td><?php echo VCH_RCPT;?></td><td><input type="text" name="recipient" value="<?php echo $v['recipient_name']; ?>" class="inp1"</td></tr>
                    <tr><td><?php echo VCH_COST;?></td>   <td ><?php echo $v['purchase_value']; ?></td></tr>
                    <tr><td><?php echo VCH_VALUE;?></td><td ><input type="text" name="current_value" value="<?php echo $v['current_value']; ?>" class="inp1"</td></tr>
                    <tr><td><?php echo VCH_EXPIRY;?></td>   <td ><input type="text" name="expiry" value="<?php echo $v['expiry']; ?>" class="inp1 datepick"</td></tr>
                    <tr><td><?php echo VCH_CUSTCON;?></td><td><input type="text" name="customer_contact" value="<?php echo $v['customer_contact']; ?>" class="inp1"/></td></tr>
                    <tr><td><?php echo VCH_RCPTCON;?></td>   <td><input name="recipient_contact" type="text" value="<?php echo $v['recipient_contact']; ?>" class="inp1"></td></tr>
                </table>
                <input type="hidden" name="voucher_id" value="<?php echo intval($_GET['edit']); ?>">

                <input type="submit" name="editvoucher" value="<?php echo TXT_SAVE; ?>">
                <a href="voucherprint.php?<?php echo $printurl; ?>&voucher=<?php echo intval($_GET['edit']); ?>"><input type="button" name="printvoucher" value="<?php echo TXT_PRINT; ?>"></a>
            </form>
        <br><a href="<?php echo $url;?>">&Lt; -- <?php echo VCH_VLIST;?></a>
    <?php
} else {
    $sql = "select * from vouchers ";
    $data = $db->QPComplete($sql);
    ?><script type="text/javascript">function printvoucher(id) {
                        window.open("voucherprint.php?<?php echo $printurl; ?>&id="+id, "", "width=680,height=760,toolbars=0,location=0,titlebar=0,left=20");
                    }
            </script>
            <form name="voucherform" method="post" action="<?php echo $url;?>" id="voucherform">
                <table id="vouchertable">
                    <tr>
                        <th><?php echo VCH_ID;?></th>
                        <th><?php echo VCH_SID;?></th>
                        <th><?php echo VCH_CODE;?></th>
                        <th><?php echo VCH_DATE;?></th>
                        <th><?php echo VCH_CID;?></th>
                        <th><?php echo VCH_RCPT;?></th>
                        <th><?php echo VCH_COST;?></th>
                        <th><?php echo VCH_VALUE;?></th>
                        <th><?php echo VCH_EXPIRY;?></th>
                        <th><?php echo VCH_CUSTCON;?></th>
                        <th><?php echo VCH_RCPT;?></th>
                        <th><?php echo VCH_PRINT;?></th>
                        <?php if($edit){?><th><?php echo TXT_EDIT;?></th><th><?php echo TXT_DELETE;?></th><?php }?></tr>
            <?php
            $classzebra = false;
            foreach ($data as $k => $v) {
                ?>
                        <tr class='<?php echo ($classzebra) ? 'even' : 'odd'; ?>'>
                            <td style="  width: 50px;"><?php echo $v['id']; ?></td>
                            <td style="  width: 50px;"><?php echo $v['sale_id']; ?> </td>
                            <td style="  width: 200px;"><?php echo $v['code']; ?></td>
                            <td style="  width: 120px;"><?php echo $v['date']; ?></td>
                            <td><?php echo $v['customer_id']; ?></td>
                            <td><?php echo $v['recipient_name']; ?></td>
                            <td style="  width: 60px;"><?php echo $v['purchase_value']; ?></td>
                            <td style="  width: 60px;"><?php echo $v['current_value']; ?></td>
                            <td style="  width: 120px;"><?php echo $v['expiry']; ?></td>
                            <td><?php echo $v['customer_contact']; ?></td>
                            <td><?php echo $v['recipient_contact']; ?></td>
                            <td class="center"><img src="<?php echo $themepath; ?>images/print.png" title="<?php echo VCH_PRNTVCH.' '.$v['id'] ?>" onclick="printvoucher(<?php echo $v['id'] ?>);"/>
                            </td>
                            <?php if($edit){?>
                            <td class="center"><a href="<?php echo $url;?>&edit=<?php echo $v['id']; ?>"><img src="<?php echo $themepath; ?>images/edit.png" title="<?php echo VCH_EDITVCH.' '.$v['id'] ?>" /></a></td><td class="center"><input type="checkbox" name="delete_<?php echo $v['id']; ?>" class="inp1" onclick="deletevoucher(<?php echo $v['id']; ?>)" value="<?php echo TXT_DELETE;?>" id="delete-<?php echo $v['id']; ?>"/>
                            </td>
                            <?php }?>
                        </tr>

        <?php
        $classzebra = !$classzebra;
    }
    ?>
                </table><?php if($edit){?>
                <input type="hidden" name="deleted" id="deleted"/>
                <input type="submit" name="submit" value="<?php echo TXT_SAVE;?>" class="inp2 metal" />
                <?php }?>
            </form>
                    <?php
                }
                ?>
    </div>
</body>
</html>
